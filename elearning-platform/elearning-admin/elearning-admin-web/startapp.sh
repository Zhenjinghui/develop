#!/bin/bash
cd /opt/elearning

if [ $1 = 'dev' ]
then
	echo $1
	java -Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=5005 -Xms512m -Xmx512m -jar jar/elearning-admin-web.jar --spring.profiles.active=$1
elif [ $1 = 'test' ]
then
	echo $1
	java -Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=5005 -Xms512m -Xmx512m -jar jar/elearning-admin-web.jar --spring.profiles.active=$1
elif [ $1 = 'prod' ]
then
	echo $1
	java -jar jar/elearning-admin-web.jar -Xms512m -Xmx512m --spring.profiles.active=$1
else
	echo $1	
fi