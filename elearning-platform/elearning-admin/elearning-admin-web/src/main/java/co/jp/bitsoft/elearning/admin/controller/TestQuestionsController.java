package co.jp.bitsoft.elearning.admin.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminTestQuestionsDto;
import co.jp.bitsoft.elearning.admin.service.AdminTestQuestionsService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * テスト問題
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/testquestions")
public class TestQuestionsController {
    @Autowired
    private AdminTestQuestionsService adminTestQuestionsService;


    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:testquestions:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminTestQuestionsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{testQuestionsId}")
    @RequiresPermissions("service:testquestions:info")
    public R info(@PathVariable("testQuestionsId") Long testQuestionsId){
    	AdminTestQuestionsDto testQuestions = adminTestQuestionsService.testQuestionsInfo(testQuestionsId);

        return R.ok().put("testQuestions", testQuestions);
    }

    /**
     * 保存
     * @throws Exception
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:testquestions:save")
    @Transactional
    public R save(@RequestBody AdminTestQuestionsDto adminTestQuestionsDto) throws Exception{

    	adminTestQuestionsService.testQuestionsSave(adminTestQuestionsDto);
        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:testquestions:update")
    @Transactional
    public R update(@RequestBody AdminTestQuestionsDto adminTestQuestionsDto){

    	adminTestQuestionsService.testQuestionsUpdate(adminTestQuestionsDto);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:testquestions:delete")
    @Transactional
    public R delete(@RequestBody Long[] testQuestionsIds){

    	adminTestQuestionsService.testQuestionsDelete(testQuestionsIds);

        return R.ok();

    }

}
