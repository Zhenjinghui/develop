package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import co.jp.bitsoft.elearning.admin.domain.model.inquiry.InquirySaveRequest;
import co.jp.bitsoft.elearning.admin.domain.model.inquiry.InquiryUpdateRequest;
import co.jp.bitsoft.elearning.admin.service.AdminInquiryMessageService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.InquiryEntity;
import co.jp.bitsoft.elearning.admin.service.AdminInquiryService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * 問い合わせ
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/inquiry")
public class InquiryController {
    @Autowired
    private AdminInquiryService adminInquiryService;

    @Autowired
    private AdminInquiryMessageService adminInquiryMessageService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:inquiry:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminInquiryService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{inquiryId}")
    @RequiresPermissions("service:inquiry:info")
    public R info(@PathVariable("inquiryId") Long inquiryId){
		InquiryEntity inquiry = adminInquiryService.getById(inquiryId);

        return R.ok().put("inquiry", inquiry);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:inquiry:save")
    public R save(@RequestBody InquirySaveRequest inquirySaveRequest){
        InquiryEntity inquiry = new InquiryEntity();
        BeanUtils.copyProperties(inquirySaveRequest, inquiry);
        inquiry.setCreateTime(new Date());
        inquiry.setCreateUserId(1l);
        inquiry.setDelFlag("0");
        inquiry.setVersionNo(0);
        adminInquiryService.save(inquiry);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:inquiry:update")
    public R update(@RequestBody InquiryUpdateRequest inquiryUpdateRequest){
        InquiryEntity inquiry = new InquiryEntity();
        BeanUtils.copyProperties(inquiryUpdateRequest, inquiry);
        adminInquiryService.updateById(inquiry);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:inquiry:delete")
    public R delete(@RequestBody Long[] inquiryIds){
        adminInquiryService.removeByIds(Arrays.asList(inquiryIds));
        adminInquiryMessageService.removeByInquiryIds(inquiryIds);
        return R.ok();
    }

}
