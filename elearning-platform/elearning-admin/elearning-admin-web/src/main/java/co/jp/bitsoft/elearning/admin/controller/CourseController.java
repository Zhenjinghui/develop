package co.jp.bitsoft.elearning.admin.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminCourseSelectDto;
import co.jp.bitsoft.elearning.admin.service.AdminCourseService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.modules.sys.controller.AbstractController;



/**
 * 講座
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/course")
public class CourseController extends AbstractController {

    @Autowired
    private AdminCourseService adminCourseService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:course:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminCourseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{courseId}")
    @RequiresPermissions("service:course:info")
    public R info(@PathVariable("courseId") Long courseId){
    	AdminCourseDto adminCourseDto = adminCourseService.courseInfo(courseId);

        return R.ok().put("course", adminCourseDto);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:course:save")
    public R save(@RequestBody AdminCourseDto adminCourseDto){

    	adminCourseService.courseSave(adminCourseDto);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:course:update")
    public R update(@RequestBody AdminCourseDto adminCourseDto){

    	adminCourseService.courseUpdate(adminCourseDto);
        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:course:delete")
    @Transactional
    public R delete(@RequestBody Long[] courseIds){
    	adminCourseService.courseDelete(courseIds);

        return R.ok();
    }

    /**
     * 講座下拉框
     */
    @RequestMapping("/courseSelect")
    public R courseSelect(@RequestParam Map<String, Object> params){
    	List<AdminCourseSelectDto> courseSelect= adminCourseService.courseSelect(params);

        return R.ok().put("courseSelect", courseSelect);
    }

}
