package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.service.QuestionNaireQuestionsChoicesService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * アンケート問題選択肢
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/questionnairequestionschoices")
public class QuestionNaireQuestionsChoicesController {
    @Autowired
    private QuestionNaireQuestionsChoicesService questionNaireQuestionsChoicesService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:questionnairequestionschoices:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = questionNaireQuestionsChoicesService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{questionNaireQuestionsChoicesId}")
    @RequiresPermissions("service:questionnairequestionschoices:info")
    public R info(@PathVariable("questionNaireQuestionsChoicesId") Long questionNaireQuestionsChoicesId){
		QuestionNaireQuestionsChoicesEntity questionNaireQuestionsChoices = questionNaireQuestionsChoicesService.getById(questionNaireQuestionsChoicesId);

        return R.ok().put("questionNaireQuestionsChoices", questionNaireQuestionsChoices);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:questionnairequestionschoices:save")
    public R save(@RequestBody QuestionNaireQuestionsChoicesEntity questionNaireQuestionsChoices){
		questionNaireQuestionsChoicesService.save(questionNaireQuestionsChoices);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:questionnairequestionschoices:update")
    public R update(@RequestBody QuestionNaireQuestionsChoicesEntity questionNaireQuestionsChoices){
		questionNaireQuestionsChoicesService.updateById(questionNaireQuestionsChoices);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:questionnairequestionschoices:delete")
    public R delete(@RequestBody Long[] questionNaireQuestionsChoicesIds){
		questionNaireQuestionsChoicesService.removeByIds(Arrays.asList(questionNaireQuestionsChoicesIds));

        return R.ok();
    }

}
