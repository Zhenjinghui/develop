package co.jp.bitsoft.elearning.common;

import lombok.Data;

@Data
public class UserInfo {
	private Long userId;
	
	private String userName;
}
