package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.service.TestQuestionsChoicesService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * テスト問題選択肢
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/testquestionschoices")
public class TestQuestionsChoicesController {
    @Autowired
    private TestQuestionsChoicesService testQuestionsChoicesService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:testquestionschoices:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = testQuestionsChoicesService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{testQuestionsChoicesId}")
    @RequiresPermissions("service:testquestionschoices:info")
    public R info(@PathVariable("testQuestionsChoicesId") Long testQuestionsChoicesId){
		TestQuestionsChoicesEntity testQuestionsChoices = testQuestionsChoicesService.getById(testQuestionsChoicesId);

        return R.ok().put("testQuestionsChoices", testQuestionsChoices);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:testquestionschoices:save")
    public R save(@RequestBody TestQuestionsChoicesEntity testQuestionsChoices){
		testQuestionsChoicesService.save(testQuestionsChoices);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:testquestionschoices:update")
    public R update(@RequestBody TestQuestionsChoicesEntity testQuestionsChoices){
		testQuestionsChoicesService.updateById(testQuestionsChoices);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:testquestionschoices:delete")
    public R delete(@RequestBody Long[] testQuestionsChoicesIds){
		testQuestionsChoicesService.removeByIds(Arrays.asList(testQuestionsChoicesIds));

        return R.ok();
    }

}
