package co.jp.bitsoft.elearning.admin.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminUnitTestDto;
import co.jp.bitsoft.elearning.admin.service.AdminUnitTestService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.service.UnitTestService;



/**
 * 単元テスト
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/unittest")
public class UnitTestController {
    @Autowired
    private UnitTestService unitTestService;
    @Autowired
    private AdminUnitTestService adminUnitTestService;
    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:unittest:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminUnitTestService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{unitTestId}")
    @RequiresPermissions("service:unittest:info")
    public R info(@PathVariable("unitTestId") Long unitTestId){
		UnitTestEntity unitTest = unitTestService.getById(unitTestId);

        return R.ok().put("unitTest", unitTest);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:unittest:save")
    public R save(@RequestBody AdminUnitTestDto adminUnitTestDto){

	    adminUnitTestService.unitTestSave(adminUnitTestDto);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:unittest:update")
    public R update(@RequestBody AdminUnitTestDto adminUnitTestDto){

    	adminUnitTestService.unitTestUpdate(adminUnitTestDto);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:unittest:delete")
    @Transactional
    public R delete(@RequestBody Long[] unitTestIds){

    	adminUnitTestService.unitTestDelete(unitTestIds);

        return R.ok();
    }

    @RequestMapping("/unittestSearchList")
    @RequiresPermissions("service:unittest:unittestSearchList")
    public R unitTestSearchlist(@RequestParam Map<String, Object> params){
        PageUtils page = adminUnitTestService.selectUnitTestPage(params);

        return R.ok().put("page", page);
    }


}
