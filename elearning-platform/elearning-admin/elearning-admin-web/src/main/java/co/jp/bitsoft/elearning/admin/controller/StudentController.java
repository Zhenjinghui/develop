package co.jp.bitsoft.elearning.admin.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentExportDto;
import co.jp.bitsoft.elearning.admin.service.AdminStudentService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.modules.sys.controller.AbstractController;



/**
 * 受講者
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/student")
public class StudentController extends AbstractController{
    @Autowired
    private AdminStudentService adminStudentService;


    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:student:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminStudentService.queryStudent(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{studentId}")
    @RequiresPermissions("service:student:info")
    public R info(@PathVariable("studentId") Long studentId){

    	AdminStudentDto student = adminStudentService.studentInfo(studentId);

        return R.ok().put("student", student);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:student:save")
    public R save(@RequestBody AdminStudentDto adminStudentDto){

    	adminStudentService.studentSave(adminStudentDto);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:student:update")
    public R update(@RequestBody AdminStudentDto adminStudentDto){

    	adminStudentService.studentUpdate(adminStudentDto);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:student:delete")
    @Transactional
    public R delete(@RequestBody Long[] studentIds){
    	adminStudentService.studentDelete(studentIds);
        return R.ok();
    }

	/**
	 * 用户エクスポート
	 */
	@GetMapping("/export")
	@RequiresPermissions("service:student:export")
	public R export(@RequestParam Map<String, Object> params){
        List<AdminStudentExportDto> studentList = adminStudentService.queryAllStudent(params);

        return R.ok().put("studentList", studentList);
	}

	/**
	 * 用户インポト
	 */
	@PostMapping("/import")
	@RequiresPermissions("service:student:import")
	@Transactional
	public R studentImport(@RequestBody JSONObject csvData){

		adminStudentService.studentImport(csvData);

		return R.ok();
	}

}
