package co.jp.bitsoft.elearning.admin.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.NoPassedAnswerDto;
import co.jp.bitsoft.elearning.admin.service.AdminStudentTestService;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.StudentTestEntity;
import co.jp.bitsoft.elearning.core.service.StudentTestService;



/**
 * 受講者テスト
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/studenttest")
public class StudentTestController {
    @Autowired
    private StudentTestService studentTestService;

    @Autowired
    private AdminStudentTestService adminStudentTestService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:studenttest:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = studentTestService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{studentTestId}")
    @RequiresPermissions("service:studenttest:info")
    public R info(@PathVariable("studentTestId") Long studentTestId){
		StudentTestEntity studentTest = studentTestService.getById(studentTestId);

        return R.ok().put("studentTest", studentTest);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:studenttest:save")
    public R save(@RequestBody StudentTestEntity studentTest){
		studentTestService.save(studentTest);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:studenttest:update")
    public R update(@RequestBody StudentTestEntity studentTest){
		studentTestService.updateById(studentTest);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:studenttest:delete")
    public R delete(@RequestBody Long[] studentTestIds){
		studentTestService.removeByIds(Arrays.asList(studentTestIds));

        return R.ok();
    }

    /**
    * リスト一覧
    */
   @RequestMapping("/studenttestHistoryList")
   public R studentTestHistorylist(@RequestParam Map<String, Object> params){
	   //分页参数
       long curPage = 1;
       long limit = 10;
       if(params.get(Constant.PAGE) != null){
           curPage = Long.parseLong((String)params.get(Constant.PAGE));
       }
       if(params.get(Constant.LIMIT) != null){
           limit = Long.parseLong((String)params.get(Constant.LIMIT));
       }

		QueryWrapper<StudentTestEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("student_id", Long.parseLong((String)params.get("studentId")));
		queryWrapper.eq("unit_test_id", Long.parseLong((String)params.get("unitTestId")));

		List<StudentTestEntity> studentTestHistorylist= studentTestService.list(queryWrapper);

		List<StudentTestEntity> resultRecords = new ArrayList<StudentTestEntity>();

		int resultListSize =  studentTestHistorylist.size();

		if(resultListSize - (int)(limit*(curPage-1)) < (int)limit) {
			resultRecords = studentTestHistorylist.subList((int)(limit*(curPage-1)), studentTestHistorylist.size());
		}else {
			resultRecords = studentTestHistorylist.subList((int)(limit*(curPage-1)), (int)(limit*curPage));
		}

		IPage<StudentTestEntity> resultPage = new Page<StudentTestEntity>(curPage, limit);
		resultPage.setRecords(resultRecords);
		resultPage.setCurrent(curPage);
		resultPage.setTotal(studentTestHistorylist.size());

       return R.ok().put("page", new PageUtils(resultPage));
   }

   /**
    * 不合格問題一覧
    */
   @RequestMapping("/studentTestUnqualifiedProblem")
   public R testSubmit(@RequestParam Map<String, Object> params) {

       List<NoPassedAnswerDto> result= adminStudentTestService.studentTestNoPassedAnswers(Long.parseLong((String)params.get("studentId")), Long.parseLong((String)params.get("studentTestId")));

       return R.ok().put("result", result);
   }

}
