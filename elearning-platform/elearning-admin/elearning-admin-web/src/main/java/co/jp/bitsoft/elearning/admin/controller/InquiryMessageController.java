package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import co.jp.bitsoft.elearning.admin.domain.model.inquirymessage.InquiryMessageSaveRequest;
import co.jp.bitsoft.elearning.core.entity.InquiryEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.InquiryMessageEntity;
import co.jp.bitsoft.elearning.admin.service.AdminInquiryMessageService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * 問い合わせメッセージ
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/inquirymessage")
public class InquiryMessageController {
    @Autowired
    private AdminInquiryMessageService inquiryMessageService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:inquirymessage:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = inquiryMessageService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{inquiryMessageId}")
    @RequiresPermissions("service:inquirymessage:info")
    public R info(@PathVariable("inquiryMessageId") Long inquiryMessageId){
		InquiryMessageEntity inquiryMessage = inquiryMessageService.getById(inquiryMessageId);

        return R.ok().put("inquiryMessage", inquiryMessage);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:inquirymessage:save")
    public R save(@RequestBody InquiryMessageSaveRequest inquiryMessageSaveRequest){
        InquiryMessageEntity inquiryMessage = new InquiryMessageEntity();
        BeanUtils.copyProperties(inquiryMessageSaveRequest, inquiryMessage);
        inquiryMessage.setCreateTime(new Date());
        inquiryMessage.setMessageTime(new Date());
        inquiryMessage.setMessageStatus("0");
		inquiryMessageService.save(inquiryMessage);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:inquirymessage:update")
    public R update(@RequestBody InquiryMessageSaveRequest inquiryMessageSaveRequest){
        InquiryMessageEntity inquiryMessage = new InquiryMessageEntity();
        BeanUtils.copyProperties(inquiryMessageSaveRequest, inquiryMessage);
		inquiryMessageService.updateById(inquiryMessage);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:inquirymessage:delete")
    public R delete(@RequestBody Long[] inquiryMessageIds){
		inquiryMessageService.removeByIds(Arrays.asList(inquiryMessageIds));

        return R.ok();
    }

    /**
     * メッセージステータスの変更
     */
    @RequestMapping("/changeStatus")
    @RequiresPermissions("service:inquirymessage:update")
    public R changeStatus(@RequestParam Map<String, String> params){
        inquiryMessageService.changeStatus(Integer.parseInt(params.get("inquiryId")), Integer.parseInt(params.get("userId")));

        return R.ok();
    }

}
