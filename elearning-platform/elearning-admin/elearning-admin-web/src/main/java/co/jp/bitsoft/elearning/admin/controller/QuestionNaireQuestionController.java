package co.jp.bitsoft.elearning.admin.controller;

import java.util.*;
import java.util.stream.Collectors;

import co.jp.bitsoft.elearning.admin.domain.model.questionnairequestion.QuestionNaireQuestionSaveRequest;
import co.jp.bitsoft.elearning.admin.dto.QuestionChoicesViewDto;
import co.jp.bitsoft.elearning.admin.service.AdminQuestionNaireQuestionService;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.service.QuestionNaireQuestionsChoicesService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.QuestionNaireQuestionDto;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionEntity;
import co.jp.bitsoft.elearning.core.service.QuestionNaireQuestionService;
import co.jp.bitsoft.elearning.admin.domain.model.questionnairequestion.QuestionNaireQuestionResponseResource;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * アンケート問題
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/questionnairequestion")
public class QuestionNaireQuestionController {
    @Autowired
    private QuestionNaireQuestionService questionNaireQuestionService;

    @Autowired
    private AdminQuestionNaireQuestionService adminQuestionNaireQuestionService;

    @Autowired
    private QuestionNaireQuestionsChoicesService questionNaireQuestionsChoicesService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:questionnairequestion:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminQuestionNaireQuestionService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{questionNaireQuestionId}")
    @RequiresPermissions("service:questionnairequestion:info")
    public R info(@PathVariable("questionNaireQuestionId") Long questionNaireQuestionId){
		QuestionNaireQuestionEntity questionNaireQuestionEntity = questionNaireQuestionService.getById(questionNaireQuestionId);

        List<QuestionNaireQuestionsChoicesEntity> testQuestionsChoicesList=
                questionNaireQuestionsChoicesService.list(new QueryWrapper<QuestionNaireQuestionsChoicesEntity>().eq("del_flag", "0").eq("question_naire_question_id", questionNaireQuestionId));

        List<QuestionChoicesViewDto> questionChoicesViewDtolist =  new ArrayList<QuestionChoicesViewDto>();
        for (QuestionNaireQuestionsChoicesEntity questionNaireQuestionsChoicesEntity : testQuestionsChoicesList) {
            QuestionChoicesViewDto questionChoicesViewDto = new QuestionChoicesViewDto();
            questionChoicesViewDto.setTestQuestionsChoicesId(questionNaireQuestionsChoicesEntity.getQuestionNaireQuestionsChoicesId());
            questionChoicesViewDto.setContent(questionNaireQuestionsChoicesEntity.getChoicesContents());
            questionChoicesViewDtolist.add(questionChoicesViewDto);
        }
        QuestionNaireQuestionResponseResource questionNaireQuestionResponseResource = new QuestionNaireQuestionResponseResource();
        questionNaireQuestionResponseResource.setItems(questionChoicesViewDtolist);

        return R.ok().put("questionNaireQuestion", questionNaireQuestionResponseResource);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:questionnairequestion:save")
    public R save(@RequestBody QuestionNaireQuestionSaveRequest questionNaireQuestionSaveRequest){
        QuestionNaireQuestionDto questionNaireQuestionDto = new QuestionNaireQuestionDto();
        BeanUtils.copyProperties(questionNaireQuestionSaveRequest, questionNaireQuestionDto);
        List<QuestionChoicesViewDto> items = new ArrayList<>();

        questionNaireQuestionDto.setChoicesNum(0);
        if(!"2".equals(questionNaireQuestionDto.getQuestionType())) {
            questionNaireQuestionDto.setChoicesNum(questionNaireQuestionDto.getItems().size());
            items = questionNaireQuestionSaveRequest.getItems().stream().map(item->{
                        QuestionChoicesViewDto questionChoicesViewDto = new QuestionChoicesViewDto();
                        BeanUtils.copyProperties(item, questionChoicesViewDto);
                        return questionChoicesViewDto;
            }).collect(Collectors.toList());

        }
        questionNaireQuestionDto.setCreateUserId(1l);
        questionNaireQuestionDto.setCreateTime(new Date());
        QuestionNaireQuestionEntity questionNaireQuestionEntity = new QuestionNaireQuestionEntity();
        BeanUtils.copyProperties(questionNaireQuestionDto, questionNaireQuestionEntity);
        questionNaireQuestionService.save(questionNaireQuestionEntity);
        if(!"2".equals(questionNaireQuestionDto.getQuestionType())) {
            List<QuestionNaireQuestionsChoicesEntity> testQuestionsChoicesList =  new ArrayList<QuestionNaireQuestionsChoicesEntity>();
            for (QuestionChoicesViewDto item : items) {
                QuestionNaireQuestionsChoicesEntity questionNaireQuestionsChoicesEntity = new QuestionNaireQuestionsChoicesEntity();
                questionNaireQuestionsChoicesEntity.setCourseId(questionNaireQuestionDto.getCourseId());
                questionNaireQuestionsChoicesEntity.setQuestionNaireId(questionNaireQuestionDto.getQuestionNaireId());
                questionNaireQuestionsChoicesEntity.setQuestionNaireQuestionId(questionNaireQuestionDto.getQuestionNaireQuestionId());
                questionNaireQuestionsChoicesEntity.setChoicesContents(item.getContent());
                questionNaireQuestionsChoicesEntity.setCreateUserId(1l);
                questionNaireQuestionsChoicesEntity.setCreateTime(new Date());
                testQuestionsChoicesList.add(questionNaireQuestionsChoicesEntity);
            }
            questionNaireQuestionsChoicesService.saveBatch(testQuestionsChoicesList);
        }

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:questionnairequestion:update")
    public R update(@RequestBody QuestionNaireQuestionSaveRequest questionNaireQuestionSaveRequest){
        QuestionNaireQuestionDto questionNaireQuestionDto = new QuestionNaireQuestionDto();
        BeanUtils.copyProperties(questionNaireQuestionSaveRequest, questionNaireQuestionDto);
        List<QuestionChoicesViewDto> items = questionNaireQuestionSaveRequest.getItems().stream().map(item->{
            QuestionChoicesViewDto questionChoicesViewDto = new QuestionChoicesViewDto();
            BeanUtils.copyProperties(item, questionChoicesViewDto);
            return questionChoicesViewDto;
        }).collect(Collectors.toList());

        questionNaireQuestionDto.setChoicesNum(0);
        if(!"2".equals(questionNaireQuestionDto.getQuestionType())) {
            questionNaireQuestionDto.setChoicesNum(questionNaireQuestionDto.getItems().size());
        }
        questionNaireQuestionDto.setUpdateUserId(1l);
        questionNaireQuestionDto.setUpdateTime(new Date());

        QuestionNaireQuestionEntity questionNaireQuestionEntity = new QuestionNaireQuestionEntity();
        BeanUtils.copyProperties(questionNaireQuestionSaveRequest, questionNaireQuestionEntity);
        questionNaireQuestionService.updateById(questionNaireQuestionEntity);

        if(!"2".equals(questionNaireQuestionEntity.getQuestionType())) {
            List<QuestionNaireQuestionsChoicesEntity> testQuestionsChoicesList =  new ArrayList<QuestionNaireQuestionsChoicesEntity>();
            for (QuestionChoicesViewDto item : items) {
                QuestionNaireQuestionsChoicesEntity questionNaireQuestionsChoicesEntity = new QuestionNaireQuestionsChoicesEntity();
                questionNaireQuestionsChoicesEntity.setQuestionNaireQuestionsChoicesId(item.getTestQuestionsChoicesId());
                questionNaireQuestionsChoicesEntity.setCourseId(questionNaireQuestionEntity.getCourseId());
                questionNaireQuestionsChoicesEntity.setQuestionNaireId(questionNaireQuestionEntity.getQuestionNaireId());
                questionNaireQuestionsChoicesEntity.setQuestionNaireQuestionId(questionNaireQuestionEntity.getQuestionNaireQuestionId());
                questionNaireQuestionsChoicesEntity.setChoicesContents(item.getContent());
                questionNaireQuestionsChoicesEntity.setUpdateUserId(1l);
                questionNaireQuestionsChoicesEntity.setUpdateTime(new Date());
                testQuestionsChoicesList.add(questionNaireQuestionsChoicesEntity);
            }
            questionNaireQuestionsChoicesService.updateBatchById(testQuestionsChoicesList);
        }

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:questionnairequestion:delete")
    public R delete(@RequestBody Long[] questionNaireQuestionIds){
		questionNaireQuestionService.removeByIds(Arrays.asList(questionNaireQuestionIds));

        return R.ok();
    }

}
