package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.StudentQuestionNaireQuestionsAnswerEntity;
import co.jp.bitsoft.elearning.core.service.StudentQuestionNaireQuestionsAnswerService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * 受講者アンケート問題回答
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/studentquestionnairequestionsanswer")
public class StudentQuestionNaireQuestionsAnswerController {
    @Autowired
    private StudentQuestionNaireQuestionsAnswerService studentQuestionNaireQuestionsAnswerService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:studentquestionnairequestionsanswer:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = studentQuestionNaireQuestionsAnswerService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{studentQuestionNaireQuestionsAnswerId}")
    @RequiresPermissions("service:studentquestionnairequestionsanswer:info")
    public R info(@PathVariable("studentQuestionNaireQuestionsAnswerId") Long studentQuestionNaireQuestionsAnswerId){
		StudentQuestionNaireQuestionsAnswerEntity studentQuestionNaireQuestionsAnswer = studentQuestionNaireQuestionsAnswerService.getById(studentQuestionNaireQuestionsAnswerId);

        return R.ok().put("studentQuestionNaireQuestionsAnswer", studentQuestionNaireQuestionsAnswer);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:studentquestionnairequestionsanswer:save")
    public R save(@RequestBody StudentQuestionNaireQuestionsAnswerEntity studentQuestionNaireQuestionsAnswer){
		studentQuestionNaireQuestionsAnswerService.save(studentQuestionNaireQuestionsAnswer);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:studentquestionnairequestionsanswer:update")
    public R update(@RequestBody StudentQuestionNaireQuestionsAnswerEntity studentQuestionNaireQuestionsAnswer){
		studentQuestionNaireQuestionsAnswerService.updateById(studentQuestionNaireQuestionsAnswer);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:studentquestionnairequestionsanswer:delete")
    public R delete(@RequestBody Long[] studentQuestionNaireQuestionsAnswerIds){
		studentQuestionNaireQuestionsAnswerService.removeByIds(Arrays.asList(studentQuestionNaireQuestionsAnswerIds));

        return R.ok();
    }

}
