package co.jp.bitsoft.elearning.admin.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminSubjectDto;
import co.jp.bitsoft.elearning.admin.service.AdminSubjectService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.SubjectEntity;
import co.jp.bitsoft.elearning.core.service.SubjectService;



/**
 * 教科
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/subject")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private AdminSubjectService adminSubjectService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:subject:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminSubjectService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{subjectId}")
    @RequiresPermissions("service:subject:info")
    public R info(@PathVariable("subjectId") Long subjectId){
		SubjectEntity subject = subjectService.getById(subjectId);

        return R.ok().put("subject", subject);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:subject:save")
    public R save(@RequestBody AdminSubjectDto adminSubjectDto){

    	adminSubjectService.subjectSave(adminSubjectDto);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:subject:update")
    public R update(@RequestBody AdminSubjectDto adminSubjectDto){

    	adminSubjectService.subjectUpdate(adminSubjectDto);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:subject:delete")
    @Transactional
    public R delete(@RequestBody Long[] subjectIds){

    	adminSubjectService.subjectDelete(subjectIds);

        return R.ok();
    }

}
