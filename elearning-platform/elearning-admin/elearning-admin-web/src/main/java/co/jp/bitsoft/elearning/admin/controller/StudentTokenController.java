package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.StudentTokenEntity;
import co.jp.bitsoft.elearning.core.service.StudentTokenService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * 受講者ユーザートークン
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/studenttoken")
public class StudentTokenController {
    @Autowired
    private StudentTokenService studentTokenService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:studenttoken:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = studentTokenService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{studentId}")
    @RequiresPermissions("service:studenttoken:info")
    public R info(@PathVariable("studentId") Long studentId){
		StudentTokenEntity studentToken = studentTokenService.getById(studentId);

        return R.ok().put("studentToken", studentToken);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:studenttoken:save")
    public R save(@RequestBody StudentTokenEntity studentToken){
		studentTokenService.save(studentToken);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:studenttoken:update")
    public R update(@RequestBody StudentTokenEntity studentToken){
		studentTokenService.updateById(studentToken);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:studenttoken:delete")
    public R delete(@RequestBody Long[] studentIds){
		studentTokenService.removeByIds(Arrays.asList(studentIds));

        return R.ok();
    }

}
