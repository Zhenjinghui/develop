package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.StudentTestQuestionAnswerEntity;
import co.jp.bitsoft.elearning.core.service.StudentTestQuestionAnswerService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * 受講者テスト問題解答
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/studenttestquestionanswer")
public class StudentTestQuestionAnswerController {
    @Autowired
    private StudentTestQuestionAnswerService studentTestQuestionAnswerService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:studenttestquestionanswer:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = studentTestQuestionAnswerService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{studentTestQuestionAnswerId}")
    @RequiresPermissions("service:studenttestquestionanswer:info")
    public R info(@PathVariable("studentTestQuestionAnswerId") Long studentTestQuestionAnswerId){
		StudentTestQuestionAnswerEntity studentTestQuestionAnswer = studentTestQuestionAnswerService.getById(studentTestQuestionAnswerId);

        return R.ok().put("studentTestQuestionAnswer", studentTestQuestionAnswer);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:studenttestquestionanswer:save")
    public R save(@RequestBody StudentTestQuestionAnswerEntity studentTestQuestionAnswer){
		studentTestQuestionAnswerService.save(studentTestQuestionAnswer);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:studenttestquestionanswer:update")
    public R update(@RequestBody StudentTestQuestionAnswerEntity studentTestQuestionAnswer){
		studentTestQuestionAnswerService.updateById(studentTestQuestionAnswer);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:studenttestquestionanswer:delete")
    public R delete(@RequestBody Long[] studentTestQuestionAnswerIds){
		studentTestQuestionAnswerService.removeByIds(Arrays.asList(studentTestQuestionAnswerIds));

        return R.ok();
    }

}
