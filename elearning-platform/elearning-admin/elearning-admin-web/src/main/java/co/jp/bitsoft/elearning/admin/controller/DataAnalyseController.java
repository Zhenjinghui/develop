package co.jp.bitsoft.elearning.admin.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseSelectDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDataAnalyseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDataAnalyseExportDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTestQuestionsDataAnalyseExportDto;
import co.jp.bitsoft.elearning.admin.service.AdminDataAnalyseService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;



/**
 * 分析項目
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/dataAnalyse")
public class DataAnalyseController {
    @Autowired
    private AdminDataAnalyseService adminDataAnalyseService;

    /**
     * クラスドロップダウンボックス
     */
    @RequestMapping("/classSelectDrop")
    public R classSelectDrop(@RequestParam Map<String, Object> params){
    	List<UserGroupEntity> classSelectDrop = adminDataAnalyseService.getClassSelectDrop();

        return R.ok().put("classSelectDrop", classSelectDrop);
    }

    /**
     * 講座ドロップダウンボックス
     */
    @RequestMapping("/courseSelectDrop")
    public R courseSelectDrop(@RequestParam Map<String, Object> params){
    	List<AdminCourseSelectDto> courseSelectDrop = adminDataAnalyseService.getCourseSelectDrop();

        return R.ok().put("courseSelectDrop", courseSelectDrop);
    }

    /**
     * 講者傾向分析項目
     */
    @RequestMapping("/studentDataAnalyse")
    public R studentAnalyse(@RequestParam Map<String, Object> params){
    	AdminStudentDataAnalyseDto studentDataAnalyse = adminDataAnalyseService.getStudentDataAnalyse(params);

        return R.ok().put("studentDataAnalyse", studentDataAnalyse);
    }

    /**
     * 受講状況（講座・教材単位）(分页)
     */
    @RequestMapping("/teachingMaterialDataAnalyse")
    public R teachingMaterialDataAnalyse(@RequestParam Map<String, Object> params){
    	PageUtils page = adminDataAnalyseService.getTeachingMaterialDataAnalyse(params);

        return R.ok().put("page", page);
    }

    /**
     * 受講状況（講座・教材単位）(分页)
     */
    @RequestMapping("/teachingMaterialDataAnalyseExport")
    public R teachingMaterialDataAnalyseExport(@RequestParam Map<String, Object> params){
    	List<AdminTeachingMaterialDataAnalyseExportDto> teachingMaterialDataAnalyseExportList = adminDataAnalyseService.getTeachingMaterialDataAnalyseExport(params);

        return R.ok().put("teachingMaterialDataAnalyseExportList", teachingMaterialDataAnalyseExportList);
    }

    /**
     * 単元テスト結果(分页)
     */
    @RequestMapping("/testDataAnalyse")
    public R testDataAnalyse(@RequestParam Map<String, Object> params){
    	PageUtils page = adminDataAnalyseService.getTestDataAnalyse(params);

        return R.ok().put("page", page);
    }

    /**
     * 単元テスト結果(分页)
     */
    @RequestMapping("/testQuestionsDataAnalyseExport")
    public R testQuestionsDataAnalyseExport(@RequestParam Map<String, Object> params){
    	List<AdminTestQuestionsDataAnalyseExportDto> testQuestionsExportList = adminDataAnalyseService.getTestQuesionsExport(params);

        return R.ok().put("testQuestionsExportList", testQuestionsExportList);
    }


}
