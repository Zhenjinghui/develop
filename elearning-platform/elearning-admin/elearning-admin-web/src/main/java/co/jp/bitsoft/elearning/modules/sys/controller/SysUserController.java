/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.dto.AdminSysUserDto;
import co.jp.bitsoft.elearning.admin.dto.AdminSysUserExportDto;
import co.jp.bitsoft.elearning.common.annotation.SysLog;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.modules.sys.form.PasswordForm;
import co.jp.bitsoft.elearning.modules.sys.service.SysUserService;

/**
 * 系统用户
 *
 * @author  BitSoft
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;

	/**
	 * 所有用户列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:user:list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = sysUserService.queryUser(params);

        return R.ok().put("page", page);
	}

	/**
	 * 获取登录的用户信息
	 */
	@GetMapping("/info")
	public R info(){
		return R.ok().put("user", getUser());
	}

	/**
	 * 修改登录用户密码
	 */
	@SysLog("修改密码")
	@PostMapping("/password")
	public R password(@RequestBody PasswordForm form){

		//更新密码
		sysUserService.updatePassword(getUserId(), form.getPassword(), form.getNewPassword());

		return R.ok();
	}

	/**
	 * 用户信息
	 */
	@GetMapping("/info/{userId}")
	@RequiresPermissions("sys:user:info")
	public R info(@PathVariable("userId") Long userId){

        AdminSysUserDto user = sysUserService.userInfo(userId);

		return R.ok().put("user", user);
	}

	/**
	 * 保存用户
	 */
	@SysLog("保存用户")
	@PostMapping("/save")
	@RequiresPermissions("sys:user:save")
	public R save(@RequestBody AdminSysUserDto user){
		sysUserService.saveUser(user);

		return R.ok();
	}

	/**
	 * 修改用户
	 */
	@SysLog("修改用户")
	@PostMapping("/update")
	@RequiresPermissions("sys:user:update")
	public R update(@RequestBody AdminSysUserDto user){

		sysUserService.update(user);

		return R.ok();
	}

	/**
	 * 删除用户
	 */
	@SysLog("删除用户")
	@PostMapping("/delete")
	@RequiresPermissions("sys:user:delete")
	public R delete(@RequestBody Long[] userIds){

		sysUserService.deleteBatch(userIds);

		return R.ok();
	}

	/**
	 * 用户エクスポート
	 */
	@GetMapping("/export")
	@RequiresPermissions("sys:user:export")
	public R export(@RequestParam Map<String, Object> params){
        List<AdminSysUserExportDto> sysUserList = sysUserService.queryAllUser(params);

        return R.ok().put("sysUserList", sysUserList);
	}
	/**
	 * 用户インポト
	 */
	@PostMapping("/import")
	@RequiresPermissions("sys:user:import")
	@Transactional
	public R userImport(@RequestBody JSONObject csvData){

	   sysUserService.userImport(csvData);

       return R.ok();
	}

	/**
	 * 用户ロール
	 */
	@GetMapping("/loginUserRole")
	public R loginUserRole(){

	   Long loginUserRoleId = sysUserService.loginUserRole();

       return R.ok().put("loginUserRoleId", loginUserRoleId);
	}

	/**
	 * リセットパスワード
	 */
	@SysLog("リセットパスワード")
	@PostMapping("/resetPassword")
	public R resetPassword(@RequestBody Long userId){

		//更新密码
		sysUserService.resetPassword(userId);

		return R.ok();
	}
}
