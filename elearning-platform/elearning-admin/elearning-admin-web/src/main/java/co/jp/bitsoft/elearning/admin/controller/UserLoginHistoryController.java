package co.jp.bitsoft.elearning.admin.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.service.AdminUserLoginHistoryService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * ユーザーログイン履歴
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/userloginhistory")
public class UserLoginHistoryController {
    @Autowired
    private AdminUserLoginHistoryService adminUserLoginHistoryService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:userloginhistory:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminUserLoginHistoryService.selectUserLoginHistory(params);

        return R.ok().put("page", page);
    }

}
