package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.CodePatternEntity;
import co.jp.bitsoft.elearning.core.service.CodePatternService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * コードパターン
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/codepattern")
public class CodePatternController {
    @Autowired
    private CodePatternService codePatternService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:codepattern:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = codePatternService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{codeId}")
    @RequiresPermissions("service:codepattern:info")
    public R info(@PathVariable("codeId") String codeId){
		CodePatternEntity codePattern = codePatternService.getById(codeId);

        return R.ok().put("codePattern", codePattern);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:codepattern:save")
    public R save(@RequestBody CodePatternEntity codePattern){
		codePatternService.save(codePattern);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:codepattern:update")
    public R update(@RequestBody CodePatternEntity codePattern){
		codePatternService.updateById(codePattern);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:codepattern:delete")
    public R delete(@RequestBody String[] codeIds){
		codePatternService.removeByIds(Arrays.asList(codeIds));

        return R.ok();
    }

}
