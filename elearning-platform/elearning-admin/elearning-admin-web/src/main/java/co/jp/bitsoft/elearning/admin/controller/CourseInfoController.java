package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.domain.model.courseinfo.CourseInfoSaveRequest;
import co.jp.bitsoft.elearning.admin.domain.model.courseinfo.CourseInfoUpdateRequest;
import co.jp.bitsoft.elearning.admin.service.AdminCourseInfoService;
import co.jp.bitsoft.elearning.common.exception.RRException;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.CourseEntity;
import co.jp.bitsoft.elearning.core.entity.CourseInfoEntity;
import co.jp.bitsoft.elearning.core.service.CourseService;



/**
 * 講座インフォメーション
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/courseinfo")
public class CourseInfoController {
    @Autowired
    private AdminCourseInfoService courseInfoService;

	@Autowired
	private CourseService courseService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:courseinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = courseInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{infoId}")
    @RequiresPermissions("service:courseinfo:info")
    public R info(@PathVariable("infoId") Long infoId){
		CourseInfoEntity courseInfo = courseInfoService.getById(infoId);

        return R.ok().put("courseInfo", courseInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:courseinfo:save")
    public R save(@RequestBody CourseInfoSaveRequest courseInfoSaveRequest){
    	CourseEntity course = courseService.getById(courseInfoSaveRequest.getCourseId());
    	if(course == null) {
    		throw new RRException("講座は存在しません！");
    	}

    	if(!"1".equals(courseInfoSaveRequest.getInfoStatus())) {
    		courseInfoSaveRequest.setOpenStartTime(new Date());
    	}

        CourseInfoEntity courseInfo = new CourseInfoEntity();
        BeanUtils.copyProperties(courseInfoSaveRequest, courseInfo);
		courseInfoService.save(courseInfo);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:courseinfo:update")
    public R update(@RequestBody CourseInfoUpdateRequest courseInfoUpdateRequest){
    	CourseEntity course = courseService.getById(courseInfoUpdateRequest.getCourseId());
    	if(course == null) {
    		throw new RRException("講座は存在しません！");
    	}

		CourseInfoEntity courseInfoDb = courseInfoService.getById(courseInfoUpdateRequest.getInfoId());

		if(!courseInfoUpdateRequest.getInfoStatus().equals(courseInfoDb.getInfoStatus())) {
	    	if(!"1".equals(courseInfoUpdateRequest.getInfoStatus())) {
	    		courseInfoUpdateRequest.setOpenStartTime(new Date());
	    	}else {
	    		courseInfoUpdateRequest.setOpenStartTime(null);
	    	}
		}

        CourseInfoEntity courseInfo = new CourseInfoEntity();
        BeanUtils.copyProperties(courseInfoUpdateRequest, courseInfo);
		courseInfoService.updateById(courseInfo);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:courseinfo:delete")
    public R delete(@RequestBody Long[] infoIds){
		courseInfoService.removeByIds(Arrays.asList(infoIds));

        return R.ok();
    }

}
