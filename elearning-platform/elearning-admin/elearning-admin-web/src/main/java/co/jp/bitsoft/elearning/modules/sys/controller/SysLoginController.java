/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.common.UserInfo;
import co.jp.bitsoft.elearning.common.UserInfoThreadContext;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.UserLoginHistoryEntity;
import co.jp.bitsoft.elearning.core.service.UserLoginHistoryService;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;
import co.jp.bitsoft.elearning.modules.sys.form.SysLoginForm;
import co.jp.bitsoft.elearning.modules.sys.service.SysCaptchaService;
import co.jp.bitsoft.elearning.modules.sys.service.SysUserService;
import co.jp.bitsoft.elearning.modules.sys.service.SysUserTokenService;

/**
 * 登录相关
 *
 * @author  BitSoft
 */
@RestController
public class SysLoginController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserTokenService sysUserTokenService;
	@Autowired
	private SysCaptchaService sysCaptchaService;
	@Autowired
	private UserLoginHistoryService userLoginHistoryService;

	/**
	 * 验证码
	 */
	@GetMapping("captcha.jpg")
	public void captcha(HttpServletResponse response, String uuid)throws IOException {
		response.setHeader("Cache-Control", "no-store, no-cache");
		response.setContentType("image/jpeg");

		//获取图片验证码
		BufferedImage image = sysCaptchaService.getCaptcha(uuid);

		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(image, "jpg", out);
		IOUtils.closeQuietly(out);
	}

	/**
	 * 登录
	 */
//	@SysLog("用户登录")
	@PostMapping("/sys/login")
	public Map<String, Object> login(@RequestBody SysLoginForm form)throws IOException {
		boolean captcha = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());

		if(!captcha){
			return R.error("検証コードが正しくありません");
		}

		//用户信息
		SysUserEntity user = sysUserService.queryByUserName(form.getUsername());

		//账号不存在、密码错误
		if(user == null || !user.getUserPassword().equals(new Sha256Hash(form.getPassword(), user.getUserSalt()).toHex())) {
			return R.error("ユーザ名またはパスワードが正しくありません");
		}

		//账号锁定
		if(user.getStatus() == 0){
			return R.error("アカウントがロックされました,システム管理者に連絡してください");
		}

//		SecurityUtils.getSubject().getSession(true).setAttribute("userId", user.getUserId());
//
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getUserId());
        userInfo.setUserName(user.getUserName());
        UserInfoThreadContext.setUserInfo(userInfo);

		//生成token，并保存到数据库
		R r = sysUserTokenService.createToken(user.getUserId(),user.getUserPassword(),user.getUserSalt());

		UserLoginHistoryEntity userLoginHistoryEntity = new UserLoginHistoryEntity();
		userLoginHistoryEntity.setUserId(user.getUserId());
		userLoginHistoryEntity.setLoginOutKubun("0");
		userLoginHistoryEntity.setLoginOutHistoryTime(new Date());
		userLoginHistoryService.save(userLoginHistoryEntity);

//		QueryWrapper<UserLoginHistoryEntity> queryWrapper = new QueryWrapper<>();
//    	queryWrapper.eq("user_id", user.getUserId());
//    	UserLoginHistoryEntity userLoginHistory = userLoginHistoryService.getOne(queryWrapper);
//
//    	if(userLoginHistory == null) {
//			userLoginHistoryService.saveOrUpdate(userLoginHistoryEntity);
//		}else {
//			userLoginHistoryEntity.setId(userLoginHistory.getId());
//			userLoginHistoryService.updateById(userLoginHistoryEntity);
//		}

		return r;
	}


	/**
	 * 退出
	 */
	@PostMapping("/sys/logout")
	public R logout() {
		sysUserTokenService.logout(getUserId());

		UserLoginHistoryEntity userLoginHistoryEntity = new UserLoginHistoryEntity();
		userLoginHistoryEntity.setUserId(getUserId());
		userLoginHistoryEntity.setLoginOutKubun("1");
		userLoginHistoryEntity.setLoginOutHistoryTime(new Date());
		userLoginHistoryService.save(userLoginHistoryEntity);
//		QueryWrapper<UserLoginHistoryEntity> queryWrapper = new QueryWrapper<>();
//    	queryWrapper.eq("user_id", getUserId());
//    	UserLoginHistoryEntity userLoginHistory = userLoginHistoryService.getOne(queryWrapper);
//
//    	if(userLoginHistory == null) {
//			userLoginHistoryService.saveOrUpdate(userLoginHistoryEntity);
//		}else {
//			userLoginHistoryEntity.setId(userLoginHistory.getId());
//			userLoginHistoryService.updateById(userLoginHistoryEntity);
//		}
		return R.ok();
	}

}
