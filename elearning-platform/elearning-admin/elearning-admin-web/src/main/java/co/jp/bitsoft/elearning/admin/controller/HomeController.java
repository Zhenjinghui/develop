package co.jp.bitsoft.elearning.admin.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminHomeDto;
import co.jp.bitsoft.elearning.admin.service.AdminHomeService;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * ホーム
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/home")
public class HomeController {
    @Autowired
    private AdminHomeService adminHomeService;

    /**
     * リスト一覧
     */
    @RequestMapping("/homeData")
    public R list(@RequestParam Map<String, Object> params){
    	AdminHomeDto homeData = adminHomeService.getHomeData();

        return R.ok().put("homeData", homeData);
    }




}
