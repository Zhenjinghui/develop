package co.jp.bitsoft.elearning.admin.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDto;
import co.jp.bitsoft.elearning.admin.service.AdminTeachingMaterialService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.TeachingMaterialEntity;
import co.jp.bitsoft.elearning.core.service.TeachingMaterialService;



/**
 * 教材
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/teachingmaterial")
public class TeachingMaterialController {
    @Autowired
    private TeachingMaterialService teachingMaterialService;

    @Autowired
    private AdminTeachingMaterialService adminTeachingMaterialService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:teachingmaterial:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminTeachingMaterialService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{teachingMaterialId}")
    @RequiresPermissions("service:teachingmaterial:info")
    public R info(@PathVariable("teachingMaterialId") Long teachingMaterialId){
		TeachingMaterialEntity teachingMaterial = teachingMaterialService.getById(teachingMaterialId);

        return R.ok().put("teachingMaterial", teachingMaterial);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:teachingmaterial:save")
    public R save(@RequestBody AdminTeachingMaterialDto adminTeachingMaterialDto){

	    adminTeachingMaterialService.teachingMaterialSave(adminTeachingMaterialDto);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:teachingmaterial:update")
    public R update(@RequestBody AdminTeachingMaterialDto adminTeachingMaterialDto){

	    adminTeachingMaterialService.teachingMaterialUpdate(adminTeachingMaterialDto);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:teachingmaterial:delete")
    public R delete(@RequestBody Long[] teachingMaterialIds){
	    adminTeachingMaterialService.teachingMaterialDelete(teachingMaterialIds);

        return R.ok();
    }

}
