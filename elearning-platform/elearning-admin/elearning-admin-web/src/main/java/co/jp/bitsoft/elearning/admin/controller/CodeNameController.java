package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.CodeNameEntity;
import co.jp.bitsoft.elearning.core.service.CodeNameService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * コード名称
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/codename")
public class CodeNameController {
    @Autowired
    private CodeNameService codeNameService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:codename:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = codeNameService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{codeId}")
    @RequiresPermissions("service:codename:info")
    public R info(@PathVariable("codeId") String codeId){
		CodeNameEntity codeName = codeNameService.getById(codeId);

        return R.ok().put("codeName", codeName);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:codename:save")
    public R save(@RequestBody CodeNameEntity codeName){
		codeNameService.save(codeName);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:codename:update")
    public R update(@RequestBody CodeNameEntity codeName){
		codeNameService.updateById(codeName);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:codename:delete")
    public R delete(@RequestBody String[] codeIds){
		codeNameService.removeByIds(Arrays.asList(codeIds));

        return R.ok();
    }

}
