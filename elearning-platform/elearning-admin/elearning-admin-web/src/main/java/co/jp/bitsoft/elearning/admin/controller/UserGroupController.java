package co.jp.bitsoft.elearning.admin.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminUserGroupDto;
import co.jp.bitsoft.elearning.admin.service.AdminUserGroupService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.AdAddressEntity;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.modules.sys.controller.AbstractController;



/**
 * 運用者グループ
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/usergroup")
public class UserGroupController extends AbstractController {
    @Autowired
    private AdminUserGroupService adminUserGroupService;
    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:usergroup:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminUserGroupService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{groupId}")
    @RequiresPermissions("service:usergroup:info")
    public R info(@PathVariable("groupId") Long groupId){
    	AdminUserGroupDto userGroup = adminUserGroupService.userGroupInfo(groupId);

        return R.ok().put("userGroup", userGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:usergroup:save")
    @Transactional
    public R save(@RequestBody AdminUserGroupDto adminUserGroupDto) {
    	adminUserGroupService.userGroupSave(adminUserGroupDto);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:usergroup:update")
    @Transactional
    public R update(@RequestBody AdminUserGroupDto adminUserGroupDto){
    	adminUserGroupService.userGroupUpdate(adminUserGroupDto);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:usergroup:delete")
    @Transactional
    public R delete(@RequestBody Long groupId){

    	adminUserGroupService.userGroupDelete(groupId);

        return R.ok();
    }
    /**
     * 郵便番号
     */
    @RequestMapping("/zipInfo/{groupZipCode}")
    public R zipAddressInfo(@PathVariable("groupZipCode") String groupZipCode){

		AdAddressEntity adAddress = adminUserGroupService.zipAddressInfo(groupZipCode);
        return R.ok().put("adAddress", adAddress);
    }

    /**
     * グループタイプ
     */
    @RequestMapping("/groupType")
    public R groupType(Long groupId){
		String groupType = adminUserGroupService.groupType(groupId);

        return R.ok().put("groupType", groupType);
    }

    /**
     * グループ下拉框
     */
    @RequestMapping("/selectDrop")
    @RequiresPermissions("service:usergroup:selectDrop")
    public R groupList(@RequestParam Map<String, Object> params){

		List<UserGroupEntity> groupList = adminUserGroupService.groupList(params);
        return R.ok().put("list", groupList);
    }
    /**
    * グループタイプ
    */
   @RequestMapping("/groupId")
   public R groupId(){

       return R.ok().put("groupId", getGroupId());
   }
}
