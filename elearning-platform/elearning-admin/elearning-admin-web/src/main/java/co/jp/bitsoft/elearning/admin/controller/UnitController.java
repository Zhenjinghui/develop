package co.jp.bitsoft.elearning.admin.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.admin.dto.AdminUnitDto;
import co.jp.bitsoft.elearning.admin.service.AdminUnitService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.entity.UnitEntity;
import co.jp.bitsoft.elearning.core.service.UnitService;



/**
 * 単元
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/unit")
public class UnitController {
	@Autowired
    private UnitService unitService;
    @Autowired
    private AdminUnitService adminUnitService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:unit:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminUnitService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{unitId}")
    @RequiresPermissions("service:unit:info")
    public R info(@PathVariable("unitId") Long unitId){
		UnitEntity unit = unitService.getById(unitId);

        return R.ok().put("unit", unit);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:unit:save")
    public R save(@RequestBody AdminUnitDto adminUnitDto){

    	adminUnitService.unitSave(adminUnitDto);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:unit:update")
    public R update(@RequestBody AdminUnitDto adminUnitDto){

    	adminUnitService.unitUpdate(adminUnitDto);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:unit:delete")
    @Transactional
    public R delete(@RequestBody Long[] unitIds){

    	adminUnitService.unitDelete(unitIds);

        return R.ok();
    }

}
