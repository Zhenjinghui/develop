package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import co.jp.bitsoft.elearning.admin.domain.model.questionnaire.QuestionNaireSaveRequest;
import co.jp.bitsoft.elearning.admin.domain.model.questionnaire.QuestionNaireUpdateRequest;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.QuestionNaireEntity;
import co.jp.bitsoft.elearning.core.service.QuestionNaireService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * アンケート
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/questionnaire")
public class QuestionNaireController {
    @Autowired
    private QuestionNaireService questionNaireService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:questionnaire:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = questionNaireService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{questionNaireId}")
    @RequiresPermissions("service:questionnaire:info")
    public R info(@PathVariable("questionNaireId") Long questionNaireId){
		QuestionNaireEntity questionNaire = questionNaireService.getById(questionNaireId);

        return R.ok().put("questionNaire", questionNaire);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:questionnaire:save")
    public R save(@RequestBody QuestionNaireSaveRequest questionNaireSaveRequest){
        QuestionNaireEntity questionNaire = new QuestionNaireEntity();
        BeanUtils.copyProperties(questionNaireSaveRequest, questionNaire);
        if("0".equals(questionNaire.getQuestionNaireStatus())) {
            questionNaire.setQuestionNaireOpenTime(new Date());
        }else if("1".equals(questionNaire.getQuestionNaireStatus())) {
            questionNaire.setQuestionNaireEndTime(new Date());
        }
        questionNaire.setCreateTime(new Date());
        questionNaire.setCreateUserId(1l);
		questionNaireService.save(questionNaire);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:questionnaire:update")
    public R update(@RequestBody QuestionNaireUpdateRequest questionNaireUpdateRequest){
        QuestionNaireEntity questionNaire = new QuestionNaireEntity();
        BeanUtils.copyProperties(questionNaireUpdateRequest, questionNaire);

        if("0".equals(questionNaire.getQuestionNaireStatus())) {
            questionNaire.setQuestionNaireOpenTime(new Date());
        }else if("1".equals(questionNaire.getQuestionNaireStatus())) {
            questionNaire.setQuestionNaireEndTime(new Date());
        }
		questionNaireService.updateById(questionNaire);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:questionnaire:delete")
    public R delete(@RequestBody Long[] questionNaireIds){
		questionNaireService.removeByIds(Arrays.asList(questionNaireIds));

        return R.ok();
    }

}
