package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.CourseCertificateTemplateEntity;
import co.jp.bitsoft.elearning.core.service.CourseCertificateTemplateService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * 講座終了証テンプレート
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/coursecertificatetemplate")
public class CourseCertificateTemplateController {
    @Autowired
    private CourseCertificateTemplateService courseCertificateTemplateService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:coursecertificatetemplate:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = courseCertificateTemplateService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{courseCertificateTemplateId}")
    @RequiresPermissions("service:coursecertificatetemplate:info")
    public R info(@PathVariable("courseCertificateTemplateId") Long courseCertificateTemplateId){
		CourseCertificateTemplateEntity courseCertificateTemplate = courseCertificateTemplateService.getById(courseCertificateTemplateId);

        return R.ok().put("courseCertificateTemplate", courseCertificateTemplate);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:coursecertificatetemplate:save")
    public R save(@RequestBody CourseCertificateTemplateEntity courseCertificateTemplate){
		courseCertificateTemplateService.save(courseCertificateTemplate);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:coursecertificatetemplate:update")
    public R update(@RequestBody CourseCertificateTemplateEntity courseCertificateTemplate){
		courseCertificateTemplateService.updateById(courseCertificateTemplate);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:coursecertificatetemplate:delete")
    public R delete(@RequestBody Long[] courseCertificateTemplateIds){
		courseCertificateTemplateService.removeByIds(Arrays.asList(courseCertificateTemplateIds));

        return R.ok();
    }

}
