package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.StudentQuestionNaireAnswerEntity;
import co.jp.bitsoft.elearning.core.service.StudentQuestionNaireAnswerService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * 受講者アンケート回答
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/studentquestionnaireanswer")
public class StudentQuestionNaireAnswerController {
    @Autowired
    private StudentQuestionNaireAnswerService studentQuestionNaireAnswerService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:studentquestionnaireanswer:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = studentQuestionNaireAnswerService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{studentQuestionNaireAnswerId}")
    @RequiresPermissions("service:studentquestionnaireanswer:info")
    public R info(@PathVariable("studentQuestionNaireAnswerId") Long studentQuestionNaireAnswerId){
		StudentQuestionNaireAnswerEntity studentQuestionNaireAnswer = studentQuestionNaireAnswerService.getById(studentQuestionNaireAnswerId);

        return R.ok().put("studentQuestionNaireAnswer", studentQuestionNaireAnswer);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:studentquestionnaireanswer:save")
    public R save(@RequestBody StudentQuestionNaireAnswerEntity studentQuestionNaireAnswer){
		studentQuestionNaireAnswerService.save(studentQuestionNaireAnswer);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:studentquestionnaireanswer:update")
    public R update(@RequestBody StudentQuestionNaireAnswerEntity studentQuestionNaireAnswer){
		studentQuestionNaireAnswerService.updateById(studentQuestionNaireAnswer);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:studentquestionnaireanswer:delete")
    public R delete(@RequestBody Long[] studentQuestionNaireAnswerIds){
		studentQuestionNaireAnswerService.removeByIds(Arrays.asList(studentQuestionNaireAnswerIds));

        return R.ok();
    }

}
