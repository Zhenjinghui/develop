package co.jp.bitsoft.elearning.admin.controller;

import java.util.Arrays;

import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryForStudentStaRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryRequest;
import co.jp.bitsoft.elearning.admin.service.impl.AdminStudentCourseStudyServiceImpl;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.core.entity.StudentCourseStudyEntity;
import co.jp.bitsoft.elearning.core.service.StudentCourseStudyService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;



/**
 * 受講者受講
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/studentcoursestudy")
public class StudentCourseStudyController {
    @Autowired
    private StudentCourseStudyService studentCourseStudyService;

    @Autowired
    private AdminStudentCourseStudyServiceImpl adminStudentCourseStudyService;

    /**
     * 教材別受講状況一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:studentcoursestudy:list")
    public R list(StudentCourseStudyQueryRequest params){
        PageUtils page = adminStudentCourseStudyService.queryStudentCourseStudy(params);

        return R.ok().put("page", page);
    }

    /**
     * 受講者教材別受講状況一覧　詳細
     */
    @RequestMapping("/student/list")
    @RequiresPermissions("service:studentcoursestudy:list")
    public R studentStaList(StudentCourseStudyQueryForStudentStaRequest params){
        PageUtils page = adminStudentCourseStudyService.queryStudentCourseStudyStudentSta(params);

        return R.ok().put("page", page);
    }


    /**
     * 情報
     */
    @RequestMapping("/info/{studentStudyId}")
    @RequiresPermissions("service:studentcoursestudy:info")
    public R info(@PathVariable("studentStudyId") Long studentStudyId){
		StudentCourseStudyEntity studentCourseStudy = studentCourseStudyService.getById(studentStudyId);

        return R.ok().put("studentCourseStudy", studentCourseStudy);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("service:studentcoursestudy:save")
    public R save(@RequestBody StudentCourseStudyEntity studentCourseStudy){
		studentCourseStudyService.save(studentCourseStudy);

        return R.ok();
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:studentcoursestudy:update")
    public R update(@RequestBody StudentCourseStudyEntity studentCourseStudy){
		studentCourseStudyService.updateById(studentCourseStudy);

        return R.ok();
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:studentcoursestudy:delete")
    public R delete(@RequestBody Long[] studentStudyIds){
		studentCourseStudyService.removeByIds(Arrays.asList(studentStudyIds));

        return R.ok();
    }


}
