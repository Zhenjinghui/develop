package co.jp.bitsoft.elearning.admin.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage.StudentCourseManageQueryForStudentStaRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage.StudentCourseManageQueryRequest;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageExportDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageUpdateDto;
import co.jp.bitsoft.elearning.admin.service.AdminStudentCourseManageService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.core.service.StudentCourseManageService;



/**
 * 受講者コース管理
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@RestController
@RequestMapping("service/studentcoursemanage")
public class StudentCourseManageController {
    @Autowired
    private AdminStudentCourseManageService adminStudentCourseManageService;
    @Autowired
    private StudentCourseManageService studentCourseManageService;

    /**
     * リスト一覧
     */
    @RequestMapping("/list")
    @RequiresPermissions("service:studentcoursemanage:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = adminStudentCourseManageService.queryStudentCourseManage(params);

        return R.ok().put("page", page);
    }

    /**
     * 講座別受講状況管理リスト一覧
     */
    @RequestMapping("/stalist")
    @RequiresPermissions("service:studentcoursemanage:list")
    public R stalist(StudentCourseManageQueryRequest params){
        PageUtils page = adminStudentCourseManageService.queryStudentCourseManageForSta(params);

        return R.ok().put("page", page);
    }

    /**
     * 受講者別受講状況管理リスト一覧
     */
    @RequestMapping("/student/stalist")
    @RequiresPermissions("service:studentcoursemanage:list")
    public R studentStalist(StudentCourseManageQueryForStudentStaRequest params){
        PageUtils page = adminStudentCourseManageService.queryStudentCourseManageForStudentSta(params);

        return R.ok().put("page", page);
    }


        /**
         * 情報
         */
    @RequestMapping("/info/{studentCourseId}")
    @RequiresPermissions("service:studentcoursemanage:info")
    public R info(@PathVariable("studentCourseId") Long studentCourseId){

    	AdminStudentCourseManageDto studentCourseManage = adminStudentCourseManageService.studentCourseManageInfo(studentCourseId);

        return R.ok().put("studentCourseManage", studentCourseManage);
    }

    /**
     * 削除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("service:studentcoursemanage:delete")
    @Transactional
    public R delete(@RequestBody Long studentCourseId){

    	studentCourseManageService.removeById(studentCourseId);

        return R.ok();
    }

    /**
     * 承認/拒否
     */
    @RequestMapping("/update")
    @RequiresPermissions("service:studentcoursemanage:update")
    @Transactional
    public R update(@RequestBody AdminStudentCourseManageUpdateDto adminStudentCourseManageDto){
        // No.73 問題対応
    	// adminStudentCourseManageService.studentCourseManageUpdate(adminStudentCourseManageDto);
        // End of No.73
        return R.ok();
    }
	/**
	 * 用户エクスポート
	 */
	@GetMapping("/export")
	@RequiresPermissions("service:studentcoursemanage:export")
	public R export(@RequestParam Map<String, Object> params){
        List<AdminStudentCourseManageExportDto> studenCourseManagetList = adminStudentCourseManageService.queryAllStudentCourseManage(params);

        return R.ok().put("studentList", studenCourseManagetList);
	}

	/**
	 * 用户エクスポート
	 */
	@PostMapping("/import")
	@RequiresPermissions("service:studentcoursemanage:import")
	@Transactional
	public R studentCourseImport(@RequestBody JSONObject csvData){
		adminStudentCourseManageService.studentCourseImport(csvData);

        return R.ok();
	}

    /**
    * 受講者別テスト状況管理
    */
    @RequestMapping("/student/unitTestList")
//    @RequiresPermissions("service:studentcoursemanage:list")
    public R studentUnitTestManageList(StudentCourseManageQueryForStudentStaRequest params){
        PageUtils page = adminStudentCourseManageService.queryStudentUnitTestManage(params);

        return R.ok().put("page", page);
    }


}
