package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 受講者傾向分析項目
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminStudentDataAnalyseDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 男女比
	 */
	private List<AdminStudentDataAnalyseUseDto> manEcharts;
	/**
	 * 職業構成
	 */
	private List<AdminStudentDataAnalyseUseDto> workEcharts;
	/**
	 * 年齢層
	 */
	private List<AdminStudentDataAnalyseUseDto> ageEcharts;

}
