/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.admin.dto;
import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 運用者ユーザー
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminSysUserExportDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ユーザーID
	 */
	private Long userId;
	/**
	 * ユーザー名
	 */
	@NotBlank(message="ユーザー名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="ユーザ名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String userName;
	/**
	 * ユーザー名（漢字）
	 */
	@NotBlank(message="ユーザー名（漢字）を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="ユーザー名（漢字）は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String userNameKanji;
	/**
	 * ユーザー名（カナ）
	 */
	@NotBlank(message="ユーザー名（カナ）を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="ユーザー名（カナ）は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
    //@Pattern(regexp = "^[\\u30A0-\\u30FF]+$", message = "ユーザー名（カナ）フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String userNameKana;

	/**
	 * ロールID
	 */
	private Long roleId;

	/**
	 * ロール名
	 */
	private String roleName;

	/**
	 * グループID
	 */
	@NotNull(message="所属グループIDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long groupId;
	/**
	 * グループ名
	 */
	private String groupName;
	/**
	 * グループ所属グループId
	 */
	private Long groupIdOf;
	/**
	 * グループ所属グループ名
	 */
	private String groupNameOf;

	/**
	 * メール
	 */
	@NotBlank(message="メールを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$", message = "メールアドレスフォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String email;

	/**
	 * 連絡・携帯
	 */
//	@NotBlank(message="連絡・携帯を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^(070|080|090)\\d{8}$", message = "連絡・携帯フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String mobile;
	/**
	 * 連絡・電話
	 */
//	@NotBlank(message="連絡・電話を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^$|^0[0-9]{9}$", message = "連絡・電話フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String tel;
	/**
	 * 連絡・FAX
	 */
//	@NotBlank(message="連絡・FAXを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^$|^0[0-9]{9}$", message = "連絡・FAXフォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String fax;

	/**
	 * 性別
	 */
	@NotBlank(message="性別を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "性別は0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String sex;

	/**
	 * 部署名
	 */
//	@NotBlank(message="部署名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="部署名は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String departName;
	/**
	 * 職位名
	 */
//	@NotBlank(message="職位名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="職位名は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String positionName;

	/**
	 * 状態
	 */
	@NotBlank(message="アカウント状態を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "アカウントス状態は0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String status;
}
