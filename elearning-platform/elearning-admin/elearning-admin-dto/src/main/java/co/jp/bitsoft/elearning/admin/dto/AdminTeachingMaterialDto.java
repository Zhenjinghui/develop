package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.baomidou.mybatisplus.annotation.TableId;
import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 教材
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminTeachingMaterialDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 教材ID
	 */
	private Long teachingMaterialId;
	/**
	 * 表示順
	 */
	private Long dispNo;
	/**
	 * 講座ID
	 */
	@NotNull(message="講座IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long courseId;
	/**
	 * 教科ID
	 */
	@NotNull(message="教科IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long subjectId;
	/**
	 * 単元ID
	 */
	@NotNull(message="単元IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long unitId;
	/**
	 * 教材名
	 */
	@NotBlank(message="教材名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="教材名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String teachingMaterialTitle;
	/**
	 * 教材画像PATH
	 */
//	@NotBlank(message="教材画像PATHを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="単元画像PATHは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String teachingMaterialImagePath;
	/**
	 * 教材AWS S3 BUCKET名
	 */
//	@NotBlank(message="教材AWS S3 BUCKET名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="単元画像PATHは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String teachingMaterialS3BucketName;
	/**
	 * 教材AWS S3 オブジェクトキー
	 */
//	@NotBlank(message="教材AWS S3 オブジェクトキーを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="単元画像PATHは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String teachingMaterialS3ObjectKey;
	/**
	 * 教材概要
	 */
//	@NotBlank(message="教材概要を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=500,message="教材概要は500ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String teachingMaterialOutline;
	/**
	 * 教材説明
	 */
//	@NotBlank(message="教材説明を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=1000,message="教材説明は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String teachingMaterialDetail;
	/**
	 * 教材ステータス
	 */
	@NotBlank(message="教材ステータスを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "教材ステータスは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String teachingMaterialStatus;
	/**
	 * コンテンツ種別
	 */
	@NotBlank(message="コンテンツ種別を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^0[0-9]$", message = "コンテンツ種別フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contentType;
	/**
	 * コンテンツURL
	 */
//	@NotBlank(message="コンテンツURLを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="コンテンツURLは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String contentUrl;
	/**
	 * コンテンツ（動画）放送時間（分）
	 */
	private Integer contentVideoTime;
	/**
	 * コンテンツページ数
	 */
	private Integer contentPages;
	/**
	 * コンテンツ文字数
	 */
	private Integer contentLettersNum;
	/**
	 * 受講解放条件区分
	 */
	private String conditionKbn;
	/**
	 * 公開開始日時
	 */
	private Date openStartTime;
	/**
	 * 公開終了日時
	 */
	private Date openEndTime;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;

}
