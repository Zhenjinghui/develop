package co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy;

import lombok.Data;

@Data
public class StudentCourseStudyQueryResponse {
    private long groupIdOf;

    private String groupIdOfName;

    private long groupId;

    private String groupName;

    private long studentId;

    private String userName;

    private long subjectId;

    private String subjectTitle;

    private long unitId;

    private String unitName;

    private long teachingMaterialId;

    private String teachingMaterialTitle;

    private String studyStatus;
}
