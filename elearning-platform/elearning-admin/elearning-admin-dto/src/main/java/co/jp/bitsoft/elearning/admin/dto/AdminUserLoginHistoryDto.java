package co.jp.bitsoft.elearning.admin.dto;

import java.util.Date;

import lombok.Data;
@Data
public class AdminUserLoginHistoryDto {

	/**
	 * ID
	 */
	private Long id;
	/**
	 * ユーザーID
	 */
	private Long userId;
	/**
	 * 運用者ユーザー名
	 */
	private String userName;
	/**
	 * アクション区分
	 */
	private String loginOutKubun;
	/**
	 * アクション履歴日時
	 */
	private Date loginOutHistoryTime;

}
