package co.jp.bitsoft.elearning.admin.dto;


import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class NoPassedAnswerDto implements Serializable {

    private static final long serialVersionUID = -4215430459204402984L;

    /**
     * 問題名
     */
    @ApiModelProperty(value = "問題名")
    private String questionTitle;

    /**
     * 問題文
     */
    @ApiModelProperty(value = "問題文")
    private String questionContent;

    /**
     * 解説文
     */
    @ApiModelProperty(value = "解説文")
    private String questionExplanation;

    /**
     * 解答内容
     */
    @ApiModelProperty(value = "解答内容")
    private String answerContent;

    /**
     * 標準解答
     */
    @ApiModelProperty(value = "標準解答")
    private String standardAnswers;

    /**
     * 解答判定結果
     */
    @ApiModelProperty(value = "解答判定結果")
    private String answerResult;

}
