package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * テスト問題
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminTestQuestionsDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * テスト問題ID
	 */
	private Long testQuestionsId;
	/**
	 * 講座ID
	 */
	@NotNull(message="講座IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long courseId;
	/**
	 * 教科ID
	 */
	@NotNull(message="教科IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long subjectId;
	/**
	 * 単元ID
	 */
	@NotNull(message="単元IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long unitId;
	/**
	 * 単元テストID
	 */
	@NotNull(message="単元テストIDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long unitTestId;
	/**
	 * 問題名
	 */
	@NotBlank(message="問題名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="問題名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String questionTitle;
	/**
	 * 固定フラグ
	 */
	@NotBlank(message="固定フラグを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "固定フラグは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String fixFlag;
	/**
	 * 出題形式
	 */
	@NotBlank(message="出題形式を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[0123]$", message = "出題形式フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String questionType;
	/**
	 * イメージカタログ画像PATH
	 */
	private String imageCatalogPath;
	/**
	 * イメージカタログ画像AWS S3 BUCKET名
	 */
	private String imageCatalogS3BucketName;
	/**
	 * イメージカタログ画像AWS S3 オブジェクトキー
	 */
	private String imageCatalogS3ObjectKey;
	/**
	 * 問題文
	 */
	@NotBlank(message="問題文を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=1000,message="問題文は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String questionContent;
	/**
	 * 解説文
	 */
//	@NotBlank(message="解説文を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=1000,message="解説文は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String questionExplanation;
	/**
	 * 備考
	 */
//	@NotBlank(message="備考を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=1000,message="備考は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String questionRemarks;
	/**
	 * 問題ステータス
	 */
	@NotBlank(message="問題ステータを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "問題ステータは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String questionStatus;
	/**
	 * 公開開始日時
	 */
	private Date openStartTime;
	/**
	 * 公開終了日時
	 */
	private Date openEndTime;
	/**
	 * 選択肢数
	 */
	@NotNull(message="出題数を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//    @Pattern(regexp = "^[1-9]\\d*$", message = "出題数は正の整数ではありません", groups = {AddGroup.class, UpdateGroup.class})
	private Integer choicesNumber;
	/**
	 * 標準解答
	 */
	@NotBlank(message="標準解答を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=1000,message="標準解答は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String standardAnswers;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;

	/**
	 * 選択肢List
	 */
	private List<AdminQuestionChoicesDto> items;

}
