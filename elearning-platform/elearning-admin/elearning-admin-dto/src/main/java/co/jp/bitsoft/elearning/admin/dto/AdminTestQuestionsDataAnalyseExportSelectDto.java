package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * 単元テスト結果(出題内容)エクスポート検索
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminTestQuestionsDataAnalyseExportSelectDto implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * グループID
	 */
	private String groupId;

	/**
	 * グループ名
	 */
	private String groupName;

	/**
	 * 講座ID
	 */
	private String courseId;

	/**
	 * 講座名
	 */
	private String courseName;

	/**
	 * 単元ID
	 */
	private String unitId;

	/**
	 * 単元名
	 */
	private String unitName;

	/**
	 * 単元テストID
	 */
	private String unitTestId;

	/**
	 * 単元テスト名
	 */
	private String testTitle;

	/**
	 * 平均正解率
	 */
	private String avgCorrectPercent;

	/**
	 * 初回正解率
	 */
	private String firstAvgCorrectPercent;

	/**
	 * 問題内容
	 */
	private String questionContent;

	/**
	 * 固定フラグ
	 */
	private String fixFlag;

	/**
	 * 出題形式
	 */
	private String questionType;

	/**
	 * 単元テスト問題人数
	 */
	private int studentTestQuestionCount;

	/**
	 * 単元テスト問題Id
	 */
	private int testQuestionsId;
}
