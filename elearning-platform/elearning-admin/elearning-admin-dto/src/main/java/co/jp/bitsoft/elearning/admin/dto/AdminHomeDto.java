package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * ホーム
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminHomeDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ログインユーザーグループ種別
	 */
	private String loginGroupType;
	/**
	 * ログインユーザーグループ名
	 */
	private String loginGroupName;
	/**
	 * ホームデータ
	 */
	private List<Integer> seriesData;

}
