package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 運用者グループ
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminUserGroupDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * グループID
	 */
	private Long groupId;
	/**
	 * グループ名
	 */
	@NotBlank(message="グループ名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=100,message="グループ名は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String groupName;
	/**
	 * グループ種別
	 */
	@NotBlank(message="テスト方式を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[123]$", message = "テスト方式は1または2または3でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String groupType;
	/**
	 * 所属グループId
	 */
	@NotNull(message="所属グループIdを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long groupIdOf;
	/**
	 * グループ説明
	 */
//	@NotBlank(message="グループ説明を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=200,message="グループ説明は200ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String groupDetail;
	/**
	 * 郵便番号
	 */
//	@NotBlank(message="郵便番号を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Pattern(regexp = "^$|^[0-9]{3}[0-9]{4}$", message = "郵便番号フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String groupZipCode;
	/**
	 * 都道府県
	 */
//	@NotBlank(message="都道府県を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="都道府県は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String groupPrefecture;
	/**
	 * 市区町村
	 */
//	@NotBlank(message="市区町村を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="市区町村は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String groupAddressCity;
	/**
	 * その他住所
	 */
//	@NotBlank(message="その他住所を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="その他住所は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String groupAddressOthers;
	/**
	 * 連絡先電話１
	 */
//	@NotBlank(message="連絡先電話１を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Pattern(regexp = "^$|^0[0-9]{9}$|0[789]0-?[0-9]{4}-?[0-9]{4}$", message = "連絡先電話１フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contactTel1;
	/**
	 * 連絡先FAX１
	 */
//	@NotBlank(message="連絡先FAX１を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Pattern(regexp = "^$|^0[0-9]{9}$", message = "連絡先FAX１フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contactFax1;
	/**
	 * 連絡先電話２
	 */
//	@NotBlank(message="連絡先電話２を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Pattern(regexp = "^$|^0[0-9]{9}$|0[789]0-?[0-9]{4}-?[0-9]{4}$", message = "連絡先電話２フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contactTel2;
	/**
	 * 連絡先FAX２
	 */
//	@NotBlank(message="連絡先FAX２を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Pattern(regexp = "^$|^0[0-9]{9}$", message = "連絡先FAX２フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contactFax2;
	/**
	 * 連絡先メール
	 */
	@NotBlank(message="メールを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$", message = "メールアドレスフォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contactEmail;
	/**
	 * 責任者氏名
	 */
//	@NotBlank(message="責任者氏名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="責任者氏名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String managerName;
	/**
	 * 責任者氏名（カナ）
	 */
//	@NotBlank(message="責任者氏名（カナ）を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="責任者氏名（カナ）は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^$|^[\\u30A0-\\u30FF]+$", message = "責任者氏名（カナ）フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String managerNameKana;
	/**
	 * 責任者部署
	 */
//	@NotBlank(message="責任者部署を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="責任者部署は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String managerDepartName;
	/**
	 * 責任者職位
	 */
//	@NotBlank(message="責任者職位を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="責任者職位は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String managerPosition;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;
	/**
	 * グループ講座
	 */
	private List<Long> courseIds;
	/**
	 * 所属グループ名
	 */
	private String groupNameOf;

	/**
	 * 下位ユーザー数制限
	 */
	@NotNull(message="下位ユーザー数制限を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long userMaxNum;

    /**
     * グループ終了証テンプレートID
     */
    private Long groupCertificateTemplateId;

	/**
	 * タイトル
	 */
	private String certificateTitle;

	/**
	 * 本文
	 */
	private String certificateContent;

	/**
	 * 発行機関名
	 */
	private String issuerName;

	/**
	 * 発行機関サブ名
	 */
	private String issuerNameSub;

	/**
	 * 発行機関責任者名
	 */
	private String issuerResponsiblePartyName;

	/**
	 * 発行機関責任者肩書
	 */
	private String issuerResponsiblePartyTitle;

	/**
	 * Reserve１
	 */
	private String reserve1;

	/**
	 * Reserve2
	 */
	private String reserve2;

	/**
	 * Reserve3
	 */
	private String reserve3;

	/**
	 * Reserve4
	 */
	private String reserve4;

	/**
	 * Reserve5
	 */
	private String reserve5;

	/**
	 * Reserve6
	 */
	private String reserve6;

	/**
	 * certificateTemplatePath
	 */
	private String certificateTemplatePath;

	/**
	 * 有効期間開始日
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startTime;

	/**
	 * 有効期間終了日
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endTime;
}
