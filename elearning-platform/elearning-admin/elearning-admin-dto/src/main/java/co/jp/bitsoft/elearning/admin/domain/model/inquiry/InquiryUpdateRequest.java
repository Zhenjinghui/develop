package co.jp.bitsoft.elearning.admin.domain.model.inquiry;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

@Data
public class InquiryUpdateRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 問い合わせID
     */
    @TableId
    private Long inquiryId;
    /**
     * 問い合わせ種類
     */
    private String inquiryType;
    /**
     * 講座ID
     */
    private Long courseId;
    /**
     * 問い合わせ件名
     */
    private String inquiryName;
    /**
     * FROMユーザーID
     */
    private Long fromUserId;
    /**
     * FROMユーザー種別
     */
    private String fromUserType;

}

