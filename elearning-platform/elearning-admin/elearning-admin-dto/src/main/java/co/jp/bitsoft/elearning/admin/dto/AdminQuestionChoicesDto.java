package co.jp.bitsoft.elearning.admin.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

@Data
public class AdminQuestionChoicesDto {


	/**
	 * 選択肢ID
	 */
	private Long testQuestionsChoicesId;
	/**
	 * 選択肢No
	 */
	@NotNull(message="選択肢Noを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[1-9]\\d*$", message = "選択肢Noは正の整数ではありません", groups = {AddGroup.class, UpdateGroup.class})
	private String prefix;
	/**
	 * 選択肢文
	 */
	@NotBlank(message="選択肢文を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=1000,message="選択肢文は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String content;

}




