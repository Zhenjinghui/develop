package co.jp.bitsoft.elearning.admin.dto;

import lombok.Data;

@Data
public class StudentTestQuestionAnswersDto {

    /**
     * 講座ID
     */
    private Long courseId;

    /**
     * 講座名
     */
    private String courseTitle;

    /**
     * 教科ID
     */
    private Long subjectId;

    /**
     * 教科名
     */
    private String subjectTitle;

    /**
     * 単元ID
     */
    private Long unitId;

    /**
     * 単元名
     */
    private String unitName;

    /**
     * 単元テストID
     */
    private Long unitTestId;

    /**
     * 単元テスト名
     */
    private String testTitle;

    /**
     * テスト正解率
     */
    private String answerCorrectRate;

    /**
     * テスト判定結果
     */
    private String testAnswerResult;

    /**
     * 問題名
     */
    private String questionTitle;

    /**
     * 出題形式
     */
    private String questionType;

    /**
     * 問題文
     */
    private String questionContent;

    /**
     * 解説文
     */
    private String questionExplanation;

    /**
     * 解答内容
     */
    private String answerContent;

    /**
     * 標準解答
     */
    private String standardAnswers;

    /**
     * 受講者テスト問題解答ID
     */
    private Long questionAnswerId;

    /**
     * 解答判定結果
     */
    private String questionAnswerResult;
}
