package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 単元テスト
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminUnitTestDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 単元テストID
	 */
	private Long unitTestId;
	/**
	 * 講座ID
	 */
	@NotNull(message="講座IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long courseId;
	/**
	 * 教科ID
	 */
	@NotNull(message="教科IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long subjectId;
	/**
	 * 単元ID
	 */
	@NotNull(message="単元IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long unitId;
	/**
	 * テスト名
	 */
	@NotBlank(message="テスト名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="テスト名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String testTitle;
	/**
	 * テスト概要
	 */
//	@NotBlank(message="テスト概要を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=500,message="テスト概要は500ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String testOutline;
	/**
	 * テスト説明
	 */
//	@NotBlank(message="テスト説明を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=1000,message="テスト説明は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String testDetail;
	/**
	 * テストステータス
	 */
	@NotBlank(message="テストステータスを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "テストステータスは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String testStatus;
	/**
	 * 公開開始日時
	 */
	private Date openOpenTime;
	/**
	 * 公開終了日時
	 */
	private Date openEndTime;
	/**
	 * テスト時間制限有無
	 */
	private String testConditionFlag;
	/**
	 * テスト時間制限（分）
	 */
	private Integer testConditionTime;
	/**
	 * テスト方式
	 */
	@NotBlank(message="テスト方式を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "テスト方式は0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String testType;
	/**
	 * 合格設定有無
	 */
	private String passFlag;
	/**
	 * 出題数
	 */
	@NotNull(message="出題数を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^([1-9]\\d*|[0]{1,1})$", message = "出題数は正の整数ではありません", groups = {AddGroup.class, UpdateGroup.class})
	private Long testQuestionsNum;
	/**
	 * 合格題数
	 */
	@NotNull(message="合格題数を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^([1-9]\\d*|[0]{1,1})$", message = "合格題数は正の整数ではありません", groups = {AddGroup.class, UpdateGroup.class})
	private Long passQuestionsNum;
	/**
	 * 合格率
	 */
	private Integer passQuestionsRate;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;
	/**
	 * 講座名
	 */
	private String courseTitle ;
	/**
	 * 教科名
	 */
	private String subjectTitle ;
	/**
	 * 単元名
	 */
	private String unitName ;

}
