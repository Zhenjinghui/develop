package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * 受講者傾向分析項目用
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminStudentDataAnalyseUseDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 名前
	 */
	private String name;
	/**
	 * 値
	 */
	private int value;

}
