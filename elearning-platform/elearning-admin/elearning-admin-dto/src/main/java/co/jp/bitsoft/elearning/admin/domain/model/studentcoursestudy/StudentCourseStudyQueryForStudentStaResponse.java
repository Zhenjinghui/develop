package co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy;

import lombok.Data;

import java.util.Date;

@Data
public class StudentCourseStudyQueryForStudentStaResponse {

    private long subjectId;

    private String subjectTitle;

    private long unitId;

    private String unitName;

    private long teachingMaterialId;

    private String teachingMaterialTitle;

    private String studyStatus;

    private Date studyStartTime;

    private Date studyEndTime;

    private Date updateTime;

}
