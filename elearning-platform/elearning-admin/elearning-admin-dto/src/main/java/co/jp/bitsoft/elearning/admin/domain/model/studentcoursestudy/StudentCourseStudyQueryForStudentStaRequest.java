package co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy;

import lombok.Data;

@Data
public class StudentCourseStudyQueryForStudentStaRequest {
    private String t;

    private Long studentCourseId;

    private String page;

    private String limit;

}
