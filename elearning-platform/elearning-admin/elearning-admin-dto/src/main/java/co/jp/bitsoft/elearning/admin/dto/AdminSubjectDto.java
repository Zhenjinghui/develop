package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 教科
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminSubjectDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 教科ID
	 */
	private Long subjectId;
	/**
	 * 表示順
	 */
	private Long dispNo;
	/**
	 * 講座ID
	 */
	@NotNull(message="講座IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long courseId;
	/**
	 * 教科名
	 */
	@NotBlank(message="教科名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="教科名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String subjectTitle;
	/**
	 * 教科画像PATH
	 */
//	@NotBlank(message="教科画像PATHを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="教科画像PATHは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String subjectImagePath;
	/**
	 * 教科AWS S3 BUCKET名
	 */
//	@NotBlank(message="教科AWS S3 BUCKET名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="教科画像PATHは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String subjectS3BucketName;
	/**
	 * 教科AWS S3 オブジェクトキー
	 */
//	@NotBlank(message="科AWS S3 オブジェクトキーを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="教科画像PATHは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String subjectS3ObjectKey;
	/**
	 * 教科概要
	 */
//	@NotBlank(message="教科概要を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=500,message="教科概要は500ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String subjectOutline;
	/**
	 * 教科説明
	 */
//	@NotBlank(message="教科説明を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=1000,message="教科説明は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String subjectDetail;
	/**
	 * 教科ステータス
	 */
	@NotBlank(message="教科ステータスを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "教科ステータスは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String subjectStatus;
	/**
	 * 公開開始日時
	 */
	private Date openStartTime;
	/**
	 * 公開終了日時
	 */
	private Date openEndTime;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;

}
