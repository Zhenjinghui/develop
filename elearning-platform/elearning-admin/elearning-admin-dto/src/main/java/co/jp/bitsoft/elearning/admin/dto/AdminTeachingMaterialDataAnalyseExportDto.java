package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 受講状況（講座・教材単位）エクスポート
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminTeachingMaterialDataAnalyseExportDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * グループ名
	 */
	private String groupName;

	/**
	 * ユーザーID
	 */
	private String studentId;

	/**
	 * 講座名
	 */
	private String courseName;

	/**
	 * 教科名
	 */
	private String subjectName;

	/**
	 * 単元名
	 */
	private String unitName;

	/**
	 * 教材名
	 */
	private String teachingMaterialName;

	/**
	 * 教材受講状態
	 */
	private String studyStatus;

	/**
	 * 最終受講日時
	 */
	private Date studyEndTime;


}
