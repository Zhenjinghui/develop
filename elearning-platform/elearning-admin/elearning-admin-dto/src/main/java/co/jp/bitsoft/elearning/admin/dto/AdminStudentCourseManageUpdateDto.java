package co.jp.bitsoft.elearning.admin.dto;

import java.util.List;

import lombok.Data;
@Data
public class AdminStudentCourseManageUpdateDto {

	/**
	 * ID
	 */
	private List<Long> studentCourseIds;
	/**
	 * 受講状態
	 */
	private String studyStatus;


}
