package co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage;

import lombok.Data;

@Data
public class StudentCourseManageQueryRequest {
    private String t;

    private Long courseId;

    private String courseName;

    private String page;

    private String limit;
}
