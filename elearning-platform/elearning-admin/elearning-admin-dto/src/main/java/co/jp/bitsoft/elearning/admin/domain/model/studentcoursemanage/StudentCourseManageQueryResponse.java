package co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage;

import lombok.Data;

@Data
public class StudentCourseManageQueryResponse {

    private Long studentCourseId;

    /**
     * 講座ID
     */
    private Long courseId;

    /**
     * 講座名
     */
    private String courseTitle;

    /**
     * 受講状態
     */
    private String studyStatus;

    /**
     * 受講者数
     */
    private int studyCount;

    /**
     * 受講者総数
     */
    private int studyCountAll;

    /**
     * 受講者総数の割合
     */
    private float studyCountPercent;

    /**
     * グループID
     */
    private Long groupId;

    /**
     * グループ名
     */
    private String groupName;
}
