package co.jp.bitsoft.elearning.admin.domain.model.inquirymessage;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class InquiryMessageUpdateRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * メッセージID
     */
    @TableId
    private Long inquiryMessageId;
    /**
     * 問い合わせID
     */
    private Long inquiryId;
    /**
     * FROMユーザーID
     */
    private Long fromUserId;
    /**
     * FROMユーザー種別
     */
    private String fromUserType;
    /**
     * TOユーザーID
     */
    private Long toUserId;
    /**
     * TOユーザー種別
     */
    private String toUserType;
    /**
     * メッセージステータス
     */
    private String messageStatus;
    /**
     * メッセージ日時
     */
    private Date messageTime;
    /**
     * メッセージ内容
     */
    private String messageContent;
    /**
     * 開封日時
     */
    private Date messageOpenTime;

}
