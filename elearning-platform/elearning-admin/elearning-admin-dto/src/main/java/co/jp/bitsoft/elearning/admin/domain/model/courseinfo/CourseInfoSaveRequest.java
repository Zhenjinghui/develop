package co.jp.bitsoft.elearning.admin.domain.model.courseinfo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CourseInfoSaveRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * インフォメーションID
     */
    @TableId
    private Long infoId;
    /**
     * 講座ID
     */
    private Long courseId;
    /**
     * インフォメーション種別
     */
    private String infoType;
    /**
     * インフォメーション件名
     */
    private String infoName;
    /**
     * インフォメーション概要
     */
    private String infoOverview;
    /**
     * インフォメーション詳細
     */
    private String infoDetail;
    /**
     * インフォメーションステータス
     */
    private String infoStatus;
    /**
     * 公開開始日時
     */
    private Date openStartTime;
    /**
     * 公開終了日時
     */
    private Date openEndTime;
    /**
     * メール送信有無
     */
    private String sendMailFlag;
    /**
     * サイト内プッシュ通知有無
     */
    private String sendMessageFlag;

}
