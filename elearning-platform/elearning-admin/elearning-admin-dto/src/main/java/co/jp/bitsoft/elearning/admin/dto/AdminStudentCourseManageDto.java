package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 受講者コース管理
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminStudentCourseManageDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者コースID
	 */
	private Long studentCourseId;
	/**
	 * 受講者ID
	 */
	private Long studentId;
	/**
	 * 講座ID
	 */
	private Long courseId;
	/**
	 * グループID
	 */
	private Long groupId;
	/**
	 * 受講状態
	 */
	private String studyStatus;
	/**
	 * 受講申請日時
	 */
	private Date courseRegTime;
	/**
	 * 受講申請理由
	 */
	private String courseRegReason;
	/**
	 * 受講申請審査者
	 */
	private Long sysUserId;
	/**
	 * 受講申請承認日時
	 */
	private Date regApprovalTime;
	/**
	 * 受講申請承認連絡事項
	 */
	private String regApprovalContact;
	/**
	 * 受講申請拒否日時
	 */
	private Date regRejectTime;
	/**
	 * 受講申請拒否理由
	 */
	private String regRejectReason;
	/**
	 * 受講受験開始日時
	 */
	private Date studyTestStartTime;
	/**
	 * 受講受験開始終了日時
	 */
	private Date studyTestEndTime;
	/**
	 * 受講キャンセル申請日時
	 */
	private Date cancelRegTime;
	/**
	 * 受講キャンセル申請理由
	 */
	private String cancelRegReason;
	/**
	 * 受講キャンセル申請審査者
	 */
	private Long cancelSysUserId;
	/**
	 * 受講キャンセル申請承認日時
	 */
	private Date cancelApprovalTime;
	/**
	 * 受講キャンセル申請承認連絡事項
	 */
	private String cancelApprovalContact;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;

	/**
	 * 講座名
	 */
	private String courseTitle;
	/**
	 * ユーザー名
	 */
	private String userName;
	/**
	 * グループ名
	 */
	private String groupName;

	/**
	 * 受講申請審査者名
	 */
	private String sysUserName;

	/**
	 * 受講キャンセル申請審査者名
	 */
	private String cancelSysUserName;

	/**
	 * 公開開始日時
	 */
	private String openStartTime;
	/**
	 * 公開終了日時
	 */
	private String openEndTime;
	/**
	 * 講座グループID
	 */
	private Long courseGroupId;
	/**
	 * email
	 */
	private String email;

}
