package co.jp.bitsoft.elearning.admin.dto;

import java.util.Date;

import lombok.Data;
@Data
public class AdminStudyLoginHistoryDto {
	/**
	 * ID
	 */
	private Long id;
	/**
	 * 受講者ID
	 */
	private Long studentId;
	/**
	 * ユーザーID
	 */
	private String userName;
	/**
	 * アクション区分
	 */
	private String loginOutKubun;
	/**
	 * アクション履歴日時
	 */
	private Date loginOutHistoryTime;

}
