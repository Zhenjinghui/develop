package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminCourseDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 講座ID
	 */
	private Long courseId;
	/**
	 * 講座名
	 */
	@NotBlank(message="講座名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="講座名は200ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String courseTitle;
	/**
	 * 所属グループId
	 */
//	@NotNull(message="所属グループIDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long groupId;
	/**
	 * 講座画像PATH
	 */
//	@NotBlank(message="講座画像PATHを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="講座画像PATHは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String courseImagePath;
	/**
	 * AWS S3 BUCKET名
	 */
//	@NotBlank(message="AWS S3 BUCKET名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="AWS S3 BUCKET名は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String courseS3BucketName;
	/**
	 * AWS S3 オブジェクトキー
	 */
//	@NotBlank(message="AWS S3 オブジェクトキーを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="AWS S3 オブジェクトキーは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String courseS3ObjectKey;
	/**
	 * 講座類別
	 */
	@NotBlank(message="講座類別を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^0[0-9]$", message = "講座類別フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String courseType;
	/**
	 * 講座タグ
	 */
//	@NotBlank(message="講座タグを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//    @Pattern(regexp = "^0[0-9]$", message = "講座タグフォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String courseTag;
	/**
	 * 講座概要
	 */
//	@NotBlank(message="講座概要を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=500,message="講座概要は500ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String courseOutline;
	/**
	 * 講座説明
	 */
//	@NotBlank(message="講座説明を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=1000,message="講座説明は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String courseDetail;
	/**
	 * 受講申請要否フラグ
	 */
	@NotBlank(message="受講申請要否フラグを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "受講申請要否フラグは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String courseRegFlag;
	/**
	 * 講座ステータス
	 */
	@NotBlank(message="講座ステータスを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "講座ステータスは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String courseStatus;
	/**
	 * 公開開始日時
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date openStartTime;
	/**
	 * 公開終了日時
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date openEndTime;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;

	/**
	 * 所属グループ名
	 */
	private String groupName;

    /**
     * 講座終了証テンプレートID
     */
    private Long courseCertificateTemplateId;

	/**
	 * タイトル
	 */
	private String certificateTitle;

	/**
	 * 本文
	 */
	private String certificateContent;

	/**
	 * 発行機関名
	 */
	private String issuerName;

	/**
	 * 発行機関サブ名
	 */
	private String issuerNameSub;

	/**
	 * 発行機関責任者名
	 */
	private String issuerResponsiblePartyName;

	/**
	 * 発行機関責任者肩書
	 */
	private String issuerResponsiblePartyTitle;

	/**
	 * Reserve１
	 */
	private String reserve1;

	/**
	 * Reserve2
	 */
	private String reserve2;

	/**
	 * Reserve3
	 */
	private String reserve3;

	/**
	 * Reserve4
	 */
	private String reserve4;

	/**
	 * Reserve5
	 */
	private String reserve5;

	/**
	 * Reserve6
	 */
	private String reserve6;

	/**
	 * certificateTemplatePath
	 */
	private String certificateTemplatePath;
}