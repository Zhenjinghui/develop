package co.jp.bitsoft.elearning.admin.dto;

import lombok.Data;

@Data
public class QueryStudentCourseManageForStudentStaRespDto {

    private Long studentCourseId;

    private Long groupIdOf;

    private String groupIdOfName;

    private Long groupId;

    private String groupName;

    private Long studentId;

    private String userName;

    private Long courseId;

    private String courseTitle;

    private String studyStatus;

    private Long unitId;

    private String unitName;

    private Long unitTestId;

    private String testTitle;



}
