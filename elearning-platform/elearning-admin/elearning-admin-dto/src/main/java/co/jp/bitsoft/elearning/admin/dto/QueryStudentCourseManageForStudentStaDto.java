package co.jp.bitsoft.elearning.admin.dto;

import lombok.Data;

@Data
public class QueryStudentCourseManageForStudentStaDto {
    private Long courseId;

    private String courseName;

    private Long groupId;

    private String groupName;

    private Long studentId;

    private String userName;

    private String studyStatus;

    private Long selfGroupId;

    private String groupType;

    private String unitName;

    private String testTitle;

}
