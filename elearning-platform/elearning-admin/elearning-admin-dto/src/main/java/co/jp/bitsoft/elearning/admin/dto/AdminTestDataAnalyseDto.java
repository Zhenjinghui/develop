package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * 単元テスト結果(出題内容)
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminTestDataAnalyseDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * グループID
	 */
	private String groupId;

	/**
	 * グループ名
	 */
	private String groupName;

	/**
	 * 講座ID
	 */
	private String courseId;

	/**
	 * 講座名
	 */
	private String courseName;

	/**
	 * 教科ID
	 */
	private String subjectId;

	/**
	 * 教科名
	 */
	private String subjectName;

	/**
	 * 単元ID
	 */
	private String unitId;

	/**
	 * 単元名
	 */
	private String unitName;

	/**
	 * 単元テストID
	 */
	private String unitTestId;

	/**
	 * 単元テスト名
	 */
	private String testTitle;

	/**
	 * 平均正解率
	 */
	private String avgCorrectPercent;

	/**
	 * 初回正解率
	 */
	private String firstAvgCorrectPercent;

	/**
	 * 最終正解率
	 */
	private String lastAvgCorrectPercent;

	/**
	 * 出題数
	 */
	private int testQuestionsNum;

	/**
	 * 合格問題数
	 */
	private int passQuestionsNum;

	/**
	 * 平均合格率
	 */
	private String avgPassPercent;

	/**
	 * 単元テスト数
	 */
	private int studentTestCount;

}
