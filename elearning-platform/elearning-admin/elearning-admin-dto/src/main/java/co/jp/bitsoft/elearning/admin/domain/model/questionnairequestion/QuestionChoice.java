package co.jp.bitsoft.elearning.admin.domain.model.questionnairequestion;

import lombok.Data;

@Data
public class QuestionChoice {


    /**
     * 選択肢ID
     */
    private Long testQuestionsChoicesId;
    /**
     * 選択肢No
     */
    private String prefix;
    /**
     * 選択肢文
     */
    private String content;
//	/**
//	 * 選択肢操作flag
//	 */
//	private String flag;

}