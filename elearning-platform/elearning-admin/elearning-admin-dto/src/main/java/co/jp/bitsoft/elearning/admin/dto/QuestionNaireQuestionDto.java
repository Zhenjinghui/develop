package co.jp.bitsoft.elearning.admin.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * アンケート問題
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class QuestionNaireQuestionDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * アンケート問題ID
	 */
	private Long questionNaireQuestionId;
	/**
	 * アンケートID
	 */
	private Long questionNaireId;
	/**
	 * 講座ID
	 */
	private Long courseId;
	/**
	 * 問題名
	 */
	private String questionName;
	/**
	 * アンケート出題形式
	 */
	private String questionType;
	/**
	 * イメージカタログ画像PATH
	 */
	private String imageCatalogPath;
	/**
	 * イメージカタログ画像AWS S3 BUCKET名
	 */
	private String imageCatalogS3BucketName;
	/**
	 * イメージカタログ画像AWS S3 オブジェクトキー
	 */
	private String imageCatalogS3ObjectKey;
	/**
	 * 問題文
	 */
	private String questionContent;
	/**
	 * 解説文
	 */
	private String questionExplanation;
	/**
	 * 備考
	 */
	private String questionRemarks;
	/**
	 * アンケート問題ステータス
	 */
	private String questionStatus;
	/**
	 * 公開開始日時
	 */
	private Date openStartTime;
	/**
	 * 公開終了日時
	 */
	private Date openEndTime;
	/**
	 * 選択肢数
	 */
	private Integer choicesNum;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	@Version
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;
	/**
	 * 選択肢List
	 */
	@TableField(exist=false)
	private List<QuestionChoicesViewDto> items;

}
