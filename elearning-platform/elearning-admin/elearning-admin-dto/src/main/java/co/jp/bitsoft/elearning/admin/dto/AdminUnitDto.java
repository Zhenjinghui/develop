package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 単元
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminUnitDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 単元ID
	 */
	private Long unitId;
	/**
	 * 表示順
	 */
	private Long dispNo;
	/**
	 * 講座ID
	 */
	@NotNull(message="講座IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long courseId;
	/**
	 * 教科ID
	 */
	@NotNull(message="教科IDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long subjectId;
	/**
	 * 単元名
	 */
	@NotBlank(message="単元名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="単元名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String unitName;
	/**
	 * 単元画像PATH
	 */
//	@NotBlank(message="単元画像PATHを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="単元画像PATHは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String unitIamgePath;
	/**
	 * 単元AWS S3 BUCKET名
	 */
//	@NotBlank(message="単元AWS S3 BUCKET名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="単元AWS S3 BUCKET名は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String unitS3BucketName;
	/**
	 * 単元AWS S3 オブジェクトキー
	 */
//	@NotBlank(message="単元AWS S3 オブジェクトキーを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Length(min=1,max=100,message="単元AWS S3 オブジェクトキーは100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String unitS3ObjectKey;
	/**
	 * 単元概要
	 */
//	@NotBlank(message="単元概要を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=500,message="単元概要は500ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String unitOutline;
	/**
	 * 単元説明
	 */
//	@NotBlank(message="単元説明を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=1000,message="単元説明は1000ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String unitDetail;
	/**
	 * 単元ステータス
	 */
	@NotBlank(message="単元ステータスを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "単元ステータスは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String unitStatus;
	/**
	 * 公開開始日時
	 */
	private Date openStartTime;
	/**
	 * 公開終了日時
	 */
	private Date openEndTime;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;

}
