package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * 受講状況（講座・教材単位）
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminTeachingMaterialDataAnalyseDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * グループID
	 */
	private String groupId;

	/**
	 * グループ名
	 */
	private String groupName;

	/**
	 * 講座ID
	 */
	private String courseId;

	/**
	 * 講座名
	 */
	private String courseName;

	/**
	 * 教科ID
	 */
	private String subjectId;

	/**
	 * 教科名
	 */
	private String subjectName;

	/**
	 * 単元ID
	 */
	private String unitId;

	/**
	 * 単元名
	 */
	private String unitName;

	/**
	 * 教材ID
	 */
	private String teachingMaterialId;

	/**
	 * 教材名
	 */
	private String teachingMaterialName;

	/**
	 * 受講完了人数
	 */
	private String completeNum;

	/**
	 * 受講完了率
	 */
	private String completePercent;

	/**
	 * 受講完了人数(Db)
	 */
	private int completeNumDb;


}
