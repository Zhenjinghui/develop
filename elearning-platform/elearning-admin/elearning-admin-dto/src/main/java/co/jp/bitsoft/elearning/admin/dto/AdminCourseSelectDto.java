package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminCourseSelectDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 講座ID
	 */
	private Long courseId;
	/**
	 * 講座名
	 */
	private String courseTitle;

}
