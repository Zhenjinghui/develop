package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;

import javax.validation.constraints.Pattern;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 受講者コースexport
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminStudentCourseManageExportDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者コースID
	 */
	private Long studentCourseId;
	/**
	 * 受講者ID
	 */
	private Long studentId;
	/**
	 * ユーザーID
	 */
	private String userName;
	/**
	 * 受講者グループID
	 */
	private Long groupId;
	/**
	 * クラス名
	 */
	private String groupName;
	/**
	 * 講座ID
	 */
	private Long courseId;
	/**
	 * 講座名
	 */
	private String courseTitle;
	/**
	 * 受講状態
	 */
    @Pattern(regexp = "^[01234]$", message = "受講状態フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String studyStatus;
	/**
	 * 公開開始日時
	 */
	private String openStartTime;
	/**
	 * 公開終了日時
	 */
	private String openEndTime;

}
