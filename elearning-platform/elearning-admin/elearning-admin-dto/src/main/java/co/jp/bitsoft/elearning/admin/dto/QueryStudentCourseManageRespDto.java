package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * 受講者コース管理
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class QueryStudentCourseManageRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 講座ID
     */
    private Long studentCourseId;

    /**
     * 講座ID
     */
    private Long courseId;

    /**
     * 受講者ID
     */
    private Long studentId;

    /**
     * 講座名
     */
    private String courseTitle;

    /**
     * 受講状態
     */
    private String studyStatus;

    /**
     * グループID
     */
    private Long groupId;

    /**
     * グループ名
     */
    private String groupName;

    /**
     * グループ下位ユーザー数
     */
    private int userMaxNum;

}

