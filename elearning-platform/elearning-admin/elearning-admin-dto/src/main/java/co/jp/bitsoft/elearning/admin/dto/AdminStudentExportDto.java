package co.jp.bitsoft.elearning.admin.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 受講者
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminStudentExportDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者ID
	 */
	private Long studentId;

	/**
	 * ユーザーID
	 */
	@NotBlank(message="ユーザーIDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=6,max=50,message="ユーザーIDは6～50文字が必要です。", groups = {AddGroup.class, UpdateGroup.class})
	@Pattern(regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]*$", message = "ユーザーIDは半角英数字です,数字とアルファベットをそれぞれ1字以上含めてください", groups = {AddGroup.class, UpdateGroup.class})
	private String userName;

	/**
	 * ユーザー名（漢字）
	 */
	@NotBlank(message="ユーザー名（漢字）を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="ユーザー名（漢字）は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String userNameKanji;

	/**
	 * ユーザー名（カナ）
	 */
//	@NotBlank(message="ユーザー名（カナ）を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="ユーザー名（カナ）は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
//    @Pattern(regexp = "^$|^[\\u30A0-\\u30FF]+$", message = "ユーザー名（カナ）フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String userNameKana;

	/**
	 * ニックネーム
	 */
//	@NotBlank(message="ニックネームを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="ニックネームは50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String nickName;

	/**
	 * グループ所属グループ名
	 */
	private Long groupIdOf;

	/**
	 * グループ所属グループ名
	 */
	private String groupNameOf;

	/**
	 * 所属グループID
	 */
	private Long groupId;

	/**
	 * グループ名
	 */
	private String groupName;

	/**
	 * 性別
	 */
	@NotBlank(message="性別を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "性別は0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String sex;

	/**
	 * 生年月日
	 */
	private String birthday;

	/**
	 * 職業
	 */
	private String profession;

	/**
	 * 住所・郵便番号
	 */
//	@NotBlank(message="住所・郵便番号を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^#[0-9]{3}[0-9]{4}$", message = "住所・郵便番号フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String addressZipCode;

	/**
	 * 住所・都道府県
	 */
//	@NotBlank(message="住所・都道府県を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="住所・都道府県は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String addressPrefecture;

	/**
	 * 住所・市区町村
	 */
//	@NotBlank(message="住所・市区町村を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="住所・市区町村は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String addressCity;

	/**
	 * 住所・番地以下
	 */
//	@NotBlank(message="住所・番地以下を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="住所・番地以下は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String addressOthers;

	/**
	 * 住所・建物名
	 */
//	@NotBlank(message="住所・建物名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=50,message="住所・建物名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String addressBuildingName;

	/**
	 * メール
	 */
	@NotBlank(message="メールを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$", message = "メールアドレスフォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String email;

	/**
	 * 連絡・携帯
	 */
//	@NotBlank(message="連絡・携帯を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^#(070|080|090)\\d{8}$", message = "連絡・携帯フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contactMobile;

	/**
	 * 連絡・電話
	 */
//	@NotBlank(message="連絡・電話を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^#0[0-9]{9}$", message = "連絡・電話フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contactTel;

	/**
	 * 連絡・FAX
	 */
//	@NotBlank(message="連絡・FAXを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^#0[0-9]{9}$", message = "連絡・FAXフォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String contactFax;

	/**
	 * 状態
	 */
	@NotBlank(message="状態を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[01]$", message = "状態は0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private String accountStatus;

}
