package co.jp.bitsoft.elearning.admin.domain.model.courseinfo;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class CourseInfoSelectRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * インフォメーションID
     */
    private Long infoId;
    /**
     * 講座ID
     */
    private Long courseId;
	/**
	 * 講座名
	 */
    private String courseName;
    /**
     * インフォメーション種別
     */
    private String infoType;
    /**
     * インフォメーション件名
     */
    private String infoName;
    /**
     * インフォメーション概要
     */
    private String infoOverview;
    /**
     * インフォメーション詳細
     */
    private String infoDetail;
    /**
     * インフォメーションステータス
     */
    private String infoStatus;
    /**
     * 公開開始日時
     */
    private Date openStartTime;
    /**
     * 公開終了日時
     */
    private Date openEndTime;
    /**
     * メール送信有無
     */
    private String sendMailFlag;
    /**
     * サイト内プッシュ通知有無
     */
    private String sendMessageFlag;

}
