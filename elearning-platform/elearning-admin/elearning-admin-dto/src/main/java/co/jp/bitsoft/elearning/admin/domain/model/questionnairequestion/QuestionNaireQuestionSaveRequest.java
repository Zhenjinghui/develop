package co.jp.bitsoft.elearning.admin.domain.model.questionnairequestion;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class QuestionNaireQuestionSaveRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * アンケート問題ID
     */
    @TableId
    private Long questionNaireQuestionId;
    /**
     * アンケートID
     */
    private Long questionNaireId;
    /**
     * 講座ID
     */
    private Long courseId;
    /**
     * 問題名
     */
    private String questionName;
    /**
     * アンケート出題形式
     */
    private String questionType;
    /**
     * イメージカタログ画像PATH
     */
    private String imageCatalogPath;
    /**
     * イメージカタログ画像AWS S3 BUCKET名
     */
    private String imageCatalogS3BucketName;
    /**
     * イメージカタログ画像AWS S3 オブジェクトキー
     */
    private String imageCatalogS3ObjectKey;
    /**
     * 問題文
     */
    private String questionContent;
    /**
     * 解説文
     */
    private String questionExplanation;
    /**
     * 備考
     */
    private String questionRemarks;
    /**
     * アンケート問題ステータス
     */
    private String questionStatus;
    /**
     * 公開開始日時
     */
    private Date openStartTime;
    /**
     * 公開終了日時
     */
    private Date openEndTime;
    /**
     * 選択肢数
     */
    private Integer choicesNum;
    /**
     * 選択肢List
     */
    private List<QuestionChoice> items;

}

