package co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage;

import lombok.Data;

@Data
public class StudentCourseManageQueryForStudentStaRequest {

    private String t;

    private Long courseId;

    private String courseName;

    private Long groupId;

    private String groupName;

    private String groupType;

    private Long studentId;

    private String userName;

    private String studyStatus;

    private String page;

    private String limit;

    private String unitName;

    private String testTitle;

}
