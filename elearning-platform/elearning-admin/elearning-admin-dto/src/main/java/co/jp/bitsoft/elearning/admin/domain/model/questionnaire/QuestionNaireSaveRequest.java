package co.jp.bitsoft.elearning.admin.domain.model.questionnaire;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class QuestionNaireSaveRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * アンケートID
     */
    private Long questionNaireId;
    /**
     * 講座ID
     */
    private Long courseId;
    /**
     * アンケート種別
     */
    private String questionNaireType;
    /**
     * アンケート件名
     */
    private String questionNaireName;
    /**
     * アンケート概要
     */
    private String questionNaireOverview;
    /**
     * アンケート詳細説明
     */
    private String questionNaireDetail;
    /**
     * アンケートステータス
     */
    private String questionNaireStatus;
    /**
     * アンケート公開日時
     */
    private Date questionNaireOpenTime;
    /**
     * アンケート非公開日時
     */
    private Date questionNaireEndTime;
    /**
     * メール送信有無
     */
    private String sendMailFlag;
    /**
     * サイト内プッシュ通知有無
     */
    private String sendMessageFlag;

}
