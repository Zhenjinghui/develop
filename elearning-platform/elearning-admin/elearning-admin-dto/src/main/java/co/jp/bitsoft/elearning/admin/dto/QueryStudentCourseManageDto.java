package co.jp.bitsoft.elearning.admin.dto;

import lombok.Data;

@Data
public class QueryStudentCourseManageDto {

    private Long courseId;

    private String courseName;

    private Long groupId;

    private String groupType;
}
