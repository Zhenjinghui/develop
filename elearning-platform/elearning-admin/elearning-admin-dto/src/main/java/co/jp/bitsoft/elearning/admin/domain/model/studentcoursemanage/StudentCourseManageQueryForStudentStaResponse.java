package co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage;

import lombok.Data;

@Data
public class StudentCourseManageQueryForStudentStaResponse {
    private Long studentCourseId;

    private Long groupIdOf;

    private String groupIdOfName;

    private Long groupId;

    private String groupName;

    private Long studentId;

    private String userName;

    private Long courseId;

    private String courseTitle;

    private String studyStatus;
}
