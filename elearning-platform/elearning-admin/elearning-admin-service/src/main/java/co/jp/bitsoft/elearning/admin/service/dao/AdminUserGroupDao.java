package co.jp.bitsoft.elearning.admin.service.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.AdminUserGroupDto;

/**
 * 受講者
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminUserGroupDao extends BaseMapper<AdminUserGroupDto> {

	/**
	 * 查询用户组(分页)
	 */
	IPage<AdminUserGroupDto> queryUserGroup(Page<AdminUserGroupDto> page,Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询用户组(无分页)
	 */
	List<AdminUserGroupDto> queryUserGroup(Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

}
