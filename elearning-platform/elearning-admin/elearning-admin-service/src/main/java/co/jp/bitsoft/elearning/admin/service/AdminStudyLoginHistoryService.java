package co.jp.bitsoft.elearning.admin.service;

import java.util.Map;

import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * 受講者ユーザーログイン履歴
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminStudyLoginHistoryService {

    PageUtils selectStudyLoginHistory(Map<String, Object> params);

    Integer countStudyLoginHistory();

}

