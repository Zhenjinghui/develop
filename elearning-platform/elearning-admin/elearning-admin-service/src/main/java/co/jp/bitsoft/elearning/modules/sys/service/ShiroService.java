/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.service;

import java.util.Set;

import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserTokenEntity;

/**
 * shiro相关接口
 *
 * @author  BitSoft
 */
public interface ShiroService {
    /**
     * 获取用户权限列表
     */
    Set<String> getUserPermissions(long userId);

    SysUserTokenEntity queryByToken(String token);

    /**
     * 根据用户ID，查询用户
     * @param userId
     */
    SysUserEntity queryUser(Long userId);
}
