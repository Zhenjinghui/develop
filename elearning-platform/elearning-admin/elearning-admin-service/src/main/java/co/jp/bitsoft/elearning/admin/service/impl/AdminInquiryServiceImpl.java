package co.jp.bitsoft.elearning.admin.service.impl;

import co.jp.bitsoft.elearning.admin.service.AdminInquiryService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.InquiryEntity;
import co.jp.bitsoft.elearning.core.service.dao.InquiryDao;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("adminInquiryService")
public class AdminInquiryServiceImpl extends ServiceImpl<InquiryDao, InquiryEntity> implements AdminInquiryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String inquiryName = (String) params.get("inquiryName");
        IPage<InquiryEntity> page = this.page(
                new Query<InquiryEntity>().getPage(params),
                new QueryWrapper<InquiryEntity>()
                        .like(StringUtils.isNotBlank(inquiryName),"inquiry_name", inquiryName)
        );

        return new PageUtils(page);
    }

}
