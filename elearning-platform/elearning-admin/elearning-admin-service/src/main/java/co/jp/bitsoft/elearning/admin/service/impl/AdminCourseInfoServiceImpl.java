package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.admin.domain.model.courseinfo.CourseInfoSelectRequest;
import co.jp.bitsoft.elearning.admin.service.AdminCourseInfoService;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.CourseInfoEntity;
import co.jp.bitsoft.elearning.core.entity.CourseOpenInfoEntity;
import co.jp.bitsoft.elearning.core.service.CourseOpenInfoService;
import co.jp.bitsoft.elearning.core.service.CourseService;
import co.jp.bitsoft.elearning.core.service.UserGroupService;
import co.jp.bitsoft.elearning.core.service.dao.CourseInfoDao;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;

@Service("adminCourseInfoService")
public class AdminCourseInfoServiceImpl extends ServiceImpl<CourseInfoDao, CourseInfoEntity> implements AdminCourseInfoService {

	@Autowired
	private UserGroupService userGroupService;

	@Autowired
    private CourseOpenInfoService courseOpenInfoService;

	@Autowired
	private CourseService courseService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

    	SysUserEntity userEntity = CommonServiceImpl.getUser();
		String groupType = userGroupService.getById(userEntity.getGroupId()).getGroupType();
		List<Long> courseIds = new ArrayList<Long>();
		if("3".equals(groupType)) {
			QueryWrapper<CourseOpenInfoEntity> courseOpenInfoWrapper = new QueryWrapper<CourseOpenInfoEntity>();
			courseOpenInfoWrapper.eq("group_id", userEntity.getGroupId());
			List<CourseOpenInfoEntity> courseOpenInfoList= courseOpenInfoService.list(courseOpenInfoWrapper);
			courseIds = courseOpenInfoList.stream().map(CourseOpenInfoEntity::getCourseId).collect(Collectors.toList());
			if(courseIds == null || courseIds.size()==0) {
				IPage<CourseInfoEntity> pageNull = new Page<>();
				return new PageUtils(pageNull);
			}
		}

        long curPage = 1;
        long limit = 10;
        if(params.get(Constant.PAGE) != null){
            curPage = Long.parseLong((String)params.get(Constant.PAGE));
        }
        if(params.get(Constant.LIMIT) != null){
            limit = Long.parseLong((String)params.get(Constant.LIMIT));
        }

        String infoName = (String)params.get("infoName");
        String infoType = (String)params.get("infoType");
        IPage<CourseInfoEntity> page = this.page(
                new Query<CourseInfoEntity>().getPage(params),
                new QueryWrapper<CourseInfoEntity>().eq("del_flag", "0")
                        .like(StringUtils.isNotBlank(infoName), "info_name", infoName)
                        .eq(StringUtils.isNotBlank((infoType)), "info_type", infoType)
                        .in("3".equals(groupType), "course_id", courseIds)
        );

        List<CourseInfoSelectRequest> resultList = new ArrayList<CourseInfoSelectRequest>();
        for(CourseInfoEntity cie : page.getRecords()) {
        	CourseInfoSelectRequest courseInfoSelectRequest = new CourseInfoSelectRequest();
        	BeanUtils.copyProperties(cie, courseInfoSelectRequest);
        	courseInfoSelectRequest.setCourseName(courseService.getById(cie.getCourseId()).getCourseTitle());
        	resultList.add(courseInfoSelectRequest);
        }

		List<CourseInfoSelectRequest> resultRecords = new ArrayList<CourseInfoSelectRequest>();

		int resultListSize =  resultList.size();

		if(resultListSize - (int)(limit*(curPage-1)) < (int)limit) {
			resultRecords = resultList.subList((int)(limit*(curPage-1)), resultList.size());
		}else {
			resultRecords = resultList.subList((int)(limit*(curPage-1)), (int)(limit*curPage));
		}

        IPage<CourseInfoSelectRequest> resultPage = new Page<CourseInfoSelectRequest>(curPage,limit);
		resultPage.setRecords(resultRecords);
		resultPage.setCurrent(curPage);
		resultPage.setTotal(resultList.size());

        return new PageUtils(resultPage);
    }

}
