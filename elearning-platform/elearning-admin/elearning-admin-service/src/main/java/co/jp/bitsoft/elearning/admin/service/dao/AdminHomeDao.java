package co.jp.bitsoft.elearning.admin.service.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * ホーム
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminHomeDao {

	/**
	 * 查询テストに合格した人数
	 */
//	Integer queryTestPassCount(@Param("groupIdList") List<Long> groupIdList);

}
