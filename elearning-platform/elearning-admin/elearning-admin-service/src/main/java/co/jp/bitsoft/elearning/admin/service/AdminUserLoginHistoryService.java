package co.jp.bitsoft.elearning.admin.service;

import java.util.Map;

import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * ユーザーログイン履歴
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminUserLoginHistoryService {

    PageUtils selectUserLoginHistory(Map<String, Object> params);
}

