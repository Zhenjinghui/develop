package co.jp.bitsoft.elearning.admin.service.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.AdminStudentDataAnalyseUseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDataAnalyseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDataAnalyseExportDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTestDataAnalyseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTestQuestionsDataAnalyseExportSelectDto;

/**
 * 分析項目
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminDataAnalyseDao extends BaseMapper<AdminStudentDataAnalyseUseDto> {

    /**
     * 男女比
     */
	List<AdminStudentDataAnalyseUseDto> getManEcharts(@Param("params") Map<String, Object> params, @Param("groupIdList") List<Long> groupIdList);

    /**
     * 職業構成
     */
	List<AdminStudentDataAnalyseUseDto> getWorkEcharts(@Param("params") Map<String, Object> params, @Param("groupIdList") List<Long> groupIdList);

    /**
     * 年齢層
     */
	List<AdminStudentDto> getAgeEcharts(@Param("params") Map<String, Object> params, @Param("groupIdList") List<Long> groupIdList);

	/**
	 * 受講状況（講座・教材単位）(分页)
	 */
	IPage<AdminTeachingMaterialDataAnalyseDto> getTeachingMaterialDataAnalyse(Page<AdminTeachingMaterialDataAnalyseDto> page,@Param("params") Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 受講状況（講座・教材単位）(受講完了人数)
	 */
	int getCompleteCount(@Param("params") Map<String, Object> params);

    /**
	 * 受講状況（講座・教材単位）エクスポート
	 */
	List<AdminTeachingMaterialDataAnalyseExportDto> getTeachingMaterialDataAnalyseExport(@Param("params")Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 単元テスト結果(分页)
	 */
	IPage<AdminTestDataAnalyseDto> getTestDataAnalyse(Page<AdminTestDataAnalyseDto> page,@Param("params") Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 初回正解率
	 */
	String getFirstAvgCorrectRate(@Param("params") Map<String, Object> params);

	/**
	 * 最終正解率
	 */
	String getLastAvgCorrectRate(@Param("params") Map<String, Object> params);

	/**
	 * 平均合格率
	 */
	int getAvgPassRate(@Param("params") Map<String, Object> params);

	/**
	 * 単元テスト結果(出題内容)エクスポート検索
	 */
	List<AdminTestQuestionsDataAnalyseExportSelectDto> getTestQuestionsExport(@Param("params") Map<String, Object> params);

	/**
	 * 単元テスト結果(出題内容)エクスポート検索(正解人数)
	 */
	int getTestQuestionsPassCount(@Param("params") Map<String, Object> params);

	/**
	 * 単元テスト結果(出題内容)エクスポート検索(初回総人数)
	 */
	int getTestQuestionsFirstCount(@Param("params") Map<String, Object> params);

	/**
	 * 単元テスト結果(出題内容)エクスポート検索(初回正解人数)
	 */
	int getTestQuestionsFirstPassCount(@Param("params") Map<String, Object> params);

}
