package co.jp.bitsoft.elearning.admin.service.dao;

import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryForStudentStaRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryForStudentStaResponse;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryResponse;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AdminStudentCourseStudyDao {

    IPage<StudentCourseStudyQueryResponse> queryStudentCourseStudy(Page<StudentCourseStudyQueryResponse> page, @Param("params") StudentCourseStudyQueryRequest params);

    IPage<StudentCourseStudyQueryForStudentStaResponse> queryStudentCourseStudyStudentSta(Page<StudentCourseStudyQueryForStudentStaResponse> page,
                                                                                          @Param("params")StudentCourseStudyQueryForStudentStaRequest queryRequest);
}
