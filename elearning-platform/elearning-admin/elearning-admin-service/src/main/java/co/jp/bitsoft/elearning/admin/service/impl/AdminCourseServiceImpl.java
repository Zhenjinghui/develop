package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminCourseSelectDto;
import co.jp.bitsoft.elearning.admin.service.AdminCourseService;
import co.jp.bitsoft.elearning.admin.service.dao.AdminCourseDao;
import co.jp.bitsoft.elearning.common.exception.RRException;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import co.jp.bitsoft.elearning.core.entity.CourseCertificateTemplateEntity;
import co.jp.bitsoft.elearning.core.entity.CourseEntity;
import co.jp.bitsoft.elearning.core.entity.CourseOpenInfoEntity;
import co.jp.bitsoft.elearning.core.entity.SubjectEntity;
import co.jp.bitsoft.elearning.core.entity.TeachingMaterialEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;
import co.jp.bitsoft.elearning.core.entity.UnitEntity;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.CourseCertificateTemplateService;
import co.jp.bitsoft.elearning.core.service.CourseOpenInfoService;
import co.jp.bitsoft.elearning.core.service.CourseService;
import co.jp.bitsoft.elearning.core.service.SubjectService;
import co.jp.bitsoft.elearning.core.service.TeachingMaterialService;
import co.jp.bitsoft.elearning.core.service.TestQuestionsService;
import co.jp.bitsoft.elearning.core.service.UnitService;
import co.jp.bitsoft.elearning.core.service.UnitTestService;
import co.jp.bitsoft.elearning.core.service.UserGroupService;


@Service("AdminCourseService")
public class AdminCourseServiceImpl extends ServiceImpl<AdminCourseDao, AdminCourseDto> implements AdminCourseService {

	@Autowired
	private CourseService courseService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private UnitService unitService;
    @Autowired
    private UnitTestService unitTestService;
    @Autowired
    private TeachingMaterialService teachingMaterialService;
    @Autowired
    private TestQuestionsService testQuestionsService;
	@Autowired
	private UserGroupService userGroupService;
    @Autowired
    private CourseCertificateTemplateService courseCertificateTemplateService;
	@Autowired
	private CourseOpenInfoService courseOpenInfoService;

	@Override
    /**
     * リスト一覧
     */
    public PageUtils queryPage(Map<String, Object> params) {
        //分页参数
        long curPage = 1;
        long limit = 10;
        if(params.get(Constant.PAGE) != null){
            curPage = Long.parseLong((String)params.get(Constant.PAGE));
        }
        if(params.get(Constant.LIMIT) != null){
            limit = Long.parseLong((String)params.get(Constant.LIMIT));
        }

		Page<AdminCourseDto> page = new Page<>(curPage, limit);

//   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
		List<Long> groupIdList = null;

		String loginGroupType = getGroupType(CommonServiceImpl.getUser().getGroupId());

		IPage<AdminCourseDto> pageList = null;

		if("3".equals(loginGroupType)) {
			pageList = baseMapper.queryCourseClass(page, CommonServiceImpl.getUser().getGroupId());
		}else {
			pageList = baseMapper.queryCourse(page,params,groupIdList);
		}

        return new PageUtils(pageList);
    }
    /**
     * 情報
     */
    public AdminCourseDto courseInfo(Long courseId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("courseId", courseId);
		List<Long> groupIdList = null;

    	List<AdminCourseDto> courseList = baseMapper.queryCourse(params,groupIdList);
    	AdminCourseDto adminCourseDto = new AdminCourseDto();
        if(courseList.size()>0) {
        	adminCourseDto = courseList.get(0);
        }
    	QueryWrapper<CourseCertificateTemplateEntity> queryWrapperCertificate = new QueryWrapper<>();
    	queryWrapperCertificate.eq("course_id", courseId);
		List<CourseCertificateTemplateEntity> certificateTemplatList= courseCertificateTemplateService.list(queryWrapperCertificate);
		if(certificateTemplatList.size()>0) {
	        BeanUtils.copyProperties(certificateTemplatList.get(0), adminCourseDto);
		}

		return adminCourseDto;
    }
    /**
     * 追加
     */
    @Transactional
    public void courseSave(AdminCourseDto adminCourseDto) {

		Long loginGroupId = CommonServiceImpl.getUser().getGroupId();
		adminCourseDto.setGroupId(loginGroupId);

		insertCheckCourseTitle(adminCourseDto.getCourseTitle());

		ValidatorUtils.validateEntity(adminCourseDto, AddGroup.class);

		CourseEntity course = new CourseEntity();
        BeanUtils.copyProperties(adminCourseDto, course);
		courseService.save(course);

        CourseCertificateTemplateEntity template = new CourseCertificateTemplateEntity();
        BeanUtils.copyProperties(adminCourseDto, template);
        template.setCourseId(course.getCourseId());
        courseCertificateTemplateService.save(template);
    }
    /**
     * 更新
     */
    @Transactional
    public void courseUpdate(AdminCourseDto adminCourseDto) {

    	updateCheckCourseTitle(adminCourseDto);

		ValidatorUtils.validateEntity(adminCourseDto, UpdateGroup.class);

		CourseEntity course = new CourseEntity();
        BeanUtils.copyProperties(adminCourseDto, course);
		courseService.updateById(course);

        CourseCertificateTemplateEntity template = new CourseCertificateTemplateEntity();
        BeanUtils.copyProperties(adminCourseDto, template);
        if(template.getCourseCertificateTemplateId() != null) {
            courseCertificateTemplateService.updateById(template);
        }else {
            courseCertificateTemplateService.save(template);
        }
    }
    /**
     * 削除
     */
    @Transactional
    public void courseDelete(Long[] courseIds) {

    	for(long id : courseIds) {
    		courseService.removeById(id);
    		subjectService.update(new UpdateWrapper<SubjectEntity>().set("del_flag", "1").eq("course_id", id));
    		unitService.update(new UpdateWrapper<UnitEntity>().set("del_flag", "1").eq("course_id", id));
    		unitTestService.update(new UpdateWrapper<UnitTestEntity>().set("del_flag", "1").eq("course_id", id));
    		teachingMaterialService.update(new UpdateWrapper<TeachingMaterialEntity>().set("del_flag", "1").eq("course_id", id));
    		testQuestionsService.update(new UpdateWrapper<TestQuestionsEntity>().set("del_flag", "1").eq("course_id", id));
//    		testQuestionsChoicesService.update(new UpdateWrapper<TestQuestionsChoicesEntity>().set("del_flag", "1").eq("course_id", id));
            courseCertificateTemplateService.update(new UpdateWrapper<CourseCertificateTemplateEntity>().set("del_flag",
                                                                                                             "1")
                                                                                                        .eq("course_id",
                                                                                                            id));
    		courseOpenInfoService.remove(new QueryWrapper<CourseOpenInfoEntity>().eq("course_id", id));
    	}
    }

    /**
    * 講座下拉框
    */
    public List<AdminCourseSelectDto> courseSelect(Map<String, Object> params) {
//    	String proxyGroupId = (String)params.get("proxyGroupId");
//    	List<AdminCourseSelectDto> adminCourseSelectAll = null;
//    	if(proxyGroupId == null || proxyGroupId.isEmpty()) {
    	List<AdminCourseSelectDto> adminCourseSelectAll = baseMapper.queryCourseAll();
//    	}else {
//    		adminCourseSelectAll = baseMapper.queryCourseByProxyGroupId(Long.parseLong(proxyGroupId));
//    	}
   		return adminCourseSelectAll;
    }

    /**
    * 講座下拉框（ご契約者様）
    */
    public List<AdminCourseSelectDto> courseProxySelect(Long proxyGroupId) {

    	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
	    queryWrapper.select("group_id").eq("group_id_of", proxyGroupId);
    	List<UserGroupEntity> groupList = userGroupService.list(queryWrapper);
    	List<AdminCourseSelectDto> courseProxySelect  = new ArrayList<AdminCourseSelectDto> ();
        if(groupList == null || groupList.isEmpty()) {
        	return courseProxySelect;
        }

    	List<Long> groupIdList = new ArrayList<Long>();
    	for(UserGroupEntity userGroupEntity : groupList) {
    		groupIdList.add(userGroupEntity.getGroupId());
    	}

    	courseProxySelect = baseMapper.queryCourseByProxyGroupId(groupIdList);

   		return courseProxySelect;
    }

    /**
    * 講座下拉框(クラス)
    */
    public List<AdminCourseSelectDto> courseClassSelect(Long classGroupId) {

    	List<AdminCourseSelectDto> courseClassSelect = baseMapper.queryCourseByClassGroupId(classGroupId);
   		return courseClassSelect;
    }

	/**
	 * 插入时检查講座名是否被使用
	 */
	private void insertCheckCourseTitle(String courseTitle){

    	QueryWrapper<CourseEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("course_title", courseTitle);
		int count = courseService.count(queryWrapper);
		if(count > 0){
			throw new RRException("講座名が使用されました！");
		}
	}

	/**
	 * 更新时检查講座名是否被使用
	 */
	private void updateCheckCourseTitle(AdminCourseDto course){
		if(course.getCourseId()!=null) {
			CourseEntity courseEntity = courseService.getById(course.getCourseId());
			if(courseEntity.getCourseTitle().equals(course.getCourseTitle())) {
				return;
			}else {
				insertCheckCourseTitle(course.getCourseTitle());
			}
		}else {
			insertCheckCourseTitle(course.getCourseTitle());
		}
	}
    /**
    * 获取グループタイプ
    */
   public String getGroupType(long groupId) {
	   	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
	   	queryWrapper.select("group_type").eq("group_id", groupId);

	   	String groupType = userGroupService.getOne(queryWrapper).getGroupType();
		return groupType;
   }


}