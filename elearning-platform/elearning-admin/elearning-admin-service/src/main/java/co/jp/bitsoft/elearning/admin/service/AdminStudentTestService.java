package co.jp.bitsoft.elearning.admin.service;

import java.util.List;

import co.jp.bitsoft.elearning.admin.dto.NoPassedAnswerDto;

/**
 * 受講者テスト
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminStudentTestService{

	List<NoPassedAnswerDto> studentTestNoPassedAnswers(Long studentId,Long studentTestId);

}

