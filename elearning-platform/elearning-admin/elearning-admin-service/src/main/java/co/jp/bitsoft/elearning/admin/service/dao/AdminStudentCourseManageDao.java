package co.jp.bitsoft.elearning.admin.service.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageDto;
import co.jp.bitsoft.elearning.admin.dto.QueryStudentCourseManageDto;
import co.jp.bitsoft.elearning.admin.dto.QueryStudentCourseManageForStudentStaDto;
import co.jp.bitsoft.elearning.admin.dto.QueryStudentCourseManageForStudentStaRespDto;
import co.jp.bitsoft.elearning.admin.dto.QueryStudentCourseManageRespDto;

/**
 * 受講者コース管理
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminStudentCourseManageDao extends BaseMapper<AdminStudentCourseManageDto> {
	/**
	 * 查询一般用户讲座(分页)
	 */
	IPage<AdminStudentCourseManageDto> queryStudentCourseManage(Page<AdminStudentCourseManageDto> page,Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询一般用户讲座(无分页)
	 */
	List<AdminStudentCourseManageDto> queryStudentCourseManage(Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);


	List<QueryStudentCourseManageRespDto> queryStudentCourseManageForSta(@Param("params") QueryStudentCourseManageDto params);

	IPage<QueryStudentCourseManageForStudentStaRespDto> queryStudentCourseManageForStudentSta(Page<QueryStudentCourseManageForStudentStaRespDto> page, @Param("params") QueryStudentCourseManageForStudentStaDto params);

    /**
    * 受講者別テスト状況管理
    */
	IPage<QueryStudentCourseManageForStudentStaRespDto> queryStudentUnitTestManage(Page<QueryStudentCourseManageForStudentStaRespDto> page, @Param("params") QueryStudentCourseManageForStudentStaDto params);

}
