package co.jp.bitsoft.elearning.admin.service.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.admin.dto.StudentTestQuestionAnswersDto;
import co.jp.bitsoft.elearning.admin.dto.StudentTestTestQuestionsChoicesDto;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;

@Mapper
public interface TestQuestionsChoicesDbDao extends BaseMapper<TestQuestionsChoicesEntity> {
    List<StudentTestTestQuestionsChoicesDto> studentTestTestQuestionsChoicesList(@Param("studentTestId") Long studentTestId);

    List<StudentTestQuestionAnswersDto> studentTestQuestionAnswers(@Param("studentId") Long studentId,
            @Param("studentTestId") Long studentTestId);
}
