package co.jp.bitsoft.elearning.admin.service;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.InquiryEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface AdminInquiryService extends IService<InquiryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}
