package co.jp.bitsoft.elearning.admin.service.impl;

import co.jp.bitsoft.elearning.admin.service.AdminQuestionNaireQuestionService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionEntity;
import co.jp.bitsoft.elearning.core.service.QuestionNaireQuestionService;
import co.jp.bitsoft.elearning.core.service.dao.QuestionNaireQuestionDao;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("adminQuestionNaireQuestionService")
public class AdminQuestionNaireQuestionServiceImpl extends ServiceImpl<QuestionNaireQuestionDao, QuestionNaireQuestionEntity> implements AdminQuestionNaireQuestionService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String questionName = (String)params.get("questionName");
        IPage<QuestionNaireQuestionEntity> page = this.page(
                new Query<QuestionNaireQuestionEntity>().getPage(params),
                new QueryWrapper<QuestionNaireQuestionEntity>()
                .like(StringUtils.isNotBlank(questionName),"question_name", questionName)
        );

        return new PageUtils(page);
    }

}
