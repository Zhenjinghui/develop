package co.jp.bitsoft.elearning.admin.service;

import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryForStudentStaRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryRequest;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

public interface AdminStudentCourseStudyService {

    PageUtils queryStudentCourseStudy(StudentCourseStudyQueryRequest queryRequest);

    PageUtils queryStudentCourseStudyStudentSta(StudentCourseStudyQueryForStudentStaRequest queryRequest);
}
