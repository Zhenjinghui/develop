package co.jp.bitsoft.elearning.admin.service;

import org.apache.shiro.crypto.hash.Sha256Hash;

public class PasswordTool {

	public static void main(String[] args) {
		String salt = "TkKr4F2s6OSVjBgKVXMm";
		String password = new Sha256Hash("20210508student@gmail.com", salt).toHex();
		System.out.println(password);
	}

}
