package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.admin.dto.NoPassedAnswerDto;
import co.jp.bitsoft.elearning.admin.dto.StudentTestQuestionAnswersDto;
import co.jp.bitsoft.elearning.admin.dto.StudentTestTestQuestionsChoicesDto;
import co.jp.bitsoft.elearning.admin.service.AdminStudentTestService;
import co.jp.bitsoft.elearning.admin.service.dao.TestQuestionsChoicesDbDao;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import lombok.var;


@Service("AdminstudentTestService")
public class AdminStudentTestServiceImpl extends ServiceImpl<TestQuestionsChoicesDbDao,TestQuestionsChoicesEntity> implements AdminStudentTestService {

    @Override
	public List<NoPassedAnswerDto> studentTestNoPassedAnswers(Long studentId,Long studentTestId){

    	List<StudentTestTestQuestionsChoicesDto> studentTestTestQuestionsChoicesList = baseMapper.studentTestTestQuestionsChoicesList(studentTestId);

        List<StudentTestQuestionAnswersDto> studentTestQuestionAnswersList = baseMapper.studentTestQuestionAnswers(studentId,studentTestId);

        List<NoPassedAnswerDto> noPassedResult = null;

        if (!CollectionUtils.isEmpty(studentTestQuestionAnswersList)) {
            // テストが合格にしても不合格の内容も画面に表示する
            noPassedResult= studentTestQuestionAnswersList.stream()
                                                                                             .filter(entity -> "1".equals(entity.getQuestionAnswerResult())) // 不合格の解答
                                                                                             .map(entity -> {
                                                                                            	 NoPassedAnswerDto noPassedAnswer = new NoPassedAnswerDto();
                                                                                                 noPassedAnswer.setQuestionTitle(entity.getQuestionTitle());
                                                                                                 noPassedAnswer.setQuestionContent(entity.getQuestionContent());
                                                                                                 noPassedAnswer.setQuestionExplanation(entity.getQuestionExplanation());
                                                                                                 noPassedAnswer.setAnswerResult(entity.getQuestionAnswerResult());

                                                                                                 // 記述以外の場合
                                                                                                 if (!"3".equals(entity.getQuestionType())) {
                                                                                                     var choicesDtoList = studentTestTestQuestionsChoicesList.stream()
                                                                                                                                                             .filter(choice -> entity.getQuestionAnswerId()
                                                                                                                                                                                     .equals(choice.getStudentTestQuestionAnswerId()))
                                                                                                                                                             .collect(Collectors.toList());

                                                                                                     // 解答内容が選択肢NOのため、該当選択肢NOに対応する選択肢文を取得し、不合格問題情報の「解答内容」とする
                                                                                                     var answerContent = new ArrayList<String>();
                                                                                                     for (String no : entity.getAnswerContent()
                                                                                                                            .split(",")) {
                                                                                                         for (var dto : choicesDtoList) {
                                                                                                             if (dto.getChoicesNo()
                                                                                                                    .toString()
                                                                                                                    .equals(no)) {
                                                                                                                 answerContent.add(dto.getChoicesContents());
                                                                                                                 break;
                                                                                                             }
                                                                                                         }
                                                                                                     }

                                                                                                     noPassedAnswer.setAnswerContent(String.join("\n\n",
                                                                                                                                                 answerContent));

                                                                                                     // 正解の選択肢NOの選択肢文を取得し、不合格問題情報の「標準解答」とする
                                                                                                     var correctAnswer = new ArrayList<String>();
                                                                                                     for (String no : entity.getStandardAnswers()
                                                                                                                            .split(",")) {
                                                                                                         for (var dto : choicesDtoList) {
                                                                                                             if (dto.getChoicesNo()
                                                                                                                    .toString()
                                                                                                                    .equals(no)) {
                                                                                                                 correctAnswer.add(dto.getChoicesContents());
                                                                                                                 break;
                                                                                                             }
                                                                                                         }
                                                                                                     }

                                                                                                     noPassedAnswer.setStandardAnswers(String.join("\n\n",
                                                                                                                                                   correctAnswer));
                                                                                                 }
                                                                                                 // 記述の場合、そのままにする
                                                                                                 else {
                                                                                                     noPassedAnswer.setAnswerContent(entity.getAnswerContent());
                                                                                                     noPassedAnswer.setStandardAnswers(entity.getStandardAnswers());
                                                                                                 }

                                                                                                 return noPassedAnswer;
                                                                                             })
                                                                                             .collect(Collectors.toList());
        }

    	return noPassedResult;
    };


}