package co.jp.bitsoft.elearning.admin.service;

import java.util.Map;

import co.jp.bitsoft.elearning.admin.dto.AdminUnitTestDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * 単元テスト
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminUnitTestService {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils selectUnitTestPage(Map<String, Object> params);

    void unitTestSave(AdminUnitTestDto adminUnitTestDto);

    void unitTestUpdate(AdminUnitTestDto adminUnitTestDto);

    void unitTestDelete(Long[] unitTestIds);

}

