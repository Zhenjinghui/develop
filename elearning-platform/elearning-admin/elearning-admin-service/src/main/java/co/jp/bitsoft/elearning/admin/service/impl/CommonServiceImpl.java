package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.UserGroupService;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;


public class CommonServiceImpl {

   	/**
   	 * 获取当前用户信息
   	 */
	public static SysUserEntity getUser() {
   		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
   	}


   	/**
   	 * group限制
   	 */
   	public static List<Long> groupSelectLimit(UserGroupService userGroupService) {
   		List<Long> groupIdList = new ArrayList<Long>();

        Long logingroupId = getUser().getGroupId();
	   	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
	   	queryWrapper.select("group_type").eq("group_id", logingroupId);
	   	String groupType = userGroupService.getOne(queryWrapper).getGroupType();

	   	if("1".equals(groupType)) {
			return groupIdList;
		}

	   	QueryWrapper<UserGroupEntity> queryWrapperSelect = new QueryWrapper<>();
	   	queryWrapperSelect.select("group_id").eq("group_id", logingroupId).or().eq("group_id_of", logingroupId);
   		List<UserGroupEntity> userGroupList = userGroupService.list(queryWrapperSelect);
   		for(UserGroupEntity usergroup : userGroupList) {
   			groupIdList.add(usergroup.getGroupId());
   		}
   		return groupIdList;
   	}


}