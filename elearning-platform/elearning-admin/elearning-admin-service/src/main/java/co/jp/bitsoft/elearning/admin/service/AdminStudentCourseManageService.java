package co.jp.bitsoft.elearning.admin.service;

import java.util.List;
import java.util.Map;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage.StudentCourseManageQueryForStudentStaRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage.StudentCourseManageQueryRequest;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageExportDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageUpdateDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * 受講者コース管理
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminStudentCourseManageService {

    /**
	 * 查询系统用户(分页)
	 */
	PageUtils queryStudentCourseManage(Map<String, Object> params);

	/**
	 * 导出(无分页)
	 */
	List<AdminStudentCourseManageExportDto> queryAllStudentCourseManage(Map<String, Object> params);

	AdminStudentCourseManageDto studentCourseManageInfo(Long studentCourseId);

    void studentCourseManageUpdate(AdminStudentCourseManageUpdateDto adminStudentCourseManageDto);

	PageUtils queryStudentCourseManageForSta(StudentCourseManageQueryRequest queryRequest);

	PageUtils queryStudentCourseManageForStudentSta(StudentCourseManageQueryForStudentStaRequest queryRequest);

	void studentCourseImport(JSONObject csvData);

    /**
    * 受講者別テスト状況管理
    */
	PageUtils queryStudentUnitTestManage(StudentCourseManageQueryForStudentStaRequest queryRequest);

}

