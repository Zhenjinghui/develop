package co.jp.bitsoft.elearning.admin.service.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminCourseSelectDto;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminCourseDao extends BaseMapper<AdminCourseDto> {

	/**
	 * 查询講座(分页)
	 */
	IPage<AdminCourseDto> queryCourse(Page<AdminCourseDto> page,Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询講座(无分页)
	 */
	List<AdminCourseDto> queryCourse(Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询所有講座(下拉框用)
	 */
	List<AdminCourseSelectDto> queryCourseAll();

	/**
	 * 查询对应ご契約者様講座(下拉框用)
	 */
	List<AdminCourseSelectDto> queryCourseByProxyGroupId(@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询对应クラス講座(下拉框用)
	 */
	List<AdminCourseSelectDto> queryCourseByClassGroupId(Long groupId);

	/**
	 * 查询講座(分页)一般group
	 */
	Page<AdminCourseDto> queryCourseClass(Page<AdminCourseDto> page,Long groupId);

}
