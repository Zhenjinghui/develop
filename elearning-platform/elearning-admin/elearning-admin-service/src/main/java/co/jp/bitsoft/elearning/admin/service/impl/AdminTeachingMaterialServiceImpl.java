package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDto;
import co.jp.bitsoft.elearning.admin.service.AdminTeachingMaterialService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import co.jp.bitsoft.elearning.core.entity.TeachingMaterialEntity;
import co.jp.bitsoft.elearning.core.service.TeachingMaterialService;


@Service("AdminTeachingMaterialService")
public class AdminTeachingMaterialServiceImpl implements AdminTeachingMaterialService{

    @Autowired
    private TeachingMaterialService teachingMaterialService;

    @Override
    /**
    * リスト一覧
    */
    public PageUtils queryPage(Map<String, Object> params) {
    	String unitId = (String)params.get("unitId");
    	String teachingMaterialTitle = (String)params.get("teachingMaterialTitle");

        IPage<TeachingMaterialEntity> page = teachingMaterialService.page(
                new Query<TeachingMaterialEntity>().getPage(params),
                new QueryWrapper<TeachingMaterialEntity>()
                .eq("unit_id",Integer.parseInt(unitId))
                .like(StringUtils.isNotBlank(teachingMaterialTitle),"teaching_material_title", teachingMaterialTitle)
                .orderByAsc("disp_no")

        );

        return new PageUtils(page);
    }


    /**
     * 追加
     */
    public void teachingMaterialSave(AdminTeachingMaterialDto adminTeachingMaterialDto) {

		ValidatorUtils.validateEntity(adminTeachingMaterialDto, AddGroup.class);

	    TeachingMaterialEntity teachingMaterial = new TeachingMaterialEntity();
        BeanUtils.copyProperties(adminTeachingMaterialDto, teachingMaterial);

		teachingMaterialService.save(teachingMaterial);


    }
    /**
     * 更新
     */
    public void teachingMaterialUpdate(AdminTeachingMaterialDto adminTeachingMaterialDto) {

		ValidatorUtils.validateEntity(adminTeachingMaterialDto, UpdateGroup.class);

	    TeachingMaterialEntity teachingMaterial = new TeachingMaterialEntity();
        BeanUtils.copyProperties(adminTeachingMaterialDto, teachingMaterial);

		teachingMaterialService.updateById(teachingMaterial);


    }
    /**
     * 削除
     */
    public void teachingMaterialDelete(Long[] teachingMaterialIds) {

    	for(long id : teachingMaterialIds) {
    		teachingMaterialService.removeById(id);
    	}

    }
}