package co.jp.bitsoft.elearning.admin.service;

import co.jp.bitsoft.elearning.admin.dto.AdminHomeDto;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminHomeService{

	AdminHomeDto getHomeData();

}

