package co.jp.bitsoft.elearning.admin.service;

import co.jp.bitsoft.elearning.core.entity.CourseInfoEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.InquiryMessageEntity;

import java.util.Map;

/**
 * 問い合わせメッセージ
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminInquiryMessageService extends IService<InquiryMessageEntity> {

    PageUtils queryPage(Map<String, Object> params);
    void removeByInquiryIds(Long[] inquiryIds);
    void changeStatus(int inquiryId, int userId);
}

