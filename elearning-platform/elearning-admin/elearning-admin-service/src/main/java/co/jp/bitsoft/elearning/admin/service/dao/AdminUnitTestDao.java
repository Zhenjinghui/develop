package co.jp.bitsoft.elearning.admin.service.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminUnitTestDto;

/**
 * 単元テスト
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminUnitTestDao extends BaseMapper<AdminUnitTestDto> {

	IPage<AdminCourseDto> selectUnitTestPage(Page<AdminCourseDto> page,Map<String, Object> params);


}
