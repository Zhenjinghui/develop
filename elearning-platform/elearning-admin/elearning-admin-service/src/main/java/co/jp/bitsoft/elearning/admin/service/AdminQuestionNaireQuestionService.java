package co.jp.bitsoft.elearning.admin.service;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface AdminQuestionNaireQuestionService extends IService<QuestionNaireQuestionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}
