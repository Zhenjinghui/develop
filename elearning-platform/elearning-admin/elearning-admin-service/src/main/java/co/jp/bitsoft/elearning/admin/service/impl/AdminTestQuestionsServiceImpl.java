package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import co.jp.bitsoft.elearning.admin.dto.AdminQuestionChoicesDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTestQuestionsDto;
import co.jp.bitsoft.elearning.admin.service.AdminTestQuestionsService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;
import co.jp.bitsoft.elearning.core.service.TestQuestionsChoicesService;
import co.jp.bitsoft.elearning.core.service.TestQuestionsService;


@Service("AdminTestQuestionsService")
public class AdminTestQuestionsServiceImpl implements AdminTestQuestionsService {

    @Autowired
    private TestQuestionsService testQuestionsService;
    @Autowired
    private TestQuestionsChoicesService testQuestionsChoicesService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
		String unitTestId = (String)params.get("unitTestId");
		String questionTitle = (String)params.get("questionTitle");
		String questionType = (String)params.get("questionType");

        IPage<TestQuestionsEntity> page = testQuestionsService.page(
                new Query<TestQuestionsEntity>().getPage(params),
                new QueryWrapper<TestQuestionsEntity>()
                .eq("unit_test_id",Integer.parseInt(unitTestId))
                .like(StringUtils.isNotBlank(questionTitle),"question_title", questionTitle)
                .eq(StringUtils.isNotBlank(questionType),"question_type",questionType)
        );

        return new PageUtils(page);
    }

    /**
     * 情報
     */
    public AdminTestQuestionsDto testQuestionsInfo(Long testQuestionsId) {

		TestQuestionsEntity testQuestions = testQuestionsService.getById(testQuestionsId);

		AdminTestQuestionsDto adminTestQuestionsDto = new AdminTestQuestionsDto();

		BeanUtils.copyProperties(testQuestions, adminTestQuestionsDto);

		List<TestQuestionsChoicesEntity> testQuestionsChoicesList=
				testQuestionsChoicesService.list(new QueryWrapper<TestQuestionsChoicesEntity>().eq("test_questions_id", testQuestionsId));

		List<AdminQuestionChoicesDto> adminQuestionChoicesDtolist =  new ArrayList<AdminQuestionChoicesDto>();

		for(int i=0;i<testQuestionsChoicesList.size();i++) {
			AdminQuestionChoicesDto adminQuestionChoicesDto = new AdminQuestionChoicesDto();
			adminQuestionChoicesDto.setTestQuestionsChoicesId(testQuestionsChoicesList.get(i).getTestQuestionsChoicesId());
			adminQuestionChoicesDto.setPrefix(testQuestionsChoicesList.get(i).getChoicesNo()+"");
			adminQuestionChoicesDto.setContent(testQuestionsChoicesList.get(i).getChoicesContents());
			adminQuestionChoicesDtolist.add(adminQuestionChoicesDto);
		}
		adminTestQuestionsDto.setItems(adminQuestionChoicesDtolist);

		return adminTestQuestionsDto;

    }

    /**
     * 追加
     */
    public void testQuestionsSave(AdminTestQuestionsDto adminTestQuestionsDto) {

		if(!"3".equals(adminTestQuestionsDto.getQuestionType())) {
			adminTestQuestionsDto.setChoicesNumber(adminTestQuestionsDto.getItems().size());
		}else {
			String testQuestionsNum = adminTestQuestionsDto.getStandardAnswers();
			String[] testQuestionsNumArr = testQuestionsNum.split(",");
			adminTestQuestionsDto.setChoicesNumber(testQuestionsNumArr.length);
		}
		ValidatorUtils.validateEntity(adminTestQuestionsDto, AddGroup.class);

		TestQuestionsEntity testQuestions = new TestQuestionsEntity();
		BeanUtils.copyProperties(adminTestQuestionsDto, testQuestions);

		testQuestionsService.save(testQuestions);
		if(!"3".equals(adminTestQuestionsDto.getQuestionType())) {
			List<TestQuestionsChoicesEntity> testQuestionsChoicesList =  new ArrayList<TestQuestionsChoicesEntity>();
			for(int i=0;i<adminTestQuestionsDto.getItems().size();i++) {
				TestQuestionsChoicesEntity testQuestionsChoicesEntity = new TestQuestionsChoicesEntity();
				testQuestionsChoicesEntity.setCourseId(testQuestions.getCourseId());
				testQuestionsChoicesEntity.setSubjectId(testQuestions.getSubjectId());
				testQuestionsChoicesEntity.setUnitId(testQuestions.getUnitId());
				testQuestionsChoicesEntity.setUnitTestId(testQuestions.getUnitTestId());
				testQuestionsChoicesEntity.setTestQuestionsId(testQuestions.getTestQuestionsId());
				testQuestionsChoicesEntity.setChoicesNo(Integer.parseInt(adminTestQuestionsDto.getItems().get(i).getPrefix()));
				testQuestionsChoicesEntity.setChoicesContents(adminTestQuestionsDto.getItems().get(i).getContent());

				ValidatorUtils.validateEntity(adminTestQuestionsDto.getItems().get(i), AddGroup.class);

				testQuestionsChoicesList.add(testQuestionsChoicesEntity);
			}
			testQuestionsChoicesService.saveBatch(testQuestionsChoicesList);
		}

    }
    /**
     * 更新
     */
    public void testQuestionsUpdate(AdminTestQuestionsDto adminTestQuestionsDto) {

		if(!"3".equals(adminTestQuestionsDto.getQuestionType())) {
			adminTestQuestionsDto.setChoicesNumber(adminTestQuestionsDto.getItems().size());
		}else {
			String testQuestionsNum = adminTestQuestionsDto.getStandardAnswers();
			String[] testQuestionsNumArr = testQuestionsNum.split(",");
			adminTestQuestionsDto.setChoicesNumber(testQuestionsNumArr.length);
		}
		ValidatorUtils.validateEntity(adminTestQuestionsDto, UpdateGroup.class);

		TestQuestionsEntity testQuestions = new TestQuestionsEntity();
		BeanUtils.copyProperties(adminTestQuestionsDto, testQuestions);
		testQuestionsService.updateById(testQuestions);

		if(!"3".equals(testQuestions.getQuestionType())) {
			QueryWrapper<TestQuestionsChoicesEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("del_flag", "0").eq("test_questions_id", testQuestions.getTestQuestionsId());
			List<TestQuestionsChoicesEntity> testQuestionsChoicesList = testQuestionsChoicesService.list(queryWrapper);

			List<Long> testQuestionsChoicesViewIds = new ArrayList<Long>();
			List<AdminQuestionChoicesDto> adminQuestionChoicesDtolist = adminTestQuestionsDto.getItems();
			for(AdminQuestionChoicesDto dminQuestionChoicesDto : adminQuestionChoicesDtolist) {
				ValidatorUtils.validateEntity(dminQuestionChoicesDto, UpdateGroup.class);

				TestQuestionsChoicesEntity testQuestionsChoicesEntity = new TestQuestionsChoicesEntity();
				testQuestionsChoicesEntity.setCourseId(testQuestions.getCourseId());
				testQuestionsChoicesEntity.setSubjectId(testQuestions.getSubjectId());
				testQuestionsChoicesEntity.setUnitId(testQuestions.getUnitId());
				testQuestionsChoicesEntity.setUnitTestId(testQuestions.getUnitTestId());
				testQuestionsChoicesEntity.setTestQuestionsId(testQuestions.getTestQuestionsId());
				testQuestionsChoicesEntity.setChoicesNo(Integer.parseInt(dminQuestionChoicesDto.getPrefix()));
				testQuestionsChoicesEntity.setChoicesContents(dminQuestionChoicesDto.getContent());
				if(dminQuestionChoicesDto.getTestQuestionsChoicesId()==null) {
					testQuestionsChoicesService.save(testQuestionsChoicesEntity);
				}else {
					testQuestionsChoicesEntity.setTestQuestionsChoicesId(dminQuestionChoicesDto.getTestQuestionsChoicesId());
					testQuestionsChoicesService.updateById(testQuestionsChoicesEntity);
				}
				testQuestionsChoicesViewIds.add(dminQuestionChoicesDto.getTestQuestionsChoicesId());
			}

			for(TestQuestionsChoicesEntity testQuestionsChoicesEntity:testQuestionsChoicesList) {
				if(!testQuestionsChoicesViewIds.contains(testQuestionsChoicesEntity.getTestQuestionsChoicesId())) {
		       		testQuestionsChoicesService.update(new UpdateWrapper<TestQuestionsChoicesEntity>().set("del_flag", "1").eq("test_questions_choices_id", testQuestionsChoicesEntity.getTestQuestionsChoicesId()));
				}
			}
		}

    }
    /**
     * 削除
     */
    public void testQuestionsDelete(Long[] testQuestionsIds) {

       	for(long id : testQuestionsIds) {
       		testQuestionsService.removeById(id);
       		testQuestionsChoicesService.update(new UpdateWrapper<TestQuestionsChoicesEntity>().set("del_flag", "1").eq("test_questions_id", id));
    	}
    }


}