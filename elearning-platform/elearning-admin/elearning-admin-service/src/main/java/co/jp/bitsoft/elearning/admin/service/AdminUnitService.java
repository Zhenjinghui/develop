package co.jp.bitsoft.elearning.admin.service;

import java.util.Map;

import co.jp.bitsoft.elearning.admin.dto.AdminUnitDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * 単元
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminUnitService {

    PageUtils queryPage(Map<String, Object> params);

    void unitSave(AdminUnitDto adminUnitDto);

    void unitUpdate(AdminUnitDto adminUnitDto);

    void unitDelete(Long[] unitIds);
}

