package co.jp.bitsoft.elearning.admin.service.impl;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.InquiryMessageEntity;
import co.jp.bitsoft.elearning.admin.service.dao.AdminInquiryMessageDao;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import co.jp.bitsoft.elearning.admin.service.AdminInquiryMessageService;

import java.util.Map;


@Service("adminInquiryMessageService")
public class AdminInquiryMessageServiceImpl extends ServiceImpl<AdminInquiryMessageDao, InquiryMessageEntity> implements AdminInquiryMessageService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InquiryMessageEntity> page = this.page(
                new Query<InquiryMessageEntity>().getPage(params),
                new QueryWrapper<InquiryMessageEntity>()
        );

        return new PageUtils(page);
    }

    public void removeByInquiryIds(Long[] inquiryIds) {
        baseMapper.removeByInquiryIds(inquiryIds);
    }
    public void changeStatus(int inquiryId, int userId) {
        baseMapper.changeStatus(inquiryId, userId);
    }

}