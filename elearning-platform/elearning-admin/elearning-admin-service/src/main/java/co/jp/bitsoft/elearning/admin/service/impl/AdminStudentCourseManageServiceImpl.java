package co.jp.bitsoft.elearning.admin.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage.StudentCourseManageQueryForStudentStaRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage.StudentCourseManageQueryRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursemanage.StudentCourseManageQueryResponse;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageExportDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentCourseManageUpdateDto;
import co.jp.bitsoft.elearning.admin.dto.QueryStudentCourseManageDto;
import co.jp.bitsoft.elearning.admin.dto.QueryStudentCourseManageForStudentStaDto;
import co.jp.bitsoft.elearning.admin.dto.QueryStudentCourseManageForStudentStaRespDto;
import co.jp.bitsoft.elearning.admin.dto.QueryStudentCourseManageRespDto;
import co.jp.bitsoft.elearning.admin.service.AdminStudentCourseManageService;
import co.jp.bitsoft.elearning.admin.service.dao.AdminStudentCourseManageDao;
import co.jp.bitsoft.elearning.common.exception.RRException;
import co.jp.bitsoft.elearning.common.mail.CommonMailUtils;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.core.entity.CourseEntity;
import co.jp.bitsoft.elearning.core.entity.CourseOpenInfoEntity;
import co.jp.bitsoft.elearning.core.entity.StudentCourseManageEntity;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.CourseOpenInfoService;
import co.jp.bitsoft.elearning.core.service.CourseService;
import co.jp.bitsoft.elearning.core.service.StudentCourseManageService;
import co.jp.bitsoft.elearning.core.service.StudentService;
import co.jp.bitsoft.elearning.core.service.UserGroupService;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;


@Service("AdminStudentCourseManageService")
public class AdminStudentCourseManageServiceImpl extends ServiceImpl<AdminStudentCourseManageDao, AdminStudentCourseManageDto> implements AdminStudentCourseManageService {

	@Autowired
	private UserGroupService userGroupService;

    @Autowired
    private StudentCourseManageService studentCourseManageService;

    @Autowired
	private StudentService studentService;

    @Autowired
	private CourseService courseService;

	@Autowired
	private CommonMailUtils commonMailUtils;

	@Autowired
    private CourseOpenInfoService courseOpenInfoService;

    /**
   	 * 查询一般用户讲座(分页)
   	 */
   	public PageUtils queryStudentCourseManage(Map<String, Object> params) {
       //分页参数
       long curPage = 1;
       long limit = 10;
       if(params.get(Constant.PAGE) != null){
           curPage = Long.parseLong((String)params.get(Constant.PAGE));
       }
       if(params.get(Constant.LIMIT) != null){
           limit = Long.parseLong((String)params.get(Constant.LIMIT));
       }
       Page<AdminStudentCourseManageDto> page = new Page<>(curPage, limit);

       //group限制
  		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);

   		IPage<AdminStudentCourseManageDto> pageList = baseMapper.queryStudentCourseManage(page,params,groupIdList);

   		return new PageUtils(pageList);
       }

   	/**
   	 * 导出
   	 */
   	public List<AdminStudentCourseManageExportDto> queryAllStudentCourseManage(Map<String, Object> params) {

       //group限制
       List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
       List<AdminStudentCourseManageDto> studentCourseManageList = baseMapper.queryStudentCourseManage(params,groupIdList);

       List<AdminStudentCourseManageExportDto> studentCourseManageExportList = new ArrayList<AdminStudentCourseManageExportDto>();

       for(AdminStudentCourseManageDto adminStudentCourseManageDto : studentCourseManageList) {
    	   AdminStudentCourseManageExportDto adminStudentCourseManageExportDto = new AdminStudentCourseManageExportDto();
    	   BeanUtils.copyProperties(adminStudentCourseManageDto, adminStudentCourseManageExportDto);
    	   if(adminStudentCourseManageDto.getStudyStatus().equals("0")) {
    		   adminStudentCourseManageExportDto.setStudyStatus("受講申請中");
    	   }
    	   if(adminStudentCourseManageDto.getStudyStatus().equals("1")) {
    		   adminStudentCourseManageExportDto.setStudyStatus("受講申請承認済（未受講受験）");
    	   }
    	   if(adminStudentCourseManageDto.getStudyStatus().equals("2")) {
    		   adminStudentCourseManageExportDto.setStudyStatus("受講受験中");
    	   }
    	   if(adminStudentCourseManageDto.getStudyStatus().equals("3")) {
    		   adminStudentCourseManageExportDto.setStudyStatus("受講終了（全受験通過）");
    	   }
    	   if(adminStudentCourseManageDto.getStudyStatus().equals("4")) {
    		   adminStudentCourseManageExportDto.setStudyStatus("受講申請拒否");
    	   }
    	   studentCourseManageExportList.add(adminStudentCourseManageExportDto);
       }


       return studentCourseManageExportList;
       }

   	/**
     * 情報
     */
   	public AdminStudentCourseManageDto studentCourseManageInfo(Long studentCourseId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("studentCourseId", studentCourseId);

		List<Long> groupIdList = null;
		List<AdminStudentCourseManageDto> studentCourseManageList = baseMapper.queryStudentCourseManage(params,groupIdList);
		AdminStudentCourseManageDto studentCourseManage = new AdminStudentCourseManageDto();
		if(studentCourseManageList.size()>0) {
			studentCourseManage = studentCourseManageList.get(0);
        }
		return studentCourseManage;
   	}

    /**
     * 承認/拒否
     */
    public void studentCourseManageUpdate(AdminStudentCourseManageUpdateDto adminStudentCourseManageDto) {

    	// htmlテンプレートファイルを読む
    	String promise = "";
    	String mailStr = "";
		String buffer = null;
		try {
			buffer = commonMailUtils.readHtml("promiseOrRefuse.html");
		} catch (Exception e) {
			throw new RRException("htmlテンプレートファイルの読み取りに失敗しました！");
		}

    	List<Long> studentCourseIdsList = adminStudentCourseManageDto.getStudentCourseIds();
    	for(Long id : studentCourseIdsList) {
        	StudentCourseManageEntity studentCourseManage = new StudentCourseManageEntity();
        	studentCourseManage.setStudentCourseId(id);
        	studentCourseManage.setStudyStatus(adminStudentCourseManageDto.getStudyStatus());
        	if ("1".equals(adminStudentCourseManageDto.getStudyStatus())) {
        		studentCourseManage.setRegApprovalTime(new Date());;
        	}else if ("4".equals(adminStudentCourseManageDto.getStudyStatus())) {
        		studentCourseManage.setRegRejectTime(new Date());
        	}else {
    			throw new RRException("受講状態エラー！");
        	}
    		studentCourseManageService.updateById(studentCourseManage);

    	}

    	for(Long id : studentCourseIdsList) {
    		String mailTitle_pre = "【Study Pack】受講が";
			String mailTitle_tail = "されました。";
			String mailTitle = "";
        	if ("1".equals(adminStudentCourseManageDto.getStudyStatus())) {
        		promise = "承認";
        		mailStr = "講座期間終了までにご受講ください。";
        	}else if ("4".equals(adminStudentCourseManageDto.getStudyStatus())) {
        		promise = "拒否";
        		mailStr = "";
        	}else {
    			throw new RRException("受講状態エラー！");
        	}

    		Map<String, Object> params = new HashMap<String, Object>();
    		params.put("studentCourseId", id);
    		List<Long> groupIdList = null;
    		List<AdminStudentCourseManageDto> studentCourseManageList = baseMapper.queryStudentCourseManage(params,groupIdList);
    		AdminStudentCourseManageDto studentCourseManageDto = new AdminStudentCourseManageDto();
    		if(studentCourseManageList.size()>0) {
    			studentCourseManageDto = studentCourseManageList.get(0);
            }

            String htmlText = MessageFormat.format(buffer, studentCourseManageDto.getUserName(),studentCourseManageDto.getCourseTitle(),promise,mailStr);

			mailTitle = mailTitle_pre + promise + mailTitle_tail;
            boolean mailSendFlag = commonMailUtils.sendHtmlMail(studentCourseManageDto.getEmail(), mailTitle, htmlText);
	    	if(!mailSendFlag) {
    			throw new RRException("メールの送信に失敗しました！");
	    	}

    	}

    }

    /**
	 * 用户インポト
	 */
	@SuppressWarnings("unchecked")
	public void studentCourseImport(JSONObject csvData) {

		String jsonStr = csvData.get("csvJson").toString();
		List<Object> csvDataRow = JSONArray.parseArray(jsonStr);
		List<String> rowList = new ArrayList<String>();
		AdminStudentCourseManageExportDto adminStudenCoursetDto;
		if(csvDataRow.size()>1) {
			for(int i= 0;i< csvDataRow.size();i++) {
				if(i>0) {
					rowList = (List<String>) csvDataRow.get(i);
					if(rowList.size() < 3) {
						throw new RRException("csvデータに空値きがあります！");
					}
					adminStudenCoursetDto = new AdminStudentCourseManageExportDto();
					adminStudenCoursetDto.setStudentId(Long.parseLong(rowList.get(0)));
					adminStudenCoursetDto.setCourseId(Long.parseLong(rowList.get(1)));
			    	StudentEntity student = studentService.getById(Long.parseLong(rowList.get(0)));
			    	if(student == null) {
			    		throw new RRException("受讲者IDは存在しません！");
			    	}else {
						adminStudenCoursetDto.setGroupId(student.getGroupId());
			    	}
					adminStudenCoursetDto.setStudyStatus(rowList.get(2));
					studentCourseImportCheck(adminStudenCoursetDto);
					ValidatorUtils.validateEntity(adminStudenCoursetDto, AddGroup.class);

			    	StudentCourseManageEntity studentCourse = new StudentCourseManageEntity();
			        BeanUtils.copyProperties(adminStudenCoursetDto, studentCourse);
			        if(studentCourse.getStudyStatus().equals("0")) {
			        	studentCourse.setCourseRegTime(new Date());
				    }
				    if(studentCourse.getStudyStatus().equals("1")) {
				    	studentCourse.setRegApprovalTime(new Date());
				    }
				    if(studentCourse.getStudyStatus().equals("2")) {
				    	studentCourse.setStudyTestStartTime(new Date());
				    }
				    if(studentCourse.getStudyStatus().equals("3")) {
				    	studentCourse.setStudyTestEndTime(new Date());
				    }
				    if(studentCourse.getStudyStatus().equals("4")) {
				    	studentCourse.setRegRejectTime(new Date());
				    }

			        studentCourseManageService.save(studentCourse);
				}
			}
		}
	}
    /**
	 * インポトcheck
	 */
	public void studentCourseImportCheck(AdminStudentCourseManageExportDto adminStudenCoursetDto) {

    	CourseEntity course = courseService.getById(adminStudenCoursetDto.getCourseId());
    	if(course == null) {
    		throw new RRException("講座は存在しません！");
    	}

    	QueryWrapper<StudentCourseManageEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("course_id", adminStudenCoursetDto.getCourseId());
    	queryWrapper.eq("student_id", adminStudenCoursetDto.getStudentId());
    	int count = studentCourseManageService.count(queryWrapper);
    	if(count > 0) {
    		throw new RRException("レコードはデータベースに既に存在します！");
    	}

    	QueryWrapper<CourseOpenInfoEntity> queryWrapperCourseOpen = new QueryWrapper<>();
    	queryWrapperCourseOpen.eq("group_id", adminStudenCoursetDto.getGroupId());

    	List<CourseOpenInfoEntity> couresOpenList = courseOpenInfoService.list(queryWrapperCourseOpen);
    	if(couresOpenList == null || couresOpenList.size() ==0) {
    		throw new RRException("権限以外の講座を追加できません！");
    	}else {
    		boolean courseFlag = false;
    		for(CourseOpenInfoEntity courseOpenInfoEntity : couresOpenList) {
        		if(courseOpenInfoEntity.getCourseId().equals(adminStudenCoursetDto.getCourseId())) {
        			courseFlag = true;
        			break;
        		}
    		}
    		if(!courseFlag) {
    			throw new RRException("権限以外の講座を追加できません！");
    		}
    	}

    	String groupType = getGroupType(CommonServiceImpl.getUser().getGroupId());
    	if(groupType == "2") {
        	UserGroupEntity userGroup = userGroupService.getById(adminStudenCoursetDto.getGroupId());
        	if(!userGroup.getGroupIdOf().equals(CommonServiceImpl.getUser().getGroupId())) {
        		throw new RRException("権限以外のクラスユーザを追加できません！");
        	}
    	}

	}
    /**
    * 获取グループタイプ
    */
    public String getGroupType(long groupId) {
	   	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
	   	queryWrapper.select("group_type").eq("group_id", groupId);

	   	String groupType = userGroupService.getOne(queryWrapper).getGroupType();
		return groupType;
    }

	public PageUtils queryStudentCourseManageForSta(StudentCourseManageQueryRequest queryRequest) {
		SysUserEntity userEntity = CommonServiceImpl.getUser();
		String groupType = userGroupService.getById(userEntity.getGroupId()).getGroupType();
		QueryStudentCourseManageDto queryStudentCourseManageDto = new QueryStudentCourseManageDto();
		queryStudentCourseManageDto.setCourseId(queryRequest.getCourseId());
		queryStudentCourseManageDto.setCourseName(queryRequest.getCourseName());
		queryStudentCourseManageDto.setGroupId(userEntity.getGroupId());
		queryStudentCourseManageDto.setGroupType(groupType);

		long curPage = 1;
		long limit = 10;
		if(!StringUtils.isEmpty(queryRequest.getPage())){
			curPage = Long.parseLong(queryRequest.getPage());
		}
		if(!StringUtils.isEmpty(queryRequest.getLimit())){
			limit = Long.parseLong(queryRequest.getLimit());
		}

		List<QueryStudentCourseManageRespDto> records = baseMapper.queryStudentCourseManageForSta(
				queryStudentCourseManageDto);

		LinkedHashMap<String, StudentCourseManageQueryResponse> linkedHashMap = new LinkedHashMap<>();
		for (QueryStudentCourseManageRespDto record : records) {
			String key = record.getCourseId() + record.getStudyStatus() + record.getGroupId();
			if (!linkedHashMap.containsKey(key)) {
				StudentCourseManageQueryResponse ins = new StudentCourseManageQueryResponse();
				BeanUtils.copyProperties(record, ins);
				ins.setStudyCount(1);
				linkedHashMap.put(key, ins);
				ins.setStudyCountAll(record.getUserMaxNum());
			} else {
				StudentCourseManageQueryResponse ins = linkedHashMap.get(key);
				ins.setStudyCount(ins.getStudyCount() + 1);
			}
		}
		List<StudentCourseManageQueryResponse> studentCourseManageQueryResponseList = new ArrayList<>(linkedHashMap.values());
		List<StudentCourseManageQueryResponse> resultList = studentCourseManageQueryResponseList.stream()
				.peek(ins-> ins.setStudyCountPercent(ins.getStudyCount()/(float)ins.getStudyCountAll()))
				.collect(Collectors.toList());
		IPage<StudentCourseManageQueryResponse> resultPage = new Page<>(curPage, limit);

		int resultListSize =  resultList.size();
		List<StudentCourseManageQueryResponse> resultRecords = new ArrayList<StudentCourseManageQueryResponse>();

		if(resultListSize - (int)(limit*(curPage-1)) < (int)limit) {
			resultRecords = resultList.subList((int)(limit*(curPage-1)), resultList.size());
		}else {
			resultRecords = resultList.subList((int)(limit*(curPage-1)), (int)(limit*curPage));
		}

		resultPage.setRecords(resultRecords);
		resultPage.setCurrent(curPage);
		resultPage.setTotal(resultList.size());
		return new PageUtils(resultPage);
	}

	@Override
	public PageUtils queryStudentCourseManageForStudentSta(StudentCourseManageQueryForStudentStaRequest queryRequest) {
		SysUserEntity userEntity = CommonServiceImpl.getUser();
		String groupType = userGroupService.getById(userEntity.getGroupId()).getGroupType();

		QueryStudentCourseManageForStudentStaDto queryStudentCourseManageDto = new QueryStudentCourseManageForStudentStaDto();
		BeanUtils.copyProperties(queryRequest, queryStudentCourseManageDto);

		queryStudentCourseManageDto.setSelfGroupId(userEntity.getGroupId());
		queryStudentCourseManageDto.setGroupType(groupType);

		long curPage = 1;
		long limit = 10;
		if(!StringUtils.isEmpty(queryRequest.getPage())){
			curPage = Long.parseLong(queryRequest.getPage());
		}
		if(!StringUtils.isEmpty(queryRequest.getLimit())){
			limit = Long.parseLong(queryRequest.getLimit());
		}
		Page<QueryStudentCourseManageForStudentStaRespDto> page = new Page<>(curPage, limit);
		IPage<QueryStudentCourseManageForStudentStaRespDto> pageList = baseMapper.queryStudentCourseManageForStudentSta(
				page, queryStudentCourseManageDto);

		return new PageUtils(pageList);

	}

    /**
    * 受講者別テスト状況管理
    */
	@Override
	public PageUtils queryStudentUnitTestManage(StudentCourseManageQueryForStudentStaRequest queryRequest) {
		SysUserEntity userEntity = CommonServiceImpl.getUser();
		String groupType = userGroupService.getById(userEntity.getGroupId()).getGroupType();

		QueryStudentCourseManageForStudentStaDto queryStudentCourseManageDto = new QueryStudentCourseManageForStudentStaDto();
		BeanUtils.copyProperties(queryRequest, queryStudentCourseManageDto);

		queryStudentCourseManageDto.setSelfGroupId(userEntity.getGroupId());
		queryStudentCourseManageDto.setGroupType(groupType);

		long curPage = 1;
		long limit = 10;
		if(!StringUtils.isEmpty(queryRequest.getPage())){
			curPage = Long.parseLong(queryRequest.getPage());
		}
		if(!StringUtils.isEmpty(queryRequest.getLimit())){
			limit = Long.parseLong(queryRequest.getLimit());
		}
		Page<QueryStudentCourseManageForStudentStaRespDto> page = new Page<>(curPage, limit);
		IPage<QueryStudentCourseManageForStudentStaRespDto> pageList = baseMapper.queryStudentUnitTestManage(
				page, queryStudentCourseManageDto);

		return new PageUtils(pageList);

	}


}