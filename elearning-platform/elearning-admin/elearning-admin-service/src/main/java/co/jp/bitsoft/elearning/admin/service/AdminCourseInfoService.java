package co.jp.bitsoft.elearning.admin.service;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.CourseInfoEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface AdminCourseInfoService extends IService<CourseInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}
