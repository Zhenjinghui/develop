package co.jp.bitsoft.elearning.admin.service.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.AdminStudentDto;

/**
 * 受講者
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminStudentDao extends BaseMapper<AdminStudentDto> {

	/**
	 * 查询一般用户(分页)
	 */
	IPage<AdminStudentDto> queryStudent(Page<AdminStudentDto> page,Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询一般用户(无分页)
	 */
	List<AdminStudentDto> queryStudent(Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

}
