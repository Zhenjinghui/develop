package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminUnitTestDto;
import co.jp.bitsoft.elearning.admin.service.AdminUnitTestService;
import co.jp.bitsoft.elearning.admin.service.dao.AdminUnitTestDao;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.service.TestQuestionsChoicesService;
import co.jp.bitsoft.elearning.core.service.TestQuestionsService;
import co.jp.bitsoft.elearning.core.service.UnitTestService;


@Service("AdminUnitTestService")
public class AdminUnitTestServiceImpl extends ServiceImpl<AdminUnitTestDao, AdminUnitTestDto> implements AdminUnitTestService{

    @Autowired
    private UnitTestService unitTestService;
    @Autowired
    private TestQuestionsService testQuestionsService;
    @Autowired
    private TestQuestionsChoicesService testQuestionsChoicesService;

    @Override
    /**
    * リスト一覧
    */
    public PageUtils queryPage(Map<String, Object> params) {
		String unitId = (String)params.get("unitId");

        IPage<UnitTestEntity> page = unitTestService.page(
                new Query<UnitTestEntity>().getPage(params),
                new QueryWrapper<UnitTestEntity>()
                .eq("unit_id",Integer.parseInt(unitId))
        );

        return new PageUtils(page);
    }

    /**
    * 単元テスト一覧検索
    */
    public PageUtils selectUnitTestPage(Map<String, Object> params) {
        //分页参数
        long curPage = 1;
        long limit = 10;
        if(params.get(Constant.PAGE) != null){
            curPage = Long.parseLong((String)params.get(Constant.PAGE));
        }
        if(params.get(Constant.LIMIT) != null){
            limit = Long.parseLong((String)params.get(Constant.LIMIT));
        }
		Page<AdminCourseDto> page = new Page<>(curPage, limit);

		IPage<AdminCourseDto> pageList = baseMapper.selectUnitTestPage(page,params);

		return new PageUtils(pageList);
    }

    /**
     * 追加
     */
    public void unitTestSave(AdminUnitTestDto adminUnitTestDto) {

		ValidatorUtils.validateEntity(adminUnitTestDto, AddGroup.class);

		UnitTestEntity unitTest = new UnitTestEntity();
        BeanUtils.copyProperties(adminUnitTestDto, unitTest);

		unitTestService.save(unitTest);


    }
    /**
     * 更新
     */
    public void unitTestUpdate(AdminUnitTestDto adminUnitTestDto) {

    	if(adminUnitTestDto.getTestConditionFlag().equals("0")) {
    		adminUnitTestDto.setTestConditionTime(0);
    	}

		ValidatorUtils.validateEntity(adminUnitTestDto, UpdateGroup.class);

		UnitTestEntity unitTest = new UnitTestEntity();
        BeanUtils.copyProperties(adminUnitTestDto, unitTest);

        unitTestService.updateById(unitTest);


    }
    /**
     * 削除
     */
    public void unitTestDelete(Long[] unitTestIds) {

    	for(long id : unitTestIds) {
    		unitTestService.removeById(id);
    		testQuestionsService.update(new UpdateWrapper<TestQuestionsEntity>().set("del_flag", "1").eq("unit_test_id", id));
    		testQuestionsChoicesService.update(new UpdateWrapper<TestQuestionsChoicesEntity>().set("del_flag", "1").eq("unit_test_id", id));
    	}

    }


}