package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import co.jp.bitsoft.elearning.admin.dto.AdminUnitDto;
import co.jp.bitsoft.elearning.admin.service.AdminUnitService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import co.jp.bitsoft.elearning.core.entity.TeachingMaterialEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;
import co.jp.bitsoft.elearning.core.entity.UnitEntity;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.service.TeachingMaterialService;
import co.jp.bitsoft.elearning.core.service.TestQuestionsChoicesService;
import co.jp.bitsoft.elearning.core.service.TestQuestionsService;
import co.jp.bitsoft.elearning.core.service.UnitService;
import co.jp.bitsoft.elearning.core.service.UnitTestService;


@Service("AdminUnitService")
public class AdminUnitServiceImpl implements AdminUnitService {
	@Autowired
    private UnitService unitService;
    @Autowired
    private UnitTestService unitTestService;
    @Autowired
    private TeachingMaterialService TeachingMaterialService;
    @Autowired
    private TestQuestionsService testQuestionsService;
    @Autowired
    private TestQuestionsChoicesService testQuestionsChoicesService;

    /**
    * リスト一覧
    */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
		String subjectId = (String)params.get("subjectId");
		String unitName = (String)params.get("unitName");
        IPage<UnitEntity> page = unitService.page(
                new Query<UnitEntity>().getPage(params),
                new QueryWrapper<UnitEntity>()
                .eq("subject_id",Integer.parseInt(subjectId))
                .like(StringUtils.isNotBlank(unitName),"unit_name", unitName)
                .orderByAsc("disp_no")
        );

        return new PageUtils(page);
    }

    /**
     * 追加
     */
    public void unitSave(AdminUnitDto adminUnitDto) {

		ValidatorUtils.validateEntity(adminUnitDto, AddGroup.class);

		UnitEntity unit = new UnitEntity();
        BeanUtils.copyProperties(adminUnitDto, unit);

        unitService.save(unit);


    }
    /**
     * 更新
     */
    public void unitUpdate(AdminUnitDto adminUnitDto) {

		ValidatorUtils.validateEntity(adminUnitDto, UpdateGroup.class);

		UnitEntity unit = new UnitEntity();
        BeanUtils.copyProperties(adminUnitDto, unit);

        unitService.updateById(unit);


    }
    /**
     * 削除
     */
    public void unitDelete(Long[] unitIds) {

    	for(long id : unitIds) {
    		unitService.removeById(id);
    		unitTestService.update(new UpdateWrapper<UnitTestEntity>().set("del_flag", "1").eq("subject_id", id));
    		TeachingMaterialService.update(new UpdateWrapper<TeachingMaterialEntity>().set("del_flag", "1").eq("subject_id", id));
    		testQuestionsService.update(new UpdateWrapper<TestQuestionsEntity>().set("del_flag", "1").eq("subject_id", id));
    		testQuestionsChoicesService.update(new UpdateWrapper<TestQuestionsChoicesEntity>().set("del_flag", "1").eq("subject_id", id));
    	}

    }


}