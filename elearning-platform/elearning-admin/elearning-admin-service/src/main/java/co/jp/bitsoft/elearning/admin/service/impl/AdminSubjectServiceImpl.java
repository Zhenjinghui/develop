package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import co.jp.bitsoft.elearning.admin.dto.AdminSubjectDto;
import co.jp.bitsoft.elearning.admin.service.AdminSubjectService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import co.jp.bitsoft.elearning.core.entity.SubjectEntity;
import co.jp.bitsoft.elearning.core.entity.TeachingMaterialEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;
import co.jp.bitsoft.elearning.core.entity.UnitEntity;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.service.SubjectService;
import co.jp.bitsoft.elearning.core.service.TeachingMaterialService;
import co.jp.bitsoft.elearning.core.service.TestQuestionsChoicesService;
import co.jp.bitsoft.elearning.core.service.TestQuestionsService;
import co.jp.bitsoft.elearning.core.service.UnitService;
import co.jp.bitsoft.elearning.core.service.UnitTestService;


@Service("AdminSubjectService")
public class AdminSubjectServiceImpl implements AdminSubjectService {

	@Autowired
    private SubjectService subjectService;
    @Autowired
    private UnitService unitService;
    @Autowired
    private UnitTestService unitTestService;
    @Autowired
    private TeachingMaterialService TeachingMaterialService;
    @Autowired
    private TestQuestionsService testQuestionsService;
    @Autowired
    private TestQuestionsChoicesService testQuestionsChoicesService;

    @Override
    /**
     * リスト一覧
     */
    public PageUtils queryPage(Map<String, Object> params) {
		String courseId = (String)params.get("courseId");
		String subjectName = (String)params.get("subjectName");
        IPage<SubjectEntity> page = subjectService.page(
                new Query<SubjectEntity>().getPage(params),
                new QueryWrapper<SubjectEntity>()
                .eq("course_id",Integer.parseInt(courseId))
                .like(StringUtils.isNotBlank(subjectName),"subject_title", subjectName)
                .orderByAsc("disp_no")
        );

        return new PageUtils(page);
    }
    /**
     * 追加
     */
    public void subjectSave(AdminSubjectDto adminsubjectDto) {

		ValidatorUtils.validateEntity(adminsubjectDto, AddGroup.class);

		SubjectEntity subject = new SubjectEntity();
        BeanUtils.copyProperties(adminsubjectDto, subject);

		subjectService.save(subject);


    }
    /**
     * 更新
     */
    public void subjectUpdate(AdminSubjectDto adminsubjectDto) {

		ValidatorUtils.validateEntity(adminsubjectDto, UpdateGroup.class);

    	SubjectEntity subject = new SubjectEntity();
        BeanUtils.copyProperties(adminsubjectDto, subject);

		subjectService.updateById(subject);


    }
    /**
     * 削除
     */
    public void subjectDelete(Long[] subjectIds) {

    	for(long id : subjectIds) {
    		subjectService.removeById(id);
    		unitService.update(new UpdateWrapper<UnitEntity>().set("del_flag", "1").eq("subject_id", id));
    		unitTestService.update(new UpdateWrapper<UnitTestEntity>().set("del_flag", "1").eq("subject_id", id));
    		TeachingMaterialService.update(new UpdateWrapper<TeachingMaterialEntity>().set("del_flag", "1").eq("subject_id", id));
    		testQuestionsService.update(new UpdateWrapper<TestQuestionsEntity>().set("del_flag", "1").eq("subject_id", id));
    		testQuestionsChoicesService.update(new UpdateWrapper<TestQuestionsChoicesEntity>().set("del_flag", "1").eq("subject_id", id));
    	}

    }

}