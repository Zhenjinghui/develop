/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.modules.sys.entity.SysRoleMenuEntity;

import java.util.List;



/**
 * 角色与菜单对应关系
 *
 * @author  BitSoft
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {
	
	void saveOrUpdate(Long roleId, List<Long> menuIdList);
	
	/**
	 * 根据角色ID，获取菜单ID列表
	 */
	List<Long> queryMenuIdList(Long roleId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
	
}
