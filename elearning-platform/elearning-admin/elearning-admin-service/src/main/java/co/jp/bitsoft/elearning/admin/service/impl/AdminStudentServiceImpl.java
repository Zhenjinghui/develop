package co.jp.bitsoft.elearning.admin.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentExportDto;
import co.jp.bitsoft.elearning.admin.service.AdminStudentService;
import co.jp.bitsoft.elearning.admin.service.dao.AdminStudentDao;
import co.jp.bitsoft.elearning.common.exception.RRException;
import co.jp.bitsoft.elearning.common.mail.CommonMailUtils;
import co.jp.bitsoft.elearning.common.utils.Common;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import co.jp.bitsoft.elearning.core.entity.AdAddressEntity;
import co.jp.bitsoft.elearning.core.entity.StudentCourseManageEntity;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.AdAddressService;
import co.jp.bitsoft.elearning.core.service.StudentCourseManageService;
import co.jp.bitsoft.elearning.core.service.StudentService;
import co.jp.bitsoft.elearning.core.service.UserGroupService;


@Service("AdminStudentService")
public class AdminStudentServiceImpl extends ServiceImpl<AdminStudentDao, AdminStudentDto> implements AdminStudentService {

    @Autowired
    private StudentService studentService;
    @Autowired
    private UserGroupService userGroupService;
    @Autowired
    private AdAddressService adAddressService;
    @Autowired
    private StudentCourseManageService studentCourseManageService;
	@Autowired
	private CommonMailUtils commonMailUtils;
	// 半角[#]定義 番号関連の「#」挿入を導入対応
	private static final String DEF_SHARP = "#";
    /**
	 * 查询一般用户(分页)
	 */
	public PageUtils queryStudent(Map<String, Object> params) {
        //分页参数
        long curPage = 1;
        long limit = 10;
        if(params.get(Constant.PAGE) != null){
            curPage = Long.parseLong((String)params.get(Constant.PAGE));
        }
        if(params.get(Constant.LIMIT) != null){
            limit = Long.parseLong((String)params.get(Constant.LIMIT));
        }
		Page<AdminStudentDto> page = new Page<>(curPage, limit);

        //group限制
   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);

		IPage<AdminStudentDto> pageList = baseMapper.queryStudent(page,params,groupIdList);

		return new PageUtils(pageList);
    }

	/**
	 * 导出
	 */
	public List<AdminStudentExportDto> queryAllStudent(Map<String, Object> params) {

        //group限制
   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
        List<AdminStudentDto> studentList = baseMapper.queryStudent(params,groupIdList);

        List<AdminStudentExportDto> adminStudentExportList = new ArrayList<AdminStudentExportDto>();

        for(AdminStudentDto adminStudentDto : studentList) {
        	AdminStudentExportDto adminStudentExportDto = new AdminStudentExportDto();
     	    BeanUtils.copyProperties(adminStudentDto, adminStudentExportDto);

     	    if("0".equals(adminStudentDto.getSex())) {
     	    	adminStudentExportDto.setSex("男性");
     	    }else if("1".equals(adminStudentDto.getSex())) {
     	    	adminStudentExportDto.setSex("女性");
     	    }

     	    if("1".equals(adminStudentDto.getAccountStatus())) {
     	    	adminStudentExportDto.setAccountStatus("利用中");
     	    }else if("2".equals(adminStudentDto.getAccountStatus())) {
     	    	adminStudentExportDto.setAccountStatus("利用停止");
     	    }else if("0".equals(adminStudentDto.getAccountStatus())) {
     	    	adminStudentExportDto.setAccountStatus("仮登録済");
     	    }

     	    if("0".equals(adminStudentDto.getProfession())) {
     	    	adminStudentExportDto.setProfession("会社員");
     	    }else if("1".equals(adminStudentDto.getProfession())) {
     	    	adminStudentExportDto.setProfession("公務員");
     	    }else if("2".equals(adminStudentDto.getProfession())) {
     	    	adminStudentExportDto.setProfession("自営業");
     	    }else if("3".equals(adminStudentDto.getProfession())) {
     	    	adminStudentExportDto.setProfession("会社役員・経営者");
     	    }else if("4".equals(adminStudentDto.getProfession())) {
     	    	adminStudentExportDto.setProfession("専業主婦（夫)");
     	    }else if("5".equals(adminStudentDto.getProfession())) {
     	    	adminStudentExportDto.setProfession("学生");
     	    }else if("6".equals(adminStudentDto.getProfession())) {
     	    	adminStudentExportDto.setProfession("パート・アルバイト");
     	    }else if("7".equals(adminStudentDto.getProfession())) {
     	    	adminStudentExportDto.setProfession("無職");
     	    }else {
     	    	adminStudentExportDto.setProfession("その他");
     	    }
     	    // 電話番号
			if (!"".equals(adminStudentDto.getContactTel())) {
				adminStudentExportDto.setContactTel(Common.getFormattedImpNumber(adminStudentDto.getContactTel()));
			}
			// 携帯番号
			if (!"".equals(adminStudentDto.getContactMobile())) {
				adminStudentExportDto.setContactMobile(Common.getFormattedImpNumber(adminStudentDto.getContactMobile()));
			}
			// FAX番号
			if (!"".equals(adminStudentDto.getContactFax())) {
				adminStudentExportDto.setContactFax(Common.getFormattedImpNumber(adminStudentDto.getContactFax()));
			}
			// 郵便番号
			if (!"".equals(adminStudentDto.getAddressZipCode())) {
				adminStudentExportDto.setAddressZipCode(Common.getFormattedImpNumber(adminStudentDto.getAddressZipCode()));
			}
     	   adminStudentExportList.add(adminStudentExportDto);
        }
		return adminStudentExportList;

    }
    /**
     * 情報
     */
	public AdminStudentDto studentInfo(Long studentId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("studentId", studentId);
		List<Long> groupIdList = null;
        List<AdminStudentDto> studentList = baseMapper.queryStudent(params,groupIdList);
        AdminStudentDto student = new AdminStudentDto();
        if(studentList.size()>0) {
        	student = studentList.get(0);
        }

        return student;
	}
    /**
     * 保存+送信
     */
	public void studentSave(AdminStudentDto adminStudentDto) {

		studentInsert(adminStudentDto);
		// https://hikaru.atlassian.net/browse/STUDYPACK-31
		sendEmail(adminStudentDto.getUserName(), adminStudentDto.getUserNameKanji(),adminStudentDto.getEmail());

	}
    /**
     * 保存
     */
	public void studentInsert(AdminStudentDto adminStudentDto) {
    	checkGroup(adminStudentDto);

    	UserGroupEntity userGroup = userGroupService.getById(adminStudentDto.getGroupId());
	   	QueryWrapper<StudentEntity> studentQueryWrapper = new QueryWrapper<>();
    	studentQueryWrapper.eq("group_id", adminStudentDto.getGroupId());
    	int count = studentService.count(studentQueryWrapper);

    	if (count >= userGroup.getUserMaxNum()) {
			throw new RRException("契約受講者数に達しているため登録できませんでした。");
    	}

		String salt = RandomStringUtils.randomAlphanumeric(20);
		adminStudentDto.setUserPassword(new Sha256Hash("student", salt).toHex());
		adminStudentDto.setUserSalt(salt);

		insertCheckUserName(adminStudentDto.getUserName());
//		insertCheckEmail(adminStudentDto.getEmail());
		ValidatorUtils.validateEntity(adminStudentDto, AddGroup.class);

    	StudentEntity student = new StudentEntity();
        BeanUtils.copyProperties(adminStudentDto, student);

		studentService.save(student);
	}
    /**
     * 送信
     */
	public void sendEmail(String userName,String userNameKanji, String email){
    	// htmlテンプレートファイルを読む
		String buffer = null;
		try {
			buffer = commonMailUtils.readHtml("student_new.html");
		} catch (Exception e) {
			throw new RRException("htmlテンプレートファイルの読み取りに失敗しました！");
		}

        String htmlText = MessageFormat.format(buffer, userNameKanji, userName, "student");

        boolean mailSendFlag = commonMailUtils.sendHtmlMail(email, "【Study Pack】ユーザー登録完了メール", htmlText);
    	if(!mailSendFlag) {
			throw new RRException("メールの送信に失敗しました！");
    	}
	}


    /**
     * 更新
     */
	public void studentUpdate(AdminStudentDto adminStudentDto) {
    	checkGroup(adminStudentDto);

    	UserGroupEntity userGroup = userGroupService.getById(adminStudentDto.getGroupId());
	   	QueryWrapper<StudentEntity> studentQueryWrapper = new QueryWrapper<>();
    	studentQueryWrapper.eq("group_id", adminStudentDto.getGroupId());
    	int count = studentService.count(studentQueryWrapper);

    	if (count >= userGroup.getUserMaxNum()) {
			throw new RRException("契約受講者数に達しているため登録できませんでした。");
    	}

		ValidatorUtils.validateEntity(adminStudentDto, UpdateGroup.class);
		updateCheckUserName(adminStudentDto);
//		updateCheckEmail(adminStudentDto);

    	StudentEntity student = new StudentEntity();
        BeanUtils.copyProperties(adminStudentDto, student);

		studentService.updateById(student);
	}
	/**
	 * 用户インポト
	 */
	@SuppressWarnings("unchecked")
	public void studentImport(JSONObject csvData) {

		String jsonStr = csvData.get("csvJson").toString();
		List<Object> csvDataRow = JSONArray.parseArray(jsonStr);
		List<String> rowList = new ArrayList<String>();
		AdminStudentDto adminStudentDto;
		if(csvDataRow.size()>1) {
			for(int i= 0;i< csvDataRow.size();i++) {
				if(i>0) {
					rowList = (List<String>) csvDataRow.get(i);

					// データが存在しない場合や項目数が２2個ではない場合、処理を抜ける
					if (rowList == null || rowList.size() != 22) {
						throw new RRException("一般ユーザーCSVファイル形式エラー。（"+i+"行目）");
					}
					adminStudentDto = new AdminStudentDto();
					adminStudentDto.setUserName(rowList.get(1));
					adminStudentDto.setUserNameKanji(rowList.get(2));
					adminStudentDto.setUserNameKana(rowList.get(3));
					adminStudentDto.setNickName(rowList.get(4));
					adminStudentDto.setGroupId(Long.parseLong(rowList.get(7)));
					if ("男性".equals(rowList.get(9))) {
						adminStudentDto.setSex("0");
					} else {
						adminStudentDto.setSex("1");
					}
					adminStudentDto.setBirthday(rowList.get(10));
						if("会社員".equals(rowList.get(11))) {
						adminStudentDto.setProfession("0");
					}else if("公務員".equals(rowList.get(11))) {
						adminStudentDto.setProfession("1");
					}else if("自営業".equals(rowList.get(11))) {
						adminStudentDto.setProfession("2");
					}else if("会社役員・経営者".equals(rowList.get(11))) {
						adminStudentDto.setProfession("3");
					}else if("専業主婦（夫)".equals(rowList.get(11))) {
						adminStudentDto.setProfession("4");
					}else if("学生".equals(rowList.get(11))) {
						adminStudentDto.setProfession("5");
					}else if("パート・アルバイト".equals(rowList.get(11))) {
						adminStudentDto.setProfession("6");
					}else if("無職".equals(rowList.get(11))) {
						adminStudentDto.setProfession("7");
					}else {
						adminStudentDto.setProfession("8");
					}
					// #を抜く
					adminStudentDto.setAddressZipCode(rowList.get(12).replaceAll(DEF_SHARP, ""));
					adminStudentDto.setAddressPrefecture(rowList.get(13));
					adminStudentDto.setAddressCity(rowList.get(14));
					adminStudentDto.setAddressOthers(rowList.get(15));
					adminStudentDto.setAddressBuildingName(rowList.get(16));
					adminStudentDto.setEmail(rowList.get(17));
					// #を抜く
					adminStudentDto.setContactMobile(rowList.get(18).replaceAll(DEF_SHARP, ""));
					adminStudentDto.setContactTel(rowList.get(19).replaceAll(DEF_SHARP, ""));
					adminStudentDto.setContactFax(rowList.get(20).replaceAll(DEF_SHARP, ""));

					if("利用中".equals(rowList.get(21))) {
						adminStudentDto.setAccountStatus("1");
					}else if("利用停止".equals(rowList.get(21))) {
						adminStudentDto.setAccountStatus("2");
					}else if("仮登録済".equals(rowList.get(21))) {
						adminStudentDto.setAccountStatus("0");
					}
					if(StringUtils.isBlank(rowList.get(0))) {
						ValidatorUtils.validateEntity(adminStudentDto, AddGroup.class);
						studentInsert(adminStudentDto);
					}else {
						adminStudentDto.setStudentId(Long.parseLong(rowList.get(0)));
						ValidatorUtils.validateEntity(adminStudentDto, UpdateGroup.class);
						studentUpdate(adminStudentDto);
					}
				}
			}
		}

		if(csvDataRow.size()>1) {
			for(int i= 0;i< csvDataRow.size();i++) {
				if(i>0) {
					rowList = (List<String>) csvDataRow.get(i);
					// データが存在しない場合や項目数が２2個ではない場合、処理を抜ける
					if (rowList == null || rowList.size() != 22) {
						throw new RRException("一般ユーザーCSVファイル形式エラー。（"+i+"行目）");
					}

					if(StringUtils.isBlank(rowList.get(0))) {
						// https://hikaru.atlassian.net/browse/STUDYPACK-31
						//sendEmail(rowList.get(1),rowList.get(17));
						sendEmail(rowList.get(1), rowList.get(2), rowList.get(17));
					}
				}
			}
		}
	}
    /**
     * 削除
     */
	public void studentDelete(Long[] studentIds) {
		studentService.removeByIds(Arrays.asList(studentIds));
		for(Long id : studentIds) {
			studentCourseManageService.update(new UpdateWrapper<StudentCourseManageEntity>().set("del_flag", "1").eq("student_id", id));

		}

	}

    /**
     * 郵便番号
     */
    public AdAddressEntity checkZipCode(String zipCode){
    	QueryWrapper<AdAddressEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("zip", zipCode);
		List<AdAddressEntity> adAddress = adAddressService.list(queryWrapper);
		if(adAddress == null) {
			throw new RRException("郵便番号が正しくありません！");
		}
		return adAddress.get(0);
    }
	/**
	 * 插入时检查角色名是否被使用
	 */
	private void insertCheckUserName(String userName){

    	QueryWrapper<StudentEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("user_name", userName);
		int count = studentService.count(queryWrapper);
		if(count > 0){
			throw new RRException("ユーザ名が使用されました！");
		}
	}

	/**
	 * 更新时检查角色名是否被使用
	 */
	private void updateCheckUserName(AdminStudentDto user){
		if(user.getStudentId()!=null) {
			StudentEntity studentEntity = studentService.getById(user.getStudentId());
			if(studentEntity.getUserName().equals(user.getUserName())) {
				return;
			}else {
				insertCheckUserName(user.getUserName());
			}
		}else {
			insertCheckUserName(user.getUserName());
		}
	}

	/**
	 * 插入时检查邮箱是否被使用
	 */
	private void insertCheckEmail(String email){

    	QueryWrapper<StudentEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("email", email);
		int count = studentService.count(queryWrapper);
		if(count > 0){
			throw new RRException("邮箱が使用されました！");
		}
	}

	/**
	 * 更新时检查邮箱是否被使用
	 */
	private void updateCheckEmail(AdminStudentDto user){
		if(user.getStudentId()!=null) {
			StudentEntity studentEntity = studentService.getById(user.getStudentId());
			if(studentEntity.getEmail().equals(user.getEmail())) {
				return;
			}else {
				insertCheckEmail(user.getEmail());
			}
		}else {
			insertCheckEmail(user.getEmail());
		}
	}

	/**
	 * 检查group
	 */
	private void checkGroup(AdminStudentDto user){
		Long loginGroupId = CommonServiceImpl.getUser().getGroupId();
		String loginGroupType = getGroupType(loginGroupId);
    	UserGroupEntity userGroup = userGroupService.getById(user.getGroupId());

    	if(userGroup == null) {
    		throw new RRException("クラスは存在しません！");
    	}else {
        	Long groupIdOf = userGroup.getGroupIdOf();
        	if(!"3".equals(userGroup.getGroupType())) {
        		throw new RRException("受講者の所属グループはクラスでなければなりません！");
        	}
        	if("2".equals(loginGroupType)) {
            	if(!loginGroupId.equals(groupIdOf)) {
            		throw new RRException("権限以外のクラスユーザを追加できません！");
            	}
    		}else if("3".equals(loginGroupType)) {
    			if(!loginGroupId.equals(user.getGroupId())) {
            		throw new RRException("権限以外のクラスユーザを追加できません！");
    			}
    			return;
    		}
    	}

	}

    /**
    * 获取クラスタイプ
    */
   public String getGroupType(long groupId) {
	   	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
	   	queryWrapper.select("group_type").eq("group_id", groupId);

	   	String groupType = userGroupService.getOne(queryWrapper).getGroupType();
		return groupType;
   }

}