package co.jp.bitsoft.elearning.admin.service.impl;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseSelectDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDataAnalyseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDataAnalyseUseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDataAnalyseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDataAnalyseExportDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTestDataAnalyseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTestQuestionsDataAnalyseExportDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTestQuestionsDataAnalyseExportSelectDto;
import co.jp.bitsoft.elearning.admin.service.AdminCourseService;
import co.jp.bitsoft.elearning.admin.service.AdminDataAnalyseService;
import co.jp.bitsoft.elearning.admin.service.dao.AdminDataAnalyseDao;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.UserGroupService;


@Service("AdminDataAnalyseService")
public class AdminDataAnalyseServiceImpl extends ServiceImpl<AdminDataAnalyseDao, AdminStudentDataAnalyseUseDto>  implements AdminDataAnalyseService {

	@Autowired
	private UserGroupService userGroupService;

	@Autowired
	private AdminCourseService adminCourseService;

	@Override
    /**
     * クラスドロップダウンボックス
     */
    public List<UserGroupEntity> getClassSelectDrop() {

		// ログインユーザーグループID
		Long loginGroupId =  CommonServiceImpl.getUser().getGroupId();

		// ログインユーザーグループタイプ
	   	QueryWrapper<UserGroupEntity> queryWrapperLogin = new QueryWrapper<>();
	   	queryWrapperLogin.select("group_type").eq("group_id", loginGroupId);
		String loginGroupType = userGroupService.getOne(queryWrapperLogin).getGroupType();

		// クラス
    	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
    	if("1".equals(loginGroupType)) {
    		queryWrapper.select("group_id","group_name").eq("group_type", "3");
		}else if("2".equals(loginGroupType)){
		    queryWrapper.select("group_id","group_name").eq("group_type", "3").eq("group_id_of", loginGroupId);
		}else if("3".equals(loginGroupType)) {
		    queryWrapper.select("group_id","group_name").eq("group_type", "3").eq("group_id", loginGroupId);
		}
		List<UserGroupEntity> groupList = userGroupService.list(queryWrapper);

        return groupList;
    }

	@Override
    /**
     * 講座ドロップダウンボックス
     */
    public List<AdminCourseSelectDto> getCourseSelectDrop() {

		// ログインユーザーグループID
		Long loginGroupId =  CommonServiceImpl.getUser().getGroupId();

		// ログインユーザーグループタイプ
	   	QueryWrapper<UserGroupEntity> queryWrapperLogin = new QueryWrapper<>();
	   	queryWrapperLogin.select("group_type").eq("group_id", loginGroupId);
		String loginGroupType = userGroupService.getOne(queryWrapperLogin).getGroupType();

		List<AdminCourseSelectDto> courseList = new ArrayList<AdminCourseSelectDto>();

		Map<String, Object> params = null;
		// 講座
    	if("1".equals(loginGroupType)) {
    		courseList = adminCourseService.courseSelect(params);
		}else if("2".equals(loginGroupType)){
    		courseList = adminCourseService.courseProxySelect(loginGroupId);
		}else if("3".equals(loginGroupType)) {
    		courseList = adminCourseService.courseClassSelect(loginGroupId);

		}

        return courseList;
    }

	@Override
    /**
     * 受講者傾向
     */
	public AdminStudentDataAnalyseDto getStudentDataAnalyse(Map<String, Object> params){
		if(params.get("courseId")!=null && !params.get("courseId").toString().isEmpty()) {
			params.put("courseId", Long.parseLong((String)params.get("courseId")));
		}
		if(params.get("groupId")!=null && !params.get("groupId").toString().isEmpty()) {
			params.put("groupId", Long.parseLong((String)params.get("groupId")));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(params.get("datePeriodBfore")!=null && !params.get("datePeriodBfore").toString().isEmpty()) {
				params.put("datePeriodBfore", sdf.parse(params.get("datePeriodBfore").toString()));
			}
			if(params.get("datePeriodAfter")!=null && !params.get("datePeriodAfter").toString().isEmpty()) {
				params.put("datePeriodAfter", sdf.parse(params.get("datePeriodAfter").toString()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
        //group限制
   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);

		// 男女比
		List<AdminStudentDataAnalyseUseDto> studentDataAnalyseMan = baseMapper.getManEcharts(params,groupIdList);
		for(AdminStudentDataAnalyseUseDto adminStudentDataAnalyseUseDto : studentDataAnalyseMan) {
			if("0".equals(adminStudentDataAnalyseUseDto.getName())) {
				adminStudentDataAnalyseUseDto.setName("男性");
			}else if("1".equals(adminStudentDataAnalyseUseDto.getName())){
				adminStudentDataAnalyseUseDto.setName("女性");
			}else {
				adminStudentDataAnalyseUseDto.setName("不明");
			}
		}
		// 職業構成
		List<AdminStudentDataAnalyseUseDto> studentDataAnalyseWork = baseMapper.getWorkEcharts(params,groupIdList);
		for(AdminStudentDataAnalyseUseDto adminStudentDataAnalyseUseDto : studentDataAnalyseWork) {
			if("0".equals(adminStudentDataAnalyseUseDto.getName())) {
				adminStudentDataAnalyseUseDto.setName("会社員");
			}else if("1".equals(adminStudentDataAnalyseUseDto.getName())){
				adminStudentDataAnalyseUseDto.setName("公務員");
			}else if("2".equals(adminStudentDataAnalyseUseDto.getName())){
				adminStudentDataAnalyseUseDto.setName("自営業");
			}else if("3".equals(adminStudentDataAnalyseUseDto.getName())){
			adminStudentDataAnalyseUseDto.setName("会社役員・経営者");
			}else if("4".equals(adminStudentDataAnalyseUseDto.getName())){
				adminStudentDataAnalyseUseDto.setName("専業主婦（夫）");
			}else if("5".equals(adminStudentDataAnalyseUseDto.getName())){
				adminStudentDataAnalyseUseDto.setName("学生");
			}else if("6".equals(adminStudentDataAnalyseUseDto.getName())){
				adminStudentDataAnalyseUseDto.setName("パート・アルバイト");
			}else if("7".equals(adminStudentDataAnalyseUseDto.getName())){
				adminStudentDataAnalyseUseDto.setName("無職");
			}else if("8".equals(adminStudentDataAnalyseUseDto.getName())){
				adminStudentDataAnalyseUseDto.setName("その他");
			}else {
				adminStudentDataAnalyseUseDto.setName("不明");
			}
		}

		// 年齢層
		List<AdminStudentDto> studentBrithdayList= baseMapper.getAgeEcharts(params,groupIdList);
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);

		int age0 = 0;
		int age10 = 0;
		int age20 = 0;
		int age30 = 0;
		int age40 = 0;
		int age50 = 0;
		int age60 = 0;
		int ageNull = 0;

		for(AdminStudentDto adminStudentDto : studentBrithdayList) {
			if(adminStudentDto == null || adminStudentDto.getBirthday() == null || adminStudentDto.getBirthday().trim().isEmpty()) {
				ageNull++;
				continue;
			}
			String studentBirthday = adminStudentDto.getBirthday();
			if(studentBirthday.length()>3) {
				int studentYear = Integer.parseInt(studentBirthday.substring(0, 4));
				if(year-studentYear>0 && year-studentYear<10 ) {
					age0++;
				}else if(year-studentYear>=10 && year-studentYear<20) {
					age10++;
				}else if(year-studentYear>=20 && year-studentYear<30) {
					age20++;
				}else if(year-studentYear>=30 && year-studentYear<40) {
					age30++;
				}else if(year-studentYear>=40 && year-studentYear<50) {
					age40++;
				}else if(year-studentYear>=50 && year-studentYear<60) {
					age50++;
				}else if(year-studentYear>=60){
					age60++;
				}
			}else {
				ageNull++;
			}
		}
		List<AdminStudentDataAnalyseUseDto> studentDataAnalyseAge = new ArrayList<AdminStudentDataAnalyseUseDto>();
		AdminStudentDataAnalyseUseDto adminStudentDataAnalyseUseDto= new AdminStudentDataAnalyseUseDto();
		if(!(age0==0)) {
			adminStudentDataAnalyseUseDto.setName("10代以下");
			adminStudentDataAnalyseUseDto.setValue(age0);
			studentDataAnalyseAge.add(adminStudentDataAnalyseUseDto);
		}
		if(!(age10==0)) {
			adminStudentDataAnalyseUseDto= new AdminStudentDataAnalyseUseDto();
			adminStudentDataAnalyseUseDto.setName("10代");
			adminStudentDataAnalyseUseDto.setValue(age10);
			studentDataAnalyseAge.add(adminStudentDataAnalyseUseDto);
		}
		if(!(age20==0)) {
			adminStudentDataAnalyseUseDto= new AdminStudentDataAnalyseUseDto();
			adminStudentDataAnalyseUseDto.setName("20代");
			adminStudentDataAnalyseUseDto.setValue(age20);
			studentDataAnalyseAge.add(adminStudentDataAnalyseUseDto);
		}
		if(!(age30==0)) {
			adminStudentDataAnalyseUseDto= new AdminStudentDataAnalyseUseDto();
			adminStudentDataAnalyseUseDto.setName("30代");
			adminStudentDataAnalyseUseDto.setValue(age30);
			studentDataAnalyseAge.add(adminStudentDataAnalyseUseDto);
		}
		if(!(age40==0)) {
			adminStudentDataAnalyseUseDto= new AdminStudentDataAnalyseUseDto();
			adminStudentDataAnalyseUseDto.setName("40代");
			adminStudentDataAnalyseUseDto.setValue(age40);
			studentDataAnalyseAge.add(adminStudentDataAnalyseUseDto);
		}
		if(!(age50==0)) {
			adminStudentDataAnalyseUseDto= new AdminStudentDataAnalyseUseDto();
			adminStudentDataAnalyseUseDto.setName("50代");
			adminStudentDataAnalyseUseDto.setValue(age50);
			studentDataAnalyseAge.add(adminStudentDataAnalyseUseDto);
		}
		if(!(age60==0)) {
			adminStudentDataAnalyseUseDto= new AdminStudentDataAnalyseUseDto();
			adminStudentDataAnalyseUseDto.setName("60代以上");
			adminStudentDataAnalyseUseDto.setValue(age60);
			studentDataAnalyseAge.add(adminStudentDataAnalyseUseDto);
		}
		if(!(ageNull==0)) {
			adminStudentDataAnalyseUseDto= new AdminStudentDataAnalyseUseDto();
			adminStudentDataAnalyseUseDto.setName("不明");
			adminStudentDataAnalyseUseDto.setValue(ageNull);
			studentDataAnalyseAge.add(adminStudentDataAnalyseUseDto);
		}
		AdminStudentDataAnalyseDto  studentDataAnalyse= new AdminStudentDataAnalyseDto();
		studentDataAnalyse.setManEcharts(studentDataAnalyseMan);
		studentDataAnalyse.setWorkEcharts(studentDataAnalyseWork);
		studentDataAnalyse.setAgeEcharts(studentDataAnalyseAge);
		return studentDataAnalyse;
	}

	@Override
    /**
	 * 受講状況（講座・教材単位）(分页)
	 */
	public PageUtils getTeachingMaterialDataAnalyse(Map<String, Object> params) {
        //分页参数
        long curPage = 1;
        long limit = 10;
        if(params.get(Constant.PAGE) != null){
            curPage = Long.parseLong((String)params.get(Constant.PAGE));
        }
        if(params.get(Constant.LIMIT) != null){
            limit = Long.parseLong((String)params.get(Constant.LIMIT));
        }
		if(params.get("courseId")!=null && !params.get("courseId").toString().isEmpty()) {
			params.put("courseId", Long.parseLong((String)params.get("courseId")));
		}
		if(params.get("groupId")!=null && !params.get("groupId").toString().isEmpty()) {
			params.put("groupId", Long.parseLong((String)params.get("groupId")));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(params.get("datePeriodBfore")!=null && !params.get("datePeriodBfore").toString().isEmpty()) {
				params.put("datePeriodBfore", sdf.parse(params.get("datePeriodBfore").toString()));
			}
			if(params.get("datePeriodAfter")!=null && !params.get("datePeriodAfter").toString().isEmpty()) {
				params.put("datePeriodAfter", sdf.parse(params.get("datePeriodAfter").toString()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Page<AdminTeachingMaterialDataAnalyseDto> page = new Page<>(curPage, limit);

        //group限制
		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
		IPage<AdminTeachingMaterialDataAnalyseDto> pageList = baseMapper.getTeachingMaterialDataAnalyse(page,params,groupIdList);

		List<AdminTeachingMaterialDataAnalyseDto> teachingMaterialList = pageList.getRecords();

		for(AdminTeachingMaterialDataAnalyseDto adminTeachingMaterialDataAnalyseDto : teachingMaterialList ) {
			Map<String, Object> paramsSelect = new HashMap<String, Object>();
			paramsSelect.put("courseId", Long.parseLong(adminTeachingMaterialDataAnalyseDto.getCourseId()));
			paramsSelect.put("subjectId", Long.parseLong(adminTeachingMaterialDataAnalyseDto.getSubjectId()));
			paramsSelect.put("unitId", Long.parseLong(adminTeachingMaterialDataAnalyseDto.getUnitId()));
			paramsSelect.put("teachingMaterialId", Long.parseLong(adminTeachingMaterialDataAnalyseDto.getTeachingMaterialId()));
			paramsSelect.put("groupId", Long.parseLong(adminTeachingMaterialDataAnalyseDto.getGroupId()));

	    	int count = baseMapper.getCompleteCount(paramsSelect);
	    	adminTeachingMaterialDataAnalyseDto.setCompleteNum(count+"人/"+adminTeachingMaterialDataAnalyseDto.getCompleteNumDb()+"人");
	    	NumberFormat numberFormat = NumberFormat.getInstance();
	    	// 设置精确到小数点后2位
	    	numberFormat.setMaximumFractionDigits(2);
	    	String completePercent = numberFormat.format((float)count/(float)adminTeachingMaterialDataAnalyseDto.getCompleteNumDb()*100)+"%";
	    	adminTeachingMaterialDataAnalyseDto.setCompletePercent(completePercent);
		}

		pageList.setRecords(teachingMaterialList);
		return new PageUtils(pageList);
    }

	@Override
    /**
	 * 受講状況（講座・教材単位）エクスポート
	 */
	public List<AdminTeachingMaterialDataAnalyseExportDto> getTeachingMaterialDataAnalyseExport(Map<String, Object> params) {

		if(params.get("courseId")!=null && !params.get("courseId").toString().isEmpty()) {
			params.put("courseId", Long.parseLong((String)params.get("courseId")));
		}
		if(params.get("groupId")!=null && !params.get("groupId").toString().isEmpty()) {
			params.put("groupId", Long.parseLong((String)params.get("groupId")));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(params.get("datePeriodBfore")!=null && !params.get("datePeriodBfore").toString().isEmpty()) {
				params.put("datePeriodBfore", sdf.parse(params.get("datePeriodBfore").toString()));
			}
			if(params.get("datePeriodAfter")!=null && !params.get("datePeriodAfter").toString().isEmpty()) {
				params.put("datePeriodAfter", sdf.parse(params.get("datePeriodAfter").toString()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

        //group限制
		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);

		List<AdminTeachingMaterialDataAnalyseExportDto> teachingMaterialDataAnalyseExportList = baseMapper.getTeachingMaterialDataAnalyseExport(params, groupIdList);
		return teachingMaterialDataAnalyseExportList;

	}

	@Override
    /**
	 * 単元テスト結果(分页)
	 */
	public PageUtils getTestDataAnalyse(Map<String, Object> params) {
        //分页参数
        long curPage = 1;
        long limit = 10;
        if(params.get(Constant.PAGE) != null){
            curPage = Long.parseLong((String)params.get(Constant.PAGE));
        }
        if(params.get(Constant.LIMIT) != null){
            limit = Long.parseLong((String)params.get(Constant.LIMIT));
        }
		if(params.get("courseId")!=null && !params.get("courseId").toString().isEmpty()) {
			params.put("courseId", Long.parseLong((String)params.get("courseId")));
		}
		if(params.get("groupId")!=null && !params.get("groupId").toString().isEmpty()) {
			params.put("groupId", Long.parseLong((String)params.get("groupId")));
		}

		Page<AdminTestDataAnalyseDto> page = new Page<>(curPage, limit);

        //group限制
		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
		IPage<AdminTestDataAnalyseDto> pageList = baseMapper.getTestDataAnalyse(page,params,groupIdList);

		List<AdminTestDataAnalyseDto> testList = pageList.getRecords();

		for(AdminTestDataAnalyseDto adminTestDataAnalyseDto : testList ) {
			Map<String, Object> paramsSelect = new HashMap<String, Object>();
			paramsSelect.put("courseId", Long.parseLong(adminTestDataAnalyseDto.getCourseId()));
			paramsSelect.put("unitTestId", Long.parseLong(adminTestDataAnalyseDto.getUnitTestId()));
			paramsSelect.put("groupId", Long.parseLong(adminTestDataAnalyseDto.getGroupId()));

			String firstAvgCorrectPercent = baseMapper.getFirstAvgCorrectRate(paramsSelect);
			String lastAvgCorrectPercent = baseMapper.getLastAvgCorrectRate(paramsSelect);
			int passCount =  baseMapper.getAvgPassRate(paramsSelect);

	    	NumberFormat numberFormat = NumberFormat.getInstance();
	    	// 设置精确到小数点后2位
	    	numberFormat.setMaximumFractionDigits(2);
	    	String passPercent = numberFormat.format((float)passCount/(float)adminTestDataAnalyseDto.getStudentTestCount()*100)+"%";

	    	adminTestDataAnalyseDto.setFirstAvgCorrectPercent(firstAvgCorrectPercent);
	    	adminTestDataAnalyseDto.setLastAvgCorrectPercent(lastAvgCorrectPercent);
	    	adminTestDataAnalyseDto.setAvgPassPercent(passPercent);
		}

		pageList.setRecords(testList);
		return new PageUtils(pageList);
    }

	@Override
    /**
	 * 単元テスト結果(出題内容)エクスポート
	 */
	public List<AdminTestQuestionsDataAnalyseExportDto> getTestQuesionsExport(Map<String, Object> params) {

		if(params.get("courseId")!=null && !params.get("courseId").toString().isEmpty()) {
			params.put("courseId", Long.parseLong((String)params.get("courseId")));
		}
		if(params.get("groupId")!=null && !params.get("groupId").toString().isEmpty()) {
			params.put("groupId", Long.parseLong((String)params.get("groupId")));
		}
		if(params.get("unitTestId")!=null && !params.get("unitTestId").toString().isEmpty()) {
			params.put("unitTestId", Long.parseLong((String)params.get("unitTestId")));
		}

		String courseName = (String)params.get("courseName");
		String unitName = (String)params.get("unitName");
		String testTitle = (String)params.get("testTitle");
		String groupName = (String)params.get("groupName");

		List<AdminTestQuestionsDataAnalyseExportSelectDto> testQuestionsDataAnalyseExportSelectList = baseMapper.getTestQuestionsExport(params);
		List<AdminTestQuestionsDataAnalyseExportDto> testQuestionsExportList = new ArrayList<AdminTestQuestionsDataAnalyseExportDto>();

		for(AdminTestQuestionsDataAnalyseExportSelectDto AdminTestQuestionsDataAnalyseExportSelectDto : testQuestionsDataAnalyseExportSelectList) {
			AdminTestQuestionsDataAnalyseExportDto adminTestQuestionsDataAnalyseExportDto = new AdminTestQuestionsDataAnalyseExportDto();

			BeanUtils.copyProperties(AdminTestQuestionsDataAnalyseExportSelectDto,adminTestQuestionsDataAnalyseExportDto);
			adminTestQuestionsDataAnalyseExportDto.setCourseName(courseName);
			adminTestQuestionsDataAnalyseExportDto.setUnitName(unitName);
			adminTestQuestionsDataAnalyseExportDto.setTestTitle(testTitle);
			adminTestQuestionsDataAnalyseExportDto.setGroupName(groupName);
			params.put("testQuestionsId", Long.parseLong(AdminTestQuestionsDataAnalyseExportSelectDto.getTestQuestionsId()+""));

			int quetionPassCount = baseMapper.getTestQuestionsPassCount(params);
			int questionFirstCount = baseMapper.getTestQuestionsFirstCount(params);
			int questionFirstPassCount = baseMapper.getTestQuestionsFirstPassCount(params);

	    	NumberFormat numberFormat = NumberFormat.getInstance();
	    	// 设置精确到小数点后2位
	    	numberFormat.setMaximumFractionDigits(2);

	    	String questionPassPercent = numberFormat.format((float)quetionPassCount/(float)AdminTestQuestionsDataAnalyseExportSelectDto.getStudentTestQuestionCount()*100)+"%";
	    	String questionPassFirstPercent = numberFormat.format((float)questionFirstPassCount/(float)questionFirstCount*100)+"%";
	    	adminTestQuestionsDataAnalyseExportDto.setAvgCorrectPercent(questionPassPercent);
	    	adminTestQuestionsDataAnalyseExportDto.setFirstAvgCorrectPercent(questionPassFirstPercent);

	    	testQuestionsExportList.add(adminTestQuestionsDataAnalyseExportDto);
		}

		return testQuestionsExportList;

	}


}