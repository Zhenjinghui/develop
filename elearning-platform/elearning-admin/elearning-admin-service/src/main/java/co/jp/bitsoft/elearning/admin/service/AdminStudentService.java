package co.jp.bitsoft.elearning.admin.service;

import java.util.List;
import java.util.Map;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentExportDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * 受講者
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminStudentService {


	/**
	 * 查询系统用户(分页)
	 */
	PageUtils queryStudent(Map<String, Object> params);

	/**
	 * 导出
	 */
	List<AdminStudentExportDto> queryAllStudent(Map<String, Object> params);

	AdminStudentDto studentInfo(Long studentId);

    void studentSave(AdminStudentDto adminStudentDto);

    void studentUpdate(AdminStudentDto adminStudentDto);

    void studentImport(JSONObject csvData);

    void studentDelete(Long[] studentIds);

}

