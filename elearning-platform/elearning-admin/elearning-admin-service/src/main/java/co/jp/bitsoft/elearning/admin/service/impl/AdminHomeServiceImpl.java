package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import co.jp.bitsoft.elearning.admin.dto.AdminHomeDto;
import co.jp.bitsoft.elearning.admin.service.AdminHomeService;
import co.jp.bitsoft.elearning.admin.service.AdminStudyLoginHistoryService;
import co.jp.bitsoft.elearning.core.entity.CourseEntity;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTestEntity;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.CourseService;
import co.jp.bitsoft.elearning.core.service.StudentService;
import co.jp.bitsoft.elearning.core.service.StudentTestService;
import co.jp.bitsoft.elearning.core.service.UserGroupService;


@Service("AdminHomeService")
public class AdminHomeServiceImpl implements AdminHomeService {

	@Autowired
    private StudentService studentService;
	@Autowired
    private AdminStudyLoginHistoryService adminStudyLoginHistoryService;
    @Autowired
	private CourseService courseService;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private StudentTestService studentTestService;

	@Override
    /**
     * リスト一覧
     */
    public AdminHomeDto getHomeData() {
		QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
	   	queryWrapper.select("group_type","group_name").eq("group_id", CommonServiceImpl.getUser().getGroupId());
	   	UserGroupEntity userGroup = userGroupService.getOne(queryWrapper);
	   	String loginGroupType = userGroup.getGroupType();
	   	String loginGroupName = userGroup.getGroupName();

		List<Integer> seriesData = new ArrayList<Integer>();
   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
   		// 查询人数
		QueryWrapper<StudentEntity> studentQueryWrapper = new QueryWrapper<StudentEntity>();
		studentQueryWrapper.in(groupIdList.size()>0,"group_id",groupIdList );
		int studentCount = studentService.count(studentQueryWrapper);
		// 查询ログイン人数
		int loginStudentCount = adminStudyLoginHistoryService.countStudyLoginHistory();
		// 查询テストに合格した人数
		QueryWrapper<StudentTestEntity> studentTestQueryWrapper = new QueryWrapper<StudentTestEntity>();
		studentTestQueryWrapper.eq("answer_result","0");
		studentTestQueryWrapper.in(groupIdList.size()>0,"group_id",groupIdList );
		int testPassCount = studentTestService.count(studentTestQueryWrapper);
		// 查询数
		QueryWrapper<CourseEntity> courseQueryWrapper = new QueryWrapper<CourseEntity>();
		courseQueryWrapper.in(groupIdList.size()>0,"group_id",groupIdList );
		int courseCount = courseService.count(courseQueryWrapper);

		seriesData.add(studentCount);
		seriesData.add(loginStudentCount);
		seriesData.add(testPassCount);
		seriesData.add(courseCount);

		AdminHomeDto adminHome = new AdminHomeDto();
		adminHome.setLoginGroupType(loginGroupType);
		adminHome.setLoginGroupName(loginGroupName);
		adminHome.setSeriesData(seriesData);
        return adminHome;
    }


}