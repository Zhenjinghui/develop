/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserTokenEntity;

/**
 * 用户Token
 *
 * @author  BitSoft
 */
public interface SysUserTokenService extends IService<SysUserTokenEntity> {

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	R createToken(long userId,String userpassword,String userSalt);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(long userId);

}
