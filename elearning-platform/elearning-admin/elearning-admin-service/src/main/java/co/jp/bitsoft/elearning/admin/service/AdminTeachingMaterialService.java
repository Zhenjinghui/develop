package co.jp.bitsoft.elearning.admin.service;

import java.util.Map;

import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * 教材
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminTeachingMaterialService {

    PageUtils queryPage(Map<String, Object> params);

    void teachingMaterialSave(AdminTeachingMaterialDto adminTeachingMaterialDto);

    void teachingMaterialUpdate(AdminTeachingMaterialDto adminTeachingMaterialDto);

    void teachingMaterialDelete(Long[] teachingMaterialIds);
}

