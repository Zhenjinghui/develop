package co.jp.bitsoft.elearning.admin.service;

import java.util.List;
import java.util.Map;

import co.jp.bitsoft.elearning.admin.dto.AdminUserGroupDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.AdAddressEntity;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;

/**
 * 運用者グループ
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminUserGroupService {

    PageUtils queryPage(Map<String, Object> params);

    AdminUserGroupDto userGroupInfo(Long groupId);

    void userGroupSave(AdminUserGroupDto adminUserGroupDto);

    void userGroupUpdate(AdminUserGroupDto adminUserGroupDto);

    void userGroupDelete(Long userGroupId);

    AdAddressEntity zipAddressInfo(String groupZipCode);

    String groupType(Long userGroupId);

    List<UserGroupEntity> groupList(Map<String, Object> params);


}

