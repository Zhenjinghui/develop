package co.jp.bitsoft.elearning.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryForStudentStaRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryForStudentStaResponse;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryRequest;
import co.jp.bitsoft.elearning.admin.domain.model.studentcoursestudy.StudentCourseStudyQueryResponse;
import co.jp.bitsoft.elearning.admin.service.AdminStudentCourseStudyService;
import co.jp.bitsoft.elearning.admin.service.dao.AdminStudentCourseStudyDao;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

@Service("AdminStudentCourseStudyService")
public class AdminStudentCourseStudyServiceImpl implements AdminStudentCourseStudyService {

    @Autowired
    private AdminStudentCourseStudyDao adminStudentCourseStudyDao;

    public PageUtils queryStudentCourseStudy(StudentCourseStudyQueryRequest queryRequest) {

        long curPage = 1;
        long limit = 10;
        if(!StringUtils.isEmpty(queryRequest.getPage())){
            curPage = Long.parseLong(queryRequest.getPage());
        }
        if(!StringUtils.isEmpty(queryRequest.getLimit())){
            limit = Long.parseLong(queryRequest.getLimit());
        }
        Page<StudentCourseStudyQueryResponse> page = new Page<>(curPage, limit);
        IPage<StudentCourseStudyQueryResponse> pageList = adminStudentCourseStudyDao.queryStudentCourseStudy(page, queryRequest);

        return new PageUtils(pageList);
    }

    public PageUtils queryStudentCourseStudyStudentSta(StudentCourseStudyQueryForStudentStaRequest queryRequest) {

        long curPage = 1;
        long limit = 10;
        if(!StringUtils.isEmpty(queryRequest.getPage())){
            curPage = Long.parseLong(queryRequest.getPage());
        }
        if(!StringUtils.isEmpty(queryRequest.getLimit())){
            limit = Long.parseLong(queryRequest.getLimit());
        }
        Page<StudentCourseStudyQueryForStudentStaResponse> page = new Page<>(curPage, limit);
        IPage<StudentCourseStudyQueryForStudentStaResponse> pageList = adminStudentCourseStudyDao.queryStudentCourseStudyStudentSta
                (page, queryRequest);

        return new PageUtils(pageList);
    }
}
