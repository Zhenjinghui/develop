package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.admin.dto.AdminUserGroupDto;
import co.jp.bitsoft.elearning.admin.service.AdminUserGroupService;
import co.jp.bitsoft.elearning.admin.service.dao.AdminUserGroupDao;
import co.jp.bitsoft.elearning.common.exception.RRException;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.AdAddressEntity;
import co.jp.bitsoft.elearning.core.entity.CourseEntity;
import co.jp.bitsoft.elearning.core.entity.CourseOpenInfoEntity;
import co.jp.bitsoft.elearning.core.entity.GroupCertificateTemplateEntity;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.AdAddressService;
import co.jp.bitsoft.elearning.core.service.CourseOpenInfoService;
import co.jp.bitsoft.elearning.core.service.CourseService;
import co.jp.bitsoft.elearning.core.service.GroupCertificateTemplateService;
import co.jp.bitsoft.elearning.core.service.StudentService;
import co.jp.bitsoft.elearning.core.service.UserGroupService;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;
import co.jp.bitsoft.elearning.modules.sys.service.SysUserService;


@Service("AdminUserGroupService")
public class AdminUserGroupServiceImpl extends ServiceImpl<AdminUserGroupDao, AdminUserGroupDto> implements AdminUserGroupService {

    @Autowired
    private UserGroupService userGroupService;
    @Autowired
    private AdAddressService adAddressService;
    @Autowired
    private CourseOpenInfoService courseOpenInfoService;

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private GroupCertificateTemplateService groupCertificateTemplateService;

    /**
     * リスト一覧
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        //分页参数
        long curPage = 1;
        long limit = 10;
        if(params.get(Constant.PAGE) != null){
            curPage = Long.parseLong((String)params.get(Constant.PAGE));
        }
        if(params.get(Constant.LIMIT) != null){
            limit = Long.parseLong((String)params.get(Constant.LIMIT));
        }
		Page<AdminUserGroupDto> page = new Page<>(curPage, limit);

		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
		IPage<AdminUserGroupDto> pageList = baseMapper.queryUserGroup(page,params,groupIdList);


        return new PageUtils(pageList);
    }

    /**
     * info
     */
    public AdminUserGroupDto userGroupInfo(Long groupId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("groupId", groupId);
		List<Long> groupIdList = null;

    	List<AdminUserGroupDto> userGroupList = baseMapper.queryUserGroup(params,groupIdList);
    	AdminUserGroupDto adminUserGroup = new AdminUserGroupDto();
        if(userGroupList.size()>0) {
        	adminUserGroup = userGroupList.get(0);
        }
        String groupType = getGroupType(groupId);

    	if("3".equals(groupType)) {
        	QueryWrapper<CourseOpenInfoEntity> queryWrapper = new QueryWrapper<>();
        	queryWrapper.eq("group_id", groupId);
    		List<CourseOpenInfoEntity> userGroupCourseList= courseOpenInfoService.list(queryWrapper);
    		List<Long> courseIds = new ArrayList<Long>();

    		for(CourseOpenInfoEntity courseOpenInfoEntity : userGroupCourseList) {
    			courseIds.add(courseOpenInfoEntity.getCourseId());
    		}

    		if(userGroupCourseList.size()>0) {
        		adminUserGroup.setCourseIds(courseIds);
    		}

        	QueryWrapper<GroupCertificateTemplateEntity> queryWrapperCertificate = new QueryWrapper<>();
        	queryWrapperCertificate.eq("group_id", groupId);
    		List<GroupCertificateTemplateEntity> certificateTemplatList= groupCertificateTemplateService.list(queryWrapperCertificate);
    		if(certificateTemplatList.size()>0) {
    	        BeanUtils.copyProperties(certificateTemplatList.get(0), adminUserGroup);
    		}

    	}
    	return adminUserGroup;

    }

    /**
     * 保存
     */
    public void userGroupSave(AdminUserGroupDto adminUserGroupDto) {

    	if("2".equals(adminUserGroupDto.getGroupType())) {
        	adminUserGroupDto.setGroupIdOf(CommonServiceImpl.getUser().getGroupId());
    	}

    	insertCheckGroupName(adminUserGroupDto.getGroupName());
    	UserGroupEntity userGroup = new UserGroupEntity();

        BeanUtils.copyProperties(adminUserGroupDto, userGroup);

		userGroupService.save(userGroup);
		if("3".equals(adminUserGroupDto.getGroupType())) {
			List<Long> courseIds = adminUserGroupDto.getCourseIds();
			for(Long courseId : courseIds) {
				if(courseId != null) {
					CourseOpenInfoEntity userGroupCourse = new CourseOpenInfoEntity();
					userGroupCourse.setGroupId(userGroup.getGroupId());
					userGroupCourse.setCourseId(courseId);
					courseOpenInfoService.save(userGroupCourse);
				}
			}
	        GroupCertificateTemplateEntity template = new GroupCertificateTemplateEntity();
	        BeanUtils.copyProperties(adminUserGroupDto, template);
	        template.setGroupId(userGroup.getGroupId());
	        groupCertificateTemplateService.save(template);
		}
    }

    /**
     * 更新
     */
    public void userGroupUpdate(AdminUserGroupDto adminUserGroupDto) {
    	if(adminUserGroupDto.getUserMaxNum()>0) {
        	QueryWrapper<StudentEntity> studentQueryWrapper = new QueryWrapper<>();
        	studentQueryWrapper.eq("group_id", adminUserGroupDto.getGroupId());
        	int count = studentService.count(studentQueryWrapper);

        	if (count>adminUserGroupDto.getUserMaxNum()) {
    			throw new RRException("契約受講者数に達しているため登録できませんでした。");
        	}
    	}

    	updateCheckGroupName(adminUserGroupDto);

    	UserGroupEntity userGroup = new UserGroupEntity();

        BeanUtils.copyProperties(adminUserGroupDto, userGroup);
		userGroupService.updateById(userGroup);

		if("3".equals(adminUserGroupDto.getGroupType())) {
        	QueryWrapper<CourseOpenInfoEntity> queryWrapper = new QueryWrapper<>();
        	queryWrapper.eq("group_id", adminUserGroupDto.getGroupId());
        	courseOpenInfoService.remove(queryWrapper);
			List<Long> courseIds = adminUserGroupDto.getCourseIds();
			for(Long courseId : courseIds) {
				if(courseId != null) {
					CourseOpenInfoEntity userGroupCourse = new CourseOpenInfoEntity();
					userGroupCourse.setGroupId(userGroup.getGroupId());
					userGroupCourse.setCourseId(courseId);
					courseOpenInfoService.save(userGroupCourse);
				}

			}
	        GroupCertificateTemplateEntity template = new GroupCertificateTemplateEntity();
	        BeanUtils.copyProperties(adminUserGroupDto, template);
	        groupCertificateTemplateService.updateById(template);

		}

    }

    /**
     * 削除
     */
    public void userGroupDelete(Long userGroupId){

//    	userGroupService.update(new UpdateWrapper<UserGroupEntity>().set("del_flag", "1").eq("group_id_of", userGroupId));

    	String groupType = getGroupType(userGroupId);
    	if("2".equals(groupType)){
       		List<Long> groupIdList = new ArrayList<Long>();

    	   	QueryWrapper<UserGroupEntity> queryWrapperSelect = new QueryWrapper<>();
    	   	queryWrapperSelect.select("group_id").eq("group_id", userGroupId).or().eq("group_id_of", userGroupId);
       		List<UserGroupEntity> userGroupList = userGroupService.list(queryWrapperSelect);
       		for(UserGroupEntity usergroup : userGroupList) {
       			groupIdList.add(usergroup.getGroupId());
       		}
       		if(groupIdList.size()>1) {
    			throw new RRException("選択したご契約者様には一般グループがあります！");
       		}

        	QueryWrapper<SysUserEntity> queryWrapperSysuser = new QueryWrapper<>();
        	queryWrapperSysuser.in("group_id", groupIdList);
        	int sysUserCount = sysUserService.count(queryWrapperSysuser);
        	if(sysUserCount>0) {
    			throw new RRException("選択したご契約者様には運用者ユーザーがあります！");
        	}

        	QueryWrapper<CourseEntity> queryWrapperCourse = new QueryWrapper<>();
        	queryWrapperCourse.eq("group_id", userGroupId);
        	int courseCount = courseService.count(queryWrapperCourse);
        	if(courseCount>0) {
    			throw new RRException("選択したご契約者様には講座があります！");
        	}

    	}

    	if("3".equals(groupType)){
        	QueryWrapper<CourseOpenInfoEntity> queryWrapper = new QueryWrapper<>();
        	queryWrapper.eq("group_id", userGroupId);
        	courseOpenInfoService.remove(queryWrapper);
        	groupCertificateTemplateService.update(new UpdateWrapper<GroupCertificateTemplateEntity>().set("del_flag",
                                                                                                             "1")
                                                                                                        .eq("group_id",
                                                                                                        	userGroupId));
    	}
    	userGroupService.removeById(userGroupId);

    }

    /**
     * 郵便番号
     */
    public AdAddressEntity zipAddressInfo(String groupZipCode){
    	QueryWrapper<AdAddressEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("zip", groupZipCode);
		List<AdAddressEntity> adAddress = adAddressService.list(queryWrapper);
		if(adAddress.size() == 0 ) {
			throw new RRException("郵便番号が正しくありません！");
		}
		return adAddress.get(0);
    }

    /**
     * グループタイプ
     */
    public String groupType(Long userGroupId) {
    	String groupType = null;
    	if(userGroupId == null) {
    		groupType = getGroupType(CommonServiceImpl.getUser().getGroupId());
    	}else {
        	groupType = getGroupType(userGroupId);
    	}
    	return groupType;
    }

    /**
     * グループ下拉框
     */
    public List<UserGroupEntity> groupList(Map<String, Object> params){

//      	String paramGroupType = (String) params.get("groupType");
//    	Long loginGroupId = CommonServiceImpl.getUser().getGroupId();
//    	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
//    	if(StringUtils.isNotEmpty(paramGroupType)) {
//    		queryWrapper.select("group_id","group_name")
//	    	.eq("group_type", paramGroupType)
//            .and(wrapper -> wrapper.eq(loginGroupId != null,"group_id", loginGroupId)
//            	.or()
//            	.eq(loginGroupId != null,"group_id_of", loginGroupId));
//    	}else {
//  	    	queryWrapper.select("group_id","group_name")
//	    	.eq(loginGroupId != null,"group_id", loginGroupId)
//            .or()
//            .eq(loginGroupId != null,"group_id_of", loginGroupId);
//    	}
    	String paramProxyGroupId = (String) params.get("proxyGroupId");
    	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
    	if(paramProxyGroupId == null || paramProxyGroupId.isEmpty()) {
    		queryWrapper.select("group_id","group_name").eq("group_type", "2");
		}else {
		    queryWrapper.select("group_id","group_name").eq("group_id_of", Long.parseLong(paramProxyGroupId));
		}
		List<UserGroupEntity> groupList = userGroupService.list(queryWrapper);

		return groupList;
    }

    /**
    * 获取グループタイプ
    */
   public String getGroupType(long groupId) {
	   	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
	   	queryWrapper.select("group_type").eq("group_id", groupId);

	   	String groupType = userGroupService.getOne(queryWrapper).getGroupType();
		return groupType;
   }

	/**
	 * 插入时检查组名是否被使用
	 */
	private void insertCheckGroupName(String groupName){

   	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
   	queryWrapper.eq("group_name", groupName);
		int count = userGroupService.count(queryWrapper);
		if(count > 0){
			throw new RRException("グループ名が使用されました！");
		}
	}

	/**
	 * 更新时检查组名是否被使用
	 */
	private void updateCheckGroupName(AdminUserGroupDto group){
		if(group.getGroupId()!=null) {
			UserGroupEntity groupEntity = userGroupService.getById(group.getGroupId());
			if(groupEntity.getGroupName().equals(group.getGroupName())) {
				return;
			}else {
				insertCheckGroupName(group.getGroupName());
			}
		}else {
			insertCheckGroupName(group.getGroupName());
		}
	}

}