package co.jp.bitsoft.elearning.admin.service;

import java.util.Map;

import co.jp.bitsoft.elearning.admin.dto.AdminSubjectDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * 教科
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminSubjectService {

    PageUtils queryPage(Map<String, Object> params);

    void subjectSave(AdminSubjectDto adminsubjectDto);

    void subjectUpdate(AdminSubjectDto adminsubjectDto);

    void subjectDelete(Long[] subjectIds);
}

