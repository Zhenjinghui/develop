package co.jp.bitsoft.elearning.admin.service;

import java.util.List;
import java.util.Map;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseSelectDto;
import co.jp.bitsoft.elearning.admin.dto.AdminStudentDataAnalyseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTeachingMaterialDataAnalyseExportDto;
import co.jp.bitsoft.elearning.admin.dto.AdminTestQuestionsDataAnalyseExportDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;

/**
 * 分析項目
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminDataAnalyseService{

    /**
     * クラスドロップダウンボックス
     */
	List<UserGroupEntity> getClassSelectDrop();

    /**
     * 講座ドロップダウンボックス
     */
	List<AdminCourseSelectDto> getCourseSelectDrop();

    /**
     * 受講者傾向分析項目
     */
	AdminStudentDataAnalyseDto getStudentDataAnalyse(Map<String, Object> params);

    /**
	 * 受講状況（講座・教材単位）(分页)
	 */
	PageUtils getTeachingMaterialDataAnalyse(Map<String, Object> params);

    /**
	 * 受講状況（講座・教材単位）エクスポート
	 */
	List<AdminTeachingMaterialDataAnalyseExportDto> getTeachingMaterialDataAnalyseExport(Map<String, Object> params);

    /**
	 * 単元テスト結果
	 */
	PageUtils getTestDataAnalyse(Map<String, Object> params);

    /**
	 * 単元テスト結果(出題内容)エクスポート
	 */
	List<AdminTestQuestionsDataAnalyseExportDto> getTestQuesionsExport(Map<String, Object> params);

}

