package co.jp.bitsoft.elearning.admin.service;

import java.util.Map;

import co.jp.bitsoft.elearning.admin.dto.AdminTestQuestionsDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * テスト問題
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminTestQuestionsService {

    PageUtils queryPage(Map<String, Object> params);

    AdminTestQuestionsDto testQuestionsInfo(Long testQuestionsId);

    void testQuestionsSave(AdminTestQuestionsDto adminTestQuestionsDto);

    void testQuestionsUpdate(AdminTestQuestionsDto adminTestQuestionsDto);

    void testQuestionsDelete(Long[] testQuestionsIds);
}

