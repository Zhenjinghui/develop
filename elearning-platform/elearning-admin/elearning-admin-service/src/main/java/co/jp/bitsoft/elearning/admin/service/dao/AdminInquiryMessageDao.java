package co.jp.bitsoft.elearning.admin.service.dao;

import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.InquiryMessageEntity;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;

/**
 * 問い合わせメッセージ
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminInquiryMessageDao extends BaseMapper<InquiryMessageEntity> {
    void removeByInquiryIds(Long[] inquiryIds);
    void changeStatus(int inquiryId, int userId);
}
