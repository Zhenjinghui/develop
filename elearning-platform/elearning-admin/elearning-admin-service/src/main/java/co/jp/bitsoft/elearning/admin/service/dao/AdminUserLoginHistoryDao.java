package co.jp.bitsoft.elearning.admin.service.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.AdminUserLoginHistoryDto;

/**
 * ユーザーログイン履歴
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface AdminUserLoginHistoryDao extends BaseMapper<AdminUserLoginHistoryDto> {
	/**
	 * 查询系统用户登陆履历(分页)
	 */
	IPage<AdminUserLoginHistoryDto> selectUserLoginHistory(Page<AdminUserLoginHistoryDto> page,@Param("userName") String useraName,@Param("groupIdList") List<Long> groupIdList);

}
