package co.jp.bitsoft.elearning.admin.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.admin.dto.AdminStudyLoginHistoryDto;
import co.jp.bitsoft.elearning.admin.service.AdminStudyLoginHistoryService;
import co.jp.bitsoft.elearning.admin.service.dao.AdminStudyLoginHistoryDao;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.service.UserGroupService;


@Service("AdminStudyLoginHistoryService")
public class AdminStudyLoginHistoryServiceImpl extends ServiceImpl<AdminStudyLoginHistoryDao, AdminStudyLoginHistoryDto> implements AdminStudyLoginHistoryService {

	@Autowired
	private UserGroupService userGroupService;

    /**
   	 * 查询一般用户履历(分页)
   	 */
   	public PageUtils selectStudyLoginHistory(Map<String, Object> params) {
       //分页参数
       long curPage = 1;
       long limit = 10;
       if(params.get(Constant.PAGE) != null){
           curPage = Long.parseLong((String)params.get(Constant.PAGE));
       }
       if(params.get(Constant.LIMIT) != null){
           limit = Long.parseLong((String)params.get(Constant.LIMIT));
       }
       Page<AdminStudyLoginHistoryDto> page = new Page<>(curPage, limit);

       String userName = (String)params.get("userName");

       //group限制
   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
   		IPage<AdminStudyLoginHistoryDto> pageList = baseMapper.selectStudyLoginHistory(page,userName,groupIdList);

   		return new PageUtils(pageList);
       }
	/**
	 * 查询一般用户login数量
	 */
	public Integer countStudyLoginHistory() {
        //group限制
   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
        int countStudentLogin = baseMapper.countStudentLoginHistory(groupIdList);
		return countStudentLogin;

    }



}