/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.dto.AdminSysUserDto;
import co.jp.bitsoft.elearning.admin.dto.AdminSysUserExportDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;


/**
 * 系统用户
 *
 * @author  BitSoft
 */
public interface SysUserService extends IService<SysUserEntity> {


	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);

	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);

	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);

	/**
	 * 保存用户
	 */
	void saveUser(AdminSysUserDto user);

	/**
	 * 修改用户
	 */
	void update(AdminSysUserDto user);

	/**
	 * 删除用户
	 */
	void deleteBatch(Long[] userIds);

	/**
	 * 修改密码
	 * @param userId       用户ID
	 * @param password     原密码
	 * @param newPassword  新密码
	 */
	void updatePassword(Long userId, String password, String newPassword);

	/**
	 * 查询系统用户(分页)
	 */
	PageUtils queryUser(Map<String, Object> params);

	/**
	 * 导出
	 */
	List<AdminSysUserExportDto> queryAllUser(Map<String, Object> params);

	void userImport(JSONObject csvData);

	AdminSysUserDto userInfo(Long userId);

	Long loginUserRole();

	void resetPassword(Long userId);


}
