package co.jp.bitsoft.elearning.admin.service;

import java.util.List;
import java.util.Map;

import co.jp.bitsoft.elearning.admin.dto.AdminCourseDto;
import co.jp.bitsoft.elearning.admin.dto.AdminCourseSelectDto;
import co.jp.bitsoft.elearning.common.utils.PageUtils;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface AdminCourseService{

    PageUtils queryPage(Map<String, Object> params);

    AdminCourseDto courseInfo(Long courseId);

    void courseSave(AdminCourseDto adminCourseDto);

    void courseUpdate(AdminCourseDto adminCourseDto);

    void courseDelete(Long[] courseIds);

    List<AdminCourseSelectDto> courseSelect(Map<String, Object> params);

    List<AdminCourseSelectDto> courseProxySelect(Long proxyGroupId);

    List<AdminCourseSelectDto> courseClassSelect(Long classGroupId);

}

