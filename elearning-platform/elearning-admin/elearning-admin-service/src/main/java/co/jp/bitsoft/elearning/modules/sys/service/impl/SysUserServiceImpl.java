/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.hutool.json.JSONObject;
import co.jp.bitsoft.elearning.admin.dto.AdminSysUserDto;
import co.jp.bitsoft.elearning.admin.dto.AdminSysUserExportDto;
import co.jp.bitsoft.elearning.admin.service.impl.CommonServiceImpl;
import co.jp.bitsoft.elearning.common.exception.RRException;
import co.jp.bitsoft.elearning.common.mail.CommonMailUtils;
import co.jp.bitsoft.elearning.common.utils.Common;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.validator.Assert;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.UserGroupService;
import co.jp.bitsoft.elearning.modules.sys.dao.SysUserDao;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserRoleEntity;
import co.jp.bitsoft.elearning.modules.sys.service.SysUserRoleService;
import co.jp.bitsoft.elearning.modules.sys.service.SysUserService;


/**
 * 系统用户
 *
 * @author  BitSoft
 */
@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements SysUserService{
	@Autowired
	private SysUserRoleService sysUserRoleService;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private CommonMailUtils commonMailUtils;
	@Autowired
	private SysUserService sysUser;

	@Override
	public List<String> queryAllPerms(Long userId) {
		return baseMapper.queryAllPerms(userId);
	}

	@Override
	public List<Long> queryAllMenuId(Long userId) {
		return baseMapper.queryAllMenuId(userId);
	}

	@Override
	public SysUserEntity queryByUserName(String username) {
		return baseMapper.queryByUserName(username);
	}

	// 半角[#]定義 番号関連の「#」挿入を導入対応
	private static final String DEF_SHARP = "#";
	/**
	 * 保存用户
	 */
	@Override
	@Transactional
	public void saveUser(AdminSysUserDto user) {
		user.setUserPassword("elearning123456");

		ValidatorUtils.validateEntity(user, AddGroup.class);

		//检查用户名是否被使用
		insertCheckUserName(user.getUserName());
		//检查邮箱是否被使用
//		insertCheckEmail(user.getEmail());
		checkGroup(user);
		checkRole(user);

//		//sha256加密
		if(!StringUtils.isEmpty(user.getUserPassword())) {
			String salt = RandomStringUtils.randomAlphanumeric(20);
			user.setUserPassword(new Sha256Hash(user.getUserPassword(), salt).toHex());
			user.setUserSalt(salt);
		}

    	SysUserEntity sysUser = new SysUserEntity();
        BeanUtils.copyProperties(user, sysUser);

		this.save(sysUser);

		//保存用户与角色关系
		SysUserRoleEntity sysUserRoleEntity = new SysUserRoleEntity();
		sysUserRoleEntity.setUserId(sysUser.getUserId());
		sysUserRoleEntity.setRoleId(sysUser.getRoleId());
		sysUserRoleService.save(sysUserRoleEntity);
	}

	/**
	 * 修改用户
	 */
	@Override
	@Transactional
	public void update(AdminSysUserDto user) {
		ValidatorUtils.validateEntity(user, UpdateGroup.class);

		//检查用户名是否被使用
		updateCheckUserName(user);
		//检查邮箱是否被使用
//		updateCheckEmail(user);
		checkGroup(user);
		checkRole(user);

		SysUserEntity sysUser = new SysUserEntity();
        BeanUtils.copyProperties(user, sysUser);
		this.updateById(sysUser);

		//保存用户与角色关系
		sysUserRoleService.update(new UpdateWrapper<SysUserRoleEntity>().set("role_id",user.getRoleId()).eq("user_id", user.getUserId()));
	}

	@Override
	public void deleteBatch(Long[] userIds) {
		if(ArrayUtils.contains(userIds, 1L)){
			throw new RRException("システム管理者の削除できません");
		}

		if(ArrayUtils.contains(userIds, CommonServiceImpl.getUser().getUserId())){
			throw new RRException("現在の使用者の削除できません");
		}
		this.removeByIds(Arrays.asList(userIds));

	}
	/**
	 * 修改登录用户密码
	 */
	@Override
	public void updatePassword(Long userId, String password, String newPassword) {
		Assert.isBlank(newPassword, "新パスワードを入力してください");

		if(newPassword.equals("elearning123456")) {
			throw new RRException("新パスワードはデフォルトのパスワードです，新パスワードパスワードを変更してください");
		}
		//sha256加密
		password = new Sha256Hash(password, CommonServiceImpl.getUser().getUserSalt()).toHex();
		//sha256加密
		newPassword = new Sha256Hash(newPassword, CommonServiceImpl.getUser().getUserSalt()).toHex();
		SysUserEntity userEntity = new SysUserEntity();
		userEntity.setUserPassword(newPassword);
		boolean returnFlag = this.update(userEntity,
				new QueryWrapper<SysUserEntity>().eq("user_id", userId).eq("user_password", password));
		if(!returnFlag){
			throw new RRException("旧パスワードが違います。");
		}

	}

	/**
	 * 查询系统用户(分页)
	 */
	@Override
	public PageUtils queryUser(Map<String, Object> params) {
        //分页参数
        long curPage = 1;
        long limit = 10;
        if(params.get(Constant.PAGE) != null){
            curPage = Long.parseLong((String)params.get(Constant.PAGE));
        }
        if(params.get(Constant.LIMIT) != null){
            limit = Long.parseLong((String)params.get(Constant.LIMIT));
        }
		Page<AdminSysUserDto> page = new Page<>(curPage, limit);

        //group限制
   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);

		IPage<AdminSysUserDto> pageList = baseMapper.queryUser(page,params,groupIdList);

		return new PageUtils(pageList);
    }
	/**
	 * 导出
	 */
	@Override
	public List<AdminSysUserExportDto> queryAllUser(Map<String, Object> params) {


        //group限制
   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);

        List<AdminSysUserDto> sysUserList = baseMapper.queryUser(params,groupIdList);
        List<AdminSysUserExportDto> adminSysUserExportList = new ArrayList<AdminSysUserExportDto>();

        for(AdminSysUserDto adminSysUserDto : sysUserList) {
        	AdminSysUserExportDto adminSysUserExportDto = new AdminSysUserExportDto();
     	    BeanUtils.copyProperties(adminSysUserDto, adminSysUserExportDto);
     	    if(adminSysUserDto.getSex().equals(0)) {
     	    	adminSysUserExportDto.setSex("男性");
     	    }else {
     	    	adminSysUserExportDto.setSex("女性");
     	    }
     	    if(adminSysUserDto.getStatus().equals(0)) {
     	    	adminSysUserExportDto.setStatus("使用停止");
     	    }else if(adminSysUserDto.getStatus().equals(1)) {
     	    	adminSysUserExportDto.setStatus("正常");
     	    }
     	    // 番号関連の「#」挿入を導入
     	    if (!"".equals(adminSysUserDto.getMobile())) {
				adminSysUserExportDto.setMobile(Common.getFormattedImpNumber(adminSysUserDto.getMobile()));
			}
			if (!"".equals(adminSysUserDto.getTel())) {
				adminSysUserExportDto.setTel(Common.getFormattedImpNumber(adminSysUserDto.getTel()));
			}
			if (!"".equals(adminSysUserDto.getTel())) {
				adminSysUserExportDto.setFax(Common.getFormattedImpNumber(adminSysUserDto.getFax()));
			}

     	   adminSysUserExportList.add(adminSysUserExportDto);
        }
		return adminSysUserExportList;
    }

	/**
	 * 用户インポト
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void userImport(JSONObject csvData) {
		String jsonStr = csvData.get("csvJson").toString();
		List<Object> csvDataRow = JSONArray.parseArray(jsonStr);
		List<String> rowList = new ArrayList<String>();
		AdminSysUserDto adminSysUserDto;
		if(csvDataRow.size()>1) {
			for(int i= 0;i< csvDataRow.size();i++) {
				if(i>0) {
					rowList = (List<String>) csvDataRow.get(i);
					adminSysUserDto = new AdminSysUserDto();
					adminSysUserDto.setUserName(rowList.get(1));
					adminSysUserDto.setUserNameKanji(rowList.get(2));
					adminSysUserDto.setUserNameKana(rowList.get(3));
					adminSysUserDto.setRoleId(Long.parseLong(rowList.get(4)));
					adminSysUserDto.setGroupId(Long.parseLong(rowList.get(6)));
					adminSysUserDto.setEmail(rowList.get(10));

					// #を抜く
					adminSysUserDto.setMobile(rowList.get(11).replaceAll(DEF_SHARP, ""));
					adminSysUserDto.setTel(rowList.get(12).replaceAll(DEF_SHARP, ""));
					adminSysUserDto.setFax(rowList.get(13).replaceAll(DEF_SHARP, ""));

					if ("男性".equals(rowList.get(14))) {
						adminSysUserDto.setSex(0);
					} else {
						adminSysUserDto.setSex(1);
					}
					adminSysUserDto.setDepartName(rowList.get(15));
					adminSysUserDto.setPositionName(rowList.get(16));
					if ("正常".equals(rowList.get(17))) {
						adminSysUserDto.setStatus(1);
					} else {
						adminSysUserDto.setStatus(1);
					}

					if(StringUtils.isBlank(rowList.get(0))) {
						ValidatorUtils.validateEntity(adminSysUserDto, AddGroup.class);
						saveUser(adminSysUserDto);
					}else {
						adminSysUserDto.setUserId(Long.parseLong(rowList.get(0)));
						ValidatorUtils.validateEntity(adminSysUserDto, UpdateGroup.class);
						update(adminSysUserDto);
					}
				}
			}
		}
	}
	/**
	 * 获取用户信息
	 */
	@Override
	public AdminSysUserDto userInfo(Long userId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);

		List<Long> groupList = null;
        List<AdminSysUserDto> sysUserList = baseMapper.queryUser(params,groupList);
        AdminSysUserDto user = new AdminSysUserDto();
        if(sysUserList.size()>0) {
        	 user = sysUserList.get(0);
        }
        return user;
	}

	/**
	 * 保存时检查角色名是否被使用
	 */
	private void insertCheckUserName(String userName){
    	QueryWrapper<SysUserEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("user_name", userName);
		int count = this.count(queryWrapper);
		if(count > 0){
			throw new RRException("ユーザ名が使用されました！");
		}
	}

	/**
	 * 更新时检查角色名是否被使用
	 */
	private void updateCheckUserName(AdminSysUserDto user){
		if(user.getUserId()!=null) {
			SysUserEntity sysUserEntity = this.getById(user.getUserId());
			if(sysUserEntity.getUserName().equals(user.getUserName())) {
				return;
			}else {
				insertCheckUserName(user.getUserName());
			}
		}else {
			insertCheckUserName(user.getUserName());
		}
	}

	/**
	 * 保存时检查邮箱是否被使用
	 */
	private void insertCheckEmail(String email){
		/*
    	QueryWrapper<SysUserEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("email", email);
		int count = this.count(queryWrapper);
		if(count > 0){
			throw new RRException("メールアドレスが");
		}
		*/
		// empty
	}

	/**
	 * 更新时检查邮箱是否被使用
	 */
	private void updateCheckEmail(AdminSysUserDto user){
		if(user.getUserId()!=null) {
			SysUserEntity sysUserEntity = this.getById(user.getUserId());
			if(sysUserEntity.getEmail().equals(user.getEmail())) {
				return;
			}else {
				insertCheckEmail(user.getEmail());
			}
		}else {
			insertCheckEmail(user.getEmail());
		}
	}

	/**
	 * 检查group
	 */
	private void checkGroup(AdminSysUserDto user){
		Long loginGroupId = CommonServiceImpl.getUser().getGroupId();
		String loginGroupType = getGroupType(loginGroupId);
		if("1".equals(loginGroupType)) {
			return;
		}

		if(loginGroupId.equals(user.getGroupId())) {
			return;
		}
    	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.select("group_id_of").eq("group_id", user.getGroupId());

    	UserGroupEntity userSelect= userGroupService.getOne(queryWrapper);
    	if(userSelect == null) {
    		throw new RRException("グループは存在しません！");
    	}else {
        	Long groupIdof = userSelect.getGroupIdOf();
        	if(!loginGroupId.equals(groupIdof)) {
        		throw new RRException("権限以外のグループユーザを追加できません。！");
        	}
    	}

	}

	/**
	 * 检查角色是否越权
	 */
	private void checkRole(AdminSysUserDto user){
		String groupType = getGroupType(user.getGroupId());

    	if("1".equals(groupType)) {
    		if(!user.getRoleId().equals(2l) && !user.getRoleId().equals(3l)) {
    			throw new RRException("権限以外のロールを追加できません。！");
    		}
    	}else if("2".equals(groupType)) {
    		if(!user.getRoleId().equals(4l) && !user.getRoleId().equals(5l)) {
    			throw new RRException("権限以外のロールを追加できません。！");
    		}
    	}else if("3".equals(groupType)) {
    		if(!user.getRoleId().equals(6l) && !user.getRoleId().equals(7l)) {
    			throw new RRException("権限以外のロールを追加できません。！");
    		}
    	}
	}

	/**
	 * 用户ロール
	 */
	public Long loginUserRole(){

    	QueryWrapper<SysUserRoleEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.select("role_id").eq("user_id", CommonServiceImpl.getUser().getUserId());

    	List<SysUserRoleEntity> roleList = sysUserRoleService.list(queryWrapper);

    	Long roleId = 0l;
    	if(roleList.size()>0) {
    		roleId = roleList.get(0).getRoleId();
    	}

    	return roleId;
//    	if("1".equals(groupType)) {
//    		if(!user.getRoleId().equals(2l) && !user.getRoleId().equals(3l)) {
//    			throw new RRException("権限以外のロールを追加できません。！");
//    		}
//    	}else if("2".equals(groupType)) {
//    		if(!user.getRoleId().equals(4l) && !user.getRoleId().equals(5l)) {
//    			throw new RRException("権限以外のロールを追加できません。！");
//    		}
//    	}else if("3".equals(groupType)) {
//    		if(!user.getRoleId().equals(6l) && !user.getRoleId().equals(7l)) {
//    			throw new RRException("権限以外のロールを追加できません。！");
//    		}
//    	}
	}

    /**
    * 获取グループタイプ
    */
   public String getGroupType(long groupId) {
	   	QueryWrapper<UserGroupEntity> queryWrapper = new QueryWrapper<>();
	   	queryWrapper.select("group_type").eq("group_id", groupId);

	   	String groupType = userGroupService.getOne(queryWrapper).getGroupType();
		return groupType;
   }

	/**
	 * 重置登录用户密码
	 */
	@Override
	public void resetPassword(Long userId) {
		String password = "elearning123456";
		SysUserEntity userResetEntity = sysUser.getById(userId);
		//sha256加密
		password = new Sha256Hash(password, userResetEntity.getUserSalt()).toHex();
		SysUserEntity userEntity = new SysUserEntity();
		userEntity.setUserPassword(password);
		this.update(userEntity,new QueryWrapper<SysUserEntity>().eq("user_id", userId));

   	// htmlテンプレートファイルを読む
		String buffer = null;
		try {
			buffer = commonMailUtils.readHtml("manager_pwd_reset.html");
		} catch (Exception e) {
			throw new RRException("htmlテンプレートファイルの読み取りに失敗しました！");
		}

       String htmlText = MessageFormat.format(buffer, userResetEntity.getUserNameKanji(),userResetEntity.getUserName());

       boolean mailSendFlag = commonMailUtils.sendHtmlMail(userResetEntity.getEmail(), "【Study Pack】ユーザーパスワードリセットメール", htmlText);
   	if(!mailSendFlag) {
			throw new RRException("メールの送信に失敗しました！");
   	}

	}



}