package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 教科
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("subject")
public class SubjectEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 教科ID
	 */
    @TableId
    private Long subjectId;
	/**
	 * 表示順
	 */
	private Long dispNo;
	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 教科名
	 */
    private String subjectTitle;

	/**
	 * 教科画像PATH
	 */
    private String subjectImagePath;

	/**
	 * 教科AWS S3 BUCKET名
	 */
    private String subjectS3BucketName;

	/**
	 * 教科AWS S3 オブジェクトキー
	 */
    private String subjectS3ObjectKey;

	/**
	 * 教科概要
	 */
    private String subjectOutline;

	/**
	 * 教科説明
	 */
    private String subjectDetail;

	/**
	 * 教科ステータス
	 */
    private String subjectStatus;

	/**
	 * 公開開始日時
	 */
    private Date openStartTime;

	/**
	 * 公開終了日時
	 */
    private Date openEndTime;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
