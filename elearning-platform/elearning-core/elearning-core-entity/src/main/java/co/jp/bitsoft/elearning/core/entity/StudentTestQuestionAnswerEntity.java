package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者テスト問題解答
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_test_question_answer")
public class StudentTestQuestionAnswerEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者テスト問題解答ID
	 */
    @TableId
    private Long studentTestQuestionAnswerId;

	/**
	 * 受講者テストID
	 */
    private Long studentTestId;

	/**
	 * テスト問題ID
	 */
    private Long testQuestionsId;

	/**
	 * 解答ステータス
	 */
    private String answerStatus;

	/**
	 * 解答内容
	 */
    private String answerContent;

	/**
	 * 解答判定結果
	 */
    private String answerResult;

	/**
	 * 解答開始日時
	 */
    private Date answerStartTime;

	/**
	 * 解答終了日時
	 */
    private Date answerEndTime;

	/**
	 * 解答時間（秒）
	 */
    private Integer answerTimeSeconds;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
