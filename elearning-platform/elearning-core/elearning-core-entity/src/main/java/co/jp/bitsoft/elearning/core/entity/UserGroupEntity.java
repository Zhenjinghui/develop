package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 運用者グループ
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("user_group")
public class UserGroupEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * グループID
	 */
    @TableId
    private Long groupId;

	/**
	 * グループ名
	 */
    private String groupName;

	/**
	 * グループ種別
	 */
    private String groupType;

	/**
	 * 所属グループID
	 */
    private Long groupIdOf;

	/**
	 * グループ説明
	 */
    private String groupDetail;

	/**
	 * 郵便番号
	 */
    private String groupZipCode;

	/**
	 * 都道府県
	 */
    private String groupPrefecture;

	/**
	 * 市区町村
	 */
    private String groupAddressCity;

	/**
	 * その他住所
	 */
    private String groupAddressOthers;

	/**
	 * 連絡先電話１
	 */
    private String contactTel1;

	/**
	 * 連絡先FAX１
	 */
    private String contactFax1;

	/**
	 * 連絡先電話２
	 */
    private String contactTel2;

	/**
	 * 連絡先FAX２
	 */
    private String contactFax2;

	/**
	 * 連絡先メール
	 */
    private String contactEmail;

	/**
	 * 責任者氏名
	 */
    private String managerName;

	/**
	 * 責任者氏名（カナ）
	 */
    private String managerNameKana;

	/**
	 * 責任者部署
	 */
    private String managerDepartName;

	/**
	 * 責任者職位
	 */
    private String managerPosition;

	/**
	 * 下位ユーザー数制限
	 */
    private Long userMaxNum;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

	/**
	 * 有効期間開始日
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date startTime;
	/**
	 * 有効期間終了日
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date endTime;
}
