/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.admin.dto;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import co.jp.bitsoft.elearning.common.validator.group.AddGroup;
import co.jp.bitsoft.elearning.common.validator.group.UpdateGroup;
import lombok.Data;

/**
 * 運用者ユーザー
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
public class AdminSysUserDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 運用者ユーザーID
	 */
	private Long userId;
	/**
	 * グループID
	 */
	@NotNull(message="所属グループIDを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	private Long groupId;
	/**
	 * パスワード
	 */
//	@NotBlank(message="密码不能为空", groups = AddGroup.class)
	private String userPassword;
	/**
	 * ステータス
	 */
	@NotNull(message="ステータスを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Range(min=0,max=1, message = "ステータは0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private Integer status;
	/**
	 * 性別
	 */
	@NotNull(message="性別を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Range(min=0,max=1, message = "性別は0または1でなければなりません", groups = {AddGroup.class, UpdateGroup.class})
	private Integer sex;
	/**
	 * ユーザー名
	 */
	@NotBlank(message="ユーザー名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="ユーザ名は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	@Pattern(regexp = "^[a-zA-Z0-9]*$", message = "ユーザー名は半角英数字です", groups = {AddGroup.class, UpdateGroup.class})
	private String userName;
	/**
	 * ユーザー名（カナ）
	 */
	@NotBlank(message="ユーザー名（カナ）を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="ユーザー名（カナ）は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
//    @Pattern(regexp = "^[\\u30A0-\\u30FF]+$", message = "ユーザー名（カナ）フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String userNameKana;
	/**
	 * ユーザー名（漢字）
	 */
	@NotBlank(message="ユーザー名（漢字）を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=1,max=50,message="ユーザー名（漢字）は50ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String userNameKanji;
	/**
	 * ユーザー塩
	 */
	private String userSalt;
	/**
	 * メール
	 */
	@NotBlank(message="メールを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,10}){1,8})$", message = "メールアドレスフォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String email;
	/**
	 * 連絡・携帯
	 */
//	@NotBlank(message="連絡・携帯を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^(070|080|090)\\d{8}$", message = "連絡・携帯フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String mobile;
	/**
	 * 連絡・電話
	 */
//	@NotBlank(message="連絡・電話を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^$|^0[0-9]{9}$", message = "連絡・電話フォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String tel;
	/**
	 * 連絡・FAX
	 */
//	@NotBlank(message="連絡・FAXを空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
//	@Pattern(regexp = "^$|^0[0-9]{9}$", message = "連絡・FAXフォーマットエラー", groups = {AddGroup.class, UpdateGroup.class})
	private String fax;
	/**
	 * 部署名
	 */
//	@NotBlank(message="部署名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="部署名は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String departName;
	/**
	 * 職位名
	 */
//	@NotBlank(message="職位名を空白にすることはできません", groups = {AddGroup.class, UpdateGroup.class})
	@Length(min=0,max=100,message="職位名は100ビット未満である必要があります", groups = {AddGroup.class, UpdateGroup.class})
	private String positionName;
	/**
	 * 作成者
	 */
	private Long createUserId;
	/**
	 * 作成日時
	 */
	private Date createTime;
	/**
	 * 更新者
	 */
	private Long updateUserId;
	/**
	 * 更新日時
	 */
	private Date updateTime;
	/**
	 * バージョン番号
	 */
	private Integer versionNo;
	/**
	 * 削除フラグ
	 */
	private String delFlag;

	/**
	 * 角色ID列表
	 */
	private List<Long> roleIdList;

	/**
	 * グループ名
	 */
	private String groupName;

	/**
	 * グループタイプ
	 */
	private String groupType;

	/**
	 * ロールID
	 */
	private Long roleId;

	/**
	 * ロール名
	 */
	private String roleName;

	/**
	 * グループ所属グループ名
	 */
	private Long groupIdOf;
	/**
	 * グループ所属グループ名
	 */
	private String groupNameOf;
}
