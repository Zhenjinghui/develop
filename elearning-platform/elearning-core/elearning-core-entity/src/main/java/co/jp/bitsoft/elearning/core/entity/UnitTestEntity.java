package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 単元テスト
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("unit_test")
public class UnitTestEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 単元テストID
	 */
    @TableId
    private Long unitTestId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 教科ID
	 */
    private Long subjectId;

	/**
	 * 単元ID
	 */
    private Long unitId;

	/**
	 * テスト名
	 */
    private String testTitle;

	/**
	 * テスト概要
	 */
    private String testOutline;

	/**
	 * テスト説明
	 */
    private String testDetail;

	/**
	 * テストステータス
	 */
    private String testStatus;

	/**
	 * 公開開始日時
	 */
    private Date openOpenTime;

	/**
	 * 公開終了日時
	 */
    private Date openEndTime;

	/**
	 * テスト時間制限有無
	 */
    private String testConditionFlag;

	/**
	 * テスト時間制限（分）
	 */
    private Integer testConditionTime;

	/**
	 * テスト方式
	 */
    private String testType;

	/**
	 * 合格設定有無
	 */
    private String passFlag;

	/**
	 * 出題数
	 */
    private Long testQuestionsNum;

	/**
	 * 合格題数
	 */
    private Long passQuestionsNum;

	/**
	 * 合格率
	 */
    private Integer passQuestionsRate;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
