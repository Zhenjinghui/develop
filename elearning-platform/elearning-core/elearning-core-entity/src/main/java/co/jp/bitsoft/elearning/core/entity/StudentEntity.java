package co.jp.bitsoft.elearning.core.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;

import lombok.Data;

/**
 * 受講者
 *
 * @author BitSoft-inc
 */
@Data
@TableName("student")
public class StudentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者ID
	 */
    @TableId
    private Long studentId;

	/**
	 * グループID
	 */
    private Long groupId;

	/**
	 * パスワード
	 */
    private String userPassword;

	/**
	 * ユーザー塩
	 */
    private String userSalt;

	/**
	 * メール
	 */
    private String email;

	/**
	 * アカウントステータス
	 */
    private String accountStatus;

	/**
	 * ユーザー名
	 */
    private String userName;

	/**
	 * ユーザー名（漢字）
	 */
    private String userNameKanji;

	/**
	 * ユーザー名（カナ）
	 */
    private String userNameKana;

	/**
	 * ニックネーム
	 */
    private String nickName;

	/**
	 * 性別
	 */
    private String sex;

	/**
	 * 職業
	 */
	private String profession;

	/**
	 * 生年月日
	 */
    private String birthday;

	/**
	 * 住所・郵便番号
	 */
    private String addressZipCode;

	/**
	 * 住所・都道府県
	 */
    private String addressPrefecture;

	/**
	 * 住所・市区町村
	 */
    private String addressCity;

	/**
	 * 住所・番地以下
	 */
    private String addressOthers;

	/**
	 * 住所・建物名
	 */
    private String addressBuildingName;

	/**
	 * 連絡・携帯
	 */
    private String contactMobile;

	/**
	 * 連絡・電話
	 */
    private String contactTel;

	/**
	 * 連絡・FAX
	 */
    private String contactFax;

	/**
	 * 適用開始日
	 */
    private Date startTime;

	/**
	 * 適用終了日
	 */
    private Date overTime;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
