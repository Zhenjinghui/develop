package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * コード名称
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("code_name")
public class CodeNameEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * コードID
	 */
    @TableId
    private String codeId;

	/**
	 * コード値
	 */
    private String codeValue;

	/**
	 * 言語
	 */
    private String lang;

	/**
	 * ソート順
	 */
    private Integer sortOrder;

	/**
	 * コード名称
	 */
    private String codeName;

	/**
	 * コード略称
	 */
    private String shortName;

	/**
	 * オプション名称01
	 */
    private String option01;

	/**
	 * オプション名称02
	 */
    private String option02;

	/**
	 * オプション名称03
	 */
    private String option03;

	/**
	 * オプション名称04
	 */
    private String option04;

	/**
	 * オプション名称05
	 */
    private String option05;

	/**
	 * オプション名称06
	 */
    private String option06;

	/**
	 * オプション名称07
	 */
    private String option07;

	/**
	 * オプション名称08
	 */
    private String option08;

	/**
	 * オプション名称09
	 */
    private String option09;

	/**
	 * オプション名称10
	 */
    private String option10;

}
