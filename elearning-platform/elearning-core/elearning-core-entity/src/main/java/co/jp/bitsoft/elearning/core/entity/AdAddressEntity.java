package co.jp.bitsoft.elearning.core.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 日本全住所
 *
 * @author BitSoft-inc
 */
@Data
@TableName("ad_address")
public class AdAddressEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId
	private Integer id;
	/**
	 * $column.comments
	 */
	private Integer kenId;
	/**
	 * $column.comments
	 */
	private Integer cityId;
	/**
	 * $column.comments
	 */
	private Integer townId;
	/**
	 * $column.comments
	 */
	private String zip;
	/**
	 * $column.comments
	 */
	private Integer officeFlg;
	/**
	 * $column.comments
	 */
	private Integer deleteFlg;
	/**
	 * $column.comments
	 */
	private String kenName;
	/**
	 * $column.comments
	 */
	private String kenFuri;
	/**
	 * $column.comments
	 */
	private String cityName;
	/**
	 * $column.comments
	 */
	private String cityFuri;
	/**
	 * $column.comments
	 */
	private String townName;
	/**
	 * $column.comments
	 */
	private String townFuri;
	/**
	 * $column.comments
	 */
	private String townMemo;
	/**
	 * $column.comments
	 */
	private String kyotoStreet;
	/**
	 * $column.comments
	 */
	private String blockName;
	/**
	 * $column.comments
	 */
	private String blockFuri;
	/**
	 * $column.comments
	 */
	private String memo;
	/**
	 * $column.comments
	 */
	private String officeName;
	/**
	 * $column.comments
	 */
	private String officeFuri;
	/**
	 * $column.comments
	 */
	private String officeAddress;
	/**
	 * $column.comments
	 */
	private String newId;

}
