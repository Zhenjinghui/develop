package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * テスト問題選択肢
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("test_questions_choices")
public class TestQuestionsChoicesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 選択肢ID
	 */
    @TableId
    private Long testQuestionsChoicesId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 教科ID
	 */
    private Long subjectId;

	/**
	 * 単元ID
	 */
    private Long unitId;

	/**
	 * 単元テストID
	 */
    private Long unitTestId;

	/**
	 * テスト問題ID
	 */
    private Long testQuestionsId;

	/**
	 * 選択肢NO
	 */
    private Integer choicesNo;

	/**
	 * 選択肢文
	 */
    private String choicesContents;

	/**
	 * 正解フラグ
	 */
    private String correctFlag;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
