/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * システム検証コード
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
@TableName("sys_captcha")
public class SysCaptchaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * UUID
	 */
	@TableId(type = IdType.INPUT)
	private String uuid;
	/**
	 * コード
	 */
	private String code;
	/**
	 * 過期時間
	 */
	private Date expireTime;

}
