package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者ユーザーログイン履歴
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_login_history")
public class StudentLoginHistoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
    @TableId
    private Long id;

	/**
	 * 受講者ID
	 */
    private Long studentId;

	/**
	 * アクション区分
	 */
    private String loginOutKubun;

	/**
	 * アクション履歴日時
	 */
    private Date loginOutHistoryTime;

}
