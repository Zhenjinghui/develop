package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * アンケート問題
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("question_naire_question")
public class QuestionNaireQuestionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * アンケート問題ID
	 */
    @TableId
    private Long questionNaireQuestionId;

	/**
	 * アンケートID
	 */
    private Long questionNaireId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 問題名
	 */
    private String questionName;

	/**
	 * アンケート出題形式
	 */
    private String questionType;

	/**
	 * イメージカタログ画像PATH
	 */
    private String imageCatalogPath;

	/**
	 * イメージカタログ画像AWS S3 BUCKET名
	 */
    private String imageCatalogS3BucketName;

	/**
	 * イメージカタログ画像AWS S3 オブジェクトキー
	 */
    private String imageCatalogS3ObjectKey;

	/**
	 * 問題文
	 */
    private String questionContent;

	/**
	 * 解説文
	 */
    private String questionExplanation;

	/**
	 * 備考
	 */
    private String questionRemarks;

	/**
	 * アンケート問題ステータス
	 */
    private String questionStatus;

	/**
	 * 公開開始日時
	 */
    private Date openStartTime;

	/**
	 * 公開終了日時
	 */
    private Date openEndTime;

	/**
	 * 選択肢数
	 */
    private Integer choicesNum;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
