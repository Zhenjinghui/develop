package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者受講
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_course_study")
public class StudentCourseStudyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者受講ID
	 */
    @TableId
    private Long studentStudyId;

	/**
	 * 受講者コースID
	 */
    private Long studentCourseId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 教科ID
	 */
    private Long subjectId;

	/**
	 * 単元ID
	 */
    private Long unitId;

	/**
	 * 教材ID
	 */
    private Long teachingMaterialId;

	/**
	 * グループID
	 */
    private Long groupId;

	/**
	 * 教材受講状態
	 */
    private String studyStatus;

	/**
	 * 受講開始日時
	 */
    private Date studyStartTime;

	/**
	 * 受講終了日時
	 */
    private Date studyEndTime;

	/**
	 * 受講経過時間（分）
	 */
    private Integer studyTime;

	/**
	 * 動画鑑賞時間（分）
	 */
    private Integer studyVideoViewedTime;

	/**
	 * テキスト受講済ページ数
	 */
    private Integer textViewedPages;

	/**
	 * テキスト受講済文字数
	 */
    private Integer textViewedLettersNumber;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
