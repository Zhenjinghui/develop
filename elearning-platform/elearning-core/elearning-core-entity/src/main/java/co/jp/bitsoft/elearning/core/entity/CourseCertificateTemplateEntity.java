package co.jp.bitsoft.elearning.core.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;

import lombok.Data;

/**
 * 講座終了証テンプレート
 *
 * @author BitSoft-inc
 */
@Data
@TableName("course_certificate_template")
public class CourseCertificateTemplateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 講座終了証テンプレートID
	 */
    @TableId
    private Long courseCertificateTemplateId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * ヘッダ
	 */
    private String certificateHeader;

	/**
	 * 番号
	 */
    private String certificateNo;

	/**
	 * タイトル
	 */
    private String certificateTitle;

	/**
	 * 本文
	 */
    private String certificateContent;

	/**
	 * フッダ
	 */
    private String certificateFooter;

	/**
	 * 発行機関名
	 */
    private String issuerName;
	/**
	 * 発行機関サブ名
	 */
    private String issuerNameSub;
	/**
	 * 発行機関責任者名
	 */
    private String issuerResponsiblePartyName;
	/**
	 * 発行機関責任者肩書
	 */
    private String issuerResponsiblePartyTitle;
	/**
	 * 発行機関印アイコンPATH
	 */
    private String issuerIconSealPath;

	/**
	 * 発行機関印AWS S3 BUCKET名
	 */
    private String issuerIconSealS3BucketName;

	/**
	 * 発行機関印AWS S3 オブジェクトキー
	 */
    private String issuerIconSealS3ObjectKey;

	/**
	 * 終了証内画像PATH
	 */
    private String imageInPath;

	/**
	 * 終了証内画像AWS S3 BUCKET名
	 */
    private String imageInS3BucketName;

	/**
	 * 終了証内画像AWS S3 オブジェクトキー
	 */
    private String imageInS3ObjectKey;

	/**
	 * イメージ画像PATH
	 */
    private String imagePath;

	/**
	 * イメージ画像AWS S3 BUCKET名
	 */
    private String imageS3BucketName;

	/**
	 * イメージ画像AWS S3 オブジェクトキー
	 */
    private String imageS3ObjectKey;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

	/**
	 * 終了証テンプレートPATH
	 */
	private String certificateTemplatePath;
}
