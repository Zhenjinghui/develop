package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * 講座公開情報
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("course_open_info")
public class CourseOpenInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 講座公開情報ID
	 */
    @TableId
    private Long courseOpenInfoId;

	/**
	 * グループID
	 */
    private Long groupId;

	/**
	 * 講座ID
	 */
    private Long courseId;

}
