package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * アンケート問題選択肢
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("question_naire_questions_choices")
public class QuestionNaireQuestionsChoicesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * アンケート問題選択肢ID
	 */
    @TableId
    private Long questionNaireQuestionsChoicesId;

	/**
	 * アンケート問題ID
	 */
    private Long questionNaireQuestionId;

	/**
	 * アンケートID
	 */
    private Long questionNaireId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 選択肢文
	 */
    private String choicesContents;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
