package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者コース管理
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_course_manage")
public class StudentCourseManageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者コースID
	 */
    @TableId
    private Long studentCourseId;

	/**
	 * 受講者ID
	 */
    private Long studentId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * グループID
	 */
    private Long groupId;

	/**
	 * 受講状態
	 */
    private String studyStatus;

	/**
	 * 受講申請日時
	 */
    private Date courseRegTime;

	/**
	 * 受講申請理由
	 */
    private String courseRegReason;

	/**
	 * 受講申請審査者
	 */
    private Long sysUserId;

	/**
	 * 受講申請承認日時
	 */
    private Date regApprovalTime;

	/**
	 * 受講申請承認連絡事項
	 */
    private String regApprovalContact;

	/**
	 * 受講申請拒否日時
	 */
    private Date regRejectTime;

	/**
	 * 受講申請拒否理由
	 */
    private String regRejectReason;

	/**
	 * 受講受験開始日時
	 */
    private Date studyTestStartTime;

	/**
	 * 受講受験終了日時
	 */
    private Date studyTestEndTime;

	/**
	 * 受講キャンセル申請日時
	 */
    private Date cancelRegTime;

	/**
	 * 受講キャンセル申請理由
	 */
    private String cancelRegReason;

	/**
	 * 受講キャンセル申請審査者
	 */
    private Long cancelSysUserId;

	/**
	 * 受講キャンセル申請承認日時
	 */
    private Date cancelApprovalTime;

	/**
	 * 受講キャンセル申請承認連絡事項
	 */
    private String cancelApprovalContact;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
