/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * システムロール
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
@TableName("sys_role")
public class SysRoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ロールID
	 */
	@TableId
	private Long roleId;

	/**
	 * ロール名
	 */
	@NotBlank(message=" ロール名不能为空")
	private String roleName;
	/**
	 * 備考
	 */
	private String remark;
	/**
	 * 作成ユーザーID
	 */
	@TableField(fill = FieldFill.INSERT)
	private Long createUserId;
	/**
	 * 作成時間
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

	@TableField(exist=false)
	private List<Long> menuIdList;
}
