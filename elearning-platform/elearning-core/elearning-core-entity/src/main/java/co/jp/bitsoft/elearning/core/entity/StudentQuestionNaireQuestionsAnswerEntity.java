package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者アンケート問題回答
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_question_naire_questions_answer")
public class StudentQuestionNaireQuestionsAnswerEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者アンケート問題回答ID
	 */
    @TableId
    private Long studentQuestionNaireQuestionsAnswerId;

	/**
	 * アンケート回答ID
	 */
    private Long studentQuestionNaireAnswerId;

	/**
	 * アンケート問題ID
	 */
    private Long questionNaireQuestionId;

	/**
	 * アンケートID
	 */
    private Long questionNaireId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * アンケート回答内容
	 */
    private String anserContents;

	/**
	 * 受講者アンケート問題回答ステータス
	 */
    private String answerStatus;

	/**
	 * 回答開始日時
	 */
    private Date answerStartTime;

	/**
	 * 回答終了日時
	 */
    private Date answerEndTime;

	/**
	 * 解答時間（秒）
	 */
    private Integer answerTimeSeconds;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Integer createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
