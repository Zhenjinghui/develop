package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 教材
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("teaching_material")
public class TeachingMaterialEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 教材ID
	 */
    @TableId
    private Long teachingMaterialId;
	/**
	 * 表示順
	 */
	private Long dispNo;
	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 教科ID
	 */
    private Long subjectId;

	/**
	 * 単元ID
	 */
    private Long unitId;

	/**
	 * 教材名
	 */
    private String teachingMaterialTitle;

	/**
	 * 教材画像PATH
	 */
    private String teachingMaterialImagePath;

	/**
	 * 教材AWS S3 BUCKET名
	 */
    private String teachingMaterialS3BucketName;

	/**
	 * 教材AWS S3 オブジェクトキー
	 */
    private String teachingMaterialS3ObjectKey;

	/**
	 * 教材概要
	 */
    private String teachingMaterialOutline;

	/**
	 * 教材説明
	 */
    private String teachingMaterialDetail;

	/**
	 * 教材ステータス
	 */
    private String teachingMaterialStatus;

	/**
	 * コンテンツ種別
	 */
    private String contentType;

	/**
	 * コンテンツURL
	 */
    private String contentUrl;

	/**
	 * コンテンツ（動画）放送時間（分）
	 */
    private Integer contentVideoTime;

	/**
	 * コンテンツページ数
	 */
    private Integer contentPages;

	/**
	 * コンテンツ文字数
	 */
    private Integer contentLettersNum;

	/**
	 * 受講解放条件区分
	 */
    private String conditionKbn;

	/**
	 * 公開開始日時
	 */
    private Date openStartTime;

	/**
	 * 公開終了日時
	 */
    private Date openEndTime;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
