package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * コードパターン
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("code_pattern")
public class CodePatternEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * コードID
	 */
    @TableId
    private String codeId;

	/**
	 * コード値
	 */
    private String codeValue;

	/**
	 * パターン01
	 */
    private String pattern01;

	/**
	 * パターン02
	 */
    private String pattern02;

	/**
	 * パターン03
	 */
    private String pattern03;

	/**
	 * パターン04
	 */
    private String pattern04;

	/**
	 * パターン05
	 */
    private String pattern05;

	/**
	 * パターン06
	 */
    private String pattern06;

	/**
	 * パターン07
	 */
    private String pattern07;

	/**
	 * パターン08
	 */
    private String pattern08;

	/**
	 * パターン09
	 */
    private String pattern09;

	/**
	 * パターン10
	 */
    private String pattern10;

	/**
	 * パターン11
	 */
    private String pattern11;

	/**
	 * パターン12
	 */
    private String pattern12;

	/**
	 * パターン13
	 */
    private String pattern13;

	/**
	 * パターン14
	 */
    private String pattern14;

	/**
	 * パターン15
	 */
    private String pattern15;

	/**
	 * パターン16
	 */
    private String pattern16;

	/**
	 * パターン17
	 */
    private String pattern17;

	/**
	 * パターン18
	 */
    private String pattern18;

	/**
	 * パターン19
	 */
    private String pattern19;

	/**
	 * パターン20
	 */
    private String pattern20;

}
