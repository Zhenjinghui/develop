package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * アンケート
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("question_naire")
public class QuestionNaireEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * アンケートID
	 */
    @TableId
    private Long questionNaireId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * アンケート種別
	 */
    private String questionNaireType;

	/**
	 * アンケート件名
	 */
    private String questionNaireName;

	/**
	 * アンケート概要
	 */
    private String questionNaireOverview;

	/**
	 * アンケート詳細説明
	 */
    private String questionNaireDetail;

	/**
	 * アンケートステータス
	 */
    private String questionNaireStatus;

	/**
	 * アンケート公開日時
	 */
    private Date questionNaireOpenTime;

	/**
	 * アンケート非公開日時
	 */
    private Date questionNaireEndTime;

	/**
	 * メール送信有無
	 */
    private String sendMailFlag;

	/**
	 * サイト内プッシュ通知有無
	 */
    private String sendMessageFlag;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
