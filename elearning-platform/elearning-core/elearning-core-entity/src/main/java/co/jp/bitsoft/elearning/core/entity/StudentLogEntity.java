package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者ログ
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_log")
public class StudentLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
    @TableId
    private Long id;

	/**
	 * 受講者ID
	 */
    private Long studentId;

	/**
	 * ユーザー操作
	 */
    private String userOperation;

	/**
	 * リクエストメソッド
	 */
    private String requestMethod;

	/**
	 * パラメータ
	 */
    private String params;

	/**
	 * 実行時間
	 */
    private Long excuteTime;

	/**
	 * IPアドレス
	 */
    private String ip;

	/**
	 * 作成時間
	 */
    private Date createDate;

}
