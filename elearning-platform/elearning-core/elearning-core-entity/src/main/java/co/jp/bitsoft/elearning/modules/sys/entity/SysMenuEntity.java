/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * システムメニュー
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
@TableName("sys_menu")
public class SysMenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * メニューID
	 */
	@TableId
	private Long menuId;

	/**
	 * 親メニューID
	 */
	private Long parentId;
	
	/**
	 * メニュー名
	 */
	private String menuName;
	/**
	 * メニューURL
	 */
	private String menuUrl;
	/**
	 * 権限
	 */
	private String perms;
	/**
	 * メニュー種別
	 */
	private Integer menuType;
	/**
	 * メニューアイコン
	 */
	private String menuIcon;
	/**
	 * ソート順
	 */
	private Integer orderNum;
	
	/**
	 * ztree属性
	 */
	@TableField(exist=false)
	private Boolean open;

	@TableField(exist=false)
	private List<?> list;
	
	/**
	 * 親メニュー名
	 */
	@TableField(exist=false)
	private String parentName;	

}
