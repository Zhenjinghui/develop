package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者テスト
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_test")
public class StudentTestEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者テストID
	 */
    @TableId
    private Long studentTestId;

	/**
	 * 受講者コースID
	 */
    private Long studentCourseId;

	/**
	 * 受講者ID
	 */
    private Long studentId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * グループID
	 */
    private Long groupId;

	/**
	 * 単元テストID
	 */
    private Long unitTestId;

	/**
	 * テストステータス
	 */
    private String answerStatus;

	/**
	 * テスト点数
	 */
    private Integer answerPoint;

	/**
	 * テスト回答問題数
	 */
    private Integer answerNum;

	/**
	 * テスト解答率（%）
	 */
    private Integer answerRate;

	/**
	 * テスト正解率（%）
	 */
    private Integer answerCorrectRate;

	/**
	 * テスト判定結果
	 */
    private String answerResult;

	/**
	 * 解答開始日時
	 */
    private Date answerStartTime;

	/**
	 * 解答終了日時
	 */
    private Date answerEndTime;

	/**
	 * 解答所要時間（分）
	 */
    private Integer answerTime;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
