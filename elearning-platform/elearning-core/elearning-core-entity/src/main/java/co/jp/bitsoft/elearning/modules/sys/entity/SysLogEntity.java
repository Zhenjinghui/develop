/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;


/**
 * システムログ
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
@TableName("sys_log")
public class SysLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 運用者ユーザーID
	 */
	private Long userId;
	/**
	 * ユーザー操作
	 */
	private String userOperation;
	/**
	 * リクエストメソッド
	 */
	private String requestMethod;
	/**
	 * パラメータ
	 */
	private String params;
	/**
	 * 実行時間
	 */
	private Long excuteTime;
	/**
	 * IPアドレス
	 */
	private String ip;
	/**
	 * 作成時間
	 */
	private Date createDate;

}
