package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 講座インフォメーション
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("course_info")
public class CourseInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * インフォメーションID
	 */
    @TableId
    private Long infoId;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * インフォメーション種別
	 */
    private String infoType;

	/**
	 * インフォメーション件名
	 */
    private String infoName;

	/**
	 * インフォメーション概要
	 */
    private String infoOverview;

	/**
	 * インフォメーション詳細
	 */
    private String infoDetail;

	/**
	 * インフォメーションステータス
	 */
    private String infoStatus;

	/**
	 * 公開開始日時
	 */
    private Date openStartTime;

	/**
	 * 公開終了日時
	 */
    private Date openEndTime;

	/**
	 * メール送信有無
	 */
    private String sendMailFlag;

	/**
	 * サイト内プッシュ通知有無
	 */
    private String sendMessageFlag;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
