package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 問い合わせメッセージ
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("inquiry_message")
public class InquiryMessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * メッセージID
	 */
    @TableId
    private Long inquiryMessageId;

	/**
	 * 問い合わせID
	 */
    private Long inquiryId;

	/**
	 * FROMユーザーID
	 */
    private Long fromUserId;

	/**
	 * FROMユーザー種別
	 */
    private String fromUserType;

	/**
	 * TOユーザーID
	 */
    private Long toUserId;

	/**
	 * TOユーザー種別
	 */
    private String toUserType;

	/**
	 * メッセージステータス
	 */
    private String messageStatus;

	/**
	 * メッセージ送信日時
	 */
    private Date messageTime;

	/**
	 * メッセージ内容
	 */
    private String messageContent;

	/**
	 * メッセージ開封日時
	 */
    private Date messageOpenTime;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Long versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
