/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * システム設定
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-29 12:55:44
 */
@Data
@TableName("sys_config")
public class SysConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * KEY
	 */
	@NotBlank(message="参数名不能为空")	 
	private String paramKey;
	/**
	 * VALUE
	 */
	@NotBlank(message="参数值不能为空")	 
	private String paramValue;
	/**
	 * ステータス
	 */
	private Integer status;
	/**
	 * 備考
	 */
	private String remark;

}
