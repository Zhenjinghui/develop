package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者アンケート回答
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_question_naire_answer")
public class StudentQuestionNaireAnswerEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * アンケート回答ID
	 */
    @TableId
    private Long studentQuestionNaireAnswerId;

	/**
	 * 受講者ID
	 */
    private Long studentId;

	/**
	 * アンケートID
	 */
    private Long questionNaireId;

	/**
	 * アンケート回答ステータス
	 */
    private String answerStatus;

	/**
	 * アンケート回答内容
	 */
    private String anserContents;

	/**
	 * アンケート回答問題数
	 */
    private Integer answerNum;

	/**
	 * アンケート解答率（%）
	 */
    private Integer answerRate;

	/**
	 * 回答開始日時
	 */
    private Date answerStartTime;

	/**
	 * 回答終了日時
	 */
    private Date answerEndTime;

	/**
	 * 解答時間（分）
	 */
    private Integer answerTime;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
