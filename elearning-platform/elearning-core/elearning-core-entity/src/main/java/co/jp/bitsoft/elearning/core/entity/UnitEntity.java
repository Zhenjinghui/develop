package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 単元
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("unit")
public class UnitEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 単元ID
	 */
    @TableId
    private Long unitId;

	/**
	 * 表示順
	 */
	private Long dispNo;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 教科ID
	 */
    private Long subjectId;

	/**
	 * 単元名
	 */
    private String unitName;

	/**
	 * 単元画像PATH
	 */
    private String unitIamgePath;

	/**
	 * 単元AWS S3 BUCKET名
	 */
    private String unitS3BucketName;

	/**
	 * 単元AWS S3 オブジェクトキー
	 */
    private String unitS3ObjectKey;

	/**
	 * 単元概要
	 */
    private String unitOutline;

	/**
	 * 単元説明
	 */
    private String unitDetail;

	/**
	 * 単元ステータス
	 */
    private String unitStatus;

	/**
	 * 公開開始日時
	 */
    private Date openStartTime;

	/**
	 * 公開終了日時
	 */
    private Date openEndTime;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
