package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 問い合わせ
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("inquiry")
public class InquiryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 問い合わせID
	 */
    @TableId
    private Long inquiryId;

	/**
	 * 問い合わせ種類
	 */
    private String inquiryType;

	/**
	 * 講座ID
	 */
    private Long courseId;

	/**
	 * 問い合わせ件名
	 */
    private String inquiryName;

	/**
	 * FROMユーザーID
	 */
    private Long fromUserId;

	/**
	 * FROMユーザー種別
	 */
    private String fromUserType;

	/**
	 * TOグループID
	 */
    private Long toGroupId;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;

}
