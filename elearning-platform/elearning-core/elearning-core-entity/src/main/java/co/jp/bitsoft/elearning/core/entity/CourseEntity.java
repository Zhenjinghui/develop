package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 講座
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("course")
public class CourseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 講座ID
	 */
    @TableId
    private Long courseId;

	/**
	 * 講座名
	 */
    private String courseTitle;

	/**
	 * 所属グループID
	 */
    private Long groupId;

	/**
	 * 講座画像PATH
	 */
    private String courseImagePath;

	/**
	 * AWS S3 BUCKET名
	 */
    private String courseS3BucketName;

	/**
	 * AWS S3 オブジェクトキー
	 */
    private String courseS3ObjectKey;

	/**
	 * 講座類別
	 */
    private String courseType;

	/**
	 * 講座タグ
	 */
    private String courseTag;

	/**
	 * 講座概要
	 */
    private String courseOutline;

	/**
	 * 講座説明
	 */
    private String courseDetail;

	/**
	 * 受講申請要否フラグ
	 */
    private String courseRegFlag;

	/**
	 * 講座ステータス
	 */
    private String courseStatus;

	/**
	 * 公開開始日時
	 */
    private Date openStartTime;

	/**
	 * 公開終了日時
	 */
    private Date openEndTime;

	/**
	 * 作成者
	 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

	/**
	 * 作成日時
	 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

	/**
	 * 更新者
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUserId;

	/**
	 * 更新日時
	 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

	/**
	 * バージョン番号
	 */
    @Version
    private Integer versionNo;

	/**
	 * 削除フラグ
	 */
    private String delFlag;
}
