package co.jp.bitsoft.elearning.core.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 受講者ユーザートークン
 * 
 * @author BitSoft-inc
 */
@Data
@TableName("student_token")
public class StudentTokenEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者ID
	 */
    @TableId(value = "student_id",type = IdType.INPUT)
    private Long studentId;

	/**
	 * トークン
	 */
    private String token;

	/**
	 * 過期時間
	 */
    private Date expireTime;

	/**
	 * 更新時間
	 */
    private Date updateTime;

}
