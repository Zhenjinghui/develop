package co.jp.bitsoft.elearning.common.mail;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import co.jp.bitsoft.elearning.common.exception.RRException;

@Component
public class CommonMailUtils {

	@Autowired
    private JavaMailSender mailSender;
	@Value("${spring.mail.from}")
	private String from;

	 /**
     * メールを送信する
     * @param to
     * @param subject
     * @param content
     */
	public boolean sendSimpleMail(String to, String subject, String content){
        MimeMessage message = mailSender.createMimeMessage();
		try {
	        MimeMessageHelper helper = new MimeMessageHelper(message, true);
	        helper.setFrom(from);
	        helper.setTo(to);
	        helper.setSubject(subject);
	        helper.setText(content);
	        mailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
    }
	 /**
     * HTMLメールを送信する
     * @param to
     * @param subject
     * @param content
     */
	public boolean sendHtmlMail(String to, String subject, String content) {
        MimeMessage message = mailSender.createMimeMessage();
		try {
	        MimeMessageHelper helper = new MimeMessageHelper(message, true);
	        helper.setFrom(from);
	        helper.setTo(to);
	        helper.setSubject(subject);
	        helper.setText(content,true);
	        mailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
    }

	/**
	 * HTMLメールを送信する
	 * @param p_from
	 * @param p_to
	 * @param p_subject
	 * @param p_content
	 */
	public boolean sendQAMail(String p_from, String p_to, String p_subject, String p_content) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(p_from);
			helper.setTo(p_to);
			helper.setSubject(p_subject);
			helper.setText(p_content,true);
			mailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * htmlテンプレートファイルを読む
	 * @return
	 * @throws Exception
	 */
	public String readHtml(String htmlName) throws Exception {

        InputStream inputStream = null;
        inputStream = this.getClass().getResourceAsStream("/html/"+htmlName);
        BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream));

        StringBuffer buffer = new StringBuffer();
        String line = "";
        try {
            while ((line = fileReader.readLine()) != null) {
                buffer.append(line);
            }
        } catch (Exception e) {
			throw new RRException("htmlテンプレートファイルの読み取りに失敗しました！");
        } finally {
			inputStream.close();
	        fileReader.close();
        }
        return buffer.toString();
	}

}
