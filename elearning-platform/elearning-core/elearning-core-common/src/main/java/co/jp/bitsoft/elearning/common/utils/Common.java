package co.jp.bitsoft.elearning.common.utils;

import java.text.DecimalFormat;

public class Common {
    private static final String DEF_SHARP = "#";
    /**
     * 電話番号をフォーマットする。
     *
     * @author 甄
     * @param str 電話番号
     * @return フォーマット後の電話番号
     */
    public static String getFormattedMobile(String str) {
        String rtn = "";
        if (null != str && str.length() == 11) {
            rtn = str.substring(0, 3) + "-" + str.substring(3, 7) + "-" + str.substring(7, 11);
        }
        return rtn;
    }

    /**
     * 固定電話（FAX)をフォーマットする。
     *
     * @author 甄
     * @param str 電話番号
     * @return フォーマット後の電話番号
     */
    public static String getFormattedTel(String str) {
        String rtn = "";
        if (null != str && str.length() == 10) {
            rtn = str.substring(0, 2) + "-" + str.substring(2, 6) + "-" + str.substring(6, 10);
        }
        return rtn;
    }

    /**
     * 郵政番号をフォーマットする。
     *
     * @author 甄
     * @param str 郵政番号
     * @return フォーマット後の郵政番号
     */
    public static String getFormattedZipCode(String str) {
        String rtn = "";
        if (null != str && str.length() == 7) {
            rtn = str.substring(0, 3) + "-" + str.substring(3, 7);
        }
        return rtn;
    }

    /**
     * 引数の先頭に「#」する。
     *
     * @author 甄
     * @param str 文字列
     * @return 文字列の先頭に「#」をつける。
     */
    public static String getFormattedImpNumber(String str) {
        String rtn = "";
        if (null != str) {
            rtn = DEF_SHARP + str;
        }
        return rtn;
    }
}
