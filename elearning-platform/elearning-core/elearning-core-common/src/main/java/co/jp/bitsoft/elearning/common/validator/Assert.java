/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.common.validator;

import org.apache.commons.lang.StringUtils;

import co.jp.bitsoft.elearning.common.exception.RRException;

/**
 * 数据校验
 *
 * @author  BitSoft
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new RRException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new RRException(message);
        }
    }
}
