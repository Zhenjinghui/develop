/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.common.validator.group;

import javax.validation.GroupSequence;

/**
 * 定义校验顺序，如果AddGroup组失败，则UpdateGroup组不会再校验
 *
 * @author  BitSoft
 */
@GroupSequence({AddGroup.class, UpdateGroup.class})
public interface Group {

}
