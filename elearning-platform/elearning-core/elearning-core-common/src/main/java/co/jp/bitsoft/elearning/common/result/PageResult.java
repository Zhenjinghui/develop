package co.jp.bitsoft.elearning.common.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 分页结果。
 * @author herryliq
 *
 */
@ApiModel("接口返回分页结果包装数据结构")
@Data
@Accessors(chain = true)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PageResult<T> extends Result<T> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 总数
     */
	@ApiModelProperty(value = "数据总条数")
    private Long total;
    
    /**
     * 总页数
     */
	@ApiModelProperty(value = "总页数")
    private Integer totalPage;

	protected PageResult() {
	}

	/**
	 * 全参数构造
	 *
	 * @param code
	 *            状态码
	 * @param msg
	 *            消息
	 * @param data
	 *            数据
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> PageResult<E> create(int code, String msg, E data) {
		PageResult<E> response = new PageResult<E>();
		response.setCode(code);
		response.setMsg(msg);
		response.setData(data);
		return response;
	}

	/**
	 * 快捷构造 成功结果
	 *
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> PageResult<E> success() {
		PageResult<E> response = new PageResult<E>();
		response.setCode(CODE_SUCCESS);
		response.setMsg(DEFAULT_MESSAGE);
		return response;
	}

	/**
	 * 快捷构造 成功结果
	 *
	 * @param data
	 *            数据
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> PageResult<E> success(E data) {
		PageResult<E> response = success();
		response.setData(data);
		return response;
	}

	/**
	 * 快捷构造 成功结果
	 *
	 * @param msg
	 *            消息
	 * @param data
	 *            数据
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> PageResult<E> success(String msg, E data) {
		return PageResult.create(CODE_SUCCESS, msg, data);
	}

	/**
	 * 快捷构造 警告结果
	 *
	 * @param msg
	 *            消息
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> PageResult<E> warn(String msg) {
		PageResult<E> response = new PageResult<E>();
		response.setCode(CODE_WARN);
		response.setMsg(msg);
		return response;
	}

	/**
	 * 快捷构造 失败结果
	 *
	 * @param msg
	 *            消息
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> PageResult<E> error(String msg) {
		PageResult<E> response = new PageResult<E>();
		response.setCode(CODE_ERROR);
		response.setMsg(msg);
		return response;
	}

	/**
	 * 快捷构造 失败结果
	 *
	 * @param msg
	 *            消息
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> PageResult<E> error(int code, String msg) {
		PageResult<E> response = new PageResult<E>();
		response.setCode(code);
		response.setMsg(msg);
		return response;
	}

}
