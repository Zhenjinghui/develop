package co.jp.bitsoft.elearning.common.result;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 应答基类 强烈建议使用泛型 sample:
 * 

 */
@ApiModel("接口返回结果包装数据结构")
@Data
//@Accessors(chain = true)
@AllArgsConstructor
public class Result<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4806860802582703097L;

	public static final int CODE_SUCCESS = 200;// 成功
	public static final int CODE_ERROR = 500;// 错误
	public static final int CODE_WARN = 800;// 警告
	/** 请求错误（4字头） */
	public static final int CD_400_Bad_Request = 400; // 参数错误
	public static final int CD_401_Unauthorized = 401; // 验证失败
	public static final int CD_403_Forbidden = 403;
	public static final int CD_404_Not_Found = 404;
	public static final int CD_405_Method_Not_Allowed = 405;
	public static final int CD_406_Not_Acceptable = 406;
	public static final int CD_407_Proxy_Authentication_Required = 407;
	public static final int CD_408_Request_Timeout = 408;
	public static final int CD_409_Conflict = 409;
	public static final int CD_410_Gone = 410;
	public static final int CD_411_Length_Required = 411;
	public static final int CD_412_Precondition_Failed = 412;
	public static final int CD_413_Request_Entity_Too_Large = 413;
	public static final int CD_414_Request_URI_Too_Long = 414;
	public static final int CD_415_Unsupported_Media_Type = 415;
	public static final int CD_416_Requested_Range_Not_Satisfiable = 416;
	public static final int CD_417_Expectation_Failed = 417;
	public static final int CD_422_Unprocessable_Entity = 422;
	public static final int CD_423_Locked = 423;
	public static final int CD_424_Failed_Dependency = 424;
	public static final int CD_425_Unordered_Collection = 425;
	public static final int CD_426_Upgrade_Required = 426;
	public static final int CD_449_Retry_With = 449;
	/** 服务器错误（5、6字头） */
	public static final int CD_500_Internal_Server_Error = 500; // 服务错误
	public static final int CD_501_Not_Implemented = 501;
	public static final int CD_502_Bad_Gateway = 502;
	public static final int CD_503_Service_Unavailable = 503;
	public static final int CD_504_Gateway_Timeout = 504;
	public static final int CD_505_HTTP_Version_Not_Supported = 505;
	public static final int CD_506_Variant_Also_Negotiates = 506;
	public static final int CD_507_Insufficient_Storage = 507;
	public static final int CD_509_Bandwidth_Limit_Exceeded = 509;
	public static final int CD_510_Not_Extended = 510;
	public static final int CD_511_Not_Bind_User = 511;//未绑定用户
	public static final int CD_512_Not_Found_User_Info = 512;//用户信息不完善
	public static final int CD_600_Unparseable_Response_Headers = 600;
	/** 警告 (8字头)*/


	public static final String DEFAULT_MESSAGE = "操作成功";// 错误

	@ApiModelProperty(value = "反馈代码", required = true)
	private int code;
	@ApiModelProperty(value = "反馈消息")
	private String msg;
	@ApiModelProperty(value = "操作返回数据")
	private T data;
	@ApiModelProperty(value = "操作时间戳", dataType="Long")
	private Long t = System.currentTimeMillis();
	@ApiModelProperty(value = "接口地址")
	private String url;
	@ApiModelProperty(value = "接口参数")
	private Object args;

	protected Result() {
	}

	/**
	 * 全参数构造
	 * 
	 * @param code
	 *            状态码
	 * @param msg
	 *            消息
	 * @param data
	 *            数据
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> Result<E> create(int code, String msg, E data) {
		Result<E> response = new Result<E>();
		response.setCode(code);
		response.setMsg(msg);
		response.setData(data);
		return response;
	}

	/**
	 * 快捷构造 成功结果
	 *
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> Result<E> success() {
		Result<E> response = new Result<E>();
		response.setCode(CODE_SUCCESS);
		response.setMsg(DEFAULT_MESSAGE);
		return response;
	}

	/**
	 * 快捷构造 成功结果
	 * 
	 * @param data
	 *            数据
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> Result<E> success(E data) {
		Result<E> response = success();
		response.setData(data);
		return response;
	}

	/**
	 * 快捷构造 成功结果
	 * 
	 * @param msg
	 *            消息
	 * @param data
	 *            数据
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> Result<E> success(String msg, E data) {
		return Result.create(CODE_SUCCESS, msg, data);
	}

	/**
	 * 快捷构造 警告结果
	 * 
	 * @param msg
	 *            消息
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> Result<E> warn(String msg) {
		Result<E> response = new Result<E>();
		response.setCode(CODE_WARN);
		response.setMsg(msg);
		return response;
	}

	/**
	 * 快捷构造 失败结果
	 * 
	 * @param msg
	 *            消息
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> Result<E> error(String msg) {
		Result<E> response = new Result<E>();
		response.setCode(CODE_ERROR);
		response.setMsg(msg);
		return response;
	}

	/**
	 * 快捷构造 失败结果
	 * 
	 * @param msg
	 *            消息
	 * @param <E>
	 *            E
	 * @return Response
	 */
	public static <E> Result<E> error(int code, String msg) {
		Result<E> response = new Result<E>();
		response.setCode(code);
		response.setMsg(msg);
		return response;
	}

	@JsonIgnore
	public boolean isSucceeded() {
		return code == CODE_SUCCESS;
	}

	@JsonIgnore
	public boolean isFailed() {
		return code != CODE_SUCCESS;
	}

}