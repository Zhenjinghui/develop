package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireEntity;
import co.jp.bitsoft.elearning.core.service.QuestionNaireService;
import co.jp.bitsoft.elearning.core.service.dao.QuestionNaireDao;


@Service("questionNaireService")
public class QuestionNaireServiceImpl extends ServiceImpl<QuestionNaireDao, QuestionNaireEntity> implements QuestionNaireService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QuestionNaireEntity> page = this.page(
                new Query<QuestionNaireEntity>().getPage(params),
                new QueryWrapper<QuestionNaireEntity>()
        );

        return new PageUtils(page);
    }

}