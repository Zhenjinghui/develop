package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.CourseCertificateTemplateEntity;
import co.jp.bitsoft.elearning.core.service.CourseCertificateTemplateService;
import co.jp.bitsoft.elearning.core.service.dao.CourseCertificateTemplateDao;


@Service("courseCertificateTemplateService")
public class CourseCertificateTemplateServiceImpl extends ServiceImpl<CourseCertificateTemplateDao, CourseCertificateTemplateEntity> implements CourseCertificateTemplateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CourseCertificateTemplateEntity> page = this.page(
                new Query<CourseCertificateTemplateEntity>().getPage(params),
                new QueryWrapper<CourseCertificateTemplateEntity>()
        );

        return new PageUtils(page);
    }

}