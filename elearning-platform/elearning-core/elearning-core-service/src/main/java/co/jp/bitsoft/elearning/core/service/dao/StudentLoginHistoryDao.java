package co.jp.bitsoft.elearning.core.service.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.StudentLoginHistoryEntity;

/**
 * 受講者ユーザーログイン履歴
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface StudentLoginHistoryDao extends BaseMapper<StudentLoginHistoryEntity> {

}
