package co.jp.bitsoft.elearning.core.service.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;

/**
 * 単元テスト
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface UnitTestDao extends BaseMapper<UnitTestEntity> {

	IPage<UnitTestEntity> selectUnitTestPage(Page<UnitTestEntity> page,UnitTestEntity unitTestEntity);


}
