package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.CodeNameEntity;
import co.jp.bitsoft.elearning.core.service.CodeNameService;
import co.jp.bitsoft.elearning.core.service.dao.CodeNameDao;


@Service("codeNameService")
public class CodeNameServiceImpl extends ServiceImpl<CodeNameDao, CodeNameEntity> implements CodeNameService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CodeNameEntity> page = this.page(
                new Query<CodeNameEntity>().getPage(params),
                new QueryWrapper<CodeNameEntity>()
        );

        return new PageUtils(page);
    }

}