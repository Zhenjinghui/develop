package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.core.entity.StudentLoginHistoryEntity;

/**
 * 受講者ユーザーログイン履歴
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface StudentLoginHistoryService extends IService<StudentLoginHistoryEntity> {

//    PageUtils queryPage(Map<String, Object> params);
//
//    PageUtils selectStudyLoginHistory(Map<String, Object> params);

}

