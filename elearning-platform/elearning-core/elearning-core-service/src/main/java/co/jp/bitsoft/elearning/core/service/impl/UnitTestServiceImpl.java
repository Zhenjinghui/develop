package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.service.UnitTestService;
import co.jp.bitsoft.elearning.core.service.dao.UnitTestDao;


@Service("unitTestService")
public class UnitTestServiceImpl extends ServiceImpl<UnitTestDao, UnitTestEntity> implements UnitTestService {

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//		String unitId = (String)params.get("unitId");
//
//        IPage<UnitTestEntity> page = this.page(
//                new Query<UnitTestEntity>().getPage(params),
//                new QueryWrapper<UnitTestEntity>()
//                .eq("unit_id",Integer.parseInt(unitId))
//        );
//
//        return new PageUtils(page);
//    }
//
//    public PageUtils selectUnitTestPage(Map<String, Object> params) {
//        //分页参数
//        long curPage = 1;
//        long limit = 10;
//        if(params.get(Constant.PAGE) != null){
//            curPage = Long.parseLong((String)params.get(Constant.PAGE));
//        }
//        if(params.get(Constant.LIMIT) != null){
//            limit = Long.parseLong((String)params.get(Constant.LIMIT));
//        }
//		Page<UnitTestEntity> page = new Page<>(curPage, limit);
//
//        String courseTitle = (String)params.get("courseTitle");
//        String subjectTitle = (String)params.get("subjectTitle");
//        String unitName = (String)params.get("unitName");
//        String testTitle = (String)params.get("testTitle");
//        String testStatus = (String)params.get("testStatus");
//
//        UnitTestEntity unitTestEntity = new UnitTestEntity();
//        unitTestEntity.setCourseTitle(courseTitle);
//        unitTestEntity.setSubjectTitle(subjectTitle);
//        unitTestEntity.setUnitName(unitName);
//        unitTestEntity.setTestTitle(testTitle);
//        unitTestEntity.setTestStatus(testStatus);
//
//		IPage<UnitTestEntity> pageList = baseMapper.selectUnitTestPage(page,unitTestEntity);
//
//		return new PageUtils(pageList);
//    }


}