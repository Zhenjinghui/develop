package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.CodePatternEntity;
import co.jp.bitsoft.elearning.core.service.CodePatternService;
import co.jp.bitsoft.elearning.core.service.dao.CodePatternDao;


@Service("codePatternService")
public class CodePatternServiceImpl extends ServiceImpl<CodePatternDao, CodePatternEntity> implements CodePatternService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CodePatternEntity> page = this.page(
                new Query<CodePatternEntity>().getPage(params),
                new QueryWrapper<CodePatternEntity>()
        );

        return new PageUtils(page);
    }

}