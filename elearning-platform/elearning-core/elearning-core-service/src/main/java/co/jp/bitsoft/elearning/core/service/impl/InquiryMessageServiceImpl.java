package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.InquiryMessageEntity;
import co.jp.bitsoft.elearning.core.service.InquiryMessageService;
import co.jp.bitsoft.elearning.core.service.dao.InquiryMessageDao;


@Service("inquiryMessageService")
public class InquiryMessageServiceImpl extends ServiceImpl<InquiryMessageDao, InquiryMessageEntity> implements InquiryMessageService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InquiryMessageEntity> page = this.page(
                new Query<InquiryMessageEntity>().getPage(params),
                new QueryWrapper<InquiryMessageEntity>()
        );

        return new PageUtils(page);
    }

//    @Override
//    public void removeByInquiryIds(Long[] inquiryIds) {
//        baseMapper.removeByInquiryIds(inquiryIds);
//    }
//
//    @Override
//    public void changeStatus(int inquiryId, int userId) {
//        baseMapper.changeStatus(inquiryId, userId);
//    }

}