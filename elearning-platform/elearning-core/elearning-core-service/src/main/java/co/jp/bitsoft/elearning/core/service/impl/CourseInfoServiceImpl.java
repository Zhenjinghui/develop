package co.jp.bitsoft.elearning.core.service.impl;

import co.jp.bitsoft.elearning.core.entity.CourseEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.CourseInfoEntity;
import co.jp.bitsoft.elearning.core.service.CourseInfoService;
import co.jp.bitsoft.elearning.core.service.dao.CourseInfoDao;


@Service("courseInfoService")
public class CourseInfoServiceImpl extends ServiceImpl<CourseInfoDao, CourseInfoEntity> implements CourseInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String infoName = (String)params.get("infoName");
        String infoId = (String)params.get("infoId");
        IPage<CourseInfoEntity> page = this.page(
                new Query<CourseInfoEntity>().getPage(params),
                new QueryWrapper<CourseInfoEntity>().eq("del_flag", "0")
                        .like(StringUtils.isNotBlank(infoName), "info_name", infoName)
                        .eq(StringUtils.isNotBlank((infoId)), "info_id", infoId)
        );

        return new PageUtils(page);
    }

}