package co.jp.bitsoft.elearning.core.service;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.AdAddressEntity;

/**
 * ${comments}
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-02 15:27:59
 */
public interface AdAddressService extends IService<AdAddressEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

