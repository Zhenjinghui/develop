package co.jp.bitsoft.elearning.core.service.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.UserLoginHistoryEntity;

/**
 * ユーザーログイン履歴
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface UserLoginHistoryDao extends BaseMapper<UserLoginHistoryEntity> {

}
