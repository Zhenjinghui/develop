package co.jp.bitsoft.elearning.core.service.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.core.entity.StudentCourseManageEntity;

/**
 * 受講者コース管理
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface StudentCourseManageDao extends BaseMapper<StudentCourseManageEntity> {
	/**
	 * 查询一般用户讲座(分页)
	 */
	IPage<StudentCourseManageEntity> queryStudentCourseManage(Page<StudentCourseManageEntity> page,@Param("studentCourseManageEntity") StudentCourseManageEntity studentCourseManageEntity,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询一般用户讲座(无分页)
	 */
	List<StudentCourseManageEntity> queryStudentCourseManage(@Param("studentCourseManageEntity") StudentCourseManageEntity studentCourseManageEntity,@Param("groupIdList") List<Long> groupIdList);

}
