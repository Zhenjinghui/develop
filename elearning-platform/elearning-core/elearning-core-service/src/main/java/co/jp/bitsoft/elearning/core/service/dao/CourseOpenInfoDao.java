package co.jp.bitsoft.elearning.core.service.dao;

import co.jp.bitsoft.elearning.core.entity.CourseOpenInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 講座公開情報
 * 
 * @author BitSoft-inc
 * @date 2021-01-03 00:25:09
 */
@Mapper
public interface CourseOpenInfoDao extends BaseMapper<CourseOpenInfoEntity> {
	
}
