package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;

/**
 * 単元テスト
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface UnitTestService extends IService<UnitTestEntity> {

//    PageUtils queryPage(Map<String, Object> params);
//
//    PageUtils selectUnitTestPage(Map<String, Object> params);

}

