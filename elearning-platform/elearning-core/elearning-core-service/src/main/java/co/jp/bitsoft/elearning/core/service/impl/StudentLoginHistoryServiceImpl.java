package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.StudentLoginHistoryEntity;
import co.jp.bitsoft.elearning.core.service.StudentLoginHistoryService;
import co.jp.bitsoft.elearning.core.service.dao.StudentLoginHistoryDao;


@Service("studentLoginHistoryService")
public class StudentLoginHistoryServiceImpl extends ServiceImpl<StudentLoginHistoryDao, StudentLoginHistoryEntity> implements StudentLoginHistoryService {

}