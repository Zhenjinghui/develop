package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.UserLoginHistoryEntity;
import co.jp.bitsoft.elearning.core.service.UserLoginHistoryService;
import co.jp.bitsoft.elearning.core.service.dao.UserLoginHistoryDao;


@Service("userLoginHistoryService")
public class UserLoginHistoryServiceImpl extends ServiceImpl<UserLoginHistoryDao, UserLoginHistoryEntity> implements UserLoginHistoryService {

//	@Autowired
//	private UserGroupService userGroupService;
//
//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<UserLoginHistoryEntity> page = this.page(
//                new Query<UserLoginHistoryEntity>().getPage(params),
//                new QueryWrapper<UserLoginHistoryEntity>()
//        );
//
//        return new PageUtils(page);
//    }
//
//    /**
//	 * 查询系统用户(分页)
//	 */
//	public PageUtils selectUserLoginHistory(Map<String, Object> params) {
//        //分页参数
//        long curPage = 1;
//        long limit = 10;
//        if(params.get(Constant.PAGE) != null){
//            curPage = Long.parseLong((String)params.get(Constant.PAGE));
//        }
//        if(params.get(Constant.LIMIT) != null){
//            limit = Long.parseLong((String)params.get(Constant.LIMIT));
//        }
//		Page<UserLoginHistoryResponseResource> page = new Page<>(curPage, limit);
//
//        String userName = (String)params.get("userName");
//
//        //group限制
//		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
//		System.out.println(groupIdList);
//		IPage<UserLoginHistoryResponseResource> pageList = baseMapper.selectUserLoginHistory(page,userName,groupIdList);
//
//		return new PageUtils(pageList);
//    }
}