package co.jp.bitsoft.elearning.core.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.InquiryEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 問い合わせ
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface InquiryDao extends BaseMapper<InquiryEntity> {
	
}
