package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.core.entity.StudentCourseManageEntity;

/**
 * 受講者コース管理
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface StudentCourseManageService extends IService<StudentCourseManageEntity> {

//    /**
//	 * 查询系统用户(分页)
//	 */
//	PageUtils queryStudentCourseManage(Map<String, Object> params);
//
//	/**
//	 * 查询系统用户(无分页)
//	 */
//	List<StudentCourseManageEntity> queryAllStudentCourseManage(Map<String, Object> params);
}

