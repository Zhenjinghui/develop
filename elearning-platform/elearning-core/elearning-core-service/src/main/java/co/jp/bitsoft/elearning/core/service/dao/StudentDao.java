package co.jp.bitsoft.elearning.core.service.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.core.entity.StudentEntity;

/**
 * 受講者
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface StudentDao extends BaseMapper<StudentEntity> {

	/**
	 * 受講者の権限取得
	 *
	 * @param studentId  受講者ID
	 */
	List<String> queryAllPerms(Long studentId);

	/**
	 * 查询一般用户(分页)
	 */
	IPage<StudentEntity> queryStudent(Page<StudentEntity> page,@Param("studentEntity") StudentEntity studentEntity,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询一般用户(无分页)
	 */
	List<StudentEntity> queryStudent(@Param("studentEntity") StudentEntity studentEntity,@Param("groupIdList") List<Long> groupIdList);

}
