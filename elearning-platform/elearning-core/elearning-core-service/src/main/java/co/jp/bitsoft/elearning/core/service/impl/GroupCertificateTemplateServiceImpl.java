package co.jp.bitsoft.elearning.core.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.GroupCertificateTemplateEntity;
import co.jp.bitsoft.elearning.core.service.GroupCertificateTemplateService;
import co.jp.bitsoft.elearning.core.service.dao.GroupCertificateTemplateDao;


@Service("groupCertificateTemplateService")
public class GroupCertificateTemplateServiceImpl extends ServiceImpl<GroupCertificateTemplateDao, GroupCertificateTemplateEntity> implements GroupCertificateTemplateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<GroupCertificateTemplateEntity> page = this.page(
                new Query<GroupCertificateTemplateEntity>().getPage(params),
                new QueryWrapper<GroupCertificateTemplateEntity>()
        );

        return new PageUtils(page);
    }

}