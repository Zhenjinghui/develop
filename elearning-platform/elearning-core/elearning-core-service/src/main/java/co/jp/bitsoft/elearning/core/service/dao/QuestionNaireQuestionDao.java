package co.jp.bitsoft.elearning.core.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * アンケート問題
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface QuestionNaireQuestionDao extends BaseMapper<QuestionNaireQuestionEntity> {
	
}
