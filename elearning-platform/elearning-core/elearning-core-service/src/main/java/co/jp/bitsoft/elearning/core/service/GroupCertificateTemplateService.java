package co.jp.bitsoft.elearning.core.service;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.GroupCertificateTemplateEntity;

/**
 * 講座終了証テンプレート
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface GroupCertificateTemplateService extends IService<GroupCertificateTemplateEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

