package co.jp.bitsoft.elearning.core.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.service.StudentService;
import co.jp.bitsoft.elearning.core.service.UserGroupService;
import co.jp.bitsoft.elearning.core.service.dao.StudentDao;


@Service("studentService")
public class StudentServiceImpl extends ServiceImpl<StudentDao, StudentEntity> implements StudentService {

//	@Autowired
//	private UserGroupService userGroupService;
//    /**
//	 * 查询一般用户(分页)
//	 */
//	public PageUtils queryStudent(Map<String, Object> params) {
//        //分页参数
//        long curPage = 1;
//        long limit = 10;
//        if(params.get(Constant.PAGE) != null){
//            curPage = Long.parseLong((String)params.get(Constant.PAGE));
//        }
//        if(params.get(Constant.LIMIT) != null){
//            limit = Long.parseLong((String)params.get(Constant.LIMIT));
//        }
//		Page<StudentEntity> page = new Page<>(curPage, limit);
//
//        String groupName = (String)params.get("groupName");
//        String userName = (String)params.get("userName");
//
//        StudentEntity studentEntity = new StudentEntity();
//        studentEntity.setGroupName(groupName);
//        studentEntity.setUserName(userName);
//
//        //group限制
//   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
//
//		IPage<StudentEntity> pageList = baseMapper.queryStudent(page,studentEntity,groupIdList);
//
//		return new PageUtils(pageList);
//    }
//
//	/**
//	 * 查询系统用户(无分页)
//	 */
//	public List<StudentEntity> queryAllStudent(Map<String, Object> params) {
//
//        String groupName = (String)params.get("groupName");
//        String userName = (String)params.get("userName");
//        Long studentId = (Long)params.get("studentId");
//
//        StudentEntity studentEntity = new StudentEntity();
//        studentEntity.setGroupName(groupName);
//        studentEntity.setUserName(userName);
//        studentEntity.setStudentId(studentId);
//
//        //group限制
//   		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
//        List<StudentEntity> studentList = baseMapper.queryStudent(studentEntity,groupIdList);
//
//		return studentList;
//    }

}