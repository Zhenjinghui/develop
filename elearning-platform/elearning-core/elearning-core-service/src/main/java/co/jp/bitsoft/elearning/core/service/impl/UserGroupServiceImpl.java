package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.UserGroupEntity;
import co.jp.bitsoft.elearning.core.service.UserGroupService;
import co.jp.bitsoft.elearning.core.service.dao.UserGroupDao;


@Service("userGroupService")
public class UserGroupServiceImpl extends ServiceImpl<UserGroupDao, UserGroupEntity> implements UserGroupService {

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//
//		String groupName = (String)params.get("groupName");
//
//		Long groupId = (Long)params.get("groupId");
//		String groupType = (String)params.get("groupType");
//
//		params.put(Constant.ORDER_FIELD, "group_id");
//		params.put(Constant.ORDER, Constant.ASC);
//
//        IPage<UserGroupEntity> page = this.page(
//                new Query<UserGroupEntity>().getPage(params),
//                new QueryWrapper<UserGroupEntity>()
//                .like(StringUtils.isNotBlank(groupName),"group_name", groupName)
//                .eq(groupId != null,"group_id", groupId)
//                .or()
//                .eq(groupId != null,"group_id_of", groupId)
//                .eq(StringUtils.isNotBlank(groupType),"group_type", groupType)
//        );
//
//        return new PageUtils(page);
//    }

}