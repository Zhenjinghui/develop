package co.jp.bitsoft.elearning.core.service.dao;

import co.jp.bitsoft.elearning.core.entity.InquiryMessageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 問い合わせメッセージ
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface InquiryMessageDao extends BaseMapper<InquiryMessageEntity> {

}
