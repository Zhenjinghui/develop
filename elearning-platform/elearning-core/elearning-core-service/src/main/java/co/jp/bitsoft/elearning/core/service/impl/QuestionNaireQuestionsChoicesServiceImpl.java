package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.service.QuestionNaireQuestionsChoicesService;
import co.jp.bitsoft.elearning.core.service.dao.QuestionNaireQuestionsChoicesDao;


@Service("questionNaireQuestionsChoicesService")
public class QuestionNaireQuestionsChoicesServiceImpl extends ServiceImpl<QuestionNaireQuestionsChoicesDao, QuestionNaireQuestionsChoicesEntity> implements QuestionNaireQuestionsChoicesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QuestionNaireQuestionsChoicesEntity> page = this.page(
                new Query<QuestionNaireQuestionsChoicesEntity>().getPage(params),
                new QueryWrapper<QuestionNaireQuestionsChoicesEntity>()
        );

        return new PageUtils(page);
    }

}