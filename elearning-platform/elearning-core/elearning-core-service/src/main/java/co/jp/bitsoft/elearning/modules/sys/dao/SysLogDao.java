/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.modules.sys.entity.SysLogEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 *
 * @author  BitSoft
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {
	
}
