package co.jp.bitsoft.elearning.core.service.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.AdAddressEntity;

/**
 * ${comments}
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-02 15:27:59
 */
@Mapper
public interface AdAddressDao extends BaseMapper<AdAddressEntity> {

}
