package co.jp.bitsoft.elearning.core.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * テスト問題
 * 
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
@Mapper
public interface TestQuestionsDao extends BaseMapper<TestQuestionsEntity> {
	
}
