package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.StudentQuestionNaireQuestionsAnswerEntity;
import co.jp.bitsoft.elearning.core.service.StudentQuestionNaireQuestionsAnswerService;
import co.jp.bitsoft.elearning.core.service.dao.StudentQuestionNaireQuestionsAnswerDao;


@Service("studentQuestionNaireQuestionsAnswerService")
public class StudentQuestionNaireQuestionsAnswerServiceImpl extends ServiceImpl<StudentQuestionNaireQuestionsAnswerDao, StudentQuestionNaireQuestionsAnswerEntity> implements StudentQuestionNaireQuestionsAnswerService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StudentQuestionNaireQuestionsAnswerEntity> page = this.page(
                new Query<StudentQuestionNaireQuestionsAnswerEntity>().getPage(params),
                new QueryWrapper<StudentQuestionNaireQuestionsAnswerEntity>()
        );

        return new PageUtils(page);
    }

}