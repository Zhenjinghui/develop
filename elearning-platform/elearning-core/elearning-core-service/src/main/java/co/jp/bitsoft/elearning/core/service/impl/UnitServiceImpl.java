package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.UnitEntity;
import co.jp.bitsoft.elearning.core.service.UnitService;
import co.jp.bitsoft.elearning.core.service.dao.UnitDao;


@Service("unitService")
public class UnitServiceImpl extends ServiceImpl<UnitDao, UnitEntity> implements UnitService {

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//		String subjectId = (String)params.get("subjectId");
//		String unitName = (String)params.get("unitName");
//        IPage<UnitEntity> page = this.page(
//                new Query<UnitEntity>().getPage(params),
//                new QueryWrapper<UnitEntity>()
//                .eq("subject_id",Integer.parseInt(subjectId))
//                .like(StringUtils.isNotBlank(unitName),"unit_name", unitName)
//        );
//
//        return new PageUtils(page);
//    }

}