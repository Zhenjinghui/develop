package co.jp.bitsoft.elearning.core.service.impl;

import java.util.List;
import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.common.utils.Constant;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.StudentCourseManageEntity;
import co.jp.bitsoft.elearning.core.service.StudentCourseManageService;
import co.jp.bitsoft.elearning.core.service.UserGroupService;
import co.jp.bitsoft.elearning.core.service.dao.StudentCourseManageDao;


@Service("studentCourseManageService")
public class StudentCourseManageServiceImpl extends ServiceImpl<StudentCourseManageDao, StudentCourseManageEntity> implements StudentCourseManageService {

//	@Autowired
//	private UserGroupService userGroupService;
//
//    /**
//   	 * 查询一般用户讲座(分页)
//   	 */
//   	public PageUtils queryStudentCourseManage(Map<String, Object> params) {
//       //分页参数
//       long curPage = 1;
//       long limit = 10;
//       if(params.get(Constant.PAGE) != null){
//           curPage = Long.parseLong((String)params.get(Constant.PAGE));
//       }
//       if(params.get(Constant.LIMIT) != null){
//           limit = Long.parseLong((String)params.get(Constant.LIMIT));
//       }
//       Page<StudentCourseManageEntity> page = new Page<>(curPage, limit);
//
//       String groupName = (String)params.get("groupName");
//       String userName = (String)params.get("userName");
//       String courseTitle = (String)params.get("courseTitle");
//       String studyStatus = (String)params.get("studyStatus");
//
//       StudentCourseManageEntity studentCourseManageEntity = new StudentCourseManageEntity();
//       studentCourseManageEntity.setGroupName(groupName);
//       studentCourseManageEntity.setUserName(userName);
//       studentCourseManageEntity.setCourseTitle(courseTitle);
//       studentCourseManageEntity.setStudyStatus(studyStatus);
//
//       //group限制
//  		List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
//
//   		IPage<StudentCourseManageEntity> pageList = baseMapper.queryStudentCourseManage(page,studentCourseManageEntity,groupIdList);
//
//   		return new PageUtils(pageList);
//       }
//
//   	/**
//   	 * 查询一般用户讲座(无分页)
//   	 */
//   	public List<StudentCourseManageEntity> queryAllStudentCourseManage(Map<String, Object> params) {
//
//       String groupName = (String)params.get("groupName");
//       String userName = (String)params.get("userName");
//       String courseTitle = (String)params.get("courseTitle");
//       String studyStatus = (String)params.get("studyStatus");
//       Long studentCourseId = (Long)params.get("studentCourseId");
//
//       StudentCourseManageEntity studentCourseManageEntity = new StudentCourseManageEntity();
//       studentCourseManageEntity.setGroupName(groupName);
//       studentCourseManageEntity.setUserName(userName);
//       studentCourseManageEntity.setCourseTitle(courseTitle);
//       studentCourseManageEntity.setStudyStatus(studyStatus);
//       studentCourseManageEntity.setStudentCourseId(studentCourseId);
//
//       //group限制
//       List<Long> groupIdList = CommonServiceImpl.groupSelectLimit(userGroupService);
//       List<StudentCourseManageEntity> studentCourseManageList = baseMapper.queryStudentCourseManage(studentCourseManageEntity,groupIdList);
//
//       return studentCourseManageList;
//       }


//    @Override
//    public PageUtils queryPageCustom(Map<String, Object> params) {
//        long curPage = 1;
//        long limit = 10;
//        if(params.get(Constant.PAGE) != null){
//            curPage = Long.parseLong((String)params.get(Constant.PAGE));
//        }
//        if(params.get(Constant.LIMIT) != null){
//            limit = Long.parseLong((String)params.get(Constant.LIMIT));
//        }
//        Page<UnitTestEntity> page = new Page<>(curPage, limit);
//
//        Long courseId = (Long)params.get("courseId");
//        String courseName = (String)params.get("courseName");
//        StudentCourseManageCustomEntity studentCourseManageCustomEntity = new StudentCourseManageCustomEntity();
//        studentCourseManageCustomEntity.setCourseId(courseId);
//        studentCourseManageCustomEntity.setCourseTitle(courseName);
//
//        IPage<UnitTestEntity> pageList = baseMapper.queryPageCustom(page, studentCourseManageCustomEntity);
//
//        return new PageUtils(pageList);
//    }

}