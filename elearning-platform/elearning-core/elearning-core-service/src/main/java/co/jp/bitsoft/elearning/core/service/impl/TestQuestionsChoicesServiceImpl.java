package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.service.TestQuestionsChoicesService;
import co.jp.bitsoft.elearning.core.service.dao.TestQuestionsChoicesDao;


@Service("testQuestionsChoicesService")
public class TestQuestionsChoicesServiceImpl extends ServiceImpl<TestQuestionsChoicesDao, TestQuestionsChoicesEntity> implements TestQuestionsChoicesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TestQuestionsChoicesEntity> page = this.page(
                new Query<TestQuestionsChoicesEntity>().getPage(params),
                new QueryWrapper<TestQuestionsChoicesEntity>()
        );

        return new PageUtils(page);
    }

}