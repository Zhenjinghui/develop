package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.service.dao.CourseOpenInfoDao;
import co.jp.bitsoft.elearning.core.entity.CourseOpenInfoEntity;
import co.jp.bitsoft.elearning.core.service.CourseOpenInfoService;


@Service("courseOpenInfoService")
public class CourseOpenInfoServiceImpl extends ServiceImpl<CourseOpenInfoDao, CourseOpenInfoEntity> implements CourseOpenInfoService {

}