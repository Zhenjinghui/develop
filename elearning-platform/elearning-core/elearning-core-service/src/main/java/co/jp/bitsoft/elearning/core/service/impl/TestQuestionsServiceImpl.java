package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;
import co.jp.bitsoft.elearning.core.service.TestQuestionsService;
import co.jp.bitsoft.elearning.core.service.dao.TestQuestionsDao;


@Service("testQuestionsService")
public class TestQuestionsServiceImpl extends ServiceImpl<TestQuestionsDao, TestQuestionsEntity> implements TestQuestionsService {

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//		String unitTestId = (String)params.get("unitTestId");
//		String questionTitle = (String)params.get("questionTitle");
//		String questionType = (String)params.get("questionType");
//
//        IPage<TestQuestionsEntity> page = this.page(
//                new Query<TestQuestionsEntity>().getPage(params),
//                new QueryWrapper<TestQuestionsEntity>()
//                .eq("unit_test_id",Integer.parseInt(unitTestId))
//                .like(StringUtils.isNotBlank(questionTitle),"question_title", questionTitle)
//                .eq(StringUtils.isNotBlank(questionType),"question_type",questionType)
//        );
//
//        return new PageUtils(page);
//    }

}