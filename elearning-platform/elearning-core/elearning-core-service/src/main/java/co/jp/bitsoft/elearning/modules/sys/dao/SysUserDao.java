/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.modules.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import co.jp.bitsoft.elearning.admin.dto.AdminSysUserDto;
import co.jp.bitsoft.elearning.modules.sys.entity.SysUserEntity;

/**
 * 系统用户
 *
 * @author  BitSoft
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {

	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);

	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);

	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);

	/**
	 * 查询系统用户(分页)
	 */
	IPage<AdminSysUserDto> queryUser(Page<AdminSysUserDto> page,Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

	/**
	 * 查询系统用户(无分页)
	 */
	List<AdminSysUserDto> queryUser(Map<String, Object> params,@Param("groupIdList") List<Long> groupIdList);

}
