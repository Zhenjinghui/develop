package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;

import java.util.Map;

/**
 * テスト問題選択肢
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface TestQuestionsChoicesService extends IService<TestQuestionsChoicesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

