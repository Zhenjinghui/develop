package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.InquiryEntity;
import co.jp.bitsoft.elearning.core.service.InquiryService;
import co.jp.bitsoft.elearning.core.service.dao.InquiryDao;


@Service("inquiryService")
public class InquiryServiceImpl extends ServiceImpl<InquiryDao, InquiryEntity> implements InquiryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InquiryEntity> page = this.page(
                new Query<InquiryEntity>().getPage(params),
                new QueryWrapper<InquiryEntity>()
        );

        return new PageUtils(page);
    }

}