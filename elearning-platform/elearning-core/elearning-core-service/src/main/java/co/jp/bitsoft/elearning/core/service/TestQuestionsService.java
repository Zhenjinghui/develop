package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;

/**
 * テスト問題
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface TestQuestionsService extends IService<TestQuestionsEntity> {

//    PageUtils queryPage(Map<String, Object> params);
}

