package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import co.jp.bitsoft.elearning.core.entity.CourseOpenInfoEntity;

import java.util.Map;

/**
 * 講座公開情報
 *
 * @author BitSoft-inc
 * @date 2021-01-03 00:25:09
 */
public interface CourseOpenInfoService extends IService<CourseOpenInfoEntity> {

}

