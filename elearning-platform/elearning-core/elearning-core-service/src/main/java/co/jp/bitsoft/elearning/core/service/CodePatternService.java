package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.CodePatternEntity;

import java.util.Map;

/**
 * コードパターン
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface CodePatternService extends IService<CodePatternEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

