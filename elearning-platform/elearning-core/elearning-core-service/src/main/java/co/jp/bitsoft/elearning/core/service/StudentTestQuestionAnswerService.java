package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.StudentTestQuestionAnswerEntity;

import java.util.Map;

/**
 * 受講者テスト問題解答
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface StudentTestQuestionAnswerService extends IService<StudentTestQuestionAnswerEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

