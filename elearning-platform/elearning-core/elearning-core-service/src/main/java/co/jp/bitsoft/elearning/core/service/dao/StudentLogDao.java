/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.core.service.dao;


import co.jp.bitsoft.elearning.core.entity.StudentLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 受講者操作ログ
 *
 * @author  BitSoft
 */
@Mapper
public interface StudentLogDao extends BaseMapper<StudentLogEntity> {
	
}
