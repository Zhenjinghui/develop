package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.core.entity.StudentEntity;

/**
 * 受講者
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface StudentService extends IService<StudentEntity> {

//    PageUtils queryPage(Map<String, Object> params);

//	/**
//	 * 查询系统用户(分页)
//	 */
//	PageUtils queryStudent(Map<String, Object> params);
//
//	/**
//	 * 查询系统用户(无分页)
//	 */
//	List<StudentEntity> queryAllStudent(Map<String, Object> params);
}

