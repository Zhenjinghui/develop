package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.StudentTokenEntity;
import co.jp.bitsoft.elearning.core.service.StudentTokenService;
import co.jp.bitsoft.elearning.core.service.dao.StudentTokenDao;


@Service("studentTokenService")
public class StudentTokenServiceImpl extends ServiceImpl<StudentTokenDao, StudentTokenEntity> implements StudentTokenService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StudentTokenEntity> page = this.page(
                new Query<StudentTokenEntity>().getPage(params),
                new QueryWrapper<StudentTokenEntity>()
        );

        return new PageUtils(page);
    }

}