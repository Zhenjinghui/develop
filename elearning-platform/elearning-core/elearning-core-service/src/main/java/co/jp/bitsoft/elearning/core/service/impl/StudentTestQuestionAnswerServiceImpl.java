package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.StudentTestQuestionAnswerEntity;
import co.jp.bitsoft.elearning.core.service.StudentTestQuestionAnswerService;
import co.jp.bitsoft.elearning.core.service.dao.StudentTestQuestionAnswerDao;


@Service("studentTestQuestionAnswerService")
public class StudentTestQuestionAnswerServiceImpl extends ServiceImpl<StudentTestQuestionAnswerDao, StudentTestQuestionAnswerEntity> implements StudentTestQuestionAnswerService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StudentTestQuestionAnswerEntity> page = this.page(
                new Query<StudentTestQuestionAnswerEntity>().getPage(params),
                new QueryWrapper<StudentTestQuestionAnswerEntity>()
        );

        return new PageUtils(page);
    }

}