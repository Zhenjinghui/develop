package co.jp.bitsoft.elearning.core.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.AdAddressEntity;
import co.jp.bitsoft.elearning.core.service.AdAddressService;
import co.jp.bitsoft.elearning.core.service.dao.AdAddressDao;


@Service("adAddressService")
public class AdAddressServiceImpl extends ServiceImpl<AdAddressDao, AdAddressEntity> implements AdAddressService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AdAddressEntity> page = this.page(
                new Query<AdAddressEntity>().getPage(params),
                new QueryWrapper<AdAddressEntity>()
        );

        return new PageUtils(page);
    }

}