package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;

import co.jp.bitsoft.elearning.core.entity.TeachingMaterialEntity;

/**
 * 教材
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface TeachingMaterialService extends IService<TeachingMaterialEntity> {

//    PageUtils queryPage(Map<String, Object> params);
}

