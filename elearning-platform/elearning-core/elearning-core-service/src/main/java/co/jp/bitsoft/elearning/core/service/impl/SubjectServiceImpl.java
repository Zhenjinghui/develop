package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.SubjectEntity;
import co.jp.bitsoft.elearning.core.service.SubjectService;
import co.jp.bitsoft.elearning.core.service.dao.SubjectDao;


@Service("subjectService")
public class SubjectServiceImpl extends ServiceImpl<SubjectDao, SubjectEntity> implements SubjectService {

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//		String courseId = (String)params.get("courseId");
//		String subjectName = (String)params.get("subjectName");
//        IPage<SubjectEntity> page = this.page(
//                new Query<SubjectEntity>().getPage(params),
//                new QueryWrapper<SubjectEntity>()
//                .eq("course_id",Integer.parseInt(courseId))
//                .like(StringUtils.isNotBlank(subjectName),"subject_title", subjectName)
//        );
//
//        return new PageUtils(page);
//    }

}