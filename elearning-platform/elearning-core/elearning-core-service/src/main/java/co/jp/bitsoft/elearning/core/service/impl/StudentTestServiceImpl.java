package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.StudentTestEntity;
import co.jp.bitsoft.elearning.core.service.StudentTestService;
import co.jp.bitsoft.elearning.core.service.dao.StudentTestDao;


@Service("studentTestService")
public class StudentTestServiceImpl extends ServiceImpl<StudentTestDao, StudentTestEntity> implements StudentTestService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StudentTestEntity> page = this.page(
                new Query<StudentTestEntity>().getPage(params),
                new QueryWrapper<StudentTestEntity>()
        );

        return new PageUtils(page);
    }

}