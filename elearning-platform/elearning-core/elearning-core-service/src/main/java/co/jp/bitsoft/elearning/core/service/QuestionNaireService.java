package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireEntity;

import java.util.Map;

/**
 * アンケート
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface QuestionNaireService extends IService<QuestionNaireEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

