package co.jp.bitsoft.elearning.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionsChoicesEntity;

import java.util.Map;

/**
 * アンケート問題選択肢
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2020-11-09 00:34:18
 */
public interface QuestionNaireQuestionsChoicesService extends IService<QuestionNaireQuestionsChoicesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

