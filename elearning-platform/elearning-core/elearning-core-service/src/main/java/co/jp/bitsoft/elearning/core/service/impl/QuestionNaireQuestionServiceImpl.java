package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.QuestionNaireQuestionEntity;
import co.jp.bitsoft.elearning.core.service.QuestionNaireQuestionService;
import co.jp.bitsoft.elearning.core.service.dao.QuestionNaireQuestionDao;


@Service("questionNaireQuestionService")
public class QuestionNaireQuestionServiceImpl extends ServiceImpl<QuestionNaireQuestionDao, QuestionNaireQuestionEntity> implements QuestionNaireQuestionService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QuestionNaireQuestionEntity> page = this.page(
                new Query<QuestionNaireQuestionEntity>().getPage(params),
                new QueryWrapper<QuestionNaireQuestionEntity>()
        );

        return new PageUtils(page);
    }

}