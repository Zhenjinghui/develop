package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.TeachingMaterialEntity;
import co.jp.bitsoft.elearning.core.service.TeachingMaterialService;
import co.jp.bitsoft.elearning.core.service.dao.TeachingMaterialDao;


@Service("teachingMaterialService")
public class TeachingMaterialServiceImpl extends ServiceImpl<TeachingMaterialDao, TeachingMaterialEntity> implements TeachingMaterialService {

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//    	String unitId = (String)params.get("unitId");
//    	String teachingMaterialTitle = (String)params.get("teachingMaterialTitle");
//
//        IPage<TeachingMaterialEntity> page = this.page(
//                new Query<TeachingMaterialEntity>().getPage(params),
//                new QueryWrapper<TeachingMaterialEntity>()
//                .eq("unit_id",Integer.parseInt(unitId))
//                .like(StringUtils.isNotBlank(teachingMaterialTitle),"teaching_material_title", teachingMaterialTitle)
//
//        );
//
//        return new PageUtils(page);
//    }

}