package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.StudentCourseStudyEntity;
import co.jp.bitsoft.elearning.core.service.StudentCourseStudyService;
import co.jp.bitsoft.elearning.core.service.dao.StudentCourseStudyDao;


@Service("studentCourseStudyService")
public class StudentCourseStudyServiceImpl extends ServiceImpl<StudentCourseStudyDao, StudentCourseStudyEntity> implements StudentCourseStudyService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StudentCourseStudyEntity> page = this.page(
                new Query<StudentCourseStudyEntity>().getPage(params),
                new QueryWrapper<StudentCourseStudyEntity>()
        );

        return new PageUtils(page);
    }

}