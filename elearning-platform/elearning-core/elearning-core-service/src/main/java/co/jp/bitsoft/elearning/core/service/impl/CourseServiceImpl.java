package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.CourseEntity;
import co.jp.bitsoft.elearning.core.service.CourseService;
import co.jp.bitsoft.elearning.core.service.dao.CourseDao;


@Service("courseService")
public class CourseServiceImpl extends ServiceImpl<CourseDao, CourseEntity> implements CourseService {

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//		String couresName = (String)params.get("courseName");
//
//        IPage<CourseEntity> page = this.page(
//                new Query<CourseEntity>().getPage(params),
//                new QueryWrapper<CourseEntity>().like(StringUtils.isNotBlank(couresName),"course_title", couresName)
//
//        );
//
//        return new PageUtils(page);
//    }

}