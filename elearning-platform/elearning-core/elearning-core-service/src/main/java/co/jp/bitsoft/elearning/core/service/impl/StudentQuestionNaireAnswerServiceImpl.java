package co.jp.bitsoft.elearning.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.StudentQuestionNaireAnswerEntity;
import co.jp.bitsoft.elearning.core.service.StudentQuestionNaireAnswerService;
import co.jp.bitsoft.elearning.core.service.dao.StudentQuestionNaireAnswerDao;


@Service("studentQuestionNaireAnswerService")
public class StudentQuestionNaireAnswerServiceImpl extends ServiceImpl<StudentQuestionNaireAnswerDao, StudentQuestionNaireAnswerEntity> implements StudentQuestionNaireAnswerService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StudentQuestionNaireAnswerEntity> page = this.page(
                new Query<StudentQuestionNaireAnswerEntity>().getPage(params),
                new QueryWrapper<StudentQuestionNaireAnswerEntity>()
        );

        return new PageUtils(page);
    }

}