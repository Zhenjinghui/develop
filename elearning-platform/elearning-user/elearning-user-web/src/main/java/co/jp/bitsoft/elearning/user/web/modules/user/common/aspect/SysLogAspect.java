/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.user.web.modules.user.common.aspect;

import java.lang.reflect.Method;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import co.jp.bitsoft.elearning.common.annotation.SysLog;
import co.jp.bitsoft.elearning.common.utils.HttpContextUtils;
import co.jp.bitsoft.elearning.common.utils.IPUtils;
import co.jp.bitsoft.elearning.core.entity.StudentLogEntity;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.user.service.db.StudentLogService;


/**
 * 系统日志，切面处理类
 *
 * @author  BitSoft
 */
@Aspect
@Component
public class SysLogAspect {
	@Autowired
	private StudentLogService studentLogService;

	@Pointcut("@annotation(co.jp.bitsoft.elearning.common.annotation.SysLog)")
	public void logPointCut() {

	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		//执行方法
		Object result = point.proceed();
		//执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;

		//保存日志
		saveStudentLog(point, time);

		return result;
	}

	private void saveStudentLog(ProceedingJoinPoint joinPoint, long time) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		StudentLogEntity studentLog = new StudentLogEntity();
		SysLog syslog = method.getAnnotation(SysLog.class);
		if(syslog != null){
			//注解上的描述
			studentLog.setUserOperation(syslog.value());
		}

		//请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		studentLog.setRequestMethod(className + "." + methodName + "()");

		//请求的参数
		Object[] args = joinPoint.getArgs();
		try{
			String params = new Gson().toJson(args);
			studentLog.setParams(params);
		}catch (Exception e){

		}

		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址

		studentLog.setIp(IPUtils.getIpAddr(request));


//		if(methodName == "login") {
//
//		}
		//用户名
		long studentId = ((StudentEntity) SecurityUtils.getSubject().getPrincipal()).getStudentId();
		studentLog.setStudentId(studentId);

		studentLog.setExcuteTime(time);
		studentLog.setCreateDate(new Date());
		//保存系统日志
		studentLogService.save(studentLog);
	}
}
