/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.user.web.modules.user.controller;

import java.io.IOException;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;

import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.common.validator.Assert;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTokenEntity;
import co.jp.bitsoft.elearning.core.service.StudentService;
import co.jp.bitsoft.elearning.user.domain.model.student.StudentInquiryRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.student.StudentLoginRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.student.StudentLoginResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.student.UserPasswordRemindRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.student.UserPasswordResetRequestResource;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentWebService;
import co.jp.bitsoft.elearning.user.web.common.UserInfo;
import co.jp.bitsoft.elearning.user.web.common.UserInfoThreadContext;
import co.jp.bitsoft.elearning.user.web.modules.user.constant.CommonConstant;
import io.netty.util.internal.StringUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 受講者コントローラ
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2021-01-11 10:00:00
 */
@RestController
public class StudentController extends AbstractController {

	/**
	 * 受講者
	 */
	@Autowired
	private StudentService studentService;

	/**
	 * 受講者Webサービス
	 */
	@Autowired
	private StudentWebService studentWebService;

	/**
	 * 受講者ログイン
	 *
	 * @param requestResource 受講者ログインのリクエストリソース
	 * @return 受講者ログインのレスポンスリソース
	 */
	@ApiOperation(value = "受講者ログイン", notes = "受講者がログインする")
	@ApiResponses({
			@ApiResponse(code = HttpStatus.SC_OK, message = "success", response = StudentLoginResponseResource.class) })
	@PostMapping("/login")
	public R login(@RequestBody StudentLoginRequestResource requestResource) throws IOException {

		// バリデーションチェック
		ValidatorUtils.validateEntity(requestResource);

		// 受講者情報を取得
		StudentEntity student = studentWebService.getStudent(requestResource.getUserName());

		// 受講者の認証
		String message = studentWebService.authenticateStudent(student, requestResource.getPassword());
		// 受講者情報が存在しない または パスワードが不正の場合、メッセージを返却
		if (!StringUtil.isNullOrEmpty(message)) {
			return R.error(message);
		}

		// 受講者トークンの登録・更新
		String token = studentWebService.insertOrUpdateStudentToken(student.getStudentId(),
				CommonConstant.TOKEN_EXPIRE);

		// ログイン履歴の登録・更新
		studentWebService.insertStudentLoginOutHistory(student.getStudentId(), CommonConstant.LOGIN_KBN);

		// 初期登録の判定
		String defaultPasswordfalg = studentWebService.checkFirstLogin(student.getUserPassword(),
				student.getUserSalt());

		// ユーザー情報を作成
		UserInfo userInfo = new UserInfo();
		userInfo.setUserId(student.getStudentId()); // ユーザーId
		userInfo.setUserName(student.getUserName()); // ユーザー名
		userInfo.setEmail(student.getEmail()); // メールアドレス
		// セッション更新（ユーザーコンテキスト）
		UserInfoThreadContext.setUserInfo(userInfo);

		// ログインレスポンスを作成
		StudentLoginResponseResource responseResource = new StudentLoginResponseResource();
		responseResource.setDefaultPasswordfalg(defaultPasswordfalg); // デフォルトパスワードフラグ
		responseResource.setExpireTime(CommonConstant.TOKEN_EXPIRE); // トークン期限切れ
		responseResource.setToken(token); // 受講者トークン
		// レスポンスデータを返却
		Map<String, Object> data = Maps.newHashMap();
		data.put("data", responseResource); // ログインレスポンス
		return R.ok(data);
	}

	/**
	 * 受講者ログアウト
	 */
	@ApiOperation(value = "受講者ログアウト", notes = "受講者がログアウトする")
	@ApiResponses({ @ApiResponse(code = HttpStatus.SC_OK, message = "success") })
	@PostMapping("/logout")
	public R logout() {

		// 受講者IDを取得
		Long studentId = UserInfoThreadContext.getUserId();

		// 受講者トークンの更新
		studentWebService.updateStudentToken(studentId);

		// ログイン履歴の登録・更新
		studentWebService.insertStudentLoginOutHistory(studentId, CommonConstant.LOGOUT_KBN);

		// セッションクリア（ユーザーコンテキスト）
		UserInfoThreadContext.clear();

		// レスポンスデータを返却
		return R.ok();
	}

	/**
	 * パスワードのお問い合わせ(送信)
	 *
	 * @param requestResource 受講者パスワードお問い合わせのリクエスト情報
	 */
	@ApiOperation(value = "パスワードのお問い合わせ(送信)", notes = "パスワードをお問い合わせ、送信する")
	@ApiResponses({ @ApiResponse(code = HttpStatus.SC_OK, message = "success") })
	@PostMapping("/user/password/remind")
	public R remindPassword(@RequestBody UserPasswordRemindRequestResource requestResource) throws IOException {

		// バリデーションチェック
		ValidatorUtils.validateEntity(requestResource);

		// 受講者情報を取得
		StudentEntity student = studentWebService.getStudent(requestResource.getUserName());

		// 受講者情報が存在しない場合、メッセージを出力
		Assert.isNull(student, "正しいログインIDを入力してください。ログインIDをお忘れの方はクラス担当に連絡してください。");

		// 受講者トークンの登録・更新
		String token = studentWebService.insertOrUpdateStudentToken(student.getStudentId(),
				CommonConstant.TOKEN_EXPIRE);

		// メール送信
		//studentWebService.sendEmail(requestResource.getUserName(), student.getEmail(), token);
		studentWebService.sendEmail(student.getUserNameKanji(), student.getEmail(), token);

		// 正常終了
		return R.ok();
	}

/**
	 * パスワード再設定(初期表示)
	 *
	 * @param resetPasswordToken トークン
	 * @return レスポンスデータ
	 *//*

	@ApiOperation(value = "パスワード再設定(初期表示)", notes = "パスワード再設定画面を初期表示する")
	@ApiResponses({ @ApiResponse(code = HttpStatus.SC_OK, message = "success") })
	@GetMapping("/user/password/edit")
	public R resetPassword(@ApiParam(value = "トークン", required = true) @RequestParam final String resetPasswordToken) {

		// Token検証
		StudentTokenEntity studentToken = studentWebService.getStudentTokenByToken(resetPasswordToken);

		// 受講者トークン情報が存在しない場合、メッセージを出力
		Assert.isNull(studentToken, "トークンが無効です。");

		// 正常終了
		return R.ok();
	}
*/

	/**
	 * パスワード再設定
	 *
	 * @param newPassword        新パスワード
	 * @param resetPasswordToken トークン
	 * @return レスポンスデータ
	 */
	@ApiOperation(value = "パスワード再設定", notes = "パスワードを再設定する")
	@ApiResponses({ @ApiResponse(code = HttpStatus.SC_OK, message = "success") })
	@PostMapping("/user/password/update")
	public R changePassword(@RequestBody final UserPasswordResetRequestResource userPasswordResetRequestResource) {

		// バリデーションチェック
		StudentLoginRequestResource studentLoginRequest = new StudentLoginRequestResource();
		studentLoginRequest.setPassword(userPasswordResetRequestResource.getPassword()); // パスワード
		ValidatorUtils.validateEntity(studentLoginRequest, new String[] { "password" });

		// Token検証
		StudentTokenEntity studentToken = studentWebService.getStudentTokenByToken(userPasswordResetRequestResource.getToken());

		// 受講者トークン情報が存在しない場合、メッセージを出力
		Assert.isNull(studentToken, "トークンが無効です。");

		// 受講者情報を取得
		StudentEntity student = studentService.getById(studentToken.getStudentId());

		String newPasswordEncrypted = new Sha256Hash(userPasswordResetRequestResource.getPassword(), student.getUserSalt()).toHex();

		// パスワード変更を実行
		StudentEntity studentUpdate = new StudentEntity();
		studentUpdate.setUserPassword(newPasswordEncrypted);

		studentService.update(studentUpdate,
				new QueryWrapper<StudentEntity>().eq("student_Id", studentToken.getStudentId()));

		// 正常終了
		return R.ok();
	}

	/**
	 * 問い合わせメール(送信)
	 *
	 * @param studentInquiryRequestResource お問い合わせ送信の情報
	 */
	@ApiOperation(value = "問い合わせメール(送信)", notes = "問い合わせメ、送信する")
	@ApiResponses({ @ApiResponse(code = HttpStatus.SC_OK, message = "success") })
	@PostMapping("/user/inquiryMail")
	public R sendInquiryEmail(@RequestBody StudentInquiryRequestResource studentInquiryRequestResource) throws IOException {

		// 受講者IDを取得
		Long studentId = UserInfoThreadContext.getUserId();

		// 受講者情報を取得
		StudentEntity student = studentWebService.getStudent(studentId);

		// 受講者情報が存在しない場合、メッセージを出力
		Assert.isNull(student, "正しいログインIDを入力してください。ログインIDをお忘れの方はクラス担当に連絡してください。");

		// 受講者名を取得する。
		//String userName = student.getUserName();
		// https://hikaru.atlassian.net/browse/STUDYPACK-31
		String userName = student.getUserNameKanji();

		// メール送信
		studentWebService.sendInquiryEmail(student.getEmail(), userName, studentInquiryRequestResource);

		// 正常終了
		return R.ok();
	}
}
