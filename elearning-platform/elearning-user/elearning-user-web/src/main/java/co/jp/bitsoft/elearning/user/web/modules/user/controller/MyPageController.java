package co.jp.bitsoft.elearning.user.web.modules.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.jp.bitsoft.elearning.user.domain.model.mypage.MyPageResponseResource;
import co.jp.bitsoft.elearning.user.service.modules.user.service.MyPageService;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.google.common.collect.ImmutableMap;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.CourseInfoResponseResource;
import co.jp.bitsoft.elearning.user.service.modules.user.service.CourseInfoDbService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("mypage")
@Api(tags = {"マイページのコントローラー"})
public class MyPageController extends AbstractController {

    protected Logger logger = LoggerFactory.getLogger(MyPageController.class);


    @Autowired
    private CourseInfoDbService courseInfoDbService;


    @Autowired
    private MyPageService myPageService;


    /**
     * 講座インフォメーション一覧
     */
    @ApiOperation(value = "講座インフォメーション一覧", notes = "講座インフォメーション一覧を取得する")
    @ApiImplicitParams({@ApiImplicitParam(name = "page", value = "ページ数", paramType = "query",
            dataType = "Int", defaultValue = "1", required = true),
                        @ApiImplicitParam(name = "limit", value = "ページごと件数", paramType = "query",
                                dataType = "Int", defaultValue = "10", required = true)})
    @GetMapping("/infolist")
    @ApiResponses({@ApiResponse(code = HttpStatus.SC_OK, message = "success",
            response = CourseInfoResponseResource.class)})
    public R list(@ApiParam(hidden = true) @RequestParam Map<String, Object> params) {
        PageUtils page = courseInfoDbService.courseInfoListByGroupId(params, getGroupId());

        return R.ok()
                .put("data",
                     ImmutableMap.of("count",
                                     page.getTotalCount(),
                                     "courseInfoResponseResourceList",
                                     page.getList()));
    }

    /**
     * 講座インフォメーション情報
     */
    @ApiOperation(value = "講座インフォメーション情報", notes = "講座インフォメーション情報を取得する")
    @GetMapping("/info")
    @ApiResponses({@ApiResponse(code = HttpStatus.SC_OK, message = "success",
            response = CourseInfoResponseResource.class)})
    public R info(@ApiParam(value = "インフォメーションID",
            required = true) @RequestParam("info_id") Long infoId) {
        CourseInfoResponseResource result = courseInfoDbService.courseInfo(getGroupId(), infoId);

        return R.ok().put("data", result);
    }

    /**
     * 講座受講一覧
     *
     * @return {@link List < MyPageResponseResource >} 講座一覧
     */
    @RequestMapping(value = "/courselist", method = RequestMethod.GET)
    public R courseList() {
        List<MyPageResponseResource> myPageResponseResourceList = myPageService.courseList(getStudent());

        Map<String, Object> rMap = new HashMap<>();

        Map<String, Object> rMap2 = new HashMap<>();
        rMap2.put("count", myPageResponseResourceList.size());
        rMap2.put("myPageResponseResourceList", myPageResponseResourceList);

        rMap.put("data", rMap2);

        return R.ok(rMap);
    }

}
