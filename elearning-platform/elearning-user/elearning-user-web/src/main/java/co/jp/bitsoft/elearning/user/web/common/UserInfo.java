package co.jp.bitsoft.elearning.user.web.common;

import lombok.Data;

/**
 * ユーザー情報
 *
 * @author BitSoft
 * @since 1.0
 */
@Data
public class UserInfo {
	private Long userId;
	private String userName;
	private String email;
}
