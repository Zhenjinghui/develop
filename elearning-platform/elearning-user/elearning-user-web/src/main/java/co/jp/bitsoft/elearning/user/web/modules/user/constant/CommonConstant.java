package co.jp.bitsoft.elearning.user.web.modules.user.constant;

/**
 * 共通コンスタント
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2021-01-11 10:00:00
 */
public class CommonConstant {

	/**
	 * トークン期限切れ(12時間)
	 */
	public final static int TOKEN_EXPIRE = 3600 * 12;

	/**
	 * ログイン区分
	 */
	public final static String LOGIN_KBN = "0";

	/**
	 * ログアウト区分
	 */
	public final static String LOGOUT_KBN = "1";

	/**
	 * パスワードフラグ(初回の登録)
	 */
	public final static String FIRST_LOGIN = "0";

	/**
	 * パスワードフラグ(初回以外の登録)
	 */
	public final static String NOT_FIRST_LOGIN = "1";
}
