package co.jp.bitsoft.elearning.user.web.modules.user.controller;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.user.domain.model.course.TestQuestionResponceResource;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.AnswerQuestionRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.StudentTestResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.TestQuestionsRequsetResource;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentTestDbService;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentTestQuestionAnswerDbService;
import co.jp.bitsoft.elearning.user.service.modules.user.service.TestQuestionsDbService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.var;

@RestController
@RequestMapping("course")
@Api(tags = {"マイページのコントローラー"})
public class CourseController extends AbstractController {

    @Autowired
    private StudentTestQuestionAnswerDbService studentTestQuestionAnswerDbService;

    @Autowired
    private TestQuestionsDbService testQuestionsDbService;

    @Autowired
    private StudentTestDbService studentTestDbService;

    /**
     * 単元確認テスト開始
     */
    @ApiOperation(value = "単元確認テスト開始", notes = "単元確認テストを開始する")
    @PostMapping("/subject/unit/test")
    @ApiResponses({@ApiResponse(code = HttpStatus.SC_OK, message = "success", response = TestQuestionResponceResource.class)})
    public R testQuestionList(@RequestBody TestQuestionsRequsetResource testQuestionsRequsetResource) {

        var result = testQuestionsDbService.testQuestionsList(getGroupId(),
                                                            getStudentId(),
                                                            testQuestionsRequsetResource.getCourseId(),
                                                            testQuestionsRequsetResource.getSubjectId(),
                                                            testQuestionsRequsetResource.getUnitId());

        return R.ok().put("data", result);
    }

    /**
     * 質問回答提出（次の質問）
     */
    @ApiOperation(value = "単元確認テスト回答の提出", notes = "単元確認テスト問題の回答を提出する")
    @PostMapping("/subject/unit/test/next")
    public R testNextQuestion(@RequestBody AnswerQuestionRequestResource answerQuestionRequestResource) {

        studentTestQuestionAnswerDbService.answerQuestion(getStudentId(), answerQuestionRequestResource);

        return R.ok();
    }

    /**
     * 質問回答提出（最終提出）
     */
    @ApiOperation(value = "単元確認テスト回答の最終提出", notes = "単元確認テスト問題の回答を最終提出する。そして単元確認テストの結果を表示する。")
    @PostMapping("/subject/unit/test/submit")
    @ApiResponses({@ApiResponse(code = HttpStatus.SC_OK, message = "success", response = StudentTestResponseResource.class)})
    public R testSubmit(@RequestBody AnswerQuestionRequestResource answerQuestionRequestResource) {
        studentTestDbService.finishTest(getStudentId(), answerQuestionRequestResource);

        var result = studentTestQuestionAnswerDbService.studentTestQuestionAnswer(getStudentId(),
                                                                                  answerQuestionRequestResource.getCourseId(),
                                                                                  answerQuestionRequestResource.getSubjectId(),
                                                                                  answerQuestionRequestResource.getUnitId(),
                                                                                  answerQuestionRequestResource.getStudentTestId());

        return R.ok().put("data", result);
    }

    /**
     * 不合格問題一覧
     */
    @ApiOperation(value = "不合格問題一覧", notes = "不合格問題一覧")
    @PostMapping("/subject/unit/test/noPassedAnsweres")
    @ApiResponses({@ApiResponse(code = HttpStatus.SC_OK, message = "success", response = StudentTestResponseResource.class)})
    public R moPassedAnsweres(@RequestBody AnswerQuestionRequestResource answerQuestionRequestResource) {
        var result = studentTestQuestionAnswerDbService.studentTestQuestionAnswer(getStudentId(),
                                                                                  answerQuestionRequestResource.getCourseId(),
                                                                                  answerQuestionRequestResource.getSubjectId(),
                                                                                  answerQuestionRequestResource.getUnitId(),
                                                                                  answerQuestionRequestResource.getStudentTestId());

        return R.ok().put("data", result);
    }
}
