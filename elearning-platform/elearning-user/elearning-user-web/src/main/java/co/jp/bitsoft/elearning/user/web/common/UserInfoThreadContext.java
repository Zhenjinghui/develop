package co.jp.bitsoft.elearning.user.web.common;

public class UserInfoThreadContext {
	private static final ThreadLocal<UserInfo> CONTEXT = new ThreadLocal<>();

	public static void setUserInfo(UserInfo userInfo) {
		CONTEXT.set(userInfo);
	}

	public static Long getUserId() {
		return CONTEXT.get().getUserId();
	}

	public static String getEmail() {
		return CONTEXT.get().getEmail();
	}

	public static void clear() {
		CONTEXT.remove();
	}

}
