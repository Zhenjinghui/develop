package co.jp.bitsoft.elearning.user.web.modules.user.controller;

import java.util.List;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;

import cn.hutool.core.bean.BeanUtil;
import co.jp.bitsoft.elearning.common.exception.RRException;
import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.common.validator.ValidatorUtils;
import co.jp.bitsoft.elearning.core.entity.AdAddressEntity;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.service.AdAddressService;
import co.jp.bitsoft.elearning.user.domain.model.student.StudentRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.student.StudentResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.student.StudentUpdatePasswordRequestResource;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentWebService;
import co.jp.bitsoft.elearning.user.web.common.UserInfoThreadContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("mypage")
@Api(tags = { "マイページの受講者コントローラー" })
public class MyPageStudentController extends AbstractController {

	/**
	 * 受講者
	 */
	@Autowired
	private StudentWebService studentWebService;

    @Autowired
    private AdAddressService adAddressService;

	/**
	 * パスワード変更
	 *
	 * @param password    パスワード
	 * @param newPassword 新パスワード
	 * @return レスポンスデータ
	 */
	@ApiOperation(value = "パスワード変更", notes = "パスワードを変更する")
	@ApiResponses({ @ApiResponse(code = HttpStatus.SC_OK, message = "success") })
	@PostMapping("/password/update")
	public R updatePassword(@RequestBody final StudentUpdatePasswordRequestResource studentUpdatePasswordRequestResource) {

		// バリデーションチェック
		// パスワード
		ValidatorUtils.validateEntity(studentUpdatePasswordRequestResource, new String[] { "password" });
		// 新パスワード
		ValidatorUtils.validateEntity(studentUpdatePasswordRequestResource, new String[] { "newPassword" });

		// 受講者IDを取得
		Long userId = UserInfoThreadContext.getUserId();

		// 新パスワードを受講者テーブルへ更新。
		studentWebService.updatePassword(userId, studentUpdatePasswordRequestResource.getPassword(), studentUpdatePasswordRequestResource.getNewPassword());

		// 正常終了
		return R.ok();
	}

	/**
	 * 本人情報設定(初期表示)
	 *
	 * @return レスポンスデータ
	 */
	@ApiOperation(value = "本人情報設定(初期表示)", notes = "本人情報設定を初期表示する")
	@ApiResponses({
			@ApiResponse(code = HttpStatus.SC_OK, message = "success", response = StudentResponseResource.class) })
	@GetMapping("/user")
	public R user() {

		// 受講者IDを取得
		Long userId = UserInfoThreadContext.getUserId();

		// 受講者IDにより、受講者情報を取得する。
		StudentEntity student = studentWebService.getStudent(userId);

		// 受講者情報レスポンスを作成
		StudentResponseResource studentResponse = new StudentResponseResource();
		BeanUtil.copyProperties(student, studentResponse);
		// レスポンスデータを返却
		Map<String, Object> data = Maps.newHashMap();
		data.put("data", studentResponse); // 受講者情報レスポンス
		return R.ok(data);
	}

	/**
	 * マイページ・設定・本人情報変更
	 *
	 * @param studentRequestResource 受講者リクエスト情報
	 * @return レスポンスデータ
	 */
	@ApiOperation(value = "本人情報設定", notes = "本人情報を設定する")
	@ApiResponses({ @ApiResponse(code = HttpStatus.SC_OK, message = "success") })
	@PostMapping("/user/update")
	public R updateUser(@RequestBody final StudentRequestResource studentRequestResource) {

		// バリデーションチェック
		ValidatorUtils.validateEntity(studentRequestResource);

		// 受講者IDを取得
		Long userId = UserInfoThreadContext.getUserId();

		// 受講者IDにより、受講者情報を取得する。
		StudentEntity student = studentWebService.getStudent(userId);

		// リクエスト
		BeanUtil.copyProperties(studentRequestResource, student);

		// 受講者リクエスト情報から受講者テーブルへ更新
		if (!studentWebService.updateStudent(student)) {
			return R.error("本人情報変更が失敗しました");
		}

		// 正常終了
		return R.ok();
	}

    /**
     * 郵便番号
     */
	@ApiOperation(value = "郵便番号", notes = "郵便番号")
	@ApiResponses({
			@ApiResponse(code = HttpStatus.SC_OK, message = "success", response = StudentResponseResource.class) })
	@PostMapping("/zipInfo")
    public R zipAddressInfo(@RequestBody final AdAddressEntity zipCode){

    	QueryWrapper<AdAddressEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("zip", zipCode.getZip());
		List<AdAddressEntity> adAddress = adAddressService.list(queryWrapper);
		if(adAddress.size() == 0 ) {
			throw new RRException("郵便番号が正しくありません！");
		}
		// レスポンスデータを返却
		Map<String, Object> data = Maps.newHashMap();
		data.put("data", adAddress.get(0));
        return R.ok(data);
    }
}
