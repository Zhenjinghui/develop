package co.jp.bitsoft.elearning.user.web.modules.user.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import co.jp.bitsoft.elearning.common.utils.R;
import co.jp.bitsoft.elearning.user.domain.model.course.CourseCertificateResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.EndTimeUpdateRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentCourseRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentCourseResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentSubjectResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentTeachingMaterialResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentUnitResponseResource;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentCourseService;
import co.jp.bitsoft.elearning.user.service.modules.user.service.TestQuestionsDbService;
import lombok.var;


/**
 * 講座一覧
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@RestController
@RequestMapping("course")
public class StudentCourseController extends AbstractController {

    protected Logger logger = LoggerFactory.getLogger(StudentCourseController.class);

    @Autowired
    private StudentCourseService studentCourseService;

    @Autowired
    private TestQuestionsDbService testQuestionsDbService;

    private String sUrl = "/s3/";

    // 本番環境S3配置フォルダ
    private String eUrl = "/s3-elearning/";
    // 社内環境S3
    // private String eUrl = "/s3-elearning-test/";
    // 個人環境模擬パス
    // private String eUrl = "/Users/soncks/s3-elearning/";

    /**
     * 講座一覧
     *
     * @return {@link List< StudentCourseResponseResource >} 講座一覧
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list() {
        List<StudentCourseResponseResource> studentCourseResponseResourceList = studentCourseService.getCourseList(getStudent());

        Map<String, Object> rMap = new HashMap<>();

        Map<String, Object> rMap2 = new HashMap<>();
        rMap2.put("count", studentCourseResponseResourceList.size());
        rMap2.put("studentCourseResponseResourceList", studentCourseResponseResourceList);

        rMap.put("data", rMap2);

        return R.ok(rMap);
    }

    /**
     * 講座詳細
     *
     * @return {@link StudentCourseResponseResource} 講座
     */
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public R courseInfo(@RequestParam("courseId") @Pattern(regexp = "[0-9]+") Long courseId) {
        StudentCourseResponseResource studentCourseResponseResource = studentCourseService.getCourseInfo(getStudent(), courseId);
        Map<String, Object> courseDataCount = studentCourseService.queryCourseDataCount(getStudent(), courseId);

        Map<String, Object> rMap2 = new HashMap<>();
        rMap2.put("data", studentCourseResponseResource);
        rMap2.put("courseDataCount", courseDataCount);
        Map<String, Object> rMap = new HashMap<>();
        rMap.put("data", rMap2);
        return R.ok(rMap);
    }

    /**
     * 講座申請
     *
     * @param studentCourseRequestResource 申請情報
     * @return {@link StudentCourseResponseResource} 講座
     */
    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public R courseReg(@RequestBody @Valid StudentCourseRequestResource studentCourseRequestResource) {

        studentCourseService.courseReg(getStudent(), studentCourseRequestResource);

        StudentCourseResponseResource studentCourseResponseResource = studentCourseService.getCourseInfo(getStudent(), studentCourseRequestResource.getCourseId());
        Map<String, Object> courseDataCount = studentCourseService.queryCourseDataCount(getStudent(), studentCourseRequestResource.getCourseId());
        Map<String, Object> rMap = new HashMap<>();

        Map<String, Object> rMap2 = new HashMap<>();
        rMap2.put("count", studentCourseResponseResource.getSubjectResponseResourceList().size());
        rMap2.put("studentCourseResponseResourceList", studentCourseResponseResource);
        rMap2.put("courseDataCount", courseDataCount);
        rMap.put("data", rMap2);

        return R.ok(rMap);
    }

    /**
     * 教科詳細
     *
     * @return {@link StudentSubjectResponseResource} 教科詳細
     */
    @RequestMapping(value = "/subject/detail", method = RequestMethod.GET)
    public R subjectInfo(@RequestParam("courseId") @Pattern(regexp = "[0-9]+") Long courseId, @RequestParam("subjectId") @Pattern(regexp = "[0-9]+") Long subjectId) {
        StudentSubjectResponseResource studentSubjectResponseResource = studentCourseService.getSubjectInfo(getStudent(), courseId, subjectId);

        Map<String, Object> rMap = new HashMap<>();
        rMap.put("data", studentSubjectResponseResource);

        return R.ok(rMap);
    }

    /**
     * 単元詳細
     *
     * @return {@link StudentUnitResponseResource} 教科詳細
     */
    @RequestMapping(value = "/subject/unit/detail", method = RequestMethod.GET)
    public R subjectInfo(@RequestParam("courseId") @Pattern(regexp = "[0-9]+") Long courseId, @RequestParam("subjectId") @Pattern(regexp = "[0-9]+") Long subjectId, @RequestParam("unitId") @Pattern(regexp = "[0-9]+") Long unitId) {
        StudentUnitResponseResource studentUnitResponseResource = studentCourseService.getUnitInfo(getStudent(), courseId, subjectId, unitId);
        int count = testQuestionsDbService.countTestQuestions(courseId, subjectId, unitId);
        studentUnitResponseResource.setTestFlg(count > 0);

        Map<String, Object> rMap = new HashMap<>();
        rMap.put("data", studentUnitResponseResource);

        return R.ok(rMap);
    }

    /**
     * 教材詳細
     *
     * @return {@link StudentTeachingMaterialResponseResource} 教科詳細
     */
    @RequestMapping(value = "/subject/unit/material/detail", method = RequestMethod.GET)
    public R subjectInfo(@RequestParam("courseId") @Pattern(regexp = "[0-9]+") Long courseId, @RequestParam("subjectId") @Pattern(regexp = "[0-9]+") Long subjectId, @RequestParam("unitId") @Pattern(regexp = "[0-9]+") Long unitId, @RequestParam("materialId") @Pattern(regexp = "[0-9]+") Long materialId) {

        Map<String, Object> rMap = new HashMap<>();
        StudentTeachingMaterialResponseResource studentTeachingMaterialResponseResource = studentCourseService.getTeachingMaterialInfo(getStudent(), courseId, subjectId, unitId, materialId);

        rMap.put("data", studentTeachingMaterialResponseResource);

        return R.ok(rMap);
    }

    /**
     * 教材詳細
     *
     * @return {@link StudentTeachingMaterialResponseResource} 教科詳細
     */
    @RequestMapping(value = "/subject/unit/material/endTimeUpdate", method = RequestMethod.POST)
    public R endTimeUpdate(@RequestBody EndTimeUpdateRequestResource endTimeUpdateRequestResource) {
        String i = studentCourseService.getEndTimeUpdate(getStudent(), endTimeUpdateRequestResource.getCourseId(), endTimeUpdateRequestResource.getSubjectId(), endTimeUpdateRequestResource.getUnitId(), endTimeUpdateRequestResource.getMaterialId());
        Map<String, Object> rMap = new HashMap<>();
        rMap.put("data", i);
        return R.ok(rMap);
    }

    /**
     * 教材詳細
     *
     * @return {@link StudentTeachingMaterialResponseResource} 教科詳細
     */
    @RequestMapping(value = "/subject/unit/material/download", method = RequestMethod.POST)
    public ResponseEntity<byte[]> download(@RequestBody EndTimeUpdateRequestResource endTimeUpdateRequestResource) {
        StudentTeachingMaterialResponseResource studentTeachingMaterialResponseResource = studentCourseService.getTeachingMaterialInfo(getStudent(), endTimeUpdateRequestResource.getCourseId(), endTimeUpdateRequestResource.getSubjectId(), endTimeUpdateRequestResource.getUnitId(), endTimeUpdateRequestResource.getMaterialId());
        if (!StringUtils.equals(studentTeachingMaterialResponseResource.getContentType(), "01")) {
            HttpHeaders headers = null;
            byte[] bytes = new byte[0];
            try {
                headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                String url = studentTeachingMaterialResponseResource.getContentUrl();
                url = url.replace(sUrl, eUrl);
                File byteFile = new File(url);
                int size = (int) byteFile.length();
                FileInputStream inputStream = new FileInputStream(byteFile);
                bytes = new byte[size];

                int offset = 0;
                int readed;
                while (offset < size && (readed = inputStream.read(bytes, offset, inputStream.available())) != -1) {
                    offset += readed;
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
        } else {
            return null;
        }
    }

    /**
     * 修了証
     *
     * @return {@link CourseCertificateResponseResource} 修了証
     */
    @PostMapping(value = "/certificate")
    @ResponseBody
    public ResponseEntity<InputStreamResource> downloadCertificate(@RequestBody @Valid StudentCourseRequestResource studentCourseRequestResource) {
        var respResource = studentCourseService.certificate(getStudent(),getGroupId(),studentCourseRequestResource.getCourseId());
        try {
            return ResponseEntity.ok()
                                 // Content-Disposition
                                 .header(HttpHeaders.CONTENT_DISPOSITION,
                                         "attachment;filename*=utf-8''"
                                                                          + URLEncoder.encode("修了証.pdf",
                                                                                              "UTF-8"))
                                 // Content-Type
                                 .contentType(MediaType.APPLICATION_PDF)
                                 .body(new InputStreamResource(createPdf(respResource)));
        }
        catch (IOException | DocumentException e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.notFound().build();
        }
    }

//    private ResourceLoader resourceLoader;
//
//    @Autowired
//    public void setResourceLoader(ResourceLoader resourceLoader) {
//        this.resourceLoader = resourceLoader;
//    }

    // 修了証テンプレートpath
//    private static String COURSE_CERTIFICATE_TEMPLATE_PATH = "classpath:pdf/course_certificate_template.pdf";

    private InputStream createPdf(CourseCertificateResponseResource responseResource)
            throws IOException, DocumentException {
    	String url = responseResource.getCertificateTemplatePath();
        url = url.replace(sUrl, eUrl);
//        URL url = resourceLoader.getResource(COURSE_CERTIFICATE_TEMPLATE_PATH).getURL();
        PdfReader templateReader = new PdfReader(url);
        var output = new ByteArrayOutputStream();
        var templateBos = new ByteArrayOutputStream();
        var stamper = new PdfStamper(templateReader, templateBos);
        var form = stamper.getAcroFields();
        form.setGenerateAppearances(true);
        String[] keys = form.getFields().keySet().toArray(new String[0]);
        form.setField(keys[0], responseResource.getCertificateTitle());
        form.setField(keys[1], responseResource.getUserName());
        form.setField(keys[2], responseResource.getCertificateContent());
        form.setField(keys[3], responseResource.getStudyTestEndTime());
        form.setField(keys[4], responseResource.getIssuerName());
        form.setField(keys[5], responseResource.getIssuerNameSub());
        form.setField(keys[6], responseResource.getIssuerResponsiblePartyTitle());
        form.setField(keys[7], responseResource.getIssuerResponsiblePartyName());
        // created pdf cannot edit flag
        stamper.setFormFlattening(true);
        stamper.close();
        Document doc = new Document();
        PdfCopy copy = new PdfCopy(doc, output);
        doc.open();
        PdfImportedPage importPage = copy.getImportedPage(new PdfReader(templateBos.toByteArray()),
                                                          1);
        copy.addPage(importPage);
        doc.close();
        return new ByteArrayInputStream(output.toByteArray());
    }
}
