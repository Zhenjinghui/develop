/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.user.web.modules.user.controller;

import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller共通
 *
 * @author  BitSoft
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	protected StudentEntity getStudent() {
		return (StudentEntity) SecurityUtils.getSubject().getPrincipal();
	}

	protected Long getStudentId() {
		return getStudent().getStudentId();
	}

	protected Long getGroupId() {
		return getStudent().getGroupId();
	}
}
