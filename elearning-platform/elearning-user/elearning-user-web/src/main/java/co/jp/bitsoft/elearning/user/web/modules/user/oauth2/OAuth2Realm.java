/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.user.web.modules.user.oauth2;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.jp.bitsoft.elearning.user.service.common.ShiroService;
import co.jp.bitsoft.elearning.user.web.common.UserInfo;
import co.jp.bitsoft.elearning.user.web.common.UserInfoThreadContext;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTokenEntity;

/**
 * 認証
 *
 * @author  BitSoft
 */
@Component
public class OAuth2Realm extends AuthorizingRealm {
    @Autowired
    private ShiroService shiroService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof OAuth2Token;
    }

    /**
     * 認可（権限認可時使用）
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        StudentEntity user = (StudentEntity)principals.getPrimaryPrincipal();
        Long userId = user.getStudentId();

        //ユーザー権限リスト取得
        //Set<String> permsSet = shiroService.getUserPermissions(userId);

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //info.setStringPermissions(permsSet);
        return info;
    }

    /**
     * 認証（ログイン時）
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String accessToken = (String) token.getPrincipal();

        //アクセストークンにより受講者情報取得
        StudentTokenEntity tokenEntity = shiroService.queryByToken(accessToken);
        //token失效
        if(tokenEntity == null || tokenEntity.getExpireTime().getTime() < System.currentTimeMillis()){
            throw new IncorrectCredentialsException("一定時間操作が行われなかったため、ログイン有効期限が切れました。再ログインしてください。");
        }

        //查询用户信息
        StudentEntity user = shiroService.queryStudent(tokenEntity.getStudentId());
        //账号锁定
        if("2".equals(user.getAccountStatus())){
            throw new LockedAccountException("ログインIDがロックされました。システム管理者に連絡してください。");
        }

        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getStudentId());
        userInfo.setUserName(user.getUserName());
        UserInfoThreadContext.setUserInfo(userInfo);
        
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, accessToken, getName());
        return info;
    }
}
