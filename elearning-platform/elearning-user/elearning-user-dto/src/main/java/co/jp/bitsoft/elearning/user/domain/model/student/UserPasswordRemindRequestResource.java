package co.jp.bitsoft.elearning.user.domain.model.student;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@ApiModel(description = "受講者パスワードお問い合わせのリクエスト情報")
public class UserPasswordRemindRequestResource {

	/**
	 * メールアドレス
	 */
	@NotBlank(message = "ログインIDは必須です。")
	@Length(max = 50, message = "ログインIDの桁数は50文字以内を入力ください。")
	@Pattern(regexp = "[0-9A-z]*", message = "ログインIDは英数字を入力してください。")
	@ApiModelProperty(value = "ログインID")
	private String userName;

}
