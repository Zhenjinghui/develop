package co.jp.bitsoft.elearning.user.domain.model.student;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "お問い合わせ送信の情報")
public class StudentInquiryRequestResource {

	/**
	 * お問い合わせ項目
	 */
	@NotBlank(message = "お問い合わせ項目は必須です。")
	@ApiModelProperty(value = "お問い合わせ項目")
	private String type;

	/**
	 * お問い合わせ件名
	 */
	@NotBlank(message = "お問い合わせ件名は必須です。")
	@Length(min = 1, max = 50, message = "お問い合わせ件名は1～50文字は必要です。")
	@ApiModelProperty(value = "お問い合わせ件名")
	private String title;

	/**
	 * お問い合わせ内容
	 */
	@NotBlank(message = "お問い合わせ内容は必須です。")
	@Length(min = 1, max = 1000, message = "お問い合わせ内容は1～1000文字は必要です。")
	@ApiModelProperty(value = "お問い合わせ内容")
	private String content;


}
