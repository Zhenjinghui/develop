package co.jp.bitsoft.elearning.user.domain.model.courseinfo;

import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Data
@ApiModel(description = "単元確認テスト情報")
public class TestQuestionsRequsetResource {
    @ApiModelProperty(value = "講座ID", required = true)
    private Long courseId;

    @ApiModelProperty(value = "教科ID", required = true)
    private Long subjectId;

    @ApiModelProperty(value = "単元ID", required = true)
    private Long unitId;
}
