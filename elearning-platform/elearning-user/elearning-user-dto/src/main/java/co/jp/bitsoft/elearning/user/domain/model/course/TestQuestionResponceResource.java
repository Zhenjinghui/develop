package co.jp.bitsoft.elearning.user.domain.model.course;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.user.dto.TestQuestionsDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.var;

@Data
public class TestQuestionResponceResource {

    @ApiModelProperty(value = "受講者テストID", required = true)
    private Long studentTestId;

    @ApiModelProperty(value = "テスト問題", required = true)
    private TestQuestion testQuestion;

    public static List<TestQuestionResponceResource> builder(Long studentTestId,
                                                             List<TestQuestionsDto> testQuestionsDtoList,
                                                             List<TestQuestionsChoicesEntity> choicesEntityList) {
        return testQuestionsDtoList.stream().map(dto -> {
            var resources = new TestQuestionResponceResource();
            resources.setStudentTestId(studentTestId);

            var testQuestion = TestQuestion.builder(dto);
            // 「記述」以外の場合、選択肢を作成する
            if (!"3".equals(dto.getQuestionType())) {
                testQuestion.setTestQuestionChoices(TestQuestionChoices.builder(choicesEntityList.stream()
                                                                                                 .filter(entity -> dto.getTestQuestionsId()
                                                                                                                      .equals(entity.getTestQuestionsId()))
                                                                                                 .collect(Collectors.toList())));
            }
            // 「記述」の場合、空白の選択肢を作成する
            else {
                testQuestion.setTestQuestionChoices(IntStream.range(0, dto.getChoicesNumber())
                                                             .mapToObj(i -> new TestQuestionChoices())
                                                             .collect(Collectors.toList()));
            }
            resources.setTestQuestion(testQuestion);
            return resources;
        }).collect(Collectors.toList());
    }
}

@Data
class TestQuestion {
    @ApiModelProperty(value = "テスト問題ID", required = true)
    private Long testQuestionsId;

    @ApiModelProperty(value = "問題名", required = true)
    private String questionTitle;

    @ApiModelProperty(value = "問題文", required = true)
    private String questionContent;

    @ApiModelProperty(value = "出題形式", required = true)
    private String questionType;

    @ApiModelProperty(value = "問題イメージ", required = true)
    private String imagePath;

    @ApiModelProperty(value = "テスト問題選択肢", required = true)
    private List<TestQuestionChoices> testQuestionChoices;

    public static TestQuestion builder(TestQuestionsDto testQuestionsDto) {
        return new TestQuestion() {
            {
                setTestQuestionsId(testQuestionsDto.getTestQuestionsId());
                setQuestionTitle(testQuestionsDto.getQuestionTitle());
                setQuestionContent(testQuestionsDto.getQuestionContent());
                setQuestionType(testQuestionsDto.getQuestionType());
                setImagePath(testQuestionsDto.getImageCatalogPath());
            }
        };
    }
}

@Data
class TestQuestionChoices {
    @ApiModelProperty(value = "選択肢ID", required = true)
    private Long testQuestionsChoicesId;

    @ApiModelProperty(value = "選択肢NO", required = true)
    private Integer choicesNo;

    @ApiModelProperty(value = "選択肢文", required = true)
    private String choicesContents;

    public static List<TestQuestionChoices> builder(List<TestQuestionsChoicesEntity> choicesEntityList) {
        List<TestQuestionChoices> choiceList = choicesEntityList.stream()
                                   .filter(dto -> !Objects.isNull(dto.getTestQuestionsChoicesId())
                                                  && !Objects.isNull(dto.getChoicesNo())
                                                  && !Objects.isNull(dto.getChoicesContents()))
                                   .map(dto -> new TestQuestionChoices() {
                                       {
                                           setTestQuestionsChoicesId(dto.getTestQuestionsChoicesId());
                                           setChoicesNo(dto.getChoicesNo());
                                           setChoicesContents(dto.getChoicesContents());
                                       }
                                   })
                                   .collect(Collectors.toList());

        // 選択肢リストをシャッフルする
        Collections.shuffle(choiceList, new Random(System.currentTimeMillis()));
        return choiceList;
    }
}
