package co.jp.bitsoft.elearning.user.dto.course;

import lombok.Data;

import java.io.Serializable;

/**
 * 単元
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class StudentUnitDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 単元ID
	 */
	private String unitId;

	/**
	 * 単元名
	 */
	private String unitName;

	/**
	 * 教科名
	 */
	private String subjectTitle;

	/**
	 * 講座名
	 */
	private String courseTitle;

	/**
	 * 単元概要
	 */
	private String unitOutline;

	/**
	 * 単元説明
	 */
	private String unitDetail;

	/**
	 * 単元画像PATH
	 */
	private String unitImagePath;


}
