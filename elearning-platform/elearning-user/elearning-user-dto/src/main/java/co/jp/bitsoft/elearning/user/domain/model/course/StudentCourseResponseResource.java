package co.jp.bitsoft.elearning.user.domain.model.course;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class StudentCourseResponseResource implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 講座ID
     */
    private String courseId;

    /**
     * 講座名
     */
    private String courseTitle;

    /**
     * 講座類別
     */
    private String courseType;

    /**
     * 講座類別名
     */
    private String courseTypeName;

    /**
     * 講座概要
     */
    private String courseOutline;

    /**
     * 講座説明
     */
    private String courseDetail;

    /**
     * 講座画像PATH
     */
    private String courseImagePath;

    /**
     * 受講申請要否フラグ
     */
    private String courseRegFlag;

    /**
     * 受講申請要否フラグ名
     */
    private String courseRegFlagName;

    /**
     * 受講状態
     */
    private String studyStatus;

    /**
     * 受講状態名
     */
    private String studyStatusName;

    /**
     * 受講申請理由
     */
    private String courseRegReason;

    /**
     * 受講申請拒否理由
     */
    private String regRejectReason;

    /**
     * 公開開始日時
     */
    private String openStartTime;

    /**
     * 公開終了日時
     */
    private String openEndTime;

    /**
     * 公開終了日時
     */
    private String groupDetail;

    /**
     * 教科一覧
     */
    private Map<String, Object> subjectResponseResourceList;

    /**
     * 講座終了証テンプレートID
     */
    private String courseCertificateTemplateId;
}
