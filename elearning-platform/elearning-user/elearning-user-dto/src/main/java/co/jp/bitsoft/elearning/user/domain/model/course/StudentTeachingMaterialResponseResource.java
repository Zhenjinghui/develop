package co.jp.bitsoft.elearning.user.domain.model.course;

import lombok.Data;

import java.io.Serializable;

/**
 * 教材
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class StudentTeachingMaterialResponseResource implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 教材ID
     */
    private String teachingMaterialId;

    /**
     * 教材名
     */
    private String teachingMaterialTitle;

    /**
     * 単元名
     */
    private String unitName;

    /**
     * 教科名
     */
    private String subjectTitle;

    /**
     * 講座名
     */
    private String courseTitle;

    /**
     * 教材概要
     */
    private String teachingMaterialOutline;

    /**
     * 教材説明
     */
    private String teachingMaterialDetail;

    /**
     * 教材画像PATH
     */
    private String teachingMaterialImagePath;

    /**
     * コンテンツ種別
     */
    private String contentType;

    /**
     * コンテンツ種別名
     */
    private String contentTypeName;

    /**
     * コンテンツURL
     */
    private String contentUrl;

    /**
     * コンテンツ（動画）放送時間（分）
     */
    private String contentVideoTime;

    /**
     * コンテンツページ数
     */
    private String contentPages;

    /**
     * コンテンツ文字数
     */
    private String contentLettersNum;

    /**
     * 教材受講状態（'0':受講中 '1':受講完了 '2':スキップ）
     */
    private String scsStudyStatus;
}
