package co.jp.bitsoft.elearning.user.dto.course;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 修了証内容
 *
 * @author BitSoft-inc
 * @date 2021-01-01 00:00:00
 */
@Data
public class CourseCertificateDto implements Serializable {

    private static final long serialVersionUID = 1453043860807239519L;

    /** タイトル */
    private String certificateTitle;

    /** 本文 */
    private String certificateContent;

    /** 受講受験終了日時 */
    private Date studyTestEndTime;

    /** 発行機関名 */
    private String issuerName;

    /** 発行機関サブ名 */
    private String issuerNameSub;

    /** 修了証発行発行者肩書 */
    private String issuerResponsiblePartyTitle;

    /** 発行機関責任者名 */
    private String issuerResponsiblePartyName;

    /** 終了証テンプレートPATH */
    private String certificateTemplatePath;

    /** 都道府県 */
    private String groupPrefecture;

    /** 市区町村 */
    private String groupAddressCity;

    /** クラス名 */
    private String groupName;

    /** 所属ご契約者名 */
    private String groupNameOf;
}
