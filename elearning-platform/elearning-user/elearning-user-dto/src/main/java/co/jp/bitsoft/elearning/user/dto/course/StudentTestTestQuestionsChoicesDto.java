package co.jp.bitsoft.elearning.user.dto.course;


import java.io.Serializable;

import lombok.Data;

@Data
public class StudentTestTestQuestionsChoicesDto implements Serializable {

    private static final long serialVersionUID = -4215430459204402984L;

    /**
     * 受講者テスト問題解答ID
     */
    private Long studentTestQuestionAnswerId;

    /**
     * 受講者テストID
     */
    private Long studentTestId;

    /**
     * テスト問題ID
     */
    private Long testQuestionsId;

    /**
     * 解答内容
     */
    private String answerContent;

    /**
     * 出題形式
     */
    private String questionType;

    /**
     * 選択肢ID
     */
    private Long testQuestionsChoicesId;

    /**
     * 選択肢NO
     */
    private Integer choicesNo;

    /**
     * 選択肢文
     */
    private String choicesContents;

    /**
     * 正解フラグ
     */
    private String correctFlag;
}
