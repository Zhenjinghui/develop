package co.jp.bitsoft.elearning.user.domain.model.course;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class EndTimeUpdateRequestResource implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long courseId;
    private Long subjectId;
    private Long unitId;
    private Long materialId;
}
