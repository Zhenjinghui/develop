package co.jp.bitsoft.elearning.user.domain.model.course;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

/**
 * 教科
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class StudentSubjectResponseResource implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 教科ID
     */
    private String subjectId;

    /**
     * 教科名
     */
    private String subjectTitle;

    /**
     * 講座名
     */
    private String courseTitle;

    /**
     * 教科概要
     */
    private String subjectOutline;

    /**
     * 教科説明
     */
    private String subjectDetail;

    /**
     * 教科画像PATH
     */
    private String subjectImagePath;

    /**
     * 単元
     */
    Map<String, Object> studentUnitResponseResourceList;

    /**
     * 教材总数
     */
    private String unitStudyNum;

    /**
     * 教材已学习数
     */
    private String unitNum;
}
