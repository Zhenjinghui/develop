package co.jp.bitsoft.elearning.user.dto;

import lombok.Data;

@Data
public class TestQuestionsDto {
    /**
     * テスト問題ID
     */
    private Long testQuestionsId;

    /**
     * 問題名
     */
    private String questionTitle;

    /**
     * 問題文
     */
    private String questionContent;

    /**
     * 出題形式
     */
    private String questionType;

    /**
     * イメージカタログ画像PATH
     */
    private String imageCatalogPath;

    /**
     * イメージカタログ画像AWS S3 BUCKET名
     */
    private String imageCatalogS3BucketName;

    /**
     * イメージカタログ画像AWS S3 オブジェクトキー
     */
    private String imageCatalogS3ObjectKey;

    /**
     * 固定フラグ
     */
    private String fixFlag;

    /**
     * 選択肢数
     */
    private Integer choicesNumber;

    /**
     * テスト時間制限有無
     */
    private String testConditionFlag;

    /**
     * テスト時間制限（分）
     */
    private Integer testConditionTime;
}
