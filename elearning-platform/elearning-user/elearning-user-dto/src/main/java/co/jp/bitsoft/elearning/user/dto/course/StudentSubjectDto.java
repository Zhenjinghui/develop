package co.jp.bitsoft.elearning.user.dto.course;

import lombok.Data;

import java.io.Serializable;

/**
 * 教科
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class StudentSubjectDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 教科ID
	 */
	private String subjectId;

	/**
	 * 教科名
	 */
	private String subjectTitle;

	/**
	 * 講座名
	 */
	private String courseTitle;

	/**
	 * 教科概要
	 */
	private String subjectOutline;

	/**
	 * 教科説明
	 */
	private String subjectDetail;

	/**
	 * 教科画像PATH
	 */
	private String subjectImagePath;

}
