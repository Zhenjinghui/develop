package co.jp.bitsoft.elearning.user.dto.mypage;

import lombok.Data;

import java.io.Serializable;

/**
 * MyPage
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class MyPageDto implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 講座ID
	 */
	private String courseId;

	/**
	 * 講座名
	 */
	private String courseTitle;

	/**
	 * 受講状態
	 */
	private String studyStatus;

	/**
	 * 受講状態名
	 */
	private String studyStatusName;

	/**
	 * 公開開始日時
	 */
	private String openStartTime;

	/**
	 * 公開終了日時
	 */
	private String openEndTime;
}
