package co.jp.bitsoft.elearning.user.domain.model.course;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

/**
 * 単元
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class StudentUnitResponseResource implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 単元ID
     */
    private String unitId;

    /**
     * 単元名
     */
    private String unitName;

    /**
     * 教科名
     */
    private String subjectTitle;

    /**
     * 講座名
     */
    private String courseTitle;

    /**
     * 単元概要
     */
    private String unitOutline;

    /**
     * 単元説明
     */
    private String unitDetail;

    /**
     * 単元画像PATH
     */
    private String unitImagePath;

    /**
     * 教材一覧
     */
    private Map<String, Object> studentTeachingMaterialResponseResourceList;

    /**
     * 受講者テスト一覧
     */
    private Map<String, Object> studentStudentTestResponseResourceList;

    /**
     * テストフラグ
     */
    private boolean testFlg;


    /**
     * テストフラグ
     */
    private String testExistFlg;

    /**
     * テスト合格フラグ
     */
    private String testPassFlg;

    /**
     * 教材总数
     */
    private String materiaNum;

    /**
     * 教材已学习数
     */
    private String materiaStudyNum;

}
