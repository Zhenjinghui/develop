package co.jp.bitsoft.elearning.user.domain.model.courseinfo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import co.jp.bitsoft.elearning.user.dto.StudentTestQuestionAnswersDto;
import co.jp.bitsoft.elearning.user.dto.course.StudentTestTestQuestionsChoicesDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.var;

@Data
@ApiModel(description = "単元テスト結果")
public class StudentTestResponseResource {

    /**
     * 講座ID
     */
    @ApiModelProperty(value = "講座ID")
    private Long courseId;

    /**
     * 講座名
     */
    @ApiModelProperty(value = "講座名")
    private String courseName;

    /**
     * 教科ID
     */
    @ApiModelProperty(value = "教科ID")
    private Long subjectId;

    /**
     * 教科名
     */
    @ApiModelProperty(value = "教科名")
    private String subjectName;

    /**
     * 単元ID
     */
    @ApiModelProperty(value = "単元ID")
    private Long unitId;

    /**
     * 単元名
     */
    @ApiModelProperty(value = "単元名")
    private String unitName;

    /**
     * 単元テストID
     */
    @ApiModelProperty(value = "単元テストID")
    private Long unitTestId;

    /**
     * 単元テスト名
     */
    @ApiModelProperty(value = "単元テスト名")
    private String testTitle;

    /**
     * テスト正解率
     */
    @ApiModelProperty(value = "テスト正解率")
    private String answerCorrectRate;

    /**
     * テスト判定結果
     */
    @ApiModelProperty(value = "テスト判定結果")
    private String answerResult;

    /**
     * 不合格問題一覧
     */
    @ApiModelProperty(value = "不合格問題情報一覧")
    private List<NoPassedAnswer> noPassedAnsweres;

    public static StudentTestResponseResource builder(List<StudentTestQuestionAnswersDto> studentTestQuestionAnswersDtoList,
                                                      List<StudentTestTestQuestionsChoicesDto> studentTestTestQuestionsChoicesList) {
        var studentTestResponseResource = new StudentTestResponseResource();
        if (!CollectionUtils.isEmpty(studentTestQuestionAnswersDtoList)) {
            var firstDto = studentTestQuestionAnswersDtoList.get(0);
            studentTestResponseResource.setCourseId(firstDto.getCourseId());
            studentTestResponseResource.setCourseName(firstDto.getCourseTitle());
            studentTestResponseResource.setSubjectId(firstDto.getSubjectId());
            studentTestResponseResource.setSubjectName(firstDto.getSubjectTitle());
            studentTestResponseResource.setUnitId(firstDto.getUnitId());
            studentTestResponseResource.setUnitName(firstDto.getUnitName());
            studentTestResponseResource.setUnitTestId(firstDto.getUnitTestId());
            studentTestResponseResource.setTestTitle(firstDto.getTestTitle());
            studentTestResponseResource.setAnswerCorrectRate(firstDto.getAnswerCorrectRate());
            studentTestResponseResource.setAnswerResult(firstDto.getTestAnswerResult());
            // テストが合格にしても不合格の内容も画面に表示する
            studentTestResponseResource.setNoPassedAnsweres(studentTestQuestionAnswersDtoList.stream()
                                                                                             .filter(entity -> "1".equals(entity.getQuestionAnswerResult())) // 不合格の解答
                                                                                             .map(entity -> {
                                                                                                 var noPassedAnswer = new NoPassedAnswer();
                                                                                                 noPassedAnswer.setQuestionTitle(entity.getQuestionTitle());
                                                                                                 noPassedAnswer.setQuestionContent(entity.getQuestionContent());
                                                                                                 noPassedAnswer.setQuestionExplanation(entity.getQuestionExplanation());
                                                                                                 noPassedAnswer.setAnswerResult(entity.getQuestionAnswerResult());

                                                                                                 // 記述以外の場合
                                                                                                 if (!"3".equals(entity.getQuestionType())) {
                                                                                                     var choicesDtoList = studentTestTestQuestionsChoicesList.stream()
                                                                                                                                                             .filter(choice -> entity.getQuestionAnswerId()
                                                                                                                                                                                     .equals(choice.getStudentTestQuestionAnswerId()))
                                                                                                                                                             .collect(Collectors.toList());

                                                                                                     // 解答内容が選択肢NOのため、該当選択肢NOに対応する選択肢文を取得し、不合格問題情報の「解答内容」とする
                                                                                                     var answerContent = new ArrayList<String>();
                                                                                                     for (String no : entity.getAnswerContent()
                                                                                                                            .split(",")) {
                                                                                                         for (var dto : choicesDtoList) {
                                                                                                             if (dto.getChoicesNo()
                                                                                                                    .toString()
                                                                                                                    .equals(no)) {
                                                                                                                 answerContent.add(dto.getChoicesContents());
                                                                                                                 break;
                                                                                                             }
                                                                                                         }
                                                                                                     }

                                                                                                     noPassedAnswer.setAnswerContent(String.join("\n\n",
                                                                                                                                                 answerContent));

                                                                                                     // 正解の選択肢NOの選択肢文を取得し、不合格問題情報の「標準解答」とする
                                                                                                     var correctAnswer = new ArrayList<String>();
                                                                                                     for (String no : entity.getStandardAnswers()
                                                                                                                            .split(",")) {
                                                                                                         for (var dto : choicesDtoList) {
                                                                                                             if (dto.getChoicesNo()
                                                                                                                    .toString()
                                                                                                                    .equals(no)) {
                                                                                                                 correctAnswer.add(dto.getChoicesContents());
                                                                                                                 break;
                                                                                                             }
                                                                                                         }
                                                                                                     }

                                                                                                     noPassedAnswer.setStandardAnswers(String.join("\n\n",
                                                                                                                                                   correctAnswer));
                                                                                                 }
                                                                                                 // 記述の場合、そのままにする
                                                                                                 else {
                                                                                                     noPassedAnswer.setAnswerContent(entity.getAnswerContent());
                                                                                                     noPassedAnswer.setStandardAnswers(entity.getStandardAnswers());
                                                                                                 }

                                                                                                 return noPassedAnswer;
                                                                                             })
                                                                                             .collect(Collectors.toList()));
        }
        return studentTestResponseResource;
    }
}

@Data
@ApiModel(description = "不合格問題情報")
class NoPassedAnswer {

    /**
     * 問題名
     */
    @ApiModelProperty(value = "問題名")
    private String questionTitle;

    /**
     * 問題文
     */
    @ApiModelProperty(value = "問題文")
    private String questionContent;

    /**
     * 解説文
     */
    @ApiModelProperty(value = "解説文")
    private String questionExplanation;

    /**
     * 解答内容
     */
    @ApiModelProperty(value = "解答内容")
    private String answerContent;

    /**
     * 標準解答
     */
    @ApiModelProperty(value = "標準解答")
    private String standardAnswers;

    /**
     * 解答判定結果
     */
    @ApiModelProperty(value = "解答判定結果")
    private String answerResult;
}