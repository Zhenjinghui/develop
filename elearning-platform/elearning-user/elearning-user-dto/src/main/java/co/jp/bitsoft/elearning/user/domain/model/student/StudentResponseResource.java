package co.jp.bitsoft.elearning.user.domain.model.student;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "受講者のレスポンス情報")
public class StudentResponseResource {

	/**
	 * メール
	 */
	private String email;

	/**
	 * ユーザー名
	 */
	private String userName;

	/**
	 * ユーザー名（漢字）
	 */
	private String userNameKanji;

	/**
	 * ユーザー名（カナ）
	 */
	private String userNameKana;

	/**
	 * ニックネーム
	 */
	private String nickName;

	/**
	 * 性別
	 */
	private String sex;

	/**
	 * 生年月日
	 */
	private String birthday;

	/**
	 * 住所・郵便番号
	 */
	private String addressZipCode;

	/**
	 * 住所・都道府県
	 */
	private String addressPrefecture;

	/**
	 * 住所・市区町村
	 */
	private String addressCity;

	/**
	 * 住所・番地以下
	 */
	private String addressOthers;

	/**
	 * 住所・建物名
	 */
	private String addressBuildingName;

	/**
	 * 連絡・携帯
	 */
	private String contactMobile;

	/**
	 * 連絡・電話
	 */
	private String contactTel;

	/**
	 * 連絡・FAX
	 */
	private String contactFax;
}
