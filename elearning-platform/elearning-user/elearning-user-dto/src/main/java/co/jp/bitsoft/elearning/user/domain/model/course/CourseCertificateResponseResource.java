package co.jp.bitsoft.elearning.user.domain.model.course;

import java.io.Serializable;

import lombok.Data;

/**
 * 修了証
 *
 * @author BitSoft-inc
 * @date 2021-01-01 00:00:00
 */
@Data
public class CourseCertificateResponseResource implements Serializable {

    private static final long serialVersionUID = -2130644067263558555L;

    /** タイトル */
    private String certificateTitle;

    /** 受講者氏名 */
    private String userName;

    /** 本文 */
    private String certificateContent;

    /** 受講受験終了日時 */
    private String studyTestEndTime;

    /** 発行機関名 */
    private String issuerName;

    /** 発行機関サブ名 */
    private String issuerNameSub;

    /** 修了証発行発行者肩書 */
    private String issuerResponsiblePartyTitle;

    /** 発行機関責任者名 */
    private String issuerResponsiblePartyName;

    /** 終了証テンプレートPATH */
    private String certificateTemplatePath;

    /** 都道府県 */
    private String group_prefecture;

    /** 市区町村 */
    private String group_address_city;

    /** クラス名 */
    private String group_name;

    /** 所属ご契約者名 */
    private String group_name_of;
}
