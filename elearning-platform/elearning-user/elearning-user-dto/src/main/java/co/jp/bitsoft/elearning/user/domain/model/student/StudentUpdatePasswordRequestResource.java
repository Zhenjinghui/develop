package co.jp.bitsoft.elearning.user.domain.model.student;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "受講者パスワード変更の情報")
public class StudentUpdatePasswordRequestResource {

	/**
	 * パスワード
	 */
	@NotBlank(message = "パスワードは必須です。")
	@Pattern(regexp = "[0-9A-z]*", message = "パスワードは英数字を入力してください。")
	@Length(min = 6, max = 100, message = "パスワードは6～100文字は必要です。")
	@ApiModelProperty(value = "パスワード")
	private String password;

	/**
	 * 新パスワード
	 */
	@NotBlank(message = "新パスワードは必須です。")
	@Pattern(regexp = "[0-9A-z]*", message = "新パスワードは英数字を入力してください。")
	@Length(min = 6, max = 100, message = "新パスワードは6～100文字は必要です。")
	@ApiModelProperty(value = "新パスワード")
	private String newPassword;


}
