package co.jp.bitsoft.elearning.user.domain.model.student;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@ApiModel(description = "受講者のリクエスト情報")
public class StudentRequestResource {

	/**
	 * ログインID(メール）アドレス
	 */
	@NotBlank(message = "メールを空白にすることはできません")
	@Email(message = "ログインID(メール）アドレスが形式不正です。")
	@Length(max = 100, message = "ログインID(メール）アドレスは100文字以内入力ください。")
	@ApiModelProperty(value = "ログインID(メール）アドレス")
	private String email;

	/**
	 * ユーザー名（漢字）
	 */
	@Length(max = 50, message = "ユーザー名（漢字）は50文字以内入力ください。")
	@ApiModelProperty(value = "ユーザー名（漢字）")
	private String userNameKanji;

	/**
	 * ユーザー名（カナ）
	 */
	@Pattern(regexp = "^[ァ-タダ-ヶー０-９Ａ-Ｚａ-ｚ]*$", message = "ユーザー名（カナ）は不正です。")
	@Length(max = 50, message = "ユーザー名（カナ）は50文字以内入力ください。")
	@ApiModelProperty(value = "ユーザー名（カナ）")
	private String userNameKana;

	/**
	 * ニックネーム
	 */
	@Length(max = 50, message = "ニックネームは50文字以内入力ください。")
	@ApiModelProperty(value = "ニックネーム")
	private String nickName;

	/**
	 * 性別
	 */
	@NotBlank(message = "性別は必須です。")
	@Pattern(regexp = "^[01]$", message = "性別を選択ください。")
	private String sex;

	/**
	 * 生年月日
	 */
	private String birthday;

	/**
	 * 住所・郵便番号
	 */
	@Pattern(regexp = "^$|^[0-9]{3}[0-9]{4}$", message = "住所・郵便番号の形式は不正です。")
	private String addressZipCode;

	/**
	 * 住所・都道府県
	 */
	@Length(max = 50, message = "住所・都道府県は50文字以内です。正しい郵便番号を入力ください。")
	private String addressPrefecture;

	/**
	 * 住所・市区町村
	 */
	@Length(max = 50, message = "住所・市区町村は50文字以内です。正しい郵便番号を入力ください。")
	private String addressCity;

	/**
	 * 住所・番地以下
	 */
	@Length(max = 100, message = "住所・番地以下は100文字以入力ください。")
	private String addressOthers;

	/**
	 * 住所・建物名
	 */
	@Length(max = 50, message = "住所・建物名は50文字以入力ください。")
	private String addressBuildingName;

	/**
	 * 連絡・携帯
	 */
	@Pattern(regexp = "^$|0[789]0?[0-9]{4}?[0-9]{4}$", message = "連絡・携帯番号形式は不正です。")
	private String contactMobile;

	/**
	 * 連絡・電話
	 */
	@Pattern(regexp = "^[-0-9]*$",message = "連絡・電話形式は不正です。")
	private String contactTel;

	/**
	 * 連絡・FAX
	 */
	@Pattern(regexp = "^[-0-9]*$",message = "連絡・FAX形式は不正です。")
	private String contactFax;
}
