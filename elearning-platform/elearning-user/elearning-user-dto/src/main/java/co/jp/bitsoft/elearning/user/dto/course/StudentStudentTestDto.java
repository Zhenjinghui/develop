package co.jp.bitsoft.elearning.user.dto.course;

import java.io.Serializable;

import lombok.Data;

/**
 * 教科
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class StudentStudentTestDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 受講者テストID
	 */
	private String studentTestId;

	/**
	 * 解答開始日時
	 */
	private String answerStartTime;

	/**
	 * テストステータス
	 */
	private String answerStatus;

	/**
	 * テストステータス名
	 */
	private String answerStatusName;

	/**
	 * テスト判定結果
	 */
	private String answerResult;

	/**
	 * テスト判定結果名
	 */
	private String answerResultName;

	/**
	 * テスト解答率（%）
	 */
	private String answerRate;

    /**
     * テスト正解率（%）
     */
    private String answerCorrectRate;
}
