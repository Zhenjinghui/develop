package co.jp.bitsoft.elearning.user.domain.model.courseinfo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "単元確認テスト提出情報")
public class AnswerQuestionRequestResource {

    @ApiModelProperty(value = "講座ID", required = true)
    private Long courseId;

    @ApiModelProperty(value = "教科ID", required = true)
    private Long subjectId;

    @ApiModelProperty(value = "単元ID", required = true)
    private Long unitId;

    @ApiModelProperty(value = "単元テスト問題数", required = true)
    private Integer testCount;

    @ApiModelProperty(value = "受講者テストID", required = true)
    private Long studentTestId;

    @ApiModelProperty(value = "テスト問題ID", required = true)
    private Long testQuestionsId;

    @ApiModelProperty(value = "解答内容", required = true)
    private String answerContent;

    @ApiModelProperty(value = "解答開始日時", required = true)
    private Long answerStartTime;

    @ApiModelProperty(value = "出題形式", required = true)
    private String questionType;
}
