package co.jp.bitsoft.elearning.user.domain.model.student;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "受講者ログインのレスポンス情報")
public class StudentLoginResponseResource {

    /**
     * トークン
     */
    @ApiModelProperty(value = "トークン")
    private String token;

    /**
     * デフォルトパスワードフラグ
     */
    @ApiModelProperty(value = "デフォルトパスワードフラグ")
    private String defaultPasswordfalg;

    /**
     * 過期時間(秒)
     */
    @ApiModelProperty(value = "過期時間(秒)")
    private int expireTime;

}
