package co.jp.bitsoft.elearning.user.domain.model.course;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Data
public class StudentCourseRequestResource implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 講座ID
     */
    @NotNull(message = "講座IDは必須です。")
    private Long courseId;

    /**
     * 受講申請理由
     */
    private String courseRegReason;
}
