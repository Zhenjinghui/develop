package co.jp.bitsoft.elearning.user.domain.model.courseinfo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "講座インフォメーション情報")
public class CourseInfoResponseResource {

    /**
     * 講座ID
     */
    @ApiModelProperty(value = "講座ID")
    private Long courseId;

    /**
     * 講座名
     */
    @ApiModelProperty(value = "講座名")
    private String courseTitle;

    /**
     * インフォメーションID
     */
    @ApiModelProperty(value = "インフォメーションID")
    private Long infoId;

    /**
     * インフォメーション種別
     */
    @ApiModelProperty(value = "インフォメーション種別")
    private String infoType;

    /**
     * インフォメーション件名
     */
    @ApiModelProperty(value = "インフォメーション件名")
    private String infoName;

    /**
     * インフォメーション概要
     */
    @ApiModelProperty(value = "インフォメーション概要")
    private String infoOverview;

    /**
     * インフォメーション詳細
     */
    @ApiModelProperty(value = "インフォメーション詳細")
    private String infoDetail;

    /**
     * 公開開始日時
     */
    @ApiModelProperty(value = "公開開始日時", dataType = "string")
    private Date openStartTime;
}
