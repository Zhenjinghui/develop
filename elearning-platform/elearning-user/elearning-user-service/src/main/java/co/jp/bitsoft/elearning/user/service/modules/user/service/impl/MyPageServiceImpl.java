package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.user.domain.model.mypage.MyPageResponseResource;
import co.jp.bitsoft.elearning.user.service.db.MyPageDbService;
import co.jp.bitsoft.elearning.user.dto.mypage.MyPageDto;
import co.jp.bitsoft.elearning.user.service.modules.user.service.MyPageService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class MyPageServiceImpl implements MyPageService {

    @Autowired
    private MyPageDbService myPageDbService;

    @Override
    public List<MyPageResponseResource> courseList(StudentEntity studentEntity) {

        if (studentEntity == null) {
            //TODO throw error?
            return null;
        }

        List<MyPageResponseResource> myPageResponseResourceList = new ArrayList<>();
        List<MyPageDto> myPageDtoList = myPageDbService.queryCourseList(studentEntity.getStudentId());
        myPageDtoList.forEach(dto -> {
            MyPageResponseResource myPageResponseResource = new MyPageResponseResource();
            BeanUtils.copyProperties(dto, myPageResponseResource);
            myPageResponseResourceList.add(myPageResponseResource);
        });

        return myPageResponseResourceList;
    }
}