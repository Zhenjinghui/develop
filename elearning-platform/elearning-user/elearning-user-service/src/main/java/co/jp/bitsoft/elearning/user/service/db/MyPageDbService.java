package co.jp.bitsoft.elearning.user.service.db;

import co.jp.bitsoft.elearning.user.dto.mypage.MyPageDto;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * MyPage
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Mapper
public interface MyPageDbService extends BaseMapper<MyPageDto> {

    /**
     * 講座受講一覧  TODO 取得条件里。讲座公开状况要不要判断？ 用户在公开的时候申请了，并且拿到证书了。这时候又不公开了。那么用户还能不能看到自己申请的和证书？ 删除条件同理
     */
    List<MyPageDto> queryCourseList(@Param("studentId") Long studentId);
}
