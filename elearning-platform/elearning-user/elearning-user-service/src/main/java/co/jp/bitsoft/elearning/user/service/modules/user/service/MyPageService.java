package co.jp.bitsoft.elearning.user.service.modules.user.service;

import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.user.domain.model.mypage.MyPageResponseResource;

import java.util.List;

/**
 * 講座一覧サービス
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
public interface MyPageService {

    List<MyPageResponseResource> courseList(StudentEntity studentEntity);
}

