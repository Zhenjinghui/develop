/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.user.service.common.impl;

import co.jp.bitsoft.elearning.core.service.dao.StudentDao;
import co.jp.bitsoft.elearning.core.service.dao.StudentTokenDao;
import co.jp.bitsoft.elearning.user.service.common.ShiroService;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTokenEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class ShiroServiceImpl implements ShiroService {
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private StudentTokenDao studentTokenDao;

    @Override
    public StudentTokenEntity queryByToken(String token) {
        return studentTokenDao.queryByToken(token);
    }

    @Override
    public StudentEntity queryStudent(Long studentId) {
        return studentDao.selectById(studentId);
    }
}
