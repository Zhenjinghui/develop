package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.entity.CourseInfoEntity;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.CourseInfoResponseResource;
import co.jp.bitsoft.elearning.user.service.db.dao.CourseInfoDbDao;
import co.jp.bitsoft.elearning.user.service.modules.user.service.CourseInfoDbService;

@Service("courseInfoDbService")
public class CourseInfoDbServiceImpl extends
        ServiceImpl<CourseInfoDbDao, CourseInfoEntity> implements CourseInfoDbService {
    @Override
    public PageUtils courseInfoListByGroupId(Map<String, Object> params, Long groupId) {
        IPage<?> result = this.getBaseMapper()
                              .courseInfoListByGroupId(new Query<CourseInfoResponseResource>().getPage(params),
                                                       groupId,
                                                       new Timestamp(new Date().getTime()));
        return new PageUtils(result);
    }

    @Override
    public CourseInfoResponseResource courseInfo(Long groupId, Long infoId) {
        return this.getBaseMapper()
                   .courseInfo(groupId, infoId, new Timestamp(new Date().getTime()));
    }
}
