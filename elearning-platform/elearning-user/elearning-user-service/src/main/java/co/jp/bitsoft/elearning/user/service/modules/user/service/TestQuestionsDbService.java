package co.jp.bitsoft.elearning.user.service.modules.user.service;

import java.util.Map;

public interface TestQuestionsDbService {
    /**
     * テスト問題リストを取得する
     *
     */
    Map<String, Object> testQuestionsList(Long groupId,
                                          Long studentId,
                                          Long courseId,
                                          Long subjectId,
                                          Long unitId);

    /**
     * テスト問題の数を取得する
     *
     */
    int countTestQuestions(Long courseId, Long subjectId, Long unitId);
}
