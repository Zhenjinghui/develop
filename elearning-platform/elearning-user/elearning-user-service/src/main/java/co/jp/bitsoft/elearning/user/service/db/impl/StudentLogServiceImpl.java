/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.user.service.db.impl;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.common.utils.Query;
import co.jp.bitsoft.elearning.core.service.dao.StudentLogDao;
import co.jp.bitsoft.elearning.core.entity.StudentLogEntity;
import co.jp.bitsoft.elearning.user.service.db.StudentLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("StudentLogService")
public class StudentLogServiceImpl extends ServiceImpl<StudentLogDao, StudentLogEntity> implements StudentLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");

        IPage<StudentLogEntity> page = this.page(
            new Query<StudentLogEntity>().getPage(params),
            new QueryWrapper<StudentLogEntity>().like(StringUtils.isNotBlank(key),"user_name", key)
        );

        return new PageUtils(page);
    }
}
