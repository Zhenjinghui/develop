/**
 *
 */
package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import java.text.MessageFormat;
import java.util.Date;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import co.jp.bitsoft.elearning.common.exception.RRException;
import co.jp.bitsoft.elearning.common.mail.CommonMailUtils;
import co.jp.bitsoft.elearning.common.utils.TokenGenerator;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.StudentLoginHistoryEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTokenEntity;
import co.jp.bitsoft.elearning.core.service.StudentLoginHistoryService;
import co.jp.bitsoft.elearning.core.service.StudentService;
import co.jp.bitsoft.elearning.core.service.StudentTokenService;
import co.jp.bitsoft.elearning.user.domain.model.student.StudentInquiryRequestResource;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentWebService;

/**
 * 受講者Webサービス
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2021-01-11 10:00:00
 */
@Service("studentWebService")
public class StudentWebServiceImpl implements StudentWebService {

	/**
	 * 受講者
	 */
	@Autowired
	private StudentService studentService;

	/**
	 * 受講者トークン
	 */
	@Autowired
	private StudentTokenService studentTokenService;

	/**
	 * ユーザー受講者ユーザーログイン履歴
	 */
	@Autowired
	private StudentLoginHistoryService studentLoginHistoryService;

	/**
	 * 共通メール
	 */
	@Autowired
	private CommonMailUtils commonMailUtils;

	/**
	 * 受講者の取得
	 *
	 * @param userName userName
	 * @return 受講者
	 */
	@Override
	public StudentEntity getStudent(final String userName) {
		QueryWrapper<StudentEntity> wrapper = new QueryWrapper<>();
		wrapper.eq("USER_NAME", userName);
		wrapper.eq("DEL_FLAG", "0");
		return studentService.getOne(wrapper);
	}

	/**
	 * 受講者の取得
	 *
	 * @param studentId 受講者ID
	 * @return 受講者
	 */
	public StudentEntity getStudent(final Long studentId) {
		return studentService.getById(studentId);
	}

	/**
	 * 受講者の認証
	 *
	 * @param student       受講者情報
	 * @param inputPassword 入力パスワード
	 * @return エラーメッセージ
	 */
	public String authenticateStudent(final StudentEntity student, final String inputPassword) {
		if (student == null
				|| !student.getUserPassword().equals(new Sha256Hash(inputPassword, student.getUserSalt()).toHex())) {
			return "ログインIDまたはパスワードが正しくありません。";
		}
		return null;
	}

	/**
	 * 受講者トークンの登録・更新
	 *
	 * @param studentId   受講者ID
	 * @param tokenExpire 期限切れ
	 * @return 受講者トークン
	 */
	@Override
	public String insertOrUpdateStudentToken(final Long studentId, final int tokenExpire) {

		// システム時間
		Date now = new Date();
		// 過期時間を取得
		Date expireTime = new Date(now.getTime() + tokenExpire * 1000);
		// トークン生成
		String token = TokenGenerator.generateValue();

		// 受講者トークン情報を取得
		StudentTokenEntity studentToken = studentTokenService.getById(studentId);

		// 受講者トークン情報が取得できない場合、登録
		if (studentToken == null) {
			studentToken = new StudentTokenEntity();
			studentToken.setStudentId(studentId);
			studentToken.setToken(token);
			studentToken.setUpdateTime(now);
			studentToken.setExpireTime(expireTime);
			studentTokenService.save(studentToken);
		} else {
			// 受講者トークン情報が取得できる場合、更新
			studentToken.setToken(token);
			studentToken.setUpdateTime(now);
			studentToken.setExpireTime(expireTime);
			studentTokenService.updateById(studentToken);
		}

		// 生成したトークンを返却
		return token;
	}

	/**
	 * 初回登録の判定
	 *
	 * @param inputPassword 入力パスワード
	 * @param userSalt      ユーザー塩
	 * @return パスワードフラグ (0:初回の登録 1:回以外の登録)
	 */
	public String checkFirstLogin(final String inputPassword, final String userSalt) {
		String initPassword = "student";
		if (inputPassword.equals(new Sha256Hash(initPassword, userSalt).toHex())) {
			return "0";
		}
		return "1";
	}

	/**
	 * 受講者トークンの更新
	 *
	 * @param studentId 受講者ID
	 */
	@Override
	public void updateStudentToken(final Long studentId) {
		StudentTokenEntity studentToken = new StudentTokenEntity();
		studentToken.setStudentId(studentId);
		studentToken.setToken(TokenGenerator.generateValue());
		studentTokenService.updateById(studentToken);
	}

	/**
	 * 受講者ユーザーログイン・ログアウト履歴登録
	 *
	 * @param studentId     受講者ID
	 * @param loginOutKubun アクション区分（'0':ログイン '1':ログアウト)
	 */
	public void insertStudentLoginOutHistory(final Long studentId, final String loginOutKubun) {
		StudentLoginHistoryEntity studentLoginHistory = new StudentLoginHistoryEntity();
		studentLoginHistory.setStudentId(studentId);
		studentLoginHistory.setLoginOutKubun(loginOutKubun);
		studentLoginHistory.setLoginOutHistoryTime(new Date());
		studentLoginHistoryService.save(studentLoginHistory);
	}

	/**
	 * パスワードの変更
	 *
	 * @param userId      ユーザーId
	 * @param password    旧パスワード
	 * @param newPassword 新パスワード
	 */
	@Override
	public void updatePassword(final Long userId, final String password, final String newPassword) {

		// 受講者情報を取得
		StudentEntity student = studentService.getById(userId);

		// パスワードのsha256暗号化
		String passwordEncrypted = new Sha256Hash(password, student.getUserSalt()).toHex();
		String newPasswordEncrypted = new Sha256Hash(newPassword, student.getUserSalt()).toHex();

		if(passwordEncrypted.equals(newPasswordEncrypted)) {
			throw new RRException("新しいパスワードと現在のパスワードが一致する。");
		}
		// パスワード変更を実行
		StudentEntity studentUpdate = new StudentEntity();
		studentUpdate.setUserPassword(newPasswordEncrypted);
		if (!studentService.update(studentUpdate,
				new QueryWrapper<StudentEntity>().eq("student_Id", userId).eq("user_password", passwordEncrypted))) {
			throw new RRException("旧パスワードが違います。");
		}
	}

	/**
	 * 受講者トークンの取得
	 *
	 * @param token トークン
	 */
	@Override
	public StudentTokenEntity getStudentTokenByToken(final String token) {
		QueryWrapper<StudentTokenEntity> wrapper = new QueryWrapper<StudentTokenEntity>();
		wrapper.eq("token", token);
		return studentTokenService.getOne(wrapper);
	}

	/**
	 * メール送信
	 *
	 * @param email メール
	 * @param token トークン
	 */
	public void sendEmail(final String userName,final String email, final String token) {

		// HTMLテンプレートファイルを読む
		String buffer = null;
		try {
			buffer = commonMailUtils.readHtml("StudentPasswordRemindMail.html");
		} catch (Exception e) {
			throw new RRException("htmlテンプレートファイルの読み取りに失敗しました！");
		}
		String url = "/setpassword?reset_password_token=".concat(token);
		String htmlText = MessageFormat.format(buffer, userName, url);

		// メール送信
		if (!commonMailUtils.sendHtmlMail(email, "studypackパスワードリマインダー", htmlText)) {
			throw new RRException("パスワードお問い合わせの送信が失敗しました！正しいメールアドレスを入力してください。");
		}
	}

	/**
	 * 受講者の更新
	 *
	 * @param student 受講者
	 * @return 更新結果
	 */
	public boolean updateStudent(final StudentEntity student) {
		return studentService.updateById(student);
	}


	/**
	 * お問い合わせメール送信
	 *
	 * @param email メール
	 * @param userName ユーザ名
	 * @param studentInquiryRequestResource トークン
	 */
	public void sendInquiryEmail(final String email, final String userName, final StudentInquiryRequestResource studentInquiryRequestResource) {
		String toEmailAddress = "support@studypack.jp";
		//String toEmailAddress = "zhenjinghui@bitsoft-inc.co.jp";
		// HTMLテンプレートファイルを読む
		String buffer = null;
		try {
			buffer = commonMailUtils.readHtml("StudentInquiryMail.html");
		} catch (Exception e) {
			throw new RRException("htmlテンプレートファイルの読み取りに失敗しました！");
		}
		String qaType = studentInquiryRequestResource.getType();
		String qaTypeDesc = "";
		if ("0".equals(qaType)) {
			qaTypeDesc = "講座について";
		} else {
			qaTypeDesc = "サービスなど";
		}

		String htmlText = MessageFormat.format(buffer,
				email,
				qaTypeDesc,
				studentInquiryRequestResource.getTitle(),
				studentInquiryRequestResource.getContent(),
				userName);

		// メール送信
		if (!commonMailUtils.sendHtmlMail(toEmailAddress, "お問い合わせ", htmlText)) {
			throw new RRException("お問い合わせの送信が失敗しました！正しいメールアドレスを入力してください。");
		}
		// メール送信
		//if (!commonMailUtils.sendQAMail(fromEmailAddress, toEmailAddress, "お問い合わせ", htmlText)) {
		//	throw new RRException("お問い合わせの送信が失敗しました！正しいメールアドレスを入力してください。");
		//}
	}
}
