package co.jp.bitsoft.elearning.user.service.modules.user.service;

import java.util.List;

import co.jp.bitsoft.elearning.user.dto.course.StudentTestTestQuestionsChoicesDto;

public interface TestQuestionsChoicesDbService {

    List<StudentTestTestQuestionsChoicesDto> studentTestTestQuestionsChoicesList(Long studentTestId);
}
