package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.StudentCourseManageEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTestEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTestQuestionAnswerEntity;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.service.StudentCourseManageService;
import co.jp.bitsoft.elearning.core.service.StudentTestQuestionAnswerService;
import co.jp.bitsoft.elearning.core.service.StudentTestService;
import co.jp.bitsoft.elearning.core.service.UnitTestService;
import co.jp.bitsoft.elearning.core.service.dao.StudentTestDao;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.AnswerQuestionRequestResource;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentTestDbService;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentTestQuestionAnswerDbService;
import lombok.var;

@Service("studentTestDbService")
public class StudentTestDbServiceImpl extends ServiceImpl<StudentTestDao, StudentTestEntity>
        implements StudentTestDbService {

    private StudentTestQuestionAnswerService studentTestQuestionAnswerService;

    private StudentTestService studentTestService;

    private UnitTestService unitTestService;

    private StudentTestQuestionAnswerDbService studentTestQuestionAnswerDbService;

    private StudentCourseManageService studentCourseManageService;

    @Autowired
    public void setStudentTestQuestionAnswerService(StudentTestQuestionAnswerService studentTestQuestionAnswerService) {
        this.studentTestQuestionAnswerService = studentTestQuestionAnswerService;
    }

    @Autowired
    public void setStudentTestService(StudentTestService studentTestService) {
        this.studentTestService = studentTestService;
    }

    @Autowired
    public void setUnitTestService(UnitTestService unitTestService) {
        this.unitTestService = unitTestService;
    }

    @Autowired
    public void setStudentTestQuestionAnswerDbService(StudentTestQuestionAnswerDbService studentTestQuestionAnswerDbService) {
        this.studentTestQuestionAnswerDbService = studentTestQuestionAnswerDbService;
    }

    @Autowired
    public void setStudentCourseManageService(StudentCourseManageService studentCourseManageService) {
        this.studentCourseManageService = studentCourseManageService;
    }

    @Override
    @Transactional
    public void finishTest(Long studentId,
                           AnswerQuestionRequestResource answerQuestionRequestResource) {
        // 最後のテスト問題を提出する
        studentTestQuestionAnswerDbService.answerQuestion(studentId, answerQuestionRequestResource);

        var unitTestWrapper = new QueryWrapper<UnitTestEntity>().eq("course_id",
                                                                    answerQuestionRequestResource.getCourseId())
                                                                .eq("subject_id",
                                                                    answerQuestionRequestResource.getSubjectId())
                                                                .eq("unit_id",
                                                                    answerQuestionRequestResource.getUnitId())
                                                                .eq("del_flag", '0')
                                                                .eq("test_status", '0')
                                                                .select("pass_flag",
                                                                        "test_questions_num",
                                                                        "pass_questions_num");

        UnitTestEntity unitTestEntity = unitTestService.getOne(unitTestWrapper);

        // 出題数
        Long testQuestionsNum = unitTestEntity.getTestQuestionsNum();

        // 合格題数
        Long passQuestionsNum = unitTestEntity.getPassQuestionsNum();

        var studentTestId = answerQuestionRequestResource.getStudentTestId();

        var studentTestQuestionAnswerList = studentTestQuestionAnswerService.listMaps(new QueryWrapper<StudentTestQuestionAnswerEntity>().eq("student_test_id",
                                                                                                                                             studentTestId));

        var answerStatusCount = studentTestQuestionAnswerList.stream()
                                                             .filter(entity -> "1".equals(entity.get("answer_status")
                                                                                                .toString()))
                                                             .count(); // '1':解答済

        var answerCorrectRateCount = studentTestQuestionAnswerList.stream()
                                                                  .filter(entity -> !"1".equals(entity.get("answer_result")
                                                                                                     .toString()))
                                                                  .count(); // '0':合格

        // テスト解答率（%）
        var answerRate = new BigDecimal(answerStatusCount).divide(new BigDecimal(testQuestionsNum),
                                                                  2,
                                                                  BigDecimal.ROUND_DOWN)
                                                          .multiply(new BigDecimal(100))
                                                          .intValue();

        // テスト正解率（%）
        var answerCorrectRate = new BigDecimal(answerCorrectRateCount).divide(new BigDecimal(testQuestionsNum),
                                                                              2,
                                                                              BigDecimal.ROUND_DOWN)
                                                                      .multiply(new BigDecimal(100))
                                                                      .intValue();

        var answerEndTime = new Timestamp(new Date().getTime());

        var studentTestEntity = studentTestService.getById(studentTestId);

        var answerTime = Minutes.minutesBetween(new DateTime(studentTestEntity.getAnswerStartTime()),
                                                new DateTime(answerEndTime))
                                .getMinutes();

        // No.47 テスト結果の表示追加と表記の条件分岐させる　問題対応
        // 0:合格
        // 1:不合格
        // 2:単元テスト終了
        // var answerResult = answerCorrectRateCount >= passQuestionsNum ? "0" : "1";

        var answerResult = "0";

        if (passQuestionsNum <= 0) {
            answerResult = "2";
        } else {
            answerResult = answerCorrectRateCount >= passQuestionsNum ? "0" : "1";
        }

        var studentTestWrapper = new UpdateWrapper<StudentTestEntity>()
                                                                       .eq("student_test_id",
                                                                           studentTestId)
                                                                       .set("answer_status", "1")
                                                                       .set("answer_num",
                                                                            Math.toIntExact(answerStatusCount))
                                                                       .set("answer_rate",
                                                                            answerRate)
                                                                       .set("answer_correct_rate",
                                                                            answerCorrectRate)
                                                                       .set("answer_result",
                                                                            answerResult)
                                                                       .set("answer_end_time",
                                                                            answerEndTime)
                                                                       .set("answer_time",
                                                                            answerTime);

        studentTestService.update(studentTestWrapper);

        // テストが合格する場合、受講者コース管理の情報を更新するするかどうかを判断処理を行う
        if (StringUtils.equals("0", answerResult) || StringUtils.equals("2", answerResult)) {
            int unitTestCount = unitTestService.count(new QueryWrapper<UnitTestEntity>().eq("course_id",
                                                                                            answerQuestionRequestResource.getCourseId()));

            int studentTestCount = studentTestService.count(new QueryWrapper<StudentTestEntity>().eq("student_id",
                                                                                                     studentId)
                                                                                                 .eq("course_id",
                                                                                                     answerQuestionRequestResource.getCourseId())
                                                                                                 .ne("answer_result",
                                                                                                     "1")
                                                                                                 .select("distinct unit_test_id"));
            // 該当講座の単元テストの数と該当受講者の合格されたテストの数が同じである場合、
            // 「受講者コース管理」テーブルのカラム「受講状態」と「受講受験終了日時」を更新する
            if (unitTestCount == studentTestCount) {
                studentCourseManageService.update(new UpdateWrapper<StudentCourseManageEntity>().set("study_status",
                                                                                                     "3") // 3:受講終了(全受験通過)
                                                                                                .set("study_test_end_time",
                                                                                                     answerEndTime)
                                                                                                .eq("student_id",
                                                                                                    studentId)
                                                                                                .eq("course_id",
                                                                                                    answerQuestionRequestResource.getCourseId()));
            }
        }
    }
}
