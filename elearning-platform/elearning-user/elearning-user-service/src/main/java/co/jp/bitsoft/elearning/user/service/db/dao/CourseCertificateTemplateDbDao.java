package co.jp.bitsoft.elearning.user.service.db.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.CourseCertificateTemplateEntity;
import co.jp.bitsoft.elearning.user.dto.course.CourseCertificateDto;

@Mapper
public interface CourseCertificateTemplateDbDao extends BaseMapper<CourseCertificateTemplateEntity> {
    CourseCertificateDto courseCertificateByCourseId(@Param("studentId") Long studentId, @Param("courseId") Long courseId);

    CourseCertificateDto groupCertificateByGroupId(@Param("studentId") Long studentId, @Param("groupId") Long groupId, @Param("courseId") Long courseId);

}
