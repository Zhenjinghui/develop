package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.CourseCertificateTemplateEntity;
import co.jp.bitsoft.elearning.user.dto.course.CourseCertificateDto;
import co.jp.bitsoft.elearning.user.service.db.dao.CourseCertificateTemplateDbDao;
import co.jp.bitsoft.elearning.user.service.modules.user.service.CourseCertificateTemplateDbService;

@Service("courseCertificateTemplateDbService")
public class CourseCertificateTemplateDbServiceImpl extends
        ServiceImpl<CourseCertificateTemplateDbDao, CourseCertificateTemplateEntity> implements CourseCertificateTemplateDbService {

    @Override
    public CourseCertificateDto courseCertificateByCourseId(Long studentId, Long courseId) {
        return this.getBaseMapper().courseCertificateByCourseId(studentId, courseId);
    }

    @Override
    public CourseCertificateDto groupCertificateByGroupId(Long studentId, Long groupId, Long courseId) {
        return this.getBaseMapper().groupCertificateByGroupId(studentId, groupId, courseId);
    }
}
