package co.jp.bitsoft.elearning.user.service.db.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;
import co.jp.bitsoft.elearning.user.dto.TestQuestionsDto;

@Mapper
public interface TestQuestionsDbDao extends BaseMapper<TestQuestionsEntity> {
    List<TestQuestionsDto> testQuestionsList(@Param("courseId") Long courseId,
                                             @Param("subjectId") Long subjectId,
                                             @Param("unitId") Long unitId,
                                             @Param("unitTestId") Long unitTestId);
}
