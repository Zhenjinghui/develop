package co.jp.bitsoft.elearning.user.service.modules.user.service;

import co.jp.bitsoft.elearning.user.domain.model.courseinfo.AnswerQuestionRequestResource;

public interface StudentTestDbService {
    void finishTest(Long studentId, AnswerQuestionRequestResource answerQuestionRequestResource);
}
