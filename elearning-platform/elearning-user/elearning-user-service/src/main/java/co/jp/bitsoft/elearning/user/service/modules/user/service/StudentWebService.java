/**
 *
 */
package co.jp.bitsoft.elearning.user.service.modules.user.service;

import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTokenEntity;
import co.jp.bitsoft.elearning.user.domain.model.student.StudentInquiryRequestResource;

/**
 * 受講者Webサービス
 *
 * @author BitSoft-inc
 * @email contract@bitsoft-inc.co.jp
 * @date 2021-01-11 10:00:00
 */
public interface StudentWebService {

	/**
	 * 受講者の取得
	 *
	 * @param userName userName
	 * @return 受講者
	 */
	public StudentEntity getStudent(final String userName);

	/**
	 * 受講者の取得
	 *
	 * @param studentId 受講者ID
	 * @return 受講者
	 */
	public StudentEntity getStudent(final Long studentId);

	/**
	 * 受講者の認証
	 *
	 * @param student       受講者情報
	 * @param inputPassword 入力パスワード
	 * @return エラーメッセージ
	 */
	public String authenticateStudent(final StudentEntity student, final String inputPassword);

	/**
	 * 受講者トークンの登録・更新
	 *
	 * @param studentId   受講者ID
	 * @param tokenExpire 期限切れ
	 * @return 受講者トークン
	 */
	public String insertOrUpdateStudentToken(final Long studentId, final int tokenExpire);

	/**
	 * 初回登録の判定
	 *
	 * @param inputPassword 入力パスワード
	 * @param userSalt      ユーザー塩
	 * @return パスワードフラグ
	 */
	public String checkFirstLogin(final String inputPassword, final String userSalt);

	/**
	 * 受講者トークンの更新
	 *
	 * @param studentId 受講者ID
	 */
	void updateStudentToken(final Long studentId);

	/**
	 * ログイン履歴の登録・更新
	 *
	 * @param studentId     受講者ID
	 * @param loginOutKubun アクション区分
	 */
	public void insertStudentLoginOutHistory(final Long studentId, final String loginOutKubun);

	/**
	 * パスワードの変更
	 *
	 * @param userId      ユーザーId
	 * @param password    旧パスワード
	 * @param newPassword 新パスワード
	 */
	public void updatePassword(final Long userId, final String password, final String newPassword);

	/**
	 * 受講者トークンの取得
	 *
	 * @param token トークン
	 * @return 受講者トークン
	 */
	public StudentTokenEntity getStudentTokenByToken(final String token);

	/**
	 * メール送信
	 *
	 * @param email メール
	 * @param token トークン
	 */
	public void sendEmail(final String userName, final String email, final String token);

	/**
	 * 受講者の更新
	 *
	 * @param student 受講者
	 * @return 更新結果
	 */
	public boolean updateStudent(final StudentEntity student);

	/**
	 * お問い合わせメール送信
	 *
	 * @param email メール
	 * @param userName ユーザ名
	 * @param studentInquiryRequestResource お問い合わせ送信の情報
	 */
	public void sendInquiryEmail(final String email, final String userName, final StudentInquiryRequestResource studentInquiryRequestResource);
}
