package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.ImmutableMap;

import co.jp.bitsoft.elearning.core.entity.StudentCourseManageEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTestEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.core.entity.TestQuestionsEntity;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.service.StudentCourseManageService;
import co.jp.bitsoft.elearning.core.service.StudentTestService;
import co.jp.bitsoft.elearning.core.service.TestQuestionsChoicesService;
import co.jp.bitsoft.elearning.core.service.UnitTestService;
import co.jp.bitsoft.elearning.user.domain.model.course.TestQuestionResponceResource;
import co.jp.bitsoft.elearning.user.dto.TestQuestionsDto;
import co.jp.bitsoft.elearning.user.dto.course.StudentSubjectDto;
import co.jp.bitsoft.elearning.user.service.db.StudentCourseDbService;
import co.jp.bitsoft.elearning.user.service.db.dao.TestQuestionsDbDao;
import co.jp.bitsoft.elearning.user.service.modules.user.service.TestQuestionsDbService;
import lombok.var;

@Service("testQuestionsDbService")
public class TestQuestionsDbServiceImpl extends ServiceImpl<TestQuestionsDbDao, TestQuestionsEntity>
        implements TestQuestionsDbService {

    @Autowired
    private UnitTestService unitTestService;

    @Autowired
    private StudentCourseManageService studentCourseManageService;

    @Autowired
    private StudentTestService studentTestService;

    @Autowired
    private StudentCourseDbService studentCourseDbService;

    @Autowired
    private TestQuestionsChoicesService testQuestionsChoicesService;

    @Override
    @Transactional
    public Map<String, Object> testQuestionsList(Long groupId,
                                                 Long studentId,
                                                 Long courseId,
                                                 Long subjectId,
                                                 Long unitId) {
        // 単元テストを取得する
        UnitTestEntity unitTestEntity = getUnitTestEntity(courseId, subjectId, unitId);

        Long unitTestId = unitTestEntity.getUnitTestId();
        Long testQuestionsNum = unitTestEntity.getTestQuestionsNum();

        var studentCourseManagerWrapper = new QueryWrapper<StudentCourseManageEntity>().eq("student_id",
                                                                                           studentId)
                                                                                       .eq("course_id",
                                                                                           courseId);

        var studentCourseManageEntity = studentCourseManageService.getOne(studentCourseManagerWrapper);

        Timestamp now = new Timestamp(new Date().getTime());
        var studentCourseId = studentCourseManageEntity.getStudentCourseId();

        var studentTestEntity = new StudentTestEntity() {
            {
                setStudentCourseId(studentCourseId);
                setStudentId(studentId);
                setCourseId(courseId);
                setGroupId(groupId);
                setUnitTestId(unitTestId);
                // テストステータス(2:テスト中断)
                setAnswerStatus("2");
                // 解答開始日時
                setAnswerStartTime(now);
                // 削除フラグ
                setDelFlag("0");
                // テスト点数(reset value)
                setAnswerPoint(0);
                // テスト回答問題数
                setAnswerNum(0);
                // テスト解答率
                setAnswerRate(0);
                // テスト正解率
                setAnswerCorrectRate(0);
                // テスト判定結果
                setAnswerResult("1");
                // 解答所要時間（分）
                setAnswerTime(0);
            }
        };

        // 受講者テスト
        studentTestService.save(studentTestEntity);

        // すべてのテスト問題を取得する（シャフル済み）
        var testQuestionsDtoList = this.getBaseMapper()
                                       .testQuestionsList(courseId, subjectId, unitId, unitTestId);

        // 固定のテスト問題を取得する
        var testQuestionsList = testQuestionsDtoList.stream()
                                                    .filter(entity -> "0".equals(entity.getFixFlag()))
                                                    .limit(testQuestionsNum)
                                                    .collect(Collectors.toList());

        // テスト問題数を判断し、足らない場合非固定のテスト問題を取得する
        if (testQuestionsNum > testQuestionsList.size()) {
            testQuestionsList.addAll(testQuestionsDtoList.stream()
                                                         .filter(entity -> "1".equals(entity.getFixFlag()))
                                                         .limit(testQuestionsNum
                                                                - testQuestionsList.size())
                                                         .collect(Collectors.toList()));
        }

        // すべてのテスト問題選択肢を取得する
        var testQuestionsIdList = testQuestionsList.stream()
                                                   .map(TestQuestionsDto::getTestQuestionsId)
                                                   .collect(Collectors.toList());

        var choicesEntityList = testQuestionsChoicesService.list(new QueryWrapper<TestQuestionsChoicesEntity>().eq("course_id",
                                                                                                                   courseId)
                                                                                                               .eq("subject_id",
                                                                                                                   subjectId)
                                                                                                               .eq("unit_id",
                                                                                                                   unitId)
                                                                                                               .eq("unit_test_id",
                                                                                                                   unitTestId)
                                                                                                               .in("test_questions_id",
                                                                                                                   testQuestionsIdList)
                                                                                                               .select("test_questions_choices_id",
                                                                                                                       "test_questions_id",
                                                                                                                       "choices_no",
                                                                                                                       "choices_contents").orderByAsc("test_questions_choices_id"));

        // resourceのオブジェクトに変更する
        var resourceList = TestQuestionResponceResource.builder(studentTestEntity.getStudentTestId(),
                                                                testQuestionsList,
                                                                choicesEntityList);

        // テスト問題リストをシャッフルする
        Collections.shuffle(resourceList, new Random(System.currentTimeMillis()));

        // 単元テストの名など情報を取得する
        StudentSubjectDto userSubjectDto = studentCourseDbService.querySubjectInfo(groupId,
                                                                                   courseId,
                                                                                   studentId,
                                                                                   subjectId)
                                                                 .orElse(new StudentSubjectDto());

        return new ImmutableMap.Builder<String, Object>().put("count", resourceList.size())
                                                         .put("testQuestionResponceResource",
                                                              resourceList)
                                                         .put("courseName",
                                                              userSubjectDto.getCourseTitle())
                                                         .put("subjectName",
                                                              userSubjectDto.getSubjectTitle())
                                                         .put("unitName",
                                                              unitTestEntity.getTestTitle())
                                                         .put("testConditionFlag",
                                                              unitTestEntity.getTestConditionFlag())
                                                         .put("testConditionTime",
                                                              unitTestEntity.getTestConditionTime())
                                                         .build();
    }

    @Override
    public int countTestQuestions(Long courseId, Long subjectId, Long unitId) {
        // 単元テストを取得する
        UnitTestEntity unitTestEntity = getUnitTestEntity(courseId, subjectId, unitId);

        return this.getBaseMapper()
                   .selectCount(new QueryWrapper<TestQuestionsEntity>().eq("course_id", courseId)
                                                                       .eq("subject_id", subjectId)
                                                                       .eq("unit_id", unitId)
                                                                       .eq("unit_test_id",
                                                                               unitTestEntity==null?null:unitTestEntity.getUnitTestId())
                                                                       .eq("del_flag", '0'));
    }

    private UnitTestEntity getUnitTestEntity(Long courseId, Long subjectId, Long unitId) {
        var unitTestWrapper = new QueryWrapper<UnitTestEntity>().eq("course_id", courseId)
                                                                .eq("subject_id", subjectId)
                                                                .eq("unit_id", unitId)
                                                                .eq("del_flag", '0')
                                                                .eq("test_status", '0')
                                                                .select("unit_test_id",
                                                                        "test_title",
                                                                        "test_condition_flag",
                                                                        "test_condition_time",
                                                                        "test_questions_num");

        return unitTestService.getOne(unitTestWrapper);
    }
}
