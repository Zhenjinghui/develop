package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import co.jp.bitsoft.elearning.core.entity.CourseCertificateTemplateEntity;
import co.jp.bitsoft.elearning.core.entity.StudentCourseManageEntity;
import co.jp.bitsoft.elearning.core.entity.StudentCourseStudyEntity;
import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.TeachingMaterialEntity;
import co.jp.bitsoft.elearning.core.entity.UnitTestEntity;
import co.jp.bitsoft.elearning.core.service.CourseCertificateTemplateService;
import co.jp.bitsoft.elearning.core.service.StudentCourseManageService;
import co.jp.bitsoft.elearning.core.service.StudentCourseStudyService;
import co.jp.bitsoft.elearning.core.service.TeachingMaterialService;
import co.jp.bitsoft.elearning.core.service.UnitTestService;
import co.jp.bitsoft.elearning.user.domain.model.course.CourseCertificateResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentCourseRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentCourseResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentStudentTestResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentSubjectResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentTeachingMaterialResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentUnitResponseResource;
import co.jp.bitsoft.elearning.user.dto.course.CourseCertificateDto;
import co.jp.bitsoft.elearning.user.dto.course.StudentCourseDto;
import co.jp.bitsoft.elearning.user.dto.course.StudentStudentTestDto;
import co.jp.bitsoft.elearning.user.dto.course.StudentSubjectDto;
import co.jp.bitsoft.elearning.user.dto.course.StudentTeachingMaterialDto;
import co.jp.bitsoft.elearning.user.dto.course.StudentUnitDto;
import co.jp.bitsoft.elearning.user.service.db.StudentCourseDbService;
import co.jp.bitsoft.elearning.user.service.modules.user.service.CourseCertificateTemplateDbService;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentCourseService;
import lombok.var;


@Service
@Transactional
public class StudentCourseServiceImpl implements StudentCourseService {

    @Autowired
    private StudentCourseDbService studentCourseDbService;

    @Autowired
    private StudentCourseManageService studentCourseManageService;

    @Autowired
    private StudentCourseStudyService studentCourseStudyService;

    @Autowired
    private CourseCertificateTemplateDbService courseCertificateTemplateDbService;

    @Autowired
    private UnitTestService unitTestService;

    @Autowired
    private TeachingMaterialService teachingMaterialService;

    @Autowired
    private CourseCertificateTemplateService courseCertificateTemplateService;

    @Override
    public List<StudentCourseResponseResource> getCourseList(StudentEntity studentEntity) {

        if (studentEntity == null) {
            //TODO throw error?
            return null;
        }

        List<StudentCourseResponseResource> studentCourseResponseResourceList = new ArrayList<>();
        List<StudentCourseDto> studentCourseDtoList = studentCourseDbService.queryCourseList(studentEntity.getGroupId(), studentEntity.getStudentId());
        studentCourseDtoList.forEach(dto -> {
            StudentCourseResponseResource studentCourseResponseResource = new StudentCourseResponseResource();
            BeanUtils.copyProperties(dto, studentCourseResponseResource);
            studentCourseResponseResourceList.add(studentCourseResponseResource);
        });


        return studentCourseResponseResourceList;
    }

    @Override
    public StudentCourseResponseResource getCourseInfo(StudentEntity studentEntity, Long courseId) {

        if (studentEntity == null) {
            //TODO throw error?
            return null;
        }

        StudentCourseResponseResource studentCourseResponseResource = new StudentCourseResponseResource();
        Optional<StudentCourseDto> userCourseDto = studentCourseDbService.queryCourseInfo(studentEntity.getGroupId(), courseId, studentEntity.getStudentId());

        BeanUtils.copyProperties(userCourseDto.orElse(new StudentCourseDto()), studentCourseResponseResource);

        List<StudentSubjectResponseResource> studentSubjectResponseResourceList = new ArrayList<>();
        List<StudentSubjectDto> studentSubjectDtoList = studentCourseDbService.querySubjectList(courseId);

        studentSubjectDtoList.forEach((dto) -> {
            StudentSubjectResponseResource studentSubjectResponseResource = new StudentSubjectResponseResource();
            BeanUtils.copyProperties(dto, studentSubjectResponseResource);
            List<StudentUnitDto> userUnitDtoList = studentCourseDbService.queryUnitList(courseId, Long.parseLong(dto.getSubjectId()));

            int materialStudyCount = 0;
            studentSubjectResponseResource.setUnitNum(userUnitDtoList.size()+"");
            for(StudentUnitDto studentUnitDto : userUnitDtoList) {
                List<StudentTeachingMaterialDto> studentTeachingMaterialDtoList = studentCourseDbService.queryTeachingMaterialList(Long.parseLong(studentUnitDto.getUnitId()), studentEntity.getStudentId(), studentEntity.getGroupId(), courseId);
                boolean materialStudyStatus = true;
                for(StudentTeachingMaterialDto studentTeachingMaterialDto : studentTeachingMaterialDtoList) {
                	if(!"1".equals(studentTeachingMaterialDto.getScsStudyStatus())) {
                		materialStudyStatus = false;
                		break;
                	}
                }
                if(materialStudyStatus) {
                	materialStudyCount++;
                }
            }
            studentSubjectResponseResource.setUnitStudyNum(materialStudyCount+"");

            studentSubjectResponseResourceList.add(studentSubjectResponseResource);
        });

        Map<String, Object> listMap = new HashMap<>();
        listMap.put("count", studentSubjectResponseResourceList.size());
        listMap.put("studentSubjectResponseResourceList", studentSubjectResponseResourceList);
        studentCourseResponseResource.setSubjectResponseResourceList(listMap);

        CourseCertificateTemplateEntity courseCertificateTemplateEntity = courseCertificateTemplateService.getById(Long.parseLong(studentCourseResponseResource.getCourseCertificateTemplateId()));
        if(StringUtils.isBlank(courseCertificateTemplateEntity.getCertificateTitle())
        		&& StringUtils.isBlank(courseCertificateTemplateEntity.getCertificateContent())
        		&& StringUtils.isBlank(courseCertificateTemplateEntity.getIssuerName())
        		&& StringUtils.isBlank(courseCertificateTemplateEntity.getIssuerNameSub())
        		&& StringUtils.isBlank(courseCertificateTemplateEntity.getIssuerResponsiblePartyTitle())
        		&& StringUtils.isBlank(courseCertificateTemplateEntity.getIssuerResponsiblePartyName())
        		&& StringUtils.isBlank(courseCertificateTemplateEntity.getCertificateTemplatePath())) {
        	studentCourseResponseResource.setCourseCertificateTemplateId(null);
        }

        return studentCourseResponseResource;
    }

    @Override
    public void courseReg(StudentEntity studentEntity, StudentCourseRequestResource studentCourseRequestResource) {

        StudentCourseManageEntity whereEntity = new StudentCourseManageEntity();
        whereEntity.setCourseId(studentCourseRequestResource.getCourseId());
        whereEntity.setStudentId(studentEntity.getStudentId());

        StudentCourseManageEntity studentCourseManageEntity = new StudentCourseManageEntity();
        studentCourseManageService.update(studentCourseManageEntity, new UpdateWrapper<>(whereEntity).set("Del_Flag", "1"));

        studentCourseManageEntity = new StudentCourseManageEntity();
        studentCourseManageEntity.setCourseId(studentCourseRequestResource.getCourseId());
        studentCourseManageEntity.setStudentId(studentEntity.getStudentId());
        studentCourseManageEntity.setGroupId(studentEntity.getGroupId());
        // No.73問題対応
        // studentCourseManageEntity.setStudyStatus("0");
        //「⑩申請通知（システム自動処理）」の工程を削除し、
        //「⑪承認処理」を管理者ではなくシステム側で自動処理させ、
        //「⑫承認通知メール（システム自動処理）」を実行する
        studentCourseManageEntity.setStudyStatus("1");
        // end of No.73
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = LocalDateTime.now().atZone(zoneId);
        studentCourseManageEntity.setCourseRegTime(Date.from(zdt.toInstant()));
        studentCourseManageEntity.setCourseRegReason(studentCourseRequestResource.getCourseRegReason());

        studentCourseManageService.save(studentCourseManageEntity);
    }

    @Override
    public StudentSubjectResponseResource getSubjectInfo(StudentEntity studentEntity, Long courseId, Long subjectId) {

        if (studentEntity == null) {
            //TODO throw error?
            return null;
        }

        StudentSubjectResponseResource studentSubjectResponseResource = new StudentSubjectResponseResource();
        Optional<StudentSubjectDto> userSubjectDto = studentCourseDbService.querySubjectInfo(studentEntity.getGroupId(), courseId, studentEntity.getStudentId(), subjectId);
        BeanUtils.copyProperties(userSubjectDto.orElse(new StudentSubjectDto()), studentSubjectResponseResource);

        List<StudentUnitDto> userUnitDtoList = studentCourseDbService.queryUnitList(courseId, subjectId);
        List<StudentUnitResponseResource> studentUnitResponseResourceList = new ArrayList<>();
        userUnitDtoList.forEach(dto -> {
            StudentUnitResponseResource studentUnitResponseResource = new StudentUnitResponseResource();
            BeanUtils.copyProperties(dto, studentUnitResponseResource);
            studentUnitResponseResourceList.add(studentUnitResponseResource);
        });
        studentUnitResponseResourceList.forEach(unitResponseDto -> {
           	QueryWrapper<UnitTestEntity> queryWrapper = new QueryWrapper<>();
        	queryWrapper.eq("course_id", courseId);
        	queryWrapper.eq("subject_id", subjectId);
        	queryWrapper.eq("unit_id", Long.parseLong(unitResponseDto.getUnitId()));
        	int testCount = unitTestService.count(queryWrapper);
        	if(testCount > 0) {
        		unitResponseDto.setTestExistFlg("0");
        	}else {
        		unitResponseDto.setTestExistFlg("1");
        	}

            List<StudentStudentTestDto> studentStudentTestDtoList = studentCourseDbService.queryStudentTestList(studentEntity.getGroupId(), courseId, studentEntity.getStudentId(), subjectId, Long.parseLong(unitResponseDto.getUnitId()));

        	unitResponseDto.setTestPassFlg("1");
            for(StudentStudentTestDto studentStudentTestDto : studentStudentTestDtoList) {
                if("0".equals(studentStudentTestDto.getAnswerResult()) || "2".equals(studentStudentTestDto.getAnswerResult())) {
                	unitResponseDto.setTestPassFlg("0");
                    break;
                }
            }

           	QueryWrapper<TeachingMaterialEntity> queryWrapperTeach = new QueryWrapper<>();
           	queryWrapperTeach.eq("course_id", courseId);
           	queryWrapperTeach.eq("subject_id", subjectId);
           	queryWrapperTeach.eq("unit_id", Long.parseLong(unitResponseDto.getUnitId()));
           	int materialCount = teachingMaterialService.count(queryWrapperTeach);
           	unitResponseDto.setMateriaNum(materialCount + "");

            List<StudentTeachingMaterialDto> studentTeachingMaterialDtoList = studentCourseDbService.queryTeachingMaterialList(Long.parseLong(unitResponseDto.getUnitId()), studentEntity.getStudentId(), studentEntity.getGroupId(), courseId);
            int materialStudyCount = 0;
            for(StudentTeachingMaterialDto dto : studentTeachingMaterialDtoList) {
            	if("1".equals(dto.getScsStudyStatus())) {
            		materialStudyCount = materialStudyCount+1;
            	}
            }
            unitResponseDto.setMateriaStudyNum(materialStudyCount+"");
        });

        Map<String, Object> listMap = new HashMap<>();
        listMap.put("count", studentUnitResponseResourceList.size());
        listMap.put("studentUnitResponseResourceList", studentUnitResponseResourceList);
        studentSubjectResponseResource.setStudentUnitResponseResourceList(listMap);

        return studentSubjectResponseResource;
    }

    @Override
    public StudentUnitResponseResource getUnitInfo(StudentEntity studentEntity, Long courseId, Long subjectId, Long unitId) {

        StudentUnitResponseResource studentUnitResponseResource = new StudentUnitResponseResource();
        Optional<StudentUnitDto> userUnitDto = studentCourseDbService.queryUnitInfo(studentEntity.getGroupId(), courseId, studentEntity.getStudentId(), subjectId, unitId);
        BeanUtils.copyProperties(userUnitDto.orElse(new StudentUnitDto()), studentUnitResponseResource);

        List<StudentStudentTestResponseResource> studentStudentTestResponseResourceList = new ArrayList<>();
        List<StudentStudentTestDto> studentStudentTestDtoList = studentCourseDbService.queryStudentTestList(studentEntity.getGroupId(), courseId, studentEntity.getStudentId(), subjectId, unitId);
        studentStudentTestDtoList.forEach(dto -> {
            StudentStudentTestResponseResource studentStudentTestResponseResource = new StudentStudentTestResponseResource();
            BeanUtils.copyProperties(dto, studentStudentTestResponseResource);
            if(!"2".equals(studentStudentTestResponseResource.getAnswerStatus())) {
                studentStudentTestResponseResourceList.add(studentStudentTestResponseResource);
            }
        });

        Map<String, Object> listMap = new HashMap<>();
        listMap.put("count", studentStudentTestResponseResourceList.size());
        listMap.put("studentStudentTestResponseResourceList", studentStudentTestResponseResourceList);
        studentUnitResponseResource.setStudentStudentTestResponseResourceList(listMap);


        List<StudentTeachingMaterialResponseResource> studentTeachingMaterialResponseResourceList = new ArrayList<>();
        List<StudentTeachingMaterialDto> studentTeachingMaterialDtoList = studentCourseDbService.queryTeachingMaterialList(unitId, studentEntity.getStudentId(), studentEntity.getGroupId(), courseId);
        studentTeachingMaterialDtoList.forEach(dto -> {
            StudentTeachingMaterialResponseResource studentTeachingMaterialResponseResource = new StudentTeachingMaterialResponseResource();
            BeanUtils.copyProperties(dto, studentTeachingMaterialResponseResource);
            studentTeachingMaterialResponseResourceList.add(studentTeachingMaterialResponseResource);
        });

        Map<String, Object> list2Map = new HashMap<>();
        list2Map.put("count", studentTeachingMaterialResponseResourceList.size());
        list2Map.put("studentTeachingMaterialResponseResourceList", studentTeachingMaterialResponseResourceList);
        studentUnitResponseResource.setStudentTeachingMaterialResponseResourceList(list2Map);

        return studentUnitResponseResource;
    }

    @Override
    public StudentTeachingMaterialResponseResource getTeachingMaterialInfo(StudentEntity studentEntity, Long courseId, Long subjectId, Long unitId, Long materialId) {

        StudentTeachingMaterialResponseResource studentTeachingMaterialResponseResource = new StudentTeachingMaterialResponseResource();
        Optional<StudentTeachingMaterialDto> userTeachingMaterialDto = studentCourseDbService.queryTeachingMaterialInfo(studentEntity.getGroupId(), courseId, studentEntity.getStudentId(), subjectId, unitId, materialId);
        BeanUtils.copyProperties(userTeachingMaterialDto.orElse(new StudentTeachingMaterialDto()), studentTeachingMaterialResponseResource);

        if (studentTeachingMaterialResponseResource.getTeachingMaterialId() != null) {


            StudentCourseManageEntity whereEntity = new StudentCourseManageEntity();
            whereEntity.setCourseId(courseId);
            whereEntity.setStudentId(studentEntity.getStudentId());
            whereEntity.setGroupId(studentEntity.getGroupId());

            StudentCourseManageEntity studentCourseManageEntity = studentCourseManageService.getOne(new QueryWrapper<>(whereEntity));
            if (StringUtils.equals(studentCourseManageEntity.getStudyStatus(), "1")) {
                studentCourseManageEntity.setStudyStatus("2");
                ZoneId zoneId = ZoneId.systemDefault();
                ZonedDateTime zdt = LocalDateTime.now().atZone(zoneId);
                studentCourseManageEntity.setStudyTestStartTime(Date.from(zdt.toInstant()));
                studentCourseManageService.updateById(studentCourseManageEntity);
            }

            StudentCourseStudyEntity studentCourseStudyEntity = new StudentCourseStudyEntity();
            studentCourseStudyEntity.setStudentCourseId(studentCourseManageEntity.getStudentCourseId());
            studentCourseStudyEntity.setCourseId(courseId);
            studentCourseStudyEntity.setSubjectId(subjectId);
            studentCourseStudyEntity.setUnitId(unitId);
            studentCourseStudyEntity.setTeachingMaterialId(materialId);
            studentCourseStudyEntity.setDelFlag("0");
            StudentCourseStudyEntity scs = studentCourseStudyService.getOne(new QueryWrapper<>(studentCourseStudyEntity));

            if (scs == null) {
                studentCourseStudyEntity = new StudentCourseStudyEntity();
                studentCourseStudyEntity.setStudentCourseId(studentCourseManageEntity.getStudentCourseId());
                studentCourseStudyEntity.setCourseId(courseId);
                studentCourseStudyEntity.setSubjectId(subjectId);
                studentCourseStudyEntity.setUnitId(unitId);
                studentCourseStudyEntity.setTeachingMaterialId(materialId);
                studentCourseStudyEntity.setDelFlag("0");
                studentCourseStudyEntity.setGroupId(studentEntity.getGroupId());
                ZoneId zoneId = ZoneId.systemDefault();
                ZonedDateTime zdt = LocalDateTime.now().atZone(zoneId);
                studentCourseStudyEntity.setStudyStartTime(Date.from(zdt.toInstant()));
                studentCourseStudyService.save(studentCourseStudyEntity);
            } else {
                studentTeachingMaterialResponseResource.setScsStudyStatus(scs.getStudyStatus());
            }
        }

        return studentTeachingMaterialResponseResource;
    }

    @Override
    public String getEndTimeUpdate(StudentEntity studentEntity, Long courseId, Long subjectId, Long unitId, Long materialId) {

        StudentTeachingMaterialResponseResource studentTeachingMaterialResponseResource = new StudentTeachingMaterialResponseResource();
        Optional<StudentTeachingMaterialDto> userTeachingMaterialDto = studentCourseDbService.queryTeachingMaterialInfo(studentEntity.getGroupId(), courseId, studentEntity.getStudentId(), subjectId, unitId, materialId);
        BeanUtils.copyProperties(userTeachingMaterialDto.orElse(new StudentTeachingMaterialDto()), studentTeachingMaterialResponseResource);

        if (studentTeachingMaterialResponseResource.getTeachingMaterialId() != null) {

            StudentCourseManageEntity whereEntity = new StudentCourseManageEntity();
            whereEntity.setCourseId(courseId);
            whereEntity.setStudentId(studentEntity.getStudentId());
            whereEntity.setGroupId(studentEntity.getGroupId());

            StudentCourseManageEntity studentCourseManageEntity = studentCourseManageService.getOne(new QueryWrapper<>(whereEntity));
            if (StringUtils.equals(studentCourseManageEntity.getStudyStatus(), "1")) {
                studentCourseManageEntity.setStudyStatus("2");
                ZoneId zoneId = ZoneId.systemDefault();
                ZonedDateTime zdt = LocalDateTime.now().atZone(zoneId);
                studentCourseManageEntity.setStudyTestStartTime(Date.from(zdt.toInstant()));
                studentCourseManageService.updateById(studentCourseManageEntity);
            }


            StudentCourseStudyEntity studentCourseStudyEntity = new StudentCourseStudyEntity();
            studentCourseStudyEntity.setStudentCourseId(studentCourseManageEntity.getStudentCourseId());
            studentCourseStudyEntity.setCourseId(courseId);
            studentCourseStudyEntity.setSubjectId(subjectId);
            studentCourseStudyEntity.setUnitId(unitId);
            studentCourseStudyEntity.setTeachingMaterialId(materialId);
            studentCourseStudyEntity.setDelFlag("0");
            StudentCourseStudyEntity scs = studentCourseStudyService.getOne(new QueryWrapper<>(studentCourseStudyEntity));
            if (scs == null) {
                studentCourseStudyEntity = new StudentCourseStudyEntity();
                studentCourseStudyEntity.setStudentCourseId(studentCourseManageEntity.getStudentCourseId());
                studentCourseStudyEntity.setCourseId(courseId);
                studentCourseStudyEntity.setSubjectId(subjectId);
                studentCourseStudyEntity.setUnitId(unitId);
                studentCourseStudyEntity.setTeachingMaterialId(materialId);
                studentCourseStudyEntity.setDelFlag("0");
                studentCourseStudyEntity.setGroupId(studentEntity.getGroupId());
                studentCourseStudyEntity.setStudyStatus("1");
                ZoneId zoneId = ZoneId.systemDefault();
                ZonedDateTime zdt = LocalDateTime.now().atZone(zoneId);
                studentCourseStudyEntity.setStudyStartTime(Date.from(zdt.toInstant()));
                studentCourseStudyEntity.setStudyEndTime(Date.from(zdt.toInstant()));
                studentCourseStudyService.save(studentCourseStudyEntity);
                return "1";
            }else {
                scs.setStudyStatus("1");
                ZoneId zoneId = ZoneId.systemDefault();
                ZonedDateTime zdt = LocalDateTime.now().atZone(zoneId);
                scs.setStudyEndTime(Date.from(zdt.toInstant()));
                studentCourseStudyService.updateById(scs);
                return "1";
            }
        }
        return "0";
    }

    @Override
    public Map<String, Object> queryCourseDataCount(StudentEntity studentEntity, Long courseId) {

        Map<String, Object> queryCourseTeachingMaterialCount = studentCourseDbService.queryCourseTeachingMaterialCount(studentEntity.getGroupId(), courseId, studentEntity.getStudentId());
        List<Map<String, Object>> queryCourseUnitTestCount1 = studentCourseDbService.queryCourseUnitTestCount1(studentEntity.getGroupId(), courseId, studentEntity.getStudentId());
        List<Map<String, Object>> queryCourseUnitTestCount2 = studentCourseDbService.queryCourseUnitTestCount2(studentEntity.getGroupId(), courseId, studentEntity.getStudentId());

        Map<String, Object> queryCourseDataCount = new HashMap<>();
        if (queryCourseTeachingMaterialCount != null) {
            queryCourseDataCount.putAll(queryCourseTeachingMaterialCount);
        }
        if (queryCourseUnitTestCount1 != null) {
            queryCourseDataCount.put("utcount", queryCourseUnitTestCount2.size());
            queryCourseDataCount.put("stcount", queryCourseUnitTestCount1.size());
        }
        return queryCourseDataCount;
    }

    /** 修了⽇のフォーマット */
    private static SimpleDateFormat JAPASESE_FORMAT = new SimpleDateFormat("GGGG y 年 M ⽉ d ⽇",
                                                                           new Locale("ja",
                                                                                      "JP",
                                                                                      "JP"));

    @Override
    public CourseCertificateResponseResource certificate(StudentEntity studentEntity,
                                                         Long groupId, Long courseId) {
    	// 講座修了証
        CourseCertificateDto courseCertificateDto = courseCertificateTemplateDbService.courseCertificateByCourseId(studentEntity.getStudentId(),
        		courseId);
        // グループ修了証
        CourseCertificateDto groupCertificateDto = courseCertificateTemplateDbService.groupCertificateByGroupId(studentEntity.getStudentId(),
        		groupId, courseId);

        var responseResource = new CourseCertificateResponseResource();

        // 終了証テンプレートPATH
        responseResource.setCertificateTemplatePath(courseCertificateDto.getCertificateTemplatePath());

        // 修了証発行機関名
        responseResource.setIssuerName(courseCertificateDto.getIssuerName());
        // 修了証発行機関サブ名
        responseResource.setIssuerNameSub(courseCertificateDto.getIssuerNameSub());
        // 修了証発行機関責任者肩書
        responseResource.setIssuerResponsiblePartyTitle(courseCertificateDto.getIssuerResponsiblePartyTitle());
        // 修了証発行機関責任者名
        responseResource.setIssuerResponsiblePartyName(courseCertificateDto.getIssuerResponsiblePartyName());
        // タイトル
        responseResource.setCertificateTitle(courseCertificateDto.getCertificateTitle());
        // ユーザー名
        responseResource.setUserName(String.format("%s 殿", studentEntity.getUserNameKanji()));
        // 本文
        responseResource.setCertificateContent(courseCertificateDto.getCertificateContent());
        // 修了⽇
        responseResource.setStudyTestEndTime(String.format("修了⽇ %s",
                                                           JAPASESE_FORMAT.format(courseCertificateDto.getStudyTestEndTime())));
        if(groupCertificateDto!=null &&
        		!(StringUtils.isBlank(groupCertificateDto.getCertificateTitle())
        		&& StringUtils.isBlank(groupCertificateDto.getCertificateContent())
        		&& StringUtils.isBlank(groupCertificateDto.getIssuerName())
        		&& StringUtils.isBlank(groupCertificateDto.getIssuerNameSub())
        		&& StringUtils.isBlank(groupCertificateDto.getIssuerResponsiblePartyTitle())
        		&& StringUtils.isBlank(groupCertificateDto.getIssuerResponsiblePartyName())
        		&& StringUtils.isBlank(groupCertificateDto.getCertificateTemplatePath()))) {
            // タイトル
            responseResource.setCertificateTitle(strReplace(responseResource.getCertificateTitle(),groupCertificateDto));
            // 本文
            responseResource.setCertificateContent(strReplace(responseResource.getCertificateContent(),groupCertificateDto));
            // 修了証発行機関名
            responseResource.setIssuerName(strReplace(responseResource.getIssuerName(),groupCertificateDto));
            // 修了証発行機関サブ名
            responseResource.setIssuerNameSub(strReplace(responseResource.getIssuerNameSub(),groupCertificateDto));
            // 修了証発行機関責任者肩書
            responseResource.setIssuerResponsiblePartyTitle(strReplace(responseResource.getIssuerResponsiblePartyTitle(),groupCertificateDto));
            // 修了証発行機関責任者名
            responseResource.setIssuerResponsiblePartyName(strReplace(responseResource.getIssuerResponsiblePartyName(),groupCertificateDto));
        }

        return responseResource;
    }

    private String strReplace(String strBefore,CourseCertificateDto groupCertificateDto) {

    	String strAfter = strBefore;
    	if(StringUtils.isNotBlank(groupCertificateDto.getGroupPrefecture())) {
    		strAfter = strAfter.replace("[[text/pref]]",groupCertificateDto.getGroupPrefecture());
    	}
    	if(StringUtils.isNotBlank(groupCertificateDto.getGroupAddressCity())) {
    		strAfter = strAfter.replace("[[text/city]]",groupCertificateDto.getGroupAddressCity());
    	}
    	if(StringUtils.isNotBlank(groupCertificateDto.getGroupName())) {
    		strAfter = strAfter.replace("[[text/class]]",groupCertificateDto.getGroupName());
    	}
    	if(StringUtils.isNotBlank(groupCertificateDto.getGroupNameOf())) {
    		strAfter = strAfter.replace("[[text/client]]",groupCertificateDto.getGroupNameOf());
    	}
    	if(StringUtils.isNotBlank(groupCertificateDto.getIssuerName())) {
    		strAfter = strAfter.replace("[[text/organization]]",groupCertificateDto.getIssuerName());
    	}
    	if(StringUtils.isNotBlank(groupCertificateDto.getIssuerNameSub())) {
    		strAfter = strAfter.replace("[[text/organization2]]",groupCertificateDto.getIssuerNameSub());
    	}
    	if(StringUtils.isNotBlank(groupCertificateDto.getIssuerResponsiblePartyTitle())) {
    		strAfter = strAfter.replace("[[text/position]]",groupCertificateDto.getIssuerResponsiblePartyTitle());
    	}
    	if(StringUtils.isNotBlank(groupCertificateDto.getIssuerResponsiblePartyName())) {
    		strAfter = strAfter.replace("[[text/representative]]",groupCertificateDto.getIssuerResponsiblePartyName());
    	}

    	return strAfter;
    }
}
