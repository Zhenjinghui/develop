package co.jp.bitsoft.elearning.user.service.db.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.user.dto.course.StudentTestTestQuestionsChoicesDto;

@Mapper
public interface TestQuestionsChoicesDbDao extends BaseMapper<TestQuestionsChoicesEntity> {
    List<StudentTestTestQuestionsChoicesDto> studentTestTestQuestionsChoicesList(@Param("studentTestId") Long studentTestId);
}
