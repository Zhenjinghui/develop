package co.jp.bitsoft.elearning.user.service.modules.user.service;

import co.jp.bitsoft.elearning.user.dto.course.CourseCertificateDto;

public interface CourseCertificateTemplateDbService {
    /**
     * 講座修了証情報を取得する
     *
     */
    CourseCertificateDto courseCertificateByCourseId(Long studentId, Long courseId);

    /**
     * グループ修了証情報を取得する
     *
     */
    CourseCertificateDto groupCertificateByGroupId(Long studentId, Long groupId, Long courseId);
}
