package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.StudentTestQuestionAnswerEntity;
import co.jp.bitsoft.elearning.core.service.TestQuestionsService;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.AnswerQuestionRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.StudentTestResponseResource;
import co.jp.bitsoft.elearning.user.service.db.dao.StudentTestQuestionAnswerDbDao;
import co.jp.bitsoft.elearning.user.service.modules.user.service.StudentTestQuestionAnswerDbService;
import co.jp.bitsoft.elearning.user.service.modules.user.service.TestQuestionsChoicesDbService;
import lombok.var;

@Service("studentTestQuestionAnswerDbService")
public class StudentTestQuestionAnswerDbServiceImpl
        extends ServiceImpl<StudentTestQuestionAnswerDbDao, StudentTestQuestionAnswerEntity>
        implements StudentTestQuestionAnswerDbService {

    @Autowired
    private TestQuestionsService testQuestionsService;

    @Autowired
    private TestQuestionsChoicesDbService testQuestionsChoicesDbService;

    @Override
    public void answerQuestion(Long studentId,
                               AnswerQuestionRequestResource answerQuestionRequestResource) {

        var TestQuestionsEntity = testQuestionsService.getById(answerQuestionRequestResource.getTestQuestionsId());

        String standardAnswers = TestQuestionsEntity.getStandardAnswers();

        var correctFlag = false;

        // 問題形式が「順不同」の場合、解答内容をソートしてから判断する
        if ("1".equals(TestQuestionsEntity.getQuestionType())) {
            String answerContent = Arrays.stream(answerQuestionRequestResource.getAnswerContent()
                                                                              .split(","))
                                         .sorted()
                                         .collect(Collectors.joining(","));
            correctFlag = standardAnswers.equals(answerContent);
        } else {
            correctFlag = standardAnswers.equals(answerQuestionRequestResource.getAnswerContent());
        }

        var startTime = new Timestamp(new Date(answerQuestionRequestResource.getAnswerStartTime()).getTime());
        var now = new Timestamp(new Date().getTime());

        var studentTestQuestionAnswerEntity = new StudentTestQuestionAnswerEntity() {
            {
                setStudentTestId(answerQuestionRequestResource.getStudentTestId());
                setTestQuestionsId(answerQuestionRequestResource.getTestQuestionsId());
                setAnswerStatus(StringUtils.isNotBlank(answerQuestionRequestResource.getAnswerContent()) ? "1"
                                                                                                         : "0");
                setAnswerContent(answerQuestionRequestResource.getAnswerContent());
                setAnswerStartTime(startTime);
                setAnswerEndTime(now);
                setAnswerTimeSeconds(Seconds.secondsBetween(new DateTime(startTime),
                        new DateTime(now))
                        .getSeconds());
                setCreateUserId(studentId);
                setCreateTime(now);
                setUpdateUserId(studentId);
                setUpdateTime(now);
                setDelFlag("0");
            }
        };
        studentTestQuestionAnswerEntity.setAnswerResult(correctFlag ? "0" : "1");

        this.save(studentTestQuestionAnswerEntity);
    }

    @Override
    public StudentTestResponseResource studentTestQuestionAnswer(Long studentId,
                                                                 Long courseId,
                                                                 Long subjectId,
                                                                 Long unitId,
                                                                 Long studentTestId) {

        var studentTestTestQuestionsChoicesList = testQuestionsChoicesDbService.studentTestTestQuestionsChoicesList(studentTestId);

        var studentTestQuestionAnswersList = this.getBaseMapper()
                                                 .studentTestQuestionAnswers(studentId,
                                                                             courseId,
                                                                             unitId,
                                                                             studentTestId);

        return StudentTestResponseResource.builder(studentTestQuestionAnswersList,
                                                   studentTestTestQuestionsChoicesList);
    }
}
