package co.jp.bitsoft.elearning.user.service.modules.user.service;

import java.util.List;
import java.util.Map;

import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.user.domain.model.course.CourseCertificateResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentCourseRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentCourseResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentSubjectResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentTeachingMaterialResponseResource;
import co.jp.bitsoft.elearning.user.domain.model.course.StudentUnitResponseResource;

/**
 * 講座一覧サービス
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
public interface StudentCourseService {

    List<StudentCourseResponseResource> getCourseList(StudentEntity studentEntity);

    StudentCourseResponseResource getCourseInfo(StudentEntity studentEntity, Long courseId);

    void courseReg(StudentEntity studentEntity, StudentCourseRequestResource studentCourseRequestResource);

    StudentSubjectResponseResource getSubjectInfo(StudentEntity studentEntity, Long courseId, Long subjectId);

    StudentUnitResponseResource getUnitInfo(StudentEntity studentEntity, Long courseId, Long subjectId, Long unitId);

    StudentTeachingMaterialResponseResource getTeachingMaterialInfo(StudentEntity studentEntity, Long courseId, Long subjectId, Long unitId, Long materialId);

    String getEndTimeUpdate(StudentEntity studentEntity, Long courseId, Long subjectId, Long unitId, Long materialId);

    Map<String, Object> queryCourseDataCount(StudentEntity studentEntity, Long courseId);

    CourseCertificateResponseResource certificate(StudentEntity studentEntity, Long groupId, Long courseId);
}

