package co.jp.bitsoft.elearning.user.service.modules.user.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import co.jp.bitsoft.elearning.core.entity.TestQuestionsChoicesEntity;
import co.jp.bitsoft.elearning.user.dto.course.StudentTestTestQuestionsChoicesDto;
import co.jp.bitsoft.elearning.user.service.db.dao.TestQuestionsChoicesDbDao;
import co.jp.bitsoft.elearning.user.service.modules.user.service.TestQuestionsChoicesDbService;

@Service("testQuestionsChoicesServiceImpl")
public class TestQuestionsChoicesDbServiceImpl extends ServiceImpl<TestQuestionsChoicesDbDao, TestQuestionsChoicesEntity>
        implements TestQuestionsChoicesDbService {

    public List<StudentTestTestQuestionsChoicesDto> studentTestTestQuestionsChoicesList(Long studentTestId) {
        return this.getBaseMapper().studentTestTestQuestionsChoicesList(studentTestId);
    }
}
