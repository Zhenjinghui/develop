package co.jp.bitsoft.elearning.user.service.modules.user.service;

import java.util.Map;

import co.jp.bitsoft.elearning.common.utils.PageUtils;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.CourseInfoResponseResource;

public interface CourseInfoDbService {
    PageUtils courseInfoListByGroupId(Map<String, Object> params, Long groupId);

    CourseInfoResponseResource courseInfo(Long groupId, Long infoId);
}
