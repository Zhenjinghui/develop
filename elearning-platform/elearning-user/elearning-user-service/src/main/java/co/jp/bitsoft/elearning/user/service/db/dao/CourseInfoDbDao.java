package co.jp.bitsoft.elearning.user.service.db.dao;

import java.sql.Timestamp;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import co.jp.bitsoft.elearning.core.entity.CourseInfoEntity;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.CourseInfoResponseResource;

import co.jp.bitsoft.elearning.user.domain.model.courseinfo.CourseInfoResponseResource;

@Mapper
public interface CourseInfoDbDao extends BaseMapper<CourseInfoEntity> {
    IPage<?> courseInfoListByGroupId(IPage<?> page,
                                     @Param("groupId") Long groupId,
                                     @Param("now") Timestamp now);

    CourseInfoResponseResource courseInfo(@Param("groupId") Long groupId,
                                          @Param("infoId") Long infoId,
                                          @Param("now") Timestamp now);
}
