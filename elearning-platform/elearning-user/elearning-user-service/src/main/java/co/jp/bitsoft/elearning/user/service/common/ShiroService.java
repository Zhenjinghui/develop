/**
 * Copyright (c) 2020 BitSoft-Inc All rights reserved.
 */

package co.jp.bitsoft.elearning.user.service.common;

import co.jp.bitsoft.elearning.core.entity.StudentEntity;
import co.jp.bitsoft.elearning.core.entity.StudentTokenEntity;

/**
 * shiro関係IF
 *
 * @author  BitSoft
 */
public interface ShiroService {

    /**
     * トークンより、受講者ユーザートークン取得
     *
     * @param token トークン
     * @return StudentTokenEntity 受講者ユーザートークン
     */
    StudentTokenEntity queryByToken(String token);

    /**
     * 受講者IDより、受講者エンティティ取得
     *
     * @param studentId 受講者ID
     */
    StudentEntity queryStudent(Long studentId);

}
