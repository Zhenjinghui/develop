package co.jp.bitsoft.elearning.user.service.modules.user.service;

import co.jp.bitsoft.elearning.user.domain.model.courseinfo.AnswerQuestionRequestResource;
import co.jp.bitsoft.elearning.user.domain.model.courseinfo.StudentTestResponseResource;

public interface StudentTestQuestionAnswerDbService {
    void answerQuestion(Long studentId, AnswerQuestionRequestResource answerQuestionRequestResource);

    StudentTestResponseResource studentTestQuestionAnswer(Long studentId,
                                                          Long courseId,
                                                          Long subjectId,
                                                          Long unitId,
                                                          Long studentTestId);
}
