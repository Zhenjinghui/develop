package co.jp.bitsoft.elearning.user.service.db.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import co.jp.bitsoft.elearning.core.entity.StudentTestQuestionAnswerEntity;
import co.jp.bitsoft.elearning.user.dto.StudentTestQuestionAnswersDto;

@Mapper
public interface StudentTestQuestionAnswerDbDao extends BaseMapper<StudentTestQuestionAnswerEntity> {

    List<StudentTestQuestionAnswersDto> studentTestQuestionAnswers(@Param("studentId") Long studentId,
                                                                   @Param("courseId") Long courseId,
                                                                   @Param("unitId") Long unitId,
                                                                   @Param("studentTestId") Long studentTestId);
}
