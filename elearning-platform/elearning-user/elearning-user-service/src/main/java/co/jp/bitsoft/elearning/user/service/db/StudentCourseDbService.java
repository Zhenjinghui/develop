package co.jp.bitsoft.elearning.user.service.db;

import co.jp.bitsoft.elearning.user.dto.course.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 講座
 *
 * @author BitSoft-inc
 * @email zhangyuliang@bitsoft-inc.co.jp
 * @date 2021-01-01 00:00:00
 */
@Mapper
public interface StudentCourseDbService extends BaseMapper<StudentCourseDto> {

    /**
     * 講座一覧取得
     */
    List<StudentCourseDto> queryCourseList(@Param("groupId") Long groupId, @Param("studentId") Long studentId);

    /**
     * 講座詳細
     */
    Optional<StudentCourseDto> queryCourseInfo(@Param("groupId") Long groupId, @Param("courseId") Long courseId, @Param("studentId") Long studentId);

    /**
     * 講座一覧取得
     */
    List<StudentSubjectDto> querySubjectList(@Param("courseId") Long courseId);

    /**
     * 教科詳細
     */
    Optional<StudentSubjectDto> querySubjectInfo(@Param("groupId") Long groupId, @Param("courseId") Long courseId, @Param("studentId") Long studentId, @Param("subjectId") Long subjectId);

    /**
     * 単元一覧取得
     */
    List<StudentUnitDto> queryUnitList(@Param("courseId") Long courseId, @Param("subjectId") Long subjectId);

    /**
     * 単元詳細
     */
    Optional<StudentUnitDto> queryUnitInfo(@Param("groupId") Long groupId, @Param("courseId") Long courseId, @Param("studentId") Long studentId, @Param("subjectId") Long subjectId, @Param("unitId") Long unitId);

    /**
     * 受講者テスト一覧
     */
    List<StudentStudentTestDto> queryStudentTestList(@Param("groupId") Long groupId, @Param("courseId") Long courseId, @Param("studentId") Long studentId, @Param("subjectId") Long subjectId, @Param("unitId") Long unitId);

    /**
     * 教材一覧
     */
    List<StudentTeachingMaterialDto> queryTeachingMaterialList(@Param("unitId") Long unitId, @Param("studentId") Long studentId, @Param("groupId") Long groupId, @Param("courseId") Long courseId);

    /**
     * 教材詳細
     */
    Optional<StudentTeachingMaterialDto> queryTeachingMaterialInfo(@Param("groupId") Long groupId, @Param("courseId") Long courseId, @Param("studentId") Long studentId, @Param("subjectId") Long subjectId, @Param("unitId") Long unitId, @Param("materialId") Long materialId);

    /**
     * 教材数
     * @param groupId
     * @param courseId
     * @param studentId
     * @return
     */
    Map<String, Object> queryCourseTeachingMaterialCount(@Param("groupId") Long groupId, @Param("courseId") Long courseId, @Param("studentId") Long studentId);

    /**
     * テスト数
     * @param groupId
     * @param courseId
     * @param studentId
     * @return
     */
    List<Map<String, Object>> queryCourseUnitTestCount1(@Param("groupId") Long groupId, @Param("courseId") Long courseId, @Param("studentId") Long studentId);
    List<Map<String, Object>> queryCourseUnitTestCount2(@Param("groupId") Long groupId, @Param("courseId") Long courseId, @Param("studentId") Long studentId);
}
