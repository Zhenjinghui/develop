module.exports = {
  lintOnSave: true,
  devServer: {
    disableHostCheck: true,
    proxy: {
      '/api': {
        target: 'http://localhost:8081/',
        pathRewrite: {
          '^/api': '/e-learning-user',
        },
        changeOrigin: true,
        secure: false,
        headers: {
          Referer: 'http://localhost:8081/',
          // token: 'aaaabbbbcccc',
        },
      },
    },
  },
};
