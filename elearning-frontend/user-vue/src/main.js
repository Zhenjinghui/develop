import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './plugins/element';
import 'reset.css';
import './styles/common.less';

// import './mock';

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
