import Mock from 'mockjs';
import dayjs from 'dayjs';

let isLogin = false;

Mock.mock('/api/login', 'post', () => {
  isLogin = true;

  return {
    msg: 'success',
    code: 0,
    data: {
      defaultPasswordflag: '1',
      expire: 43200,
      token: '44fe43e0e3838c4ee1007a8827f9a566',
    },
  };
});

Mock.mock('/api/logout', 'post', () => {
  isLogin = false;

  return {
    code: 0,
    data: {
      username: 'lizhenghua',
    },
  };
});

Mock.mock('/api/isLogin', 'get', () => ({
  code: 0,
  data: isLogin,
}));

// Mock.mock('/api/getCourseList', 'get', ({
//   code: 0,
//   msg: 'error',
//   data: [{
//     id: 0,
//     pic: '',
//     // pic: 'https://resource.aircourse.com/1/course/9609/image/DalTPZVElZkcXVv6Uvc6De4S.jpg',
//     // pic: 'https://www.tkhs.co.jp/img/GC200110_90.jpg',
//     title: 'コース12345:新人社員の名刺交換の基本',
//     category: 'ビジネスマナー',
//     tags: ['タグ１', 'タグ2'],
//     desc: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
//     time: '100',
//     author: '東京XX会社',
//   }].concat(new Array(9).fill({
//     id: 1,
//     // pic: 'https://resource.aircourse.com/1/course/9609/image/DalTPZVElZkcXVv6Uvc6De4S.jpg',
//     pic: 'https://www.tkhs.co.jp/img/GC200110_90.jpg',
//     title: 'コース12345:新人社員の名刺交換の基本',
//     category: 'ビジネスマナー',
//     tags: ['タグ１', 'タグ2'],
//     desc: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
//     time: '100',
//     author: '東京XX会社',
//   })),
// }));
Mock.mock('/api/course/list', 'get', ({
  msg: 'success',
  code: 0,
  data: {
    count: 9,
    studentCourseResponseResourceList: [
      {
        courseId: '11',
        courseTitle: '代理店名１の講座名１',
        courseType: '00',
        courseTypeName: '文化',
        courseOutline: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
        courseDetail: null,
        courseImagePath: 'https://www.tkhs.co.jp/img/HE320730_180.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
      {
        courseId: '12',
        courseTitle: '代理店名１の講座名２',
        courseType: '01',
        courseTypeName: '科学',
        courseOutline: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
        courseDetail: null,
        courseImagePath: 'https://www.tkhs.co.jp/img/HE320730_180.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
      {
        courseId: '13',
        courseTitle: '代理店名１の講座名３',
        courseType: '02',
        courseTypeName: '経済',
        courseOutline: '講座概要：代理店名１の講座名３',
        courseDetail: null,
        courseImagePath: 'https://www.tkhs.co.jp/img/HE320730_180.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
      {
        courseId: '14',
        courseTitle: '代理店名１の講座名４',
        courseType: '03',
        courseTypeName: null,
        courseOutline: '講座概要：代理店名１の講座名４',
        courseDetail: null,
        courseImagePath: 'https://www.tkhs.co.jp/img/HE320730_180.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
      {
        courseId: '15',
        courseTitle: '代理店名１の講座名５',
        courseType: '04',
        courseTypeName: null,
        courseOutline: '講座概要：代理店名１の講座名５',
        courseDetail: null,
        courseImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/2/15.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
      {
        courseId: '16',
        courseTitle: '代理店名１の講座名６',
        courseType: '00',
        courseTypeName: null,
        courseOutline: '講座概要：代理店名１の講座名６',
        courseDetail: null,
        courseImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/2/16.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
      {
        courseId: '17',
        courseTitle: '代理店名１の講座名７',
        courseType: '01',
        courseTypeName: null,
        courseOutline: '講座概要：代理店名１の講座名７',
        courseDetail: null,
        courseImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/2/17.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
      {
        courseId: '18',
        courseTitle: '代理店名１の講座名８',
        courseType: '02',
        courseTypeName: null,
        courseOutline: '講座概要：代理店名１の講座名８',
        courseDetail: null,
        courseImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/2/18.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
      {
        courseId: '19',
        courseTitle: '代理店名１の講座名９',
        courseType: '03',
        courseTypeName: null,
        courseOutline: '講座概要：代理店名１の講座名９',
        courseDetail: null,
        courseImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/2/19.jpg',
        courseRegFlag: null,
        courseRegFlagName: null,
        studyStatus: null,
        studyStatusName: null,
        courseRegReason: null,
        regRejectReason: null,
        subjectResponseResourceList: null,
      },
    ],
  },
}
));
Mock.mock(/\/api\/course\/subject\/detail/, 'get', ({
  msg: 'success',
  code: 0,
  data: {
    subjectId: '1',
    subjectTitle: 'aaa',
    courseTitle: '111',
    subjectOutline: 'asdfasdf',
    subjectDetail: 'dfgdgdfg',
    subjectImagePath: 'https://tpc.googlesyndication.com/simgad/10311866644712950787?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0',
    studentUnitResponseResourceList: {
      studentUnitResponseResourceList: [
        {
          unitId: '1',
          unitName: '1',
          subjectTitle: null,
          courseTitle: null,
          unitOutline: '111',
          unitDetail: '111',
          unitImagePath: '11',
          studentTeachingMaterialResponseResourceList: null,
          studentStudentTestResponseResourceList: null,
        },
      ],
      count: 1,
    },
  },
}));

Mock.mock(/\/api\/course\/subject\/unit\/detail/, 'get', ({
  msg: 'success',
  code: 0,
  data: {
    unitId: '1',
    unitName: '1',
    subjectTitle: 'aaa',
    courseTitle: '111',
    unitOutline: '111',
    unitDetail: '111',
    unitImagePath: '11',
    studentTeachingMaterialResponseResourceList: {
      count: 1,
      studentTeachingMaterialResponseResourceList: [
        {
          teachingMaterialId: '1',
          teachingMaterialTitle: '1',
          unitName: null,
          subjectTitle: null,
          courseTitle: null,
          teachingMaterialOutline: null,
          teachingMaterialDetail: null,
          teachingMaterialImagePath: '1',
          contentType: null,
          contentTypeName: null,
          contentUrl: null,
          contentVideoTime: null,
          contentPages: null,
          contentLettersNum: null,
        },
      ],
    },
    studentStudentTestResponseResourceList: {
      studentStudentTestResponseResourceList: [
        {
          studentTestId: '1',
          answerStartTime: '2021-01-22 00:01:00',
          answerStatus: '1',
          answerStatusName: '解答済',
          answerResult: '1',
          answerResultName: '不合格',
          answerRate: null,
        },
      ],
      count: 1,
    },
  },
}));
Mock.mock(/\/api\/course\/subject\/unit\/material\/detail/, 'get', ({
  msg: 'success',
  code: 0,
  data: {
    teachingMaterialId: '1',
    teachingMaterialTitle: '1',
    unitName: '1',
    subjectTitle: 'aaa',
    courseTitle: '111',
    teachingMaterialOutline: null,
    teachingMaterialDetail: null,
    teachingMaterialImagePath: '1',
    contentType: '1 ',
    contentTypeName: null,
    contentUrl: null,
    contentVideoTime: null,
    contentPages: null,
    contentLettersNum: null,
  },
}));

Mock.mock(/\/api\/course/, 'get', ({
  msg: 'success',
  code: 0,
  data: {
    courseId: '11',
    courseTitle: '代理店名１の講座名１',
    courseType: '00',
    courseTypeName: '類別無し',
    courseOutline: '講座概要：代理店名１の講座名１',
    courseDetail: '講座説明：代理店名１の講座名１',
    courseImagePath: 'https://www.tkhs.co.jp/img/HE320730_180.jpg',
    courseRegFlag: '0',
    courseRegFlagName: '不要',
    studyStatus: '1',
    studyStatusName: null,
    courseRegReason: null,
    regRejectReason: null,
    subjectResponseResourceList: {
      count: 9,
      studentSubjectResponseResourceList: [
        {
          subjectId: '1',
          subjectTitle: '代理店名１の講座名１の教科名１',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名１の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名１の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/1.jpg',
          studentUnitResponseResourceList: null,
        },
        {
          subjectId: '2',
          subjectTitle: '代理店名１の講座名１の教科名２',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名２の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名２の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/2.jpg',
          studentUnitResponseResourceList: null,
        },
        {
          subjectId: '3',
          subjectTitle: '代理店名１の講座名１の教科名３',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名３の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名３の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/3.jpg',
          studentUnitResponseResourceList: null,
        },
        {
          subjectId: '4',
          subjectTitle: '代理店名１の講座名１の教科名４',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名４の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名４の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/4.jpg',
          studentUnitResponseResourceList: null,
        },
        {
          subjectId: '5',
          subjectTitle: '代理店名１の講座名１の教科名５',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名５の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名５の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/5.jpg',
          studentUnitResponseResourceList: null,
        },
        {
          subjectId: '6',
          subjectTitle: '代理店名１の講座名１の教科名６',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名６の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名６の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/6.jpg',
          studentUnitResponseResourceList: null,
        },
        {
          subjectId: '7',
          subjectTitle: '代理店名１の講座名１の教科名７',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名７の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名７の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/7.jpg',
          studentUnitResponseResourceList: null,
        },
        {
          subjectId: '8',
          subjectTitle: '代理店名１の講座名１の教科名８',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名８の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名８の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/8.jpg',
          studentUnitResponseResourceList: null,
        },
        {
          subjectId: '9',
          subjectTitle: '代理店名１の講座名１の教科名９',
          courseTitle: null,
          subjectOutline: '代理店名１の講座名１の教科名９の教科概要',
          subjectDetail: '代理店名１の講座名１の教科名９の教科詳細説明',
          subjectImagePath: 'https://elearning-dev1.s3-ap-northeast-1.amazonaws.com/course/11/9.jpg',
          studentUnitResponseResourceList: null,
        },
      ],
    },
  },
}));

Mock.mock('/api/course/reg', 'post', ({
  msg: 'success',
  code: 0,
  data: {
    count: 2,
    studentCourseResponseResourceList: {
      courseId: '1',
      courseTitle: '111',
      courseType: '01',
      courseTypeName: '文化',
      courseOutline: 'adasdad',
      courseDetail: 'aafasdfasdf',
      courseImagePath: 'https://tpc.googlesyndication.com/simgad/10311866644712950787?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0',
      courseRegFlag: '1',
      courseRegFlagName: '要',
      studyStatus: '0',
      studyStatusName: '受講申請中',
      courseRegReason: 'aaaaaaaaaaaa',
      regRejectReason: null,
      subjectResponseResourceList: {
        count: 1,
        studentSubjectResponseResourceList: [
          {
            subjectId: '1',
            subjectTitle: 'aaa',
            courseTitle: null,
            subjectOutline: 'asdfasdf',
            subjectDetail: 'dfgdgdfg',
            subjectImagePath: 'https://tpc.googlesyndication.com/simgad/10311866644712950787?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0',
            studentUnitResponseResourceList: null,
          },
        ],
      },
    },
  },
}));

Mock.mock('/api/getSubjectList', 'get', ({
  code: 0,
  data: [{
    id: 0,
    pic: 'https://resource.aircourse.com/1/course/9609/image/DalTPZVElZkcXVv6Uvc6De4S.jpg',
    // pic: 'https://www.tkhs.co.jp/img/GC200110_90.jpg',
    title: '教科12345:新人社員の名刺交換の基本',
    status: true,
    desc: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
    time: '100',
    author: '東京XX会社',
  }].concat(new Array(9).fill({
    id: 1,
    // pic: 'https://resource.aircourse.com/1/course/9609/image/DalTPZVElZkcXVv6Uvc6De4S.jpg',
    pic: 'https://www.tkhs.co.jp/img/GC200110_90.jpg',
    title: '教科12345:新人社員の名刺交換の基本',
    status: true,
    desc: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
    time: '100',
    author: '東京XX会社',
  })),
}));

Mock.mock('/api/getUnitList', 'get', ({
  code: 0,
  data: [{
    id: 0,
    pic: 'https://resource.aircourse.com/1/course/9609/image/DalTPZVElZkcXVv6Uvc6De4S.jpg',
    // pic: 'https://www.tkhs.co.jp/img/GC200110_90.jpg',
    title: '単元12345:新人社員の名刺交換の基本',
    status: true,
    desc: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
    time: '100',
    author: '東京XX会社',
  }].concat(new Array(9).fill({
    id: 1,
    // pic: 'https://resource.aircourse.com/1/course/9609/image/DalTPZVElZkcXVv6Uvc6De4S.jpg',
    pic: 'https://www.tkhs.co.jp/img/GC200110_90.jpg',
    title: '単元12345:新人社員の名刺交換の基本',
    status: true,
    desc: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
    time: '100',
    author: '東京XX会社',
  })),
}));

Mock.mock('/api/getMaterialList', 'get', ({
  code: 0,
  data: [{
    id: 0,
    pic: 'https://resource.aircourse.com/1/course/9609/image/DalTPZVElZkcXVv6Uvc6De4S.jpg',
    // pic: 'https://www.tkhs.co.jp/img/GC200110_90.jpg',
    title: '教材12345:新人社員の名刺交換の基本',
    status: true,
    desc: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
    time: '100',
    author: '東京XX会社',
  }].concat(new Array(9).fill({
    id: 1,
    // pic: 'https://resource.aircourse.com/1/course/9609/image/DalTPZVElZkcXVv6Uvc6De4S.jpg',
    pic: 'https://www.tkhs.co.jp/img/GC200110_90.jpg',
    title: '教材12345:新人社員の名刺交換の基本',
    status: true,
    desc: '成果を確実に実現するプロジェクトマネジメント（全5回）の第1回目のコースとなります。本コースでは、プロジェクトマネジメントに関する基本知...',
    time: '100',
    author: '東京XX会社',
  })),
}));

Mock.mock('/api/mypage/courselist', 'get', ({
  msg: 'success',
  code: 0,
  data: {
    myPageResponseResourceList: [
      {
        courseId: '1',
        courseTitle: '111',
        studyStatus: '0',
        studyStatusName: '受講申請中',
        openStartTime: '2021/01/01',
        openEndTime: '2021/01/31',
      },
    ],
    count: 1,
  },
  // code: 0,
  // data: new Array(5).fill(undefined).map((item, index) => ({
  //   id: '111',
  //   name: '教科12345:新人社員の名刺交換の基本',
  //   status: String(index),
  //   range: '2021/12/12～2021/12/31',
  // })),
}));

Mock.mock(/\/api\/mypage\/infolist/, 'get', ({
  // code: 0,
  // data: new Array(5).fill(undefined).map((item, index) => ({
  //   id: String(index),
  //   time: dayjs().format('YYYY-MM-DD HH:mm:ss'),
  //   isNew: true,
  //   category: '新規講座開講のお知らせ',
  //   title: '講座A受講内容変更について講座A受講内容変更について講座A受講内容変更について',
  // })),
  msg: 'success',
  code: 0,
  data: {
    count: 2,
    courseInfoResponseResourceList: [
      {
        courseId: 1,
        courseTitle: '講座名1',
        infoDetail: 'インフォメーション詳細1',
        infoId: 1,
        infoName: 'インフォメーション件名1',
        infoOverview: 'インフォメーション概要1',
        infoType: 'インフォメーション種別1',
        openStartTime: '20210222',
      },
      {
        courseId: 2,
        courseTitle: '講座名2',
        infoDetail: 'インフォメーション詳細2',
        infoId: 2,
        infoName: 'インフォメーション件名2',
        infoOverview: 'インフォメーション概要2',
        infoType: 'インフォメーション種別2',
        openStartTime: '20210222',
      },
    ],
  },

}));

Mock.mock(/\/api\/mypage\/info/, 'get', ({
  // code: 0,
  // data: {
  //   id: 1,
  //   time: dayjs().format('YYYY-MM-DD HH:mm:ss'),
  //   category: '講座内容の訂正',
  //   title: '講座A受講内容変更について',
  //   abstract: '講座インフォメーションん概要講座インフォメーションん概要講座インフォメーションん概要講座インフォメーションん概要講座インフォメーションん概要',
  //   content: '講座インフォメーション詳細講座インフォメーション詳細講座インフォメーション詳細講座インフォメーション詳細',
  // },
  msg: 'success',
  code: 0,
  data: {
    courseInfoResponseResource: {
      courseId: 1,
      courseTitle: '講座名1',
      infoDetail: 'インフォメーション詳細1',
      infoId: 1,
      infoName: 'インフォメーション件名1',
      infoOverview: 'インフォメーション概要1',
      infoType: 'インフォメーション種別1',
      openStartTime: '20210222',
    },
  },
}));

Mock.mock('/api/getQaList', 'get', ({
  code: 0,
  data: new Array(5).fill(undefined).map((item, index) => ({
    id: String(index),
    time: dayjs().format('YYYY-MM-DD HH:mm:ss'),
    category: '講座について',
    title: '講座ID：XXXの問い合わせ',
  })),
}));

Mock.mock('/api/getQaDetail', 'get', ({
  code: 0,
  data: {
    id: 1,
    category: '講座について',
    title: '講座ID：XXXの問い合わせ',
    history: [
      {
        from: 'グループ名１',
        time: dayjs().format('YYYY-MM-DD HH:mm:ss'),
        content: 'グループ名１からの回答内容',
      },
      {
        from: '一般ユーザー１　名前 さん',
        time: dayjs().format('YYYY-MM-DD HH:mm:ss'),
        content: '一般ユーザー１からのお問い合わせ内容',
      },
    ],
  },
}));

Mock.mock('/api/mypage/user', 'get', ({
  msg: 'success',
  code: 0,
  data: {
    email: 'student1@bitsoft-inc.co.jp',
    userNameKanji: '代理店１の所属一般グループ名１の一般ユーザー１',
    userNameKana: null,
    nickName: null,
    sex: null,
    birthday: null,
    addressZipCode: null,
    addressPrefecture: null,
    addressCity: null,
    addressOthers: null,
    addressBuildingName: null,
    contactMobile: null,
    contactTel: null,
    contactFax: null,
  },
}));

Mock.mock('/api/coures/course/subject/unit/test', 'post', ({
  msg: 'success',
  code: 0,
  data: {
    testQuestionResponceResource: [
      {
        testQuestion: {
          questionTitle: '問題名1',
          questionType: '0',
          testQuestionsId: 1,
          testQuestionChoices: [
            {
              choicesContents: '選択肢文1',
              choicesNo: 1,
              testQuestionsChoicesId: 1,
            },
            {
              choicesContents: '選択肢文2',
              choicesNo: 2,
              testQuestionsChoicesId: 2,
            },
          ],
        },
      },
      {
        testQuestion: {
          questionTitle: '問題名2',
          questionType: '1',
          testQuestionsId: 2,
          testQuestionChoices: [
            {
              choicesContents: '選択肢文1',
              choicesNo: 1,
              testQuestionsChoicesId: 3,
            },
            {
              choicesContents: '選択肢文2',
              choicesNo: 2,
              testQuestionsChoicesId: 4,
            },
          ],
        },
      },
      {
        testQuestion: {
          questionTitle: '問題名3',
          questionType: '2',
          testQuestionsId: 3,
          testQuestionChoices: [
            {
              choicesContents: '選択肢文1',
              choicesNo: 1,
              testQuestionsChoicesId: 5,
            },
            {
              choicesContents: '選択肢文2',
              choicesNo: 2,
              testQuestionsChoicesId: 6,
            },
          ],
        },
      },
      {
        testQuestion: {
          questionTitle: '問題名4',
          questionType: '3',
          testQuestionsId: 4,
          testQuestionChoices: [],
        },
      },
    ],
  },
}));

Mock.mock('/api/coures/course/subject/unit/test/next', 'post', ({
  msg: 'success',
  code: 0,
}));

// 有错时候
Mock.mock('/api/coures/course/subject/unit/test/submit', 'post', ({
  msg: 'success',
  code: 0,
  data: {
    unitId: '単元ID',
    unitName: '単元名',
    unitTestId: '単元テストID',
    testTitle: '単元テスト名',
    answerCorrectRate: 'テスト正解率',
    answerResult: '1', // テスト判定結果
    noPassedAnsweres: [
      {
        questionTitle: '問題名1',
        questionContent: '問題文1',
        questionExplanation: '解説文1',
        answerContent: '解答内容1',
        standardAnswers: '標準解答1',
        answerResult: '解答判定結果1', // '0':正确 '1':错误
      },
      {
        questionTitle: '問題名2',
        questionContent: '問題文2',
        questionExplanation: '解説文2',
        answerContent: '解答内容2',
        standardAnswers: '標準解答2',
        answerResult: '解答判定結果2', // '0':正确 '1':错误
      },
      {
        questionTitle: '問題名3',
        questionContent: '問題文3',
        questionExplanation: '解説文3',
        answerContent: '解答内容3',
        standardAnswers: '標準解答3',
        answerResult: '解答判定結果3', // '0':正确 '1':错误
      },
      {
        questionTitle: '問題名4',
        questionContent: '問題文4',
        questionExplanation: '解説文4',
        answerContent: '解答内容4',
        standardAnswers: '標準解答4',
        answerResult: '解答判定結果4', // '0':正确 '1':错误
      },
    ],
  },
}));

// Mock.mock('/api/coures/course/subject/unit/test/submit', 'post', ({
//   msg: 'success',
//   code: 0,
//   data: {
//     unitId: 1,
//     unitName: '単元名',
//     unitTestId: 1,
//     testTitle: '単元テスト名',
//     answerCorrectRate: 'テスト正解率',
//     answerResult: '0', // テスト判定結果
//     noPassedAnsweres: null,
//   },
// }));
