/**
 * 邮箱
 * @param {*} s
 */
export function isEmail (s) {
  return /^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,10}){1,8})$/.test(s)
}

/**
 * 电话号码
 * @param {*} s
 */
// export function isPhone (s) {
//   return /^([0-9]{3,4}-)?[0-9]{7,8}$/.test(s)
// }

/**
 * URL地址
 * @param {*} s
 */
export function isURL (s) {
  return /^http[s]?:\/\/.*/.test(s)
}

/**
 * 郵便番号
 * @param {*} s
 */
export function isZipCode (s) {
  return /^[0-9]{3}[0-9]{4}$/.test(s)
}

/**
 * kana
 * @param {*} s
 */
export function isKana (s) {
  return /^[\u30A0-\u30FF]+$/.test(s)
}

/**
 * 日本手机号码
 * @param {*} s
 */
export function isMobile (s) {
  // return /^1[0-9]{10}$/.test(s)
  return /0[789]0-?[0-9]{4}-?[0-9]{4}$/.test(s)
}

/**
 * 日本固定电话
 * @param {*} s
 */
export function isFixedPhone (s) {
  return /^0[0-9]{9}$/.test(s)
}
/**
 * (简单)日本固定电话+手机号码
 * @param {*} s
 */
export function isFixedPhoneOrMobile (s) {
  return /^0[0-9]{9}$|0[789]0-?[0-9]{4}-?[0-9]{4}$/.test(s)
}

/**
 * 日本传真
 * @param {*} s
 */
export function isFax (s) {
  return /^0[0-9]{9}$/.test(s)
}

/**
 * 半角英数字
 * @param {*} s
 */
export function isHalfEnglishAndNumber (s) {
  return /^[a-zA-Z0-9]*$/.test(s)
}

/**
 * 半角英数字且至少含有一个数字和字母
 * @param {*} s
 */
export function isHalfEnglishAndNumberDouble (s) {
  return /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]*$/.test(s)
}
