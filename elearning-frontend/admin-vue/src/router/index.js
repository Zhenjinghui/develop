/**
 * 全站路由配置
 *
 * 建议:
 * 1. 代码中路由统一使用name属性跳转(不使用path属性)
 */
import Vue from 'vue'
import Router from 'vue-router'
import http from '@/utils/httpRequest'
import { isURL } from '@/utils/validate'
import { clearLoginInfo } from '@/utils'

Vue.use(Router)

// 开发环境不使用懒加载, 因为懒加载页面太多的话会造成webpack热更新太慢, 所以只有生产环境使用懒加载
const _import = require('./import-' + process.env.NODE_ENV)

// 全局路由(无需嵌套上左右整体布局)
const globalRoutes = [
  { path: '/', component: _import('common/login'), name: 'login', meta: { title: '登録' } },
  { path: '/404', component: _import('common/404'), name: '404', meta: { title: '404ページが見つかりません' } },
  { path: '/login', component: _import('common/login'), name: 'login', meta: { title: '登録' } },
  { path: '/updatePassword', component: _import('common/update-password'), name: 'updatePassword', meta: { title: 'パスワード変更' } }

]

// 主入口路由(需嵌套上左右整体布局)
const mainRoutes = {
  path: '/',
  component: _import('main'),
  name: 'main',
  redirect: { name: 'home' },
  meta: { title: '主入口整体布局' },
  children: [
    // 通过meta对象设置路由展示方式
    // 1. isTab: 是否通过tab展示内容, true: 是, false: 否
    // 2. iframeUrl: 是否通过iframe嵌套展示内容, '以http[s]://开头': 是, '': 否
    // 確認: 如需要通过iframe嵌套展示内容, 但不通过tab打开, 请自行创建组件使用iframe处理!
    { path: '/home', component: _import('common/home'), name: 'home', meta: { title: 'ホーム' } },
    { path: '/theme', component: _import('common/theme'), name: 'theme', meta: { title: 'テーマ' } },
    { path: '/demo-echarts', component: _import('demo/echarts'), name: 'demo-echarts', meta: { title: 'demo-echarts', isTab: true } },
    { path: '/demo-ueditor', component: _import('demo/ueditor'), name: 'demo-ueditor', meta: { title: 'demo-ueditor', isTab: true } },
    // { path: '/paper-list', component: _import('exam/paper/list'), name: 'paper-list', meta: { title: '試験リスト', isTab: true } },
    // { path: '/question-list', component: _import('exam/question/list'), name: 'question-list', meta: { title: 'テーマリスト', isTab: true } },
    // { path: '/exam/paper/edit', component: _import('exam/paper/edit'), name: 'paper-edit', meta: { title: '試験追加', isTab: true } },
    { path: '/modules/service/subject/subject', component: _import('modules/service/subject/subject'), name: 'suject-page', meta: { title: '教科管理', isTab: true } },
    { path: '/modules/service/unit/unit', component: _import('modules/service/unit/unit'), name: 'unit-page', meta: { title: '単元管理', isTab: true } },
    { path: '/modules/service/unittest/unittest', component: _import('modules/service/unittest/unittest'), name: 'unittest-page', meta: { title: '単元テスト管理', isTab: true } },
    { path: '/modules/service/teachingmaterial/teachingmaterial', component: _import('modules/service/teachingmaterial/teachingmaterial'), name: 'teachingmaterial-page', meta: { title: '教材管理', isTab: true } },
    { path: '/modules/service/testquestions/testquestions', component: _import('modules/service/testquestions/testquestions'), name: 'testquestions-page', meta: { title: '問題管理', isTab: true } },
    { path: '/modules/service/testquestions/singleChoice', component: _import('modules/service/testquestions/single-choice'), name: 'single-choice', meta: { title: '択一編集', isTab: true } },
    { path: '/modules/service/testquestions/multipleChoice', component: _import('modules/service/testquestions/multiple-choice'), name: 'multiple-choice', meta: { title: '順不同編集', isTab: true } },
    { path: '/modules/service/testquestions/multipleChoiceOrder', component: _import('modules/service/testquestions/multiple-choice-order'), name: 'multiple-choice-order', meta: { title: '完全解答編集', isTab: true } },
    { path: '/modules/service/testquestions/gapFilling', component: _import('modules/service/testquestions/gap-filling'), name: 'gap-filling', meta: { title: '記述編集', isTab: true } },
    { path: '/modules/service/questionnaire/testquestions', component: _import('modules/service/questionnaire/testquestions'), name: 'questionNaireQuestions-page', meta: { title: 'アンケート質問管理', isTab: true } },
    { path: '/modules/service/questionnaire/singleChoice', component: _import('modules/service/questionnaire/single-choice'), name: 'questionNaire-single-choice', meta: { title: 'アンケート質問択一編集', isTab: true } },
    { path: '/modules/service/questionnaire/multipleChoice', component: _import('modules/service/questionnaire/multiple-choice'), name: 'questionNaire-multiple-choice', meta: { title: 'アンケート質問順不同編集', isTab: true } },
    { path: '/modules/service/questionnaire/gapFilling', component: _import('modules/service/questionnaire/gap-filling'), name: 'questionNaire-gap-filling', meta: { title: 'アンケート質問記述編集', isTab: true } },
    { path: '/modules/service/studentcoursemanage/textbookStatInfo', component: _import('modules/service/textbookStatInfo'), name: 'textbook-stat-info', meta: { title: '教材別受講状況一覧', isTab: true } },
    { path: '/modules/service/studentcoursemanage/studentTextbookStatInfo', component: _import('modules/service/studentTextbookStatInfo'), name: 'student-textbook-stat-info', meta: { title: '詳細', isTab: true } },
    { path: '/modules/service/studentcoursemanage/studentUnitTestHistory', component: _import('modules/service/studentUnitTestHistory'), name: 'student-unit-test-history', meta: { title: '単元テスト履歴', isTab: true } },
    { path: '/modules/service/testquestions/singleChoiceDetail', component: _import('modules/service/testquestions/single-choice-detail'), name: 'single-choice-detail', meta: { title: '択一詳細', isTab: true } },
    { path: '/modules/service/testquestions/multipleChoiceDetail', component: _import('modules/service/testquestions/multiple-choice-detail'), name: 'multiple-choice-detail', meta: { title: '順不同詳細', isTab: true } },
    { path: '/modules/service/testquestions/multipleChoiceOrderDetail', component: _import('modules/service/testquestions/multiple-choice-order-detail'), name: 'multiple-choice-order-detail', meta: { title: '完全解答詳細', isTab: true } },
    { path: '/modules/service/testquestions/gapFillingDetail', component: _import('modules/service/testquestions/gap-filling-detail'), name: 'gap-filling-detail', meta: { title: '記述詳細', isTab: true } }
  ],
  beforeEnter (to, from, next) {
    let token = Vue.cookie.get('token')
    if (!token || !/\S/.test(token)) {
      clearLoginInfo()
      next({ name: 'login' })
    }
    next()
  }
}

const router = new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  isAddDynamicMenuRoutes: false, // 是否已经添加动态(菜单)路由
  routes: globalRoutes.concat(mainRoutes)
})

router.beforeEach((to, from, next) => {
  // 添加动态(菜单)路由
  // 1. 已经添加 or 全局路由, 直接访问
  // 2. 获取菜单列表, 添加并保存本地存储
  if (router.options.isAddDynamicMenuRoutes || fnCurrentRouteType(to, globalRoutes) === 'global') {
    if (to.path === '/updatePassword') {
      let token = Vue.cookie.get('token')
      if (!token || !/\S/.test(token)) {
        next({ name: 'login' })
      } else {
        next()
      }
    } else {
      next()
    }
    // next()
  } else {
    http({
      url: http.adornUrl('/sys/menu/nav'),
      method: 'get',
      params: http.adornParams()
    }).then(({data}) => {
      if (data && data.code === 0) {
        fnAddDynamicMenuRoutes(data.menuList)
        router.options.isAddDynamicMenuRoutes = true
        sessionStorage.setItem('menuList', JSON.stringify(data.menuList || '[]'))
        sessionStorage.setItem('permissions', JSON.stringify(data.permissions || '[]'))
        next({ ...to, replace: true })
      } else {
        sessionStorage.setItem('menuList', '[]')
        sessionStorage.setItem('permissions', '[]')
        next()
      }
    }).catch((e) => {
      console.log(`%c${e} 请求菜单列表和权限失败，跳转至登录页！！`, 'color:blue')
      router.push({ name: 'login' })
    })
  }
})

/**
 * 判断当前路由类型, global: 全局路由, main: 主入口路由
 * @param {*} route 当前路由
 */
function fnCurrentRouteType (route, globalRoutes = []) {
  var temp = []
  for (var i = 0; i < globalRoutes.length; i++) {
    if (route.path === globalRoutes[i].path) {
      return 'global'
    } else if (globalRoutes[i].children && globalRoutes[i].children.length >= 1) {
      temp = temp.concat(globalRoutes[i].children)
    }
  }
  return temp.length >= 1 ? fnCurrentRouteType(route, temp) : 'main'
}

/**
 * 添加动态(菜单)路由
 * @param {*} menuList 菜单列表
 * @param {*} routes 递归创建的动态(菜单)路由
 */
function fnAddDynamicMenuRoutes (menuList = [], routes = []) {
  var temp = []
  for (var i = 0; i < menuList.length; i++) {
    if (menuList[i].list && menuList[i].list.length >= 1) {
      temp = temp.concat(menuList[i].list)
    } else if (menuList[i].menuUrl && /\S/.test(menuList[i].menuUrl)) {
      menuList[i].menuUrl = menuList[i].menuUrl.replace(/^\//, '')
      var route = {
        path: menuList[i].menuUrl.replace('/', '-'),
        component: null,
        name: menuList[i].menuUrl.replace('/', '-'),
        meta: {
          menuId: menuList[i].menuId,
          title: menuList[i].menuName,
          isDynamic: true,
          isTab: true,
          iframeUrl: ''
        }
      }
      // url以http[s]://开头, 通过iframe展示
      if (isURL(menuList[i].menuUrl)) {
        route['path'] = `i-${menuList[i].menuId}`
        route['name'] = `i-${menuList[i].menuId}`
        route['meta']['iframeUrl'] = menuList[i].menuUrl
      } else {
        try {
          route['component'] = _import(`modules/${menuList[i].menuUrl}`) || null
        } catch (e) {}
      }
      routes.push(route)
    }
  }
  if (temp.length >= 1) {
    fnAddDynamicMenuRoutes(temp, routes)
  } else {
    mainRoutes.name = 'main-dynamic'
    mainRoutes.children = routes
    router.addRoutes([
      mainRoutes,
      { path: '*', redirect: { name: '404' } }
    ])
    sessionStorage.setItem('dynamicMenuRoutes', JSON.stringify(mainRoutes.children || '[]'))
    console.log('\n')
    console.log('%c!<-------------------- 动态(菜单)路由 s -------------------->', 'color:blue')
    console.log(mainRoutes.children)
    console.log('%c!<-------------------- 动态(菜单)路由 e -------------------->', 'color:blue')
  }
}

export default router
